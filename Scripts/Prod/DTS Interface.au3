#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=Images\Download.ico
#AutoIt3Wrapper_Outfile=DTS Interface-Dev.exe
#AutoIt3Wrapper_Res_Comment=Created By: Dominique J. Thornton
#AutoIt3Wrapper_Res_Description=Downtime Transfer System
#AutoIt3Wrapper_Res_Fileversion=1.2.0.0
#AutoIt3Wrapper_Res_LegalCopyright=Seton Healthcare Family
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
;DTS - GUI Side application

;This piece of the application is the Graphical User interface for easier use with the application
;~ Opt('MustDeclareVars', 1)
Opt("GUIOnEventMode", 1)




#Region Includes
#include <Date.au3>
#include <file.au3>
#include <Misc.au3>
#include <array.au3>
#include <string.au3>
#include <Animate.au3>
#include <GuiEdit.au3>
#include <Constants.au3>
#include <GuiListView.au3>
#include <XSkinAnimate.au3>
#include <GuiIPAddress.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <ButtonConstants.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <ServiceControl.au3>

#EndRegion Includes

;~ _Singleton("DTS Interface")

#Region Declarations
;GUI Global Variables
;GUI
Global 	$DTS_Interface, $AddWorkstation, $RemoveWorkstation, $ClientDeploy, $ClientRemove, $Change_Password_GUI, _
		$INI_Viewer, $Log_Interface, $General_Interface_Log_Viewer, $Error_Interface_Log_Viewer, $Label1, $Label2, $Pic1, _
		$About_GUI, $About_Logo, $Label1, $Label2, $Label3, $Label4, $Label5, $Label6, $Label7

;Menu
Global 	$File_Menu, $Add_Workstation, $Remove_Workstation, $Exititem, _
		$Action_Menu, $Start_Transfer_Service, $Stop_Transfer_Service, $Disable_Monitoring, $Enable_Monitoring, $Deploy_Client, _
		$Remove_Client, $Change_Password, $Separator, $View_menu, $Download_DTS_Transfer_Log, $View_Log_Selector, _
		$View_General_Interface_Log, $View_Error_Interface_Log, $View_INI, $Refresh_view, $Help_Menu, $Help_file, $About

;Components
Global 	$Transfer_List, $Workstation_Input, $Search_For_Workstation, $Input_Group, $Seton_Pic, $Atos_Pic, $Initiate, $Diff, _
		$oMyError, $Count_Down, $Item_Index, $CheckBoxTimer, $CheckBoxDiff, $Save_Changes, $Delete, $Screen_Refresh, $XS_n

;Misc
Global 	$oMyError, $INI_File, $ComputerFolder, $WorkstationFile, $Backup_Dir, $Log_Dir, $DeployWorkstationNameInput, $DeployIP, _
		$CP1, $CP2, $SPBox, $AddWorkstationName, $AddIP, $AddSourceInput, $AddDestinationInput, $TransferTypebox, $RemoveWorkstationName, _
		$RemoveWorkstationNameInput, $RemoveIP, $Remove_Client, $Search_Item, $Search_Func, $Save_Item, $Save_Func, _
		$Auto_Removal, $Auto_Item, $Download_DTS_Transfer_Log, $Download_Interface_Log, $Download_All_Client_Logs, _
		$Downlaod_Single_Client_Log, $Workstation_Log, $Interface_Log, $ErrLog, $Server_Program_Installation_Path, $Local_Install_Path, _
		$Service_Interface, $Status_State, $Start_Button, $Stop_Button, $Status_Pic, $ServiceName, $SystemName, _
		$ContactAdmin = @CRLF & @CRLF & 'If this is your first time recieving this message then please try again. If not then please contact you system Administrator.'

Const 	$ADS_UF_DONT_EXPIRE_PASSWD = 0X10000
Const 	$ADS_UF_PASSWD_CANT_CHANGE = 0X40

Global $oMyError = ObjEvent("AutoIt.Error", "MyErrFunc"); Install a custom COM error handler
#EndRegion Declarations

#Region Configurations
$Server_Program_Installation_Path = IniRead(@ScriptDir & '\Admin Config.ini', 'Server', 'Loc', 'Unknown'); Location server
If Not FileExists($Server_Program_Installation_Path) Or $Server_Program_Installation_Path = 'Unkown' Then
	MsgBox(16, 'Error', 'Unable to locate Server Program Installation Path. Please verify that the ini file is in the same ' & _
			'directory as the "DTS Interface executable. ' & $ContactAdmin)
	Exit
EndIf

$INI_File = $Server_Program_Installation_Path & '\Settings\Configurations.ini'
;~ MsgBox(64,'Path', $INI_File)
If Not FileExists($INI_File) Then
	MsgBox(16, 'Error', 'Unable to locate Configuration file.')
	Exit
EndIf

$WorkstationFile = $Server_Program_Installation_Path & '\computers\' & IniRead($INI_File, 'DTS Server', 'Workstations', 'Unknown') ;This is where all transfer systems are housed
If Not FileExists($WorkstationFile) Or $WorkstationFile = 'Unknown' Then
	MsgBox(16, 'Error', 'Unable to read ini file or unable to locate "Computers.csv" file on Server. ' & $ContactAdmin)
	Exit
EndIf

$Backup_Dir = $Server_Program_Installation_Path & '\Computers\Backups'; This is where we will backup the transfer systems after modification
If Not FileExists($Backup_Dir) Then
	DirCreate($Backup_Dir)
EndIf

#cs
	$Local_Install_Path = IniRead($INI_File, 'DTS Interface', 'Local_Install','Unknown'); This is where the DTS Interface will install
	If Not FileExists($Local_Install_Path) Then
	MsgBox(16, 'Error', 'Unable to locate Local Install path')
	Exit
	EndIf
#ce

$Log_Dir = @ScriptDir & '\Logs'; This is where all DTS Interface logs will be held
If Not FileExists($Log_Dir) Then
	DirCreate($Log_Dir)
EndIf

Global $Interface_Log = $Log_Dir & '\General Interface.log', _
		$ErrLog = $Log_Dir & '\Interface Error.log', _
		$ClientErrorCount = $Server_Program_Installation_Path & '\Logs\Error Count.log'


#EndRegion Configurations

#Region Images
;~ FileInstall('\\austech\is-si\Development\icons\compass.ico', @TempDir & '\compass.ico', 1)
#EndRegion Images

;#Region Code
;Main
_FileWriteLog($Interface_Log, 'The DTS Interface has been initialized by: ' & @UserName & '.')

_ImageIn_and_Out()
_DTS_Interface()

#Region Main GUI
;--------------------------------------------------------------------------------
;Main Function GUI
;--------------------------------------------------------------------------------

;===============================================================================
; Name:   _ImageIn_and_Out()
; Description:   Welcome GUI Seton Logo
; Syntax:   _ImageIn_and_Out()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   -
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _ImageIn_and_Out()
	Local $Image, $Intro, $Pic

	$Image = '.\Images\SetonLogo.jpg'
	$Intro = GUICreate('Change', 300, 300, Default, Default, $WS_POPUP, $WS_EX_TOOLWINDOW)
	$Pic = GUICtrlCreatePic('', -1, -1, 300, 300)
	GUISetState(@SW_HIDE)
	GUICtrlSetImage($Pic, $Image)
	For $x = 0 To 200 Step 5
		WinSetTrans($Intro, '', $x)
		If $x = 0 Then GUISetState(@SW_SHOW, $Intro)
		Sleep(1)
	Next

	Sleep(1000)
	For $x = 200 To 0 Step -10
		WinSetTrans($Intro, '', $x)
		Sleep(1)
	Next
	GUIDelete($Intro)
EndFunc   ;==>_ImageIn_and_Out

;===============================================================================
; Name:   _DTS_Interface()
; Description:   Initiates Main GUI
; Syntax:   _DTS_Interface()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   -
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _DTS_Interface()
	#Region ### START Koda GUI section ### Form=C:\Documents and Settings\djthornton\My Documents\Projects\DTS Project\DTS Interface.kxf
	;create Gui
	$DTS_Interface = GUICreate("DTS Interface", 1028, 638, -1, -1, $WS_OVERLAPPEDWINDOW, $WS_EX_WINDOWEDGE + $WS_EX_DLGMODALFRAME)
	GUICtrlSetResizing($DTS_Interface, $GUI_DOCKAUTO)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Exit_Interface")
	GUISetIcon(@ScriptDir & '\Images\Download.ico')
;~ 	GUISetBkColor('0xffffff', $DTS_Interface)
	GUISetBkColor('0x585858', $DTS_Interface)

	; Create File menu
	$File_Menu = GUICtrlCreateMenu("&File")
	$Add_Workstation = GUICtrlCreateMenuItem("Add Workstation", $File_Menu)
	GUICtrlSetOnEvent(-1, '_Add_Workstation_Interface')
	;	$Remove_Workstation = GUICtrlCreateMenuItem("Remove Workstation", $File_Menu)
	;	GUICtrlSetOnEvent(-1, '_Get_Selected')
	$Separator = GUICtrlCreateMenuItem("", $File_Menu)
	$Exititem = GUICtrlCreateMenuItem("Exit", $File_Menu)
	GUICtrlSetOnEvent(-1, '_Exit_Interface')

	;Action Menu
	$Action_Menu = GUICtrlCreateMenu("&Action")
	$Start_Transfer_Service = GUICtrlCreateMenuItem("Transfer Service", $Action_Menu)
	GUICtrlSetOnEvent(-1, '_Service_Interface')
	$Separator = GUICtrlCreateMenuItem("", $Action_Menu)
	$Change_Password = GUICtrlCreateMenuItem("Change Client Password", $Action_Menu)
	GUICtrlSetOnEvent(-1, '_Change_Password')
	$Deploy_Client = GUICtrlCreateMenuItem("Deploy Client", $Action_Menu)
	GUICtrlSetOnEvent(-1, '_Deploy_Button')
;~ 	$Remove_Client = GUICtrlCreateMenuItem("Remove Single Client", $Action_Menu)
;~ 	GUICtrlSetOnEvent(-1, '_Remove_Client_Interface')
	$Separator = GUICtrlCreateMenuItem("", $Action_Menu)
	$Disable_Monitoring = GUICtrlCreateMenuItem("Disable All Monitoring", $Action_Menu)
	GUICtrlSetOnEvent(-1, '_Disable_All_Monitoring')
	$Enable_Monitoring = GUICtrlCreateMenuItem("Enable All Monitoring", $Action_Menu)
	GUICtrlSetOnEvent(-1, '_Enable_All_Monitoring')

	;Create View Menu
	$View_menu = GUICtrlCreateMenu("&View")
	$View_Log_Selector = GUICtrlCreateMenuItem("Log Selector", $View_menu)
	GUICtrlSetOnEvent(-1, '_Log_Interface')
	$View_General_Interface_Log = GUICtrlCreateMenuItem("View General Log (Read-Only)", $View_menu)
	GUICtrlSetOnEvent(-1, 'General_DTS_Interface_Log')
	$View_Error_Interface_Log = GUICtrlCreateMenuItem("View Error Log (Read-Only)", $View_menu)
	GUICtrlSetOnEvent(-1, 'Error_DTS_Interface_Log')
	$View_INI = GUICtrlCreateMenuItem("View INI (Read-Only)", $View_menu)
	GUICtrlSetOnEvent(-1, '_INI_Viewer')
	$Refresh_view = GUICtrlCreateMenuItem("Refresh List", $View_menu)
	GUICtrlSetOnEvent(-1, '_Load_TransferList')

	;Creae Help Menu
	$Help_Menu = GUICtrlCreateMenu("&Help")
	$Help_file = GUICtrlCreateMenuItem("Help File", $Help_Menu)
	GUICtrlSetOnEvent(-1, '_Help')
	$About = GUICtrlCreateMenuItem("About", $Help_Menu)
	GUICtrlSetOnEvent(-1, '_About')

	;Create Gui Components
	$Save_Changes = GUICtrlCreateButton("Save Changes", 30, 80, 89, 25, 0)
	GUICtrlSetOnEvent(-1, '_Save_Changes')
	$Delete = GUICtrlCreateButton("Delete", 168, 80, 89, 25, 0)
	GUICtrlSetOnEvent(-1, '_Remove_Workstations')
	$Workstation_Input = GUICtrlCreateInput("Workstation or IP", 30, 30, 153, 21)
	GUICtrlSetState(-1, $GUI_FOCUS)
	$Search_For_Workstation = GUICtrlCreateButton("Search", 200, 30, 60, 20)
	GUICtrlSetFont(-1, 11, 400, 0, "MS Sans Serif")
	GUICtrlSetOnEvent($Search_For_Workstation, '_Search_Workstation')
	$Input_Group = _XSkinGroup("Search for a Workstation or IP", 8, 14, 270, 49, 0xffffff)
;~ 	$Input_Group = GUICtrlCreateGroup("Search for a Workstation or IP", 8, 14, 270, 49)
;~ 	GUICtrlSetColor(-1, 0xffffff)
;~ 	GUICtrlCreateGroup("", -99, -99, 1, 1)v
;~ 	$Seton_Pic = GUICtrlCreatePic(".\Images\DTS 5.jpg", 285, 5, 738, 125, BitOR($SS_NOTIFY, $WS_GROUP, $WS_CLIPSIBLINGS))
;~ 	GUICtrlSetResizing(-1, $GUI_DOCKHEIGHT)
	$Label1 = GUICtrlCreateLabel("Downtime", 288, 24, 299, 84)
	GUICtrlSetFont(-1, 50, 400, 0)
	GUICtrlSetColor(-1, 0xffffff)
	$Label2 = GUICtrlCreateLabel("System", 792, 24, 229, 84)
	GUICtrlSetFont(-1, 50, 400, 0)
	GUICtrlSetColor(-1, 0xffffff)
	$Pic1 = GUICtrlCreatePic(".\Images\Download-icon.jpg", 630, 5, 127, 127, BitOR($SS_NOTIFY, $WS_GROUP, $WS_CLIPSIBLINGS))
	;$Atos_Pic = GUICtrlCreatePic("..\Images\atosorigin_logo.gif", 950,30, 0, 0, BitOR($WS_GROUP,$WS_CLIPSIBLINGS))
	;;$Count_Down = GUICtrlCreateLabel("Count Down", 16, 584, 66, 17)
	XSkinAnimate($DTS_Interface, "", 1)
	_Load_TransferList()
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###


	While 1
		Sleep(250)
	WEnd

EndFunc   ;==>_DTS_Interface
#EndRegion Main GUI

#Region Functions
;===============================================================================
; Name:   _Load_TransferList()
; Description:   Creates and populates transfer list
; Syntax:   _Load_TransferList()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   -
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Load_TransferList()
	;Declare Variables
	Local 	$exStyles = BitOR($LVS_EX_FULLROWSELECT, $LVS_EX_CHECKBOXES), _
			$Workstations, $Workstation_Info, $WorkstationName, $WorkstationIP, $Folder_Source, $Folder_Destination, $Monitoring, $x, $y, $FailedAttempts, $noload
	;Destroy List so that when the refresh is invoked it will clear all values and start again.
	_GUICtrlListView_Destroy($Transfer_List)
	;Create Transfer List View
	$Transfer_List = GUICtrlCreateListView("Monitoring|Workstation|IP|Source|Destination|Failed Transfers", 5, 135, 1018, 477, $LVS_REPORT + $LVS_SHOWSELALWAYS, $exStyles)
	GUICtrlSetOnEvent(-1, '_Sort')
;~ 	GUICtrlSetColor($Transfer_List, 0x585858)
	_FileReadToArray($WorkstationFile, $Workstations)
	If @error = 1 Then
		MsgBox(16, "DTS Transfer System Error", "There was an error reading the Transfer List. DTS Transfer System is unable to start." & @CRLF & @CRLF & 'Please contact ' & _
				'your System Administrator for assistance.' & @CRLF & @CRLF & 'Error: ' & @error)
		Exit
	ElseIf @error = 2 Then
		MsgBox(64, "DTS Transfer System Error", "There was a problem reading the Transfer List. The list may be empty." & @CRLF & @CRLF & 'Error #: ' & @error)
		$noload = 1
		;MsgBox(0,'Loaded','List Loaded.')
	EndIf

;~ 	_ArrayDisplay($Workstations)
	If $noload = 1 Then
		;The list is believed to be empty. So there is no need to read it.
	Else
		;Y is used to increment the row for each entry in second array
		Local $y = 0
		;For each row in the array we will ready its information then break it into pices by the "," delimeter and give each it own value slot in an array.
		;To view the array before it's broken into pieces remove the ";~" from the _arraydisplay below.
;~		_ArrayDisplay($Workstations)
		_FileWriteLog($Interface_Log, $Workstations[0] & ' workstation(s) were listed.')
		For $x = 1 To $Workstations[0]
			$Workstation_Info = StringSplit($Workstations[$x], ',')
			If $Workstation_Info[0] = 5 Then
;~			_Arraydisplay($Workstation_Info)
				$Monitoring = $Workstation_Info[1]
				$WorkstationName = $Workstation_Info[2]
				$WorkstationIP = $Workstation_Info[3]
				$Folder_Source = $Workstation_Info[4]
				$Folder_Destination = $Workstation_Info[5]
				$FailedAttempts = _Read_Failed_Transfers($WorkstationName)
				If $FailedAttempts = '' Then $FailedAttempts = 'No Entry'
				If $Monitoring = 'YES' Then
					GUICtrlCreateListViewItem('Yes|' & $WorkstationName & '|' & $WorkstationIP & '|' & $Folder_Source & '|' & $Folder_Destination & '|' & $FailedAttempts, $Transfer_List)
					_GUICtrlListView_SetItemChecked($Transfer_List, $x - 1)
				Else
					GUICtrlCreateListViewItem('No|' & $WorkstationName & '|' & $WorkstationIP & '|' & $Folder_Source & '|' & $Folder_Destination & '|' & $FailedAttempts, $Transfer_List)
				EndIf
;~ 			_Time_Format($y, $Workstation_Info[2], $Workstation_Info[3]) ; No longer needed because folder contents are being moved and not just individual files - Tied to column time stamps
				_GUICtrlListView_SetColumnWidth($Transfer_List, 0, 72)
				_GUICtrlListView_SetColumnWidth($Transfer_List, 1, 75)
				_GUICtrlListView_SetColumnWidth($Transfer_List, 2, 81)
				_GUICtrlListView_SetColumnWidth($Transfer_List, 3, 348)
				_GUICtrlListView_SetColumnWidth($Transfer_List, 4, 348)
				_GUICtrlListView_SetColumnWidth($Transfer_List, 5, 94)
			EndIf
		Next
		$Workstations = ''
		$Workstation_Info = ''
	EndIf
	GUISetState()
	_GUICtrlListView_RegisterSortCallBack($Transfer_List)
;~ 	GUIRegisterMsg($WM_NOTIFY, "_WM_NOTIFY")

EndFunc   ;==>_Load_TransferList

;===============================================================================
; Name:   _Sort()
; Description:   Sort Transfer List
; Syntax:   _Sort()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   -
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Sort()
	_GUICtrlListView_SortItems($Transfer_List, GUICtrlGetState($Transfer_List))
EndFunc   ;==>_Sort

;===============================================================================
; Name:   _Read_Failed_Transfers()
; Description:   Reads Failure list and populates DTS on startup
; Syntax:   _Read_Failed_Transfers($Workstation)
; Parameter(s):   $Workstation
; Requirement(s):   None
; Return Value(s):  Success: Returns failed transfer count for system
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Read_Failed_Transfers($Workstation)
	Local $fRead, $aFailed, $sPlit, $name

	$fRead = FileRead($ClientErrorCount)
	If $fRead = '' Then
		$fRead = 'Empty'
		Return 0
	Else
		If Not _FileReadToArray($ClientErrorCount, $aFailed) Then
			MsgBox(16, 'Error File Read', 'Unable to parse Client Error Count log.')
			;Exit
		Else
			For $entry In $aFailed
				$sPlit = StringSplit($entry, ',')
				$name = $sPlit[1]
				If $Workstation = $name Then Return $sPlit[2]
			Next
			Return 0
		EndIf
	EndIf
EndFunc   ;==>_Read_Failed_Transfers

;===============================================================================
; Name:   _Add_Workstation()
; Description:   Add workstation to list and deploy client
; Syntax:   _Add_Workstation()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   Failure: -1
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Add_Workstation()
	Local $name, $IP, $Src, $DestName, $Install_Path, $Dest, $Add_Search_Name, $Downtime_Files, $ext, $oComputerFile, $Success, $Type

	$name = GUICtrlRead($AddWorkstationName)
	$IP = _GUICtrlIpAddress_Get($AddIP)
	$Src = GUICtrlRead($AddSourceInput)
	If StringRight($Src, 1) <> '\' Then $Src &= '\'
	$Dest = GUICtrlRead($AddDestinationInput)
	If StringRight($Dest, 1) <> '\' Then $Dest &= '\'

	$Type = BitAND(GUICtrlRead($TransferTypebox), $GUI_CHECKED) ; 0 = Unchecked - Workstation | 1 = Checked - Share

	$Install_Path = IniRead($INI_File, 'DTS Client', 'Install_Location', 'Error')
	If $Install_Path = 'Error' Then
		MsgBox(16, 'Add Workstation - Error', 'Unable to locate "Installation Path" in .ini file. Aborting "Workstation Add".')
		Return -1
	EndIf

	ProgressOn('Adding Workstation', 'Adding ' & $name, 'Please wait...' & @CRLF & $name & ' is being added to the DTS Transfer list.', Default, 300, 16)
	Sleep(2000)
	ProgressSet(10)

	;MsgBox(0,'','Wait')
	If $Type = 1 Then
		;skip checks (share add)
	Else
		TCPStartup()
		;MsgBox(0,'Compare',TCPNameToIP($Name)&' '&$IP)
		If TCPNameToIP($name) <> $IP Then
			ProgressOff()
			MsgBox(16, 'Add Workstation - Error', 'The provided workstation name and IP do not match. Plase check Workstation ID and IP address.')
			TCPShutdown()
			Return -1
		EndIf
		TCPShutdown()
		If Ping($IP) = 0 Then
			ProgressOff()
			MsgBox(16, 'Add Workstation - Error', 'The provided workstation name cannot be pinged. Please verify that the asset is online.')
			Return -1
		EndIf
	EndIf
	ProgressSet(20)
	If Not FileExists($Src) Then
		ProgressOff()
		MsgBox(16, 'Add Workstation - Error', 'The source could not be found.')
		Return -1
	EndIf
	If $Type = 1 Then ;For share to share - future implementations
		If Not FileExists($Dest) Then
			ProgressOff()
			MsgBox(16, 'Add Workstation - Error', 'The destination could not be found.')
			Return -1
		EndIf
	EndIf
	ProgressSet(30)
	If Not StringInStr(FileGetAttrib($Src), 'D') Then
		ProgressOff()
		MsgBox(16, 'Add Workstation - Error', 'The source must be a folder. Please check the source and try again.')
		Return -1
	EndIf
	ProgressSet(40)

	;---------------------------------------------------------------------------------------
	;New destination idea - Build destination for them so it is consistant

	If $Type = 0 Then $Dest = '\\' & $name & '\c$\' & $Install_Path & '\Files\'
	;---------------------------------------------------------------------------------------

	ProgressSet(50)
	If $Type = 0 Then
		$Add_Search_Name = _Search_Workstation($name, 1); Search if system is already in list
	Else
		$Add_Search_Name = _Search_Workstation($Src, 5, $Dest)
	EndIf

	ProgressSet(60)
	If $Add_Search_Name = -1 Then ;If Error exit add
		ProgressOff()
		Return -1
	ElseIf $Add_Search_Name = 1 Then ;If the system was found user has already been questioned.
		ProgressSet(70)
		;Skip IP Check User is aware of workstation not being listed
	ElseIf $Add_Search_Name = 0 Then ;If it was found check if the IP is already listed
		If _Search_Workstation($IP, 1) = -1 Then Return -1 ;If Error Exit Add
	EndIf

	ProgressSet(70)
	$oComputerFile = FileOpen($WorkstationFile, 1)
	If $oComputerFile = -1 Then
		ProgressOff()
		MsgBox(16, 'Add Workstation - Error', 'The Workstations list could not be accessed.' & $ContactAdmin)
		Return -1
	EndIf
	ProgressSet(80)
	$Success = FileWriteLine($oComputerFile, 'Yes,' & $name & ',' & $IP & ',' & $Src & ',' & $Dest)
	If $Success <> 1 Then
		ProgressOff()
		MsgBox(16, 'Workstation Add - Error', 'There was an error adding ' & $name & ' to the DTS Transfer List.' & $ContactAdmin)
		Return -1
	EndIf
	FileClose($oComputerFile)
	ProgressSet(100, 'Done')
	Sleep(2000)
	ProgressOff()
;~ 	MsgBox(64, 'Workstation Add - Complete', $name & ' has been successfully added to the DTS Transfer list.')
	_Load_TransferList()
	_Deploy($name)
	_Destroy_Add()
EndFunc   ;==>_Add_Workstation

;===============================================================================
; Name:   _TransferType()
; Description:   Enable/Disable Share transfer type
; Syntax:   _TransferType()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   None
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================

Func _TransferType()
	If BitAND(GUICtrlRead($TransferTypebox), $GUI_CHECKED) = 1 Then
		GUICtrlSetState($AddWorkstationName, $GUI_DISABLE)
		_GUICtrlIpAddress_ShowHide ($AddIP, @SW_HIDE)
		GUICtrlSetState($AddDestinationInput, $GUI_ENABLE)
	Else
		GUICtrlSetState($AddWorkstationName, $GUI_ENABLE)
		_GUICtrlIpAddress_ShowHide($AddIP, @SW_SHOW)
		GUICtrlSetState($AddDestinationInput, $GUI_DISABLE)
	EndIf
EndFunc

;===============================================================================
; Name:   _Remove_Workstations()
; Description:   Remove workstation from list and remove client
; Syntax:   _Remove_Workstations()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   Failure: -1
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Remove_Workstations()
	Local $rows, $Selected, $Text, $divide, $name, $k, $b, $1st, $Verify, $iColumn_info, $err, _
			$Workstation, $UserName, $Install_Path, $processes, $aprocesses, $aSplit, $cFolder, $List

	Dim $Removal_Items[1]

	ProgressOn('Client Removal', 'Removing clients.', 'Please wait...' & @CRLF & 'Client configurations are being removed.', _
			Default, 300, 16)
;~ 	Sleep(1000)
;~ 	ProgressSet(10)
	$UserName = IniRead($INI_File, 'DTS Client', 'Username', 'Error')
	;MsgBox(0,'User Name',$UserName)
	If $UserName = 'Error' Then
		ProgressOff()
		MsgBox(16, 'Client Removal - Error', 'Unable to locate "Username" in ini file.Aborting "Workstation Add".')
		Return -1
	EndIf
;~ 	ProgressSet(20)
	$Install_Path = IniRead($INI_File, 'DTS Client', 'Install_Location', 'Error')
	;MsgBox(0,'Install Path',$Install_Path)
	If $Install_Path = 'Error' Then
		ProgressOff()
		MsgBox(16, 'Client Removal - Error', 'Unable to locate "Installation Path" in ini file. Aborting "Workstation Add".')
		Return -1
	EndIf
;~ 	ProgressSet(30)
	$processes = IniRead($INI_File, 'DTS Client', 'Client_Executables', 'Error')
	;MsgBox(0,'Install Path',$Install_Path)
	If $processes = 'Error' Then
		ProgressOff()
		MsgBox(16, 'Client Removal - Error', 'Unable to locate "Client Executables" in ini file. Aborting "Workstation Add".')
		Return -1
	EndIf
	If StringInStr($processes, '|') Then
		$aSplit = StringSplit($processes, '|')
		Dim $aprocesses[$aSplit[0]]
		For $e = 0 To UBound($aprocesses) - 1
			$aprocesses[$e] = "'" & $aSplit[$e + 1] & "'" ; "'" single qoute required to terminate a process
		Next
		$processes = ''
	EndIf
;~ 	ProgressSet(10)

	;_ArrayDisplay($Removal_Items)
	$rows = _GUICtrlListView_GetItemCount($Transfer_List)
	;MsgBox(64,'Rows',$rows)
	$k = 0
	$b = 1
	For $x = 0 To $rows - 1
		;	MsgBox(64,'x',$x)
		$Selected = _GUICtrlListView_GetItemSelected($Transfer_List, $x) ;Search list for item that the user selected.
		;	MsgBox(64,'Selected',$Selected)
		If $Selected = 'True' Then
			ReDim $Removal_Items[$b]
;~ 				_ArrayDisplay($Removal_Items)
			For $y = 1 To _GUICtrlListView_GetColumnCount($Transfer_List) ;Get number of columns
				$iColumn_info = _GUICtrlListView_GetColumn($Transfer_List, $y)
				If $iColumn_info[5] = 'Workstation' Then $name = _GUICtrlListView_GetItemText($Transfer_List, $x, $y) ;pull name from column
			Next
;~ 			MsgBox(64,'Name found',$Name)
			$Removal_Items[$k] = $name
			$k = $k + 1
			$b = $b + 1
		EndIf
	Next
;~ 	_ArrayDisplay($Removal_Items)
;~ 	Exit
	If $Removal_Items[0] = '' Then
		MsgBox(16, 'Workstation Removal - Error', 'No items were selected. Please first select an Workstation to remove.')
		Return -1
	EndIf
	For $l = 0 To UBound($Removal_Items) - 1
		$List &= $Removal_Items[$l] & @CRLF
	Next
;~ 	ProgressSet(20)
	$Verify = MsgBox(36, 'Workstation Removal', 'You are about to remove the following systems.' & @CRLF & @CRLF & $List & @CRLF & 'Are you sure you want to continue?')
	If $Verify = 7 Then
		ProgressOff()
		Return -1
	EndIf
	;_ArrayDisplay($Removal_Items)
	For $i = 0 To UBound($Removal_Items) - 1 ;Iterate through all selected
		ProgressSet(($i / UBound($Removal_Items)) * 100)
		$cFolder = '\\' & $Removal_Items[$i] & '\c$\' & $Install_Path & '\'
		_Remove_Client($Removal_Items[$i], $UserName, $cFolder, $aprocesses)
	Next
	ProgressSet(100)
	Sleep(500)
	ProgressOff()
;~ 	_ArrayDisplay($Removal_Items)
	_Save_Changes(1, $Removal_Items)
EndFunc   ;==>_Remove_Workstations

;===============================================================================
; Name:   _Remove_Client()
; Description:   Remove Client from workstation
; Syntax:   _Remove_Client($Workstation, $UserName, $cFolder, $aprocesses)
; Parameter(s):   $Workstation, $UserName, $cFolder, $aprocesses
; Requirement(s):   None
; Return Value(s):   Failure: -1
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Remove_Client($Workstation, $UserName, $cFolder, $aprocesses)
	Local $objUser, $Verify, $objUserDelete, $dRemove, $objUserDelete, $dCreate, _
			$objWMIService, $objProcess, $colProcess, $strComputer, $strProcessKill

;~ 	$Workstation = GUICtrlRead($RemoveWorkstationNameInput)
;~ 	Check if folder exist if not client not installed. Abort
	If FileExists($cFolder) Then
		; Check if account exists .. if so delete ita
		$objUser = ObjGet("WinNT://" & $Workstation & "/" & $UserName)
		If @error Then
			;No account found
		Else
			$objUser = ''
			$objUser = ObjGet("WinNT://" & $Workstation & "")
			$objUserDelete = $objUser.Delete("user", $UserName)
		EndIf
;~ 		ProgressSet(60)

		FileDelete('\\' & $Workstation & '\c$\DTS Marker.txt')

;~ 		If the files are in use the removal will fail. Here we will attempt to kill the processes then try again.
		$dCreate = DirRemove($cFolder, 1)
		If $dCreate = 0 Then
			$objWMIService = ObjGet("winmgmts:" & "{impersonationLevel=impersonate}!\\" & $strComputer & "\root\cimv2")
			For $strProcessKill In $aprocesses
				$colProcess = $objWMIService.ExecQuery("Select * from Win32_Process Where Name = " & $strProcessKill)
				For $objProcess In $colProcess
					$objProcess.Terminate()
				Next
			Next

			$dCreate = DirRemove($cFolder, 1)
			If $dCreate = 0 Then
;~ 				ProgressOff()
				MsgBox(16, 'Client Removal - Verification', 'Unable to remove client from ' & $Workstation & '. ' & _
						'The program may be in use. A manual removal may be neccessary.', 10)
				_FileWriteLog($ErrLog, 'Unable to remove client from ' & $Workstation & '. ' & _
						'The program may be in use. A manual removal may be neccessary.')
				Return -1
			EndIf
		EndIf
	Else
;~ 		ProgressOff()
		MsgBox(16, 'Client Removal - Verification', 'No client was detected on ' & $Workstation & '.', 10)
	EndIf

EndFunc   ;==>_Remove_Client

;===============================================================================
; Name:   _DeployButton()
; Description:   Deploy client to workstations
; Syntax:   _DeployButton()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   Failure:
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
; Notes : This call is necessary because the parameter acknowledgment is not
; Notes : transmitted by the "OnEvent" call. It causes "$WorkstationName"
; Notes : paramter to be seen as not being declared.
;===============================================================================
Func _Deploy_Button() ;See notes above.
	_Deploy()
EndFunc

;===============================================================================
; Name:   _Deploy()
; Description:   Deploy client to workstations
; Syntax:   _Deploy()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   Failure: -1
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Deploy($WorkstationName = '')
	Local $Workstations, $UserName, $Password, $Install_Path, $Client_Executables, $Downtime_Files, $Deploy_Search_Name, $Dupe, $objUser, _
			$objGroup, $colAccounts, $oldFlags, $newFlags, $ExistUser, $User, $Existgroup, $objUsers, $aErr[1], $Return, $z, $err, $iNdex, _
			$processes, $aSplit, $aprocesses

	;$strWorkstationName = GUICtrlRead($DeployWorkstationNameInput)
	If $WorkstationName <> '' Then
;~ 		MsgBox(64,'Search Workstation',$WorkstationName)
		$iNdex = _Search_Workstation($WorkstationName, 2)
		Dim $Workstations[1][2]
		$Workstations[0][0] = $WorkstationName
		$Workstations[0][1] = $iNdex
;~ 		_ArrayDisplay($Workstations)
	Else
;~ 		MsgBox(64,'Get Selected','Acquiring index')
		$Workstations = _Get_Selected()
;~ 		_ArrayDisplay($Workstations, 'Deploy - Workstiations')
	EndIf

	ProgressOn('Client Deployment', 'Deploying Client to selected Workstations.', 'Please wait...', Default, 300, 16)
	Sleep(2000)
	;MsgBox(0,'Name',$strWorkstationName)
	ProgressSet(5)
	If Not IsArray($Workstations) Or $Workstations[0][0] = '' Then
		ProgressOff()
		MsgBox(16, 'Client Deployment - Error', 'You must first select one or more Workstations. Please try again.')
		Return 0
	EndIf

	$UserName = IniRead($INI_File, 'DTS Client', 'Username', 'Error')
	;MsgBox(0,'User Name',$UserName)
	If $UserName = 'Error' Then
		ProgressOff()
		MsgBox(16, 'Client Deployment - Error', 'Unable to locate "Username" in ini file.Aborting "Workstation Deployment".')
		Return 0
	EndIf

	$Password = IniRead($INI_File, 'DTS Client', 'Password', 'Error')
	;MsgBox(0,'Password',$Password)
	If $Password = 'Error' Then
		ProgressOff()
		MsgBox(16, 'Client Deployment - Error', 'Unable to locate "Password" in ini file. Aborting "Workstation Deployment".')
		Return 0
	EndIf
	ProgressSet(10)
	$Install_Path = IniRead($INI_File, 'DTS Client', 'Install_Location', 'Error')
	;MsgBox(0,'Install Path',$Install_Path)
	If $Install_Path = 'Error' Then
		ProgressOff()
		MsgBox(16, 'Client Deployment - Error', 'Unable to locate "Installation Path" in ini file. Aborting "client Deployment".')
		Return 0
	EndIf

	$processes = IniRead($INI_File, 'DTS Client', 'Client_Executables', 'Error')
	If $processes = 'Error' Then
		ProgressOff()
		MsgBox(16, 'Client Deployment - Error', 'Unable to locate "Client Executables" in ini file. Aborting "Workstation Add".')
		Return 0
	EndIf

	If StringInStr($processes, '|') Then
		$aSplit = StringSplit($processes, '|')
		Dim $aprocesses[$aSplit[0]]
;~ 		_ArrayDisplay($aSplit, 'Split')
		For $e = 0 To UBound($aprocesses) - 1
			$aprocesses[$e] = $aSplit[$e + 1] ;Add processes to array
;~ 			MsgBox(64,'Path', $Server_Program_Installation_Path&'\Client Executables\'&$aprocesses[$e])
			If Not FileExists($Server_Program_Installation_Path & '\Client Executables\' & $aprocesses[$e]) Then ;validate they exist first
				ProgressOff()
				MsgBox(16, 'Client Deployment - Error', "Unable to find client executable files. Verify that the client names in the " & _
						".ini file are correct and that the files are located in the server's program files directory.", 0, $AddWorkstation)
				Return 0
			EndIf
		Next
		$processes = $aprocesses
;~ 		_ArrayDisplay($processes, 'processes')
	EndIf

	ProgressSet(20)
	$z = 0
	For $x = 0 To UBound($Workstations) - 1
		ProgressSet(20 + (70 * ($x / UBound($Workstations))))
		;-----------------------------------------------------------------------------
		$Existgroup = 0
		; Init objects


		; Check if account exists .. if not create it
		$objUser = ObjGet("WinNT://" & $Workstations[$x][0] & "/" & $UserName)
		If @error Then
			;MsgBox(0,'Not Detected','No account was detected. Setting up now.')
			$colAccounts = ObjGet("WinNT://" & $Workstations[$x][0] & "")
			$objUser = $colAccounts.Create("user", $UserName)
			$objUser.SetPassword($Password)
			$objUser.Put("Fullname", "DTS Account")
			$objUser.Put("Description", "DTS Transfer Account")
			$objUser.SetInfo
			; Read current settings and Bitor to ensure the "Don't expire password swith is on and user cannot change password"
			$oldFlags = $objUser.Get("UserFlags")
			$newFlags = BitOR($oldFlags, $ADS_UF_DONT_EXPIRE_PASSWD)
			$objUser.Put("UserFlags", $newFlags) ;expire the password
			$objUser.SetInfo
			$oldFlags = $objUser.Get("UserFlags")
			$newFlags = BitOR($oldFlags, $ADS_UF_PASSWD_CANT_CHANGE)
			$objUser.Put("UserFlags", $newFlags) ;expire the password
			$objUser.SetInfo
		Else
			$ExistUser = 1 ;User already exist
			$objUser.SetPassword($Password)
			$objUser.SetInfo
			;MsgBox(0,'Detected','The account was detected. Skipping account creation.')
		EndIf

		;Add User to users group**********************************************
		$objGroup = ObjGet("WinNT://" & $Workstations[$x][0] & "/Users,group")

		For $objUsers In $objGroup.Members
			If ($objUsers.AdsPath) = 'winNT://TAXAUS/' & $Workstations[$x][0] & '/' & $UserName Then $Existgroup = 1
		Next

		If $Existgroup = 0 Then $objGroup.Add($objUser.ADsPath)

		;Create Directory & copy files
		$err = _Dir_Setup($Install_Path, $processes, $Workstations[$x][0], $UserName)
		If $err <> 0 Then ;1 = failed logs folder; 2 = failed Downtime files foder; 5 = Image Directory copy failed; 10 = Marker failed; 20 settings; 30 = Permissions failedCombinations of failure codes possible.
			ReDim $aErr[$z + 1]
			$aErr[$z] = $Workstations[$x][0] & ' failed to setup completely.'
		ElseIf $err = -1 Then
			ProgressOff()
			MsgBox(16, 'Client Deployment - Error', 'The client executables failed to copy to ' & $Workstations[$x] & '. Check error logs.', 15)
			_FileWriteLog($ErrLog, 'The client executables failed to copy to ' & $Workstations[$x] & '.')
			Return 0
		EndIf
	Next
	ProgressSet(100)
	Sleep(1000)
	ProgressOff()
	If $aErr[0] <> '' Then MsgBox(16, 'Client Deployment - Error', 'There was an error in the depoloyment. Look to error log for details.')
;~ 	_ArrayDisplay($aErr, 'Failed Client Deployments')
EndFunc   ;==>_Deploy

;===============================================================================
; Name:   _Dir_Setup()
; Description:   Directory setup for clients
; Syntax:   _Dir_Setup($Install_Path, $Downtime_Files, $Client_Executables, $Workstation, $UserName)
; Parameter(s): $Install_Path = Patch for directory to be created
;				$Client_Executables = The client program
;				$Workstation = system name
;				$UserName = Name of the local user account
; Requirement(s):   None
; Return Value(s):   Failure: -1
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Dir_Setup($Install_Path, $Client_Executables, $Workstation, $UserName)
	Local $err = 0, $dCreate, $fCopy, $fullpath = '\\' & $Workstation & '\c$\' & $Install_Path, _
			$acls, $Success, $ErrorCounter = 0, $fCreate, $Run
	;Create directories
	$dCreate = DirCreate($fullpath & '\Logs\')
	If $dCreate = 0 Then
		$err += 1
		_FileWriteLog($ErrLog, 'Client Deployment - Error: The "logs" folder was not successfully created for ' & $Workstation & '. This can be ignored.')
	EndIf
	$dCreate = DirCreate($fullpath & '\Files\')
	If $dCreate = 0 Then
		$err += 2
		_FileWriteLog($ErrLog, 'Client Deployment - Error: The "files" folder was not successfully created for ' & $Workstation & '. This can be ignored.')
	EndIf
	$dCreate = DirCopy($Server_Program_Installation_Path & '\Images', $fullpath & '\Images\', 1)
	If $dCreate = 0 Then
		$err += 5
		_FileWriteLog($ErrLog, 'Client Deployment - Error: The "Images" folder was not successfully copied for ' & $Workstation & '. This can be ignored.')
	EndIf
	;Create Marker on root pointing to install path
	FileDelete('\\' & $Workstation & '\c$\DTS Marker.txt')
	$fCreate = FileWriteLine('\\' & $Workstation & '\c$\DTS Marker.txt', 'C:\' & $Install_Path)
	$fCreate = FileWriteLine('\\' & $Workstation & '\c$\DTS Marker.txt', $UserName)
	If $fCreate = 0 Then
		$err += 10
		_FileWriteLog($ErrLog, 'Client Deployment - Error: The "Images" folder was not successfully copied for ' & $Workstation & '. This can be ignored.')
	EndIf
	;Copy config file
	$fCopy = FileCopy($Server_Program_Installation_Path & '\Client Executables\Settings.ini', $fullpath, 1)
	If $fCreate = 0 Then
		$err += 20
		_FileWriteLog($ErrLog, 'Client Deployment - Error: The "settings.ini" file was not successfully copied for ' & $Workstation & '. This can be ignored.')
	EndIf
	;Copy over client executables
	If IsArray($Client_Executables) Then
		For $executable In $Client_Executables
;~ 			MsgBox(64,'File Path', $Server_Program_Installation_Path&'\Client Executables\'&$executable)
			$fCopy = FileCopy($Server_Program_Installation_Path & '\Client Executables\' & $executable, $fullpath & '\', 1)
			If $fCopy = 0 Then
				Return -1
			EndIf
		Next
	Else
		$fCopy = FileCopy($Server_Program_Installation_Path & '\Client Executables\' & $executable, $fullpath & '\', 1)
		If $fCopy = 0 Then
			Return -1
		EndIf
	EndIf

	;Set Transfer account for read access###Move to Security Script
	$acls = ('cacls.exe "' & $fullpath & '" /E /T /C /G "Power Users":F')
;~ 	MsgBox(64,'ACLS', $acls)
	$Run = RunWait(@ComSpec & " /C " & $acls, "", @SW_HIDE);_RunDOS($acls)

	Return $err
EndFunc   ;==>_Dir_Setup

;===============================================================================
; Name:   _Show_Password()
; Description:   Unhide password
; Syntax:   _Show_Password()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   None
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Show_Password()
	If BitAND(GUICtrlRead($SPBox), $GUI_CHECKED) = 0 Then
		;MsgBox(0,'Unchecked',BitAND(GUICtrlRead($SPBox),$GUI_CHECKED))
		_GUICtrlEdit_SetPasswordChar($CP1, '*')
		_GUICtrlEdit_SetPasswordChar($CP2, '*')
		GUICtrlSetState($CP2, $GUI_FOCUS)
		GUICtrlSetState($CP1, $GUI_FOCUS)
	Else
		;MsgBox(0,'Checked',BitAND(GUICtrlRead($SPBox),$GUI_CHECKED))
		_GUICtrlEdit_SetPasswordChar($CP1)
		_GUICtrlEdit_SetPasswordChar($CP2)
		GUICtrlSetState($CP2, $GUI_FOCUS)
		GUICtrlSetState($CP1, $GUI_FOCUS)
	EndIf
EndFunc   ;==>_Show_Password

;===============================================================================
; Name:   _Save_Password()
; Description:   Save new password
; Syntax:   _Save_Password()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   Failure = -1
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Save_Password()
	Local $Password1, $Password2, $wINI, $SetPWD

	$Password1 = GUICtrlRead($CP1)
	$Password2 = GUICtrlRead($CP2)
	If $Password1 == $Password2 Then
		$wINI = IniWrite($INI_File, 'DTS Client', 'PassWord', $Password1)
		If $wINI = 0 Then
			MsgBox(16, 'Change Password - Error', 'Unable to update password in INI file.' & $ContactAdmin)
			Return -1
		Else
;~ 			MsgBox(64, 'Change Password - Success', 'The INI file has been updated with the new client password. Please wait up to 10 minutes for all passwords to apply.')
			$SetPWD = _Set_Password()
			_Destroy_Change_Password()
			If IsArray($SetPWD) Then
				MsgBox(16, 'Change Password - Error', 'There was a problem changing the password on one or more systems.')
				_ArrayDisplay($SetPWD, 'Failed Password resets')
			EndIf
		EndIf
	Else
		MsgBox(16, 'Change Password - Error', 'The provided passwords do not match. Please try again.')
		Return -1
	EndIf
EndFunc   ;==>_Save_Password

;===============================================================================
; Name:   _Set_Password()
; Description:   Set new password on workstations
; Syntax:   _Set_Password()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):  Failure = -1 ;Can't read Username
;					Failure = -2 ;Can't read password
;					Failure = -3 ;No systems found
;					Success = 1
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Set_Password()
	Local $UserName, $Password, $rows, $Selected, $Workstations[1], $objUser, $colAccounts, $oldFlags, $newFlags, _
			$aFailed[1], $k = 0, $eFlag = 0
;	Const $ADS_UF_DONT_EXPIRE_PASSWD = 0X10000
;	Const $ADS_UF_PASSWD_CANT_CHANGE = 0X40
	ProgressOn('Password Changer', 'Changing client passwords', 'Please wait....', 20)
	$UserName = IniRead($INI_File, 'DTS Client', 'Username', 'Error')
	;MsgBox(0,'User Name',$UserName)
	If $UserName = 'Error' Then
		ProgressOff()
		MsgBox(16, 'Change Password - Error', 'Unable to locate "Username" in ini file.Aborting "Workstation Deployment".')
		Return -1
	EndIf

	$Password = IniRead($INI_File, 'DTS Client', 'Password', 'Error')
	;MsgBox(0,'Password',$Password)
	If $Password = 'Error' Then
		ProgressOff()
		MsgBox(16, 'Change Password - Error', 'Unable to locate "Password" in ini file. Aborting "Workstation Deployment".')
		Return -2
	EndIf

	$rows = _GUICtrlListView_GetItemCount($Transfer_List)
	;MsgBox(64,'Rows',$rows)
	ReDim $Workstations[$rows]
	For $x = 0 To $rows - 1
		ReDim $Workstations[$x + 1]
		$Selected = _GUICtrlListView_GetItemTextArray($Transfer_List, $x)
;~ 			_ArrayDisplay($Selected, 'Selected')
		$Workstations[$x] = $Selected[2] ;Name
	Next
;~ 	_ArrayDisplay($Workstations, 'Workstations')
	If Not IsArray($Workstations) Then
		ProgressOff()
		MsgBox(16, 'Change Password - Error', 'No systems were found to change a password for.')
		Return -3 ;No systems
	Else
		For $x = 0 To $rows - 1
			ProgressSet(100 * ($x / ($rows - 1)))
			$objUser = ObjGet("WinNT://" & $Workstations[$x] & "/" & $UserName)
			If @error Then
;~ 				MsgBox(64,'Not Detected','No account was detected. Setting up now.')
				$colAccounts = ObjGet("WinNT://" & $Workstations[$x] & "")
				$objUser = $colAccounts.Create("user", $UserName)
				$objUser.SetPassword($Password)
				$objUser.Put("Fullname", "DTS Account")
				$objUser.Put("Description", "DTS Transfer Account")
				$objUser.SetInfo
				If @error Then
					ReDim $aFailed[$k + 1]
					$aFailed[$k] = $Workstations[$x]
					If $k = 0 Then $k = 1
					$k += 1
					$eFlag = 1
				EndIf
				; Read current settings and Bitor to ensure the "Don't expire password swith is on and user cannot change password"
				$oldFlags = $objUser.Get("UserFlags")
				$newFlags = BitOR($oldFlags, $ADS_UF_DONT_EXPIRE_PASSWD)
				$objUser.Put("UserFlags", $newFlags) ;expire the password
				$objUser.SetInfo
				If @error Then
					$aFailed[$k] = $Workstations[$x]
					If $k = 0 Then $k = 1
					$k += 1
					$eFlag = 1
				EndIf
				$oldFlags = $objUser.Get("UserFlags")
				$newFlags = BitOR($oldFlags, $ADS_UF_PASSWD_CANT_CHANGE)
				$objUser.Put("UserFlags", $newFlags) ;expire the password
				$objUser.SetInfo
				If @error Then
					$aFailed[$k] = $Workstations[$x]
					If $k = 0 Then $k = 1
					$k += 1
					$eFlag = 1
				EndIf
			Else
;~ 				MsgBox(64,'Detected','The account was detected. Updating password.')
				$colAccounts = ObjGet("WinNT://" & $Workstations[$x] & "")
				$objUser.SetPassword($Password)
				$objUser.SetInfo
				If @error Then
					$aFailed[$k] = $Workstations[$x]
					If $k = 0 Then $k = 1
					$k += 1
					$eFlag = 1
				EndIf
			EndIf
		Next
		ProgressOff()
		MsgBox(64, 'Password Change', 'The password change is complete.', '', $Change_Password_GUI)
	EndIf
	If $eFlag = 1 Then
		Return $aFailed
	Else
		Return 1
	EndIf
EndFunc   ;==>_Set_Password

;===============================================================================
; Name:   _Disable_All_Monitoring()
; Description:   Uncheck monitoring for all workstations
; Syntax:   _Disable_All_Monitoring()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s): 	None
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Disable_All_Monitoring()
	Local $rows
	$rows = _GUICtrlListView_GetItemCount($Transfer_List)

	For $c = 0 To $rows - 1
		_GUICtrlListView_SetItemChecked($Transfer_List, -1, False)
	Next
EndFunc   ;==>_Disable_All_Monitoring

;===============================================================================
; Name:   _Enable_All_Monitoring()
; Description:   Check monitoring for all workstations
; Syntax:   _Enable_All_Monitoring()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s): 	None
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Enable_All_Monitoring()
	Local $rows
	$rows = _GUICtrlListView_GetItemCount($Transfer_List)

	For $c = 0 To $rows - 1
		_GUICtrlListView_SetItemChecked($Transfer_List, -1, True)
	Next
EndFunc   ;==>_Enable_All_Monitoring

;===============================================================================
; Name:   _Search_Workstation()
; Description:   Search for workstations (Search/Add/Deploy/Remove/Verify)
; Syntax:   _Search_Workstation($Search_Item = '', $Search_Func = 0)
; Parameter(s): $Search_Item = '' ;The asset name or IP
;				$Search_Func = 0 ; used to search for a device in the list
;				$Search_Func = 1 ; used for a Workstation Add
;				$Search_Func = 2 ; used for Client Deployment
;				$Search_Func = 3 ; used for Client Removal
;				$Search_Func = 4 ; used for Client Log Verification
;				$Search_Func = 5 ; used for share add
; Requirement(s):   Failure: -1
;					Invalid/Not Found: 0
;					Success: 1
; Return Value(s): 	None
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Search_Workstation($Search_Item = '', $Search_Func = 0, $Search_Item_2 = '')
	;MsgBox(0,'Search',$Search_Item&' - '&$Search_Func)
	Local $Dupe, $Verify, $Item_Index, $i = -1
	;MsgBox(0,'',GUICtrlRead($Workstation_Input))
	Switch $Search_Func
		Case 0 ; 0 is used to search for a device in the list
			$Search_Item = GUICtrlRead($Workstation_Input)
			If $Search_Item = 'Workstation or IP' Then
				MsgBox(16, 'Workstation Search - Error', 'You must first enter a valid Search parameter.' & @CRLF & @CRLF & 'Please try again.')
				Return 0
			EndIf
			$Item_Index = _GUICtrlListView_FindInText($Transfer_List, $Search_Item)
			If $Item_Index = -1 Then
				MsgBox(4160, "Workstation Search - Result", $Search_Item & ' could not be found.')
			Else
				_GUICtrlListView_EnsureVisible($Transfer_List, $Item_Index)
				_GUICtrlListView_SetItemSelected($Transfer_List, $Item_Index)
				_GUICtrlListView_ClickItem($Transfer_List, $Item_Index)
			EndIf
		Case 1 ; 1 is used for a Workstation Add
			$Item_Index = _GUICtrlListView_FindInText($Transfer_List, $Search_Item)
			If $Item_Index = -1 Then
;~ 				MsgBox(16,'Workstation Add - Error','There was an error while attempting to read system list. Aborting system add)
				Return 0;Return to search for IP
			Else
				$Dupe = MsgBox(36, 'Workstation Add - Duplicate Entry', $Search_Item & ' is already listed in the DTS Transfer System' & @CRLF & @CRLF & 'Do you want to continue?')
				If $Dupe = 7 Then
					MsgBox(48, 'Workstation Add - Aborted', 'Aborting Workstation Add for ' & $Search_Item & '.', 3)
					Return -1
				ElseIf $Dupe = 6 Then
					Return 1
				EndIf
			EndIf
		Case 2 ;2 is used for Client Deployment
			$Item_Index = _GUICtrlListView_FindInText($Transfer_List, $Search_Item)
			If $Item_Index = -1 Then
				$Verify = MsgBox(36, 'Client Deployment - Not Found', $Search_Item & ' is not listed in the DTS Transfer System' & @CRLF & @CRLF & 'Do you want to continue?') ;Thinking about this... it should never happen
				If $Verify = 7 Then
					MsgBox(48, 'Client Deployment - Aborted', 'Aborting Client Deployment to ' & $Search_Item & '.', 3)
					Return -1
				ElseIf $Verify = 6 Then
					Return 1
				EndIf
			Else
				Return $Item_Index;Continue
			EndIf
		Case 3 ;3 is used for Client Removal
			$Item_Index = _GUICtrlListView_FindInText($Transfer_List, $Search_Item)
			If $Item_Index = -1 Then
				$Verify = MsgBox(48, 'Workstation Removal - Not Found', $Search_Item & ' is not listed in the DTS Transfer System. ')
				Return -1
			Else
				$Verify = MsgBox(36, 'Workstation Removal - Verification', $Search_Item & ' is about to be removed from the DTS Transfer System.' & @CRLF & @CRLF & 'Do you want to continue?')
				If $Verify = 7 Then
					MsgBox(48, 'Workstation Removal - Aborted', 'Aborting Workstation removal for ' & $Search_Item & '.', 3)
					Return -1
				ElseIf $Verify = 6 Then
					Return $Item_Index
				EndIf
			EndIf
		Case 4 ;4 is used for Client Log Verification
			$Item_Index = _GUICtrlListView_FindInText($Transfer_List, $Search_Item)
			If $Item_Index = -1 Then
				$Verify = MsgBox(48, 'Workstation Log Retrieval - Not Found', $Search_Item & ' is not listed in the DTS Transfer System.' & @CRLF & @CRLF & 'Do you want to continue?')
				Return -1
			Else
				Return 1
			EndIf
		Case 5 ;5 is used for share add
			$Item_Index = _GUICtrlListView_FindInText($Transfer_List, $Search_Item)
			If $Item_Index = -1 Then
;~ 				MsgBox(16,'Share Add - Error','There was an error while attempting to read system list. Aborting system add)
				Return 0;
			Else
				$Dupe = MsgBox(36, 'Share Add - Duplicate Entry', $Search_Item & ' is already listed in the DTS Transfer System' & @CRLF & @CRLF & 'Do you want to continue?')
				If $Dupe = 7 Then
					MsgBox(48, 'Share Add - Aborted', 'Aborting share Add for ' & $Search_Item & '.', 3)
					Return -1
				ElseIf $Dupe = 6 Then
					Return 1
				EndIf
			EndIf
	EndSwitch
EndFunc   ;==>_Search_Workstation

;===============================================================================
; Name:   _Get_Selected()
; Description:   Get the items selected in the listview
; Syntax:   _Get_Selected()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s): 	2-D array of selected workstation name and index 0=Name 1=Index
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Get_Selected()
	;$Transfer_List
	Local $rows, $Selected, $Selected_Workstations[1][2], $k, $Workstation, $name, $IP, $sPlit

	$rows = _GUICtrlListView_GetItemCount($Transfer_List)
	;MsgBox(64,'Rows',$rows)
	$k = 0
	For $x = 0 To $rows - 1
		;	MsgBox(64,'x',$x)
		$Selected = _GUICtrlListView_GetItemSelected($Transfer_List, $x)
		If $Selected = 'True' Then
			ReDim $Selected_Workstations[$k + 1][2]
			$Selected = _GUICtrlListView_GetItemTextArray($Transfer_List, $x)
;~ 			_ArrayDisplay($Selected, 'Selected')
			$Selected_Workstations[$k][0] = $Selected[2] ;Name
;~ 			$Selected_Workstations[$k][1] = $sPlit[3] ;IP
			$Selected_Workstations[$k][1] = $x ;Index
			$k = $k + 1
;~ 			_ArrayDisplay($Selected_Workstations, 'Selected Workstions '&$x&'/'&$rows-1)
		EndIf
	Next
;~ 	_ArrayDisplay($Selected_Workstations, 'Final Selected Workstations')
	Return $Selected_Workstations
EndFunc   ;==>_Get_Selected

;===============================================================================
; Name:   _Save_Changes()
; Description:   Save the changes
; Syntax:   _Save_Changes($Save_Func = 0, $Save_Item = '')
; Parameter(s): 	$Save_Func = 0 ;User initiated save (Button)
;					$Save_Item = '';System initiated save (auto)
; Requirement(s):   None
; Return Value(s): 	Failure: -1
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Save_Changes($Save_Func = 0, $Save_Item = '')
	Local $Save, $rows, $iNdex, $DirSize, $oFile, $List, $Check, $Temp, $x, $y, $Workstation_Info, $Delete_count, $Delete_Item
	;Removal_Func = 1 : Means multiple system removal.
	;Removal_Func = 0 : Means single system removal
	If $Save_Func = 0 Then ;Used when user initiates form GUI Save button
		$Save = MsgBox(36, 'Save Changes - Verification', 'Are you sure you want to save these changes?')
		If $Save = 7 Then Return -1
	EndIf
	ProgressOn('Workstation List Save', 'Saving changes made to list', 'Backing up current list.', Default, Default, 16)
	Sleep(500)
	$rows = _GUICtrlListView_GetItemCount($Transfer_List)
	ProgressSet(33)
	Sleep(500)
	$DirSize = DirGetSize($Backup_Dir, 1)
	ProgressSet(66)
	Sleep(500)
	;MsgBox(0,'',@MON&'-'&@MDAY&'-'&StringTrimLeft(@YEAR,2)&' '&'Backup '&$DirSize[1]+1)
	FileCopy($WorkstationFile, $Backup_Dir & '\' & @MON & '-' & @MDAY & '-' & StringTrimLeft(@YEAR, 2) & ' ' & 'Backup ' & $DirSize[1] + 1 & '.csv')
	ProgressSet(100)
	Sleep(500)
	;Build a loop to verify that the file was transfered.
	ProgressSet(0, 'Committing changes to file.')
	Sleep(1000)
	For $c = 0 To $rows - 1
		ProgressSet(100 * ($c / $rows))
		If _GUICtrlListView_GetItemChecked($Transfer_List, $c) Then
			$Check = 'Yes'
		Else
			$Check = 'No'
		EndIf
		$Temp = _GUICtrlListView_GetItemTextArray($Transfer_List, $c)
;~ 		_ArrayDisplay($Temp, 'Temp')
		If $Save_Func = 1 Then ; Used when removing items
			If IsArray($Save_Item) Then
				$Delete_count = UBound($Save_Item)
				$Delete_Item = 0
				For $j = 0 To $Delete_count - 1

					If StringInStr($Temp[2], $Save_Item[$j]) Then $Delete_Item = 1; Temp[2] = name

				Next
				If $Delete_Item = 0 Then $List &= $Check & ',' & $Temp[2] & ',' & $Temp[3] & ',' & $Temp[4] & ',' & $Temp[5] & @CRLF
			Else
				If StringInStr($Temp[1], $Save_Item) Then
					;don't write to file
				Else
					$List &= $Check & ',' & $Temp[2] & ',' & $Temp[3] & ',' & $Temp[4] & ',' & $Temp[5] & @CRLF
				EndIf
			EndIf
			;don't write to file
		Else
			$List &= $Check & ',' & $Temp[2] & ',' & $Temp[3] & ',' & $Temp[4] & ',' & $Temp[5] & @CRLF
		EndIf
		;MsgBox(0,'',$index)
	Next
	;MsgBox(0,'List',$List)
	$oFile = FileOpen($WorkstationFile, 2)
	FileWrite($oFile, $List)
	FileClose($oFile)
	ProgressSet(100, 'Save Complete')
	Sleep(1000)
	ProgressOff()
	MsgBox(64, 'Save Changes - Done', 'Save Complete.')
	_Load_TransferList()
EndFunc   ;==>_Save_Changes

;===============================================================================
; Name:   _Log_Downloader()
; Description:   Download logs from interface, server, and clients
; Syntax:   _Log_Downloader
; Parameter(s): 	None
; Requirement(s):   None
; Return Value(s): 	Failure: -1
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Log_Downloader()
	Local $Transfer, $Interface, $ClientAll, $ClientSingle, $Workstation_Search_Log, $Save_Dir, $Stamp, $Drop_Files, $Go_Get_Workstation, $Client_Log, $Verify, _
			$Ping, $Install_Path, $Transfer_Copy = 1, $Interface_Copy = 1, $All_Client_Download = 1

	$Transfer = BitAND(GUICtrlRead($Download_DTS_Transfer_Log), $GUI_CHECKED)
	$Interface = BitAND(GUICtrlRead($Download_Interface_Log), $GUI_CHECKED)
	$ClientAll = BitAND(GUICtrlRead($Download_All_Client_Logs), $GUI_CHECKED)
	$ClientSingle = BitAND(GUICtrlRead($Downlaod_Single_Client_Log), $GUI_CHECKED)

	If $ClientSingle = 1 Then
		;MsgBox(0, '', 'See it.')
		$Go_Get_Workstation = GUICtrlRead($Workstation_Log)
		If $Go_Get_Workstation = "Workstation or IP" Or $Go_Get_Workstation = '' Then
			MsgBox(16, 'Client Log Retrieval - Error', 'You must first enter a valid Workstation before downloading. Please try again.')
			Return -1
		Else
			$Workstation_Search_Log = _Search_Workstation($Go_Get_Workstation, 4)
			If $Workstation_Log = -1 Then
				$ClientSingle = 0
			Else
				$Ping = Ping($Go_Get_Workstation)
				If $Ping = 0 Then
					MsgBox(16, 'Workstation Log Retrieval - Error', 'The Workstation ' & $Go_Get_Workstation & ' cannot be pinged. The Workstation may be offline ' & _
							'or the Workstation may not be on the network. Please validate that the asset is on the network.')
					$ClientSingle = 0
				EndIf
			EndIf
		EndIf
	EndIf

	If $Transfer = 1 Or $Interface = 1 Or $ClientAll = 1 Or $ClientSingle = 1 Then
		$Save_Dir = FileSelectFolder('Where do you want to save files?', '', 7, @DesktopDir, $Log_Interface)
		If @error Then Return -1
		$Stamp = StringReplace(_NowCalc(), '/', '.')
		$Stamp = StringReplace($Stamp, ':', '.')
		$Stamp = StringReplace($Stamp, ' ', ' -- ')
		$Stamp = StringTrimLeft($Stamp, 2)
		$Drop_Files = $Save_Dir & '\DTS Transfer Logs ' & $Stamp
		;MsgBox(0, 'Dir', $Drop_Files)
		DirCreate($Drop_Files)

		If $Transfer = 1 Then $Transfer_Copy = FileCopy($Server_Program_Installation_Path & '\Logs\*', $Drop_Files & '\Server Logs\', 9)
		If $Interface = 1 Then $Interface_Copy = FileCopy($Log_Dir & '\*', $Drop_Files & '\Interface Logs\', 9)
		If $ClientAll = 1 Then $All_Client_Download = _All_Client_Logs($Drop_Files) ;FileCopy($Log_Dir & '\DTS Client Logs\Client.log', $Drop_Files & '\Client.log', 1)
		If $ClientSingle = 1 Then
			$Install_Path = IniRead($INI_File, 'DTS Client', 'Install_Location', 'Error')
			$Client_Log = '\\' & $Go_Get_Workstation & '\c$\' & $Install_Path & '\Logs\'
			;MsgBox(0,'Install Path',$Install_Path)
			If $Install_Path = 'Error' Then
				ProgressOff()
				MsgBox(16, 'Client Log Retrieval - Error', 'Unable to locate "Installation Path" in ini file. Aborting "Client Log Retrieval".')
				Return -1
			EndIf
			If Not FileExists($Client_Log) Then
				MsgBox(16, 'Client Log Retrieval - Error', 'Unable to locate "Client Log" on ' & $Go_Get_Workstation & '. Aborting "Client Log Retrieval".')
				Return -1
			EndIf
			FileCopy($Client_Log, $Drop_Files & '\Client.log', 1)
		EndIf

		If $Transfer_Copy = 0 And $Interface_Copy = 0 Then
			MsgBox(16, 'Copy Error', 'The Server DTS Logs and Interface Logs could not be downloaded. ' & $ContactAdmin)
;~ 			_FileWriteLog($Interface_Log, 'The Server DTS Logs and Interface Logs could not be downloaded.')
			_FileWriteLog($ErrLog, 'The Server DTS Logs and Interface Logs could not be downloaded.')
		ElseIf $Transfer_Copy = 0 Then
			MsgBox(16, 'Copy Error', 'The Server DTS Logs could not be downloaded. ' & $ContactAdmin)
;~ 			_FileWriteLog($Interface_Log, 'The Server DTS Log could not be downloaded.')
			_FileWriteLog($ErrLog, 'The Server DTS Logs could not be downloaded.')
		ElseIf $Interface_Copy = 0 Then
			MsgBox(16, 'Copy Error', 'The Interface Logs could not be downloaded. ' & $ContactAdmin)
;~ 			_FileWriteLog($Interface_Log, 'The Interface Log could not be downloaded.')
			_FileWriteLog($ErrLog, 'The Interface Logs could not be downloaded.')
		ElseIf $All_Client_Download = 0 Then
			MsgBox(16, 'Copy Error', 'There were errors downloading logs from 1 or more clients. Please view the Interface Log ' & _
					'click View on the Interface and select "View DTS Interface Log".')
;~ 			_FileWriteLog($Interface_Log, 'There were errors downloading logs from 1 or more clients.')
			_FileWriteLog($ErrLog, 'There were errors downloading logs from 1 or more clients.')
		EndIf
		MsgBox(64, 'Log Download - Success', 'The logs have been downloaded.')
	Else
		MsgBox(16, 'Log Download - Error', 'Please make a selection first.')
	EndIf

EndFunc   ;==>_Log_Downloader

;===============================================================================
; Name:   _All_Client_Logs()
; Description:   Download all client logs
; Syntax:   _All_Client_Logs($Drop_Files)
; Parameter(s): 	None
; Requirement(s):   None
; Return Value(s): 	Failure: 0
;					Success: 1
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _All_Client_Logs($Drop_Files)
	Local $rows, $aComputers[1], $Temp, $sPlit, $Install_Path, $Copy, $Failed = 0, _
			$Successful_Copy = 'Successfully Copied Files From:' & @CRLF, $Failed_Copy = 'Failed To Copy Files From:' & @CRLF

	$rows = _GUICtrlListView_GetItemCount($Transfer_List)
	If $rows = 0 Then
		MsgBox(16, 'Error', 'There are no clients to pull logs from. Please add clients first then try again.')
	Else
		ReDim $aComputers[$rows]
		For $i = 0 To $rows - 1
			$Temp = _GUICtrlListView_GetItemText($Transfer_List, $i, 1)
			$aComputers[$i] = $Temp
		Next
;~ 		_ArrayDisplay($aComputers, 'Computers list')
		$Install_Path = IniRead($INI_File, 'DTS Client', 'Install_Location', 'Error')
		;MsgBox(0,'Install Path',$Install_Path)
		If $Install_Path = 'Error' Then
			ProgressOff()
			MsgBox(16, 'Client Deployment - Error', 'Unable to locate "Installation Path" in ini file. Aborting "Workstation Deployment".')
			Return 0
		EndIf

		For $Computer In $aComputers
;~ 			MsgBox(64,'System', '\\' & $Computer & '\c$\' & $Install_Path & '\Logs\DTS Client.log')
			$Copy = FileCopy('\\' & $Computer & '\c$\' & $Install_Path & '\Logs\DTS Client.log', $Drop_Files & '\' & $Computer & ' DTS.log')
			If $Copy = 1 Then
				$Successful_Copy = $Successful_Copy & @TAB & $Computer & @CRLF
			Else
				$Failed_Copy = $Failed_Copy & @TAB & $Computer & @CRLF
				$Failed = 1
			EndIf
		Next
		_FileWriteLog($Interface_Log, 'All client log file copy initiated.' & @CRLF & $Successful_Copy & $Failed_Copy & 'Copy complete...')
	EndIf

	If $Failed = 1 Then
		Return 0
	Else
		Return 1
	EndIf
EndFunc   ;==>_All_Client_Logs

;===============================================================================
; Name:   Service_Status()
; Description:   Detects the current status of the transfer service
; Syntax:   Service_Status()
; Parameter(s): 	None
; Requirement(s):   None
; Return Value(s): 	None
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func Service_Status()
	Local $eXist, $Status

	$ServiceName = IniRead($INI_File, 'Service', 'ServiceName', 'Error')
	If $ServiceName = 'Error' Then
		ProgressOff()
		MsgBox(16, 'Transfer Engine Service - Error', 'Unable to locate "Service Name" in ini file. Unable to determine current status.')
		_Destroy_Service_Interface()
		Return 0
	EndIf
	$SystemName = IniRead($INI_File, 'Service', 'SystemName', 'Error')
	If $SystemName = 'Error' Then
		ProgressOff()
		MsgBox(16, 'Transfer Engine Service - Error', 'Unable to locate "System Name" in ini file. Unable to determine service location.')
		_Destroy_Service_Interface()
		Return 0
	EndIf

	$eXist = _ServiceExists($SystemName, $ServiceName)
	If $eXist = 1 Then
		$Status = _ServiceRunning($SystemName, $ServiceName)
;~ 		MsgBox(64,'Status',$Status)
	Else
		MsgBox(16, 'Transfer Engine Service - Error', 'Unable to detect service on server. Validate serivice name: ' & $ServiceName & ' is correct and is listed correctly in the settings.ini file.')
	EndIf

	If $Status = 1 Then
		GUICtrlSetState($Start_Button, $GUI_DISABLE)
		GUICtrlSetState($Stop_Button, $GUI_Enable)
		GUICtrlSetData($Status_State, 'Running')
		GUICtrlSetImage($Status_Pic, '.\Images\On.jpg')
	Else
		GUICtrlSetState($Stop_Button, $GUI_DISABLE)
		GUICtrlSetState($Start_Button, $GUI_Enable)
		GUICtrlSetData($Status_State, 'Stopped')
		GUICtrlSetImage($Status_Pic, '.\Images\Off.jpg')
	EndIf
EndFunc   ;==>Service_Status

;===============================================================================
; Name:   _Service_Action()
; Description:   Changes the state of the service
; Syntax:   _Service_Action()
; Parameter(s): 	None
; Requirement(s):   None
; Return Value(s): 	None
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Service_Action()
	Local $Status, $sReturn, $err, $nStatus

	$Status = _ServiceRunning($SystemName, $ServiceName)

	If $Status = 1 Then
		$sReturn = _StopService($SystemName, $ServiceName)
		$err = @error
		If $sReturn <> 1 Then
			MsgBox(16, 'Transfer Engine Service - Error', 'There was an error stopping the service.' & @CRLF & 'Error Code: ' & @error)
			_FileWriteLog($ErrLog, 'There was an error when stopping the service: ' & $err & '.')
		EndIf
	Else
		$sReturn = _StartService($SystemName, $ServiceName)
		If $sReturn <> 1 Then
			MsgBox(16, 'Transfer Engine Service - Error', 'There was an error starting the service.' & @CRLF & 'Error Code: ' & @error)
			_FileWriteLog($ErrLog, 'There was an error when starting the service: ' & $err & '.')
		EndIf
	EndIf

	For $t = 1 To 10 ;Keep checking for the stop for up to 10 seconds because there is no checking built into the start/stop commands
		$nStatus = _ServiceRunning($SystemName, $ServiceName)
		If $nStatus <> $Status Then
			ExitLoop
		Else
			Sleep(1000)
		EndIf
	Next

	If $nStatus = $Status Then MsgBox(16, 'Transfer Engine Service - Error', 'Unable to detect a successful change in service state.')

	If $nStatus = 1 Then
		GUICtrlSetState($Start_Button, $GUI_DISABLE)
		GUICtrlSetState($Stop_Button, $GUI_Enable)
		GUICtrlSetData($Status_State, 'Running')
		GUICtrlSetImage($Status_Pic, '.\Images\On.jpg')
	Else
		GUICtrlSetState($Stop_Button, $GUI_DISABLE)
		GUICtrlSetState($Start_Button, $GUI_Enable)
		GUICtrlSetData($Status_State, 'Stopped')
		GUICtrlSetImage($Status_Pic, '.\Images\Off.jpg')
	EndIf

EndFunc   ;==>_Service_Action

#EndRegion Functions

#Region Enable/Disable Focus
;--------------------------------------------------------------------------------
;Enable/Disable Main Interface
;--------------------------------------------------------------------------------
Func _Main_Interface_State_Change()
	Local $state
	$state = WinGetState($DTS_Interface)
	If BitAND($state, 4) Then
		GUISetState(@SW_DISABLE, $DTS_Interface)
	Else
		GUISetState(@SW_ENABLE, $DTS_Interface)
		WinActivate($DTS_Interface)
	EndIf
EndFunc   ;==>_Main_Interface_State_Change
#EndRegion Enable/Disable Focus

#Region Sub Interfaces
;--------------------------------------------------------------------------------
;Sub Interfaces
;--------------------------------------------------------------------------------
;===============================================================================
; Name:   Error_DTS_Interface_Log()
; Name:   General_DTS_Interface_Log()
; Name:   _Log_Interface()
; Name:   _Client_Deploy_Interface()
; Name:   _Add_Workstation_Interface()
; Name:   _INI_Viewer()
; Name:   _Change_Password()
; Name:   _Service_Interface()

; Description:   Interfaces for perspective functions
; Parameter(s): 	None
; Requirement(s):   None
; Return Value(s): 	None
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func Error_DTS_Interface_Log()
	Local $Viewer, $Error_Info, $oLog_File

	$oLog_File = FileOpen($ErrLog, 0)
	$Error_Info = FileRead($oLog_File)
	FileClose($oLog_File)

	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\DTS Project\Scripts\INI Viewer.kxf
	$Error_Interface_Log_Viewer = GUICreate("Error Log Viewer", 1004, 457, 128, 134, $WS_OVERLAPPEDWINDOW, $WS_EX_WINDOWEDGE)
	GUISetBkColor(0xFAFAFA)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_Error_Log_Interface_Viewer")
	GUISetIcon(@ScriptDir & '\Images\Download.ico')
	$Viewer = GUICtrlCreateEdit("", 0, 0, 1002, 455, BitOr($GUI_SS_DEFAULT_EDIT, $ES_READONLY))
	GUICtrlSetResizing(-1, $GUI_DOCKBORDERS)
	GUICtrlSetData(-1, $Error_Info)
	GUISetState(@SW_SHOW)
;~ 	_Main_Interface_State_Change()
	#EndRegion ### END Koda GUI section ###

EndFunc   ;==>Error_DTS_Interface_Log

Func General_DTS_Interface_Log()
	Local $Viewer, $General_Info, $oLog_File

	$oLog_File = FileOpen($Interface_Log, 0)
	$General_Info = FileRead($oLog_File)
	FileClose($oLog_File)

	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\DTS Project\Scripts\INI Viewer.kxf
	$General_Interface_Log_Viewer = GUICreate("General Log Viewer", 1004, 457, 128, 134, $WS_OVERLAPPEDWINDOW, $WS_EX_WINDOWEDGE)
	GUISetBkColor(0xFAFAFA)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_General_Log_Interface_Viewer")
	GUISetIcon(@ScriptDir & '\Images\Download.ico')
	$Viewer = GUICtrlCreateEdit("", 0, 0, 1002, 455,  BitOr($GUI_SS_DEFAULT_EDIT, $ES_READONLY))
	GUICtrlSetResizing(-1, $GUI_DOCKBORDERS)
	GUICtrlSetData($Viewer, $General_Info)
	GUISetState(@SW_SHOW)
;~ 	_Main_Interface_State_Change()
	#EndRegion ### END Koda GUI section ###

EndFunc   ;==>General_DTS_Interface_Log

Func _Log_Interface()
	Local $Info_Label, $Transfer_Label, $Interface_Label, $Client_Label, $Download_Logs
	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\DTS Project\Scripts\Log Interface.kxf
	$Log_Interface = GUICreate("Log Selector", 321, 338, 281, 139)
	GUISetBkColor(0xFAFAFA)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_Log_Interface")
	GUISetIcon(@ScriptDir & '\Images\Download.ico')
	$Info_Label = GUICtrlCreateLabel("Please choose what logs you would like to Download.", 24, 8, 265, 56)
	GUICtrlSetFont(-1, 12, 600, 0, "MS Sans Serif")
	$Transfer_Label = GUICtrlCreateLabel("Server DTS Log", 32, 70, 120, 20)
	GUICtrlSetFont(-1, 10, 600, 0, "MS Sans Serif")
	$Interface_Label = GUICtrlCreateLabel("DTS Interface", 32, 126, 110, 20)
	GUICtrlSetFont(-1, 10, 600, 0, "MS Sans Serif")
	$Client_Label = GUICtrlCreateLabel("DTS Client", 32, 182, 99, 20)
	GUICtrlSetFont(-1, 10, 600, 0, "MS Sans Serif")
	$Download_DTS_Transfer_Log = GUICtrlCreateCheckbox("Download Log", 56, 94, 97, 17)
	$Download_Interface_Log = GUICtrlCreateCheckbox("Download Log", 56, 150, 97, 17)
	GUICtrlCreateCheckbox('Please choose one', 32, 210, 250, 50, $BS_GROUPBOX)
	$Download_All_Client_Logs = GUICtrlCreateRadio("Download All Logs", 52, 230)
	;GUICtrlSetOnEvent($Download_All_Client_Logs, '_CheckBox_Switch')
	$Downlaod_Single_Client_Log = GUICtrlCreateRadio("Download Single Log", 160, 230)
	;GUICtrlSetOnEvent($Downlaod_Single_Client_Log, '_CheckBox_Switch')
	$Workstation_Log = GUICtrlCreateInput("Workstation or IP", 160, 285, 105, 21)
	$Download_Logs = GUICtrlCreateButton("Download", 40, 280, 89, 33)
	GUICtrlSetFont(-1, 12, 400, 0, "MS Sans Serif")
	GUICtrlSetOnEvent($Download_Logs, '_Log_Downloader')
	GUISetState(@SW_SHOW)
	_Main_Interface_State_Change()
	#EndRegion ### END Koda GUI section ###
EndFunc   ;==>_Log_Interface

Func _Client_Deploy_Interface()
	Local $DeployWorkstationNameLabel, $DeployIPLabel, $Deploy
	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\DTS Project\Images\Deploy Client.kxf
	$ClientDeploy = GUICreate("Client Deployment", 221, 184, 193, 115)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_Deploy")
	GUISetIcon(@ScriptDir & '\Images\Download.ico')
	$DeployWorkstationNameInput = GUICtrlCreateInput("Workstation Name", 16, 32, 185, 21)
	$DeployWorkstationNameLabel = GUICtrlCreateLabel("Workstation Name", 16, 8, 100, 17)
	$DeployIP = _GUICtrlIpAddress_Create($ClientDeploy, 16, 88, 153, 25)
	_GUICtrlIpAddress_Set($DeployIP, "0.0.0.0")
	$DeployIPLabel = GUICtrlCreateLabel("Workstation IP", 16, 70, 100, 17)
	$Deploy = GUICtrlCreateButton("Deploy", 56, 144, 105, 25, 0)
	GUICtrlSetOnEvent(-1, '_Deploy')
	GUISetState(@SW_SHOW)
	_Main_Interface_State_Change()
	#EndRegion ### END Koda GUI section ###
EndFunc   ;==>_Client_Deploy_Interface

Func _Add_Workstation_Interface()
	Local $AddWorkstationNameLabel, $AddIPLabel, $AddSourceLabel, $AddDestinationLabel, $AddWorkstationButton
	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\DTS Project\Scripts\Add Computer Interface.kxf
	$AddWorkstation = GUICreate("Add Workstation", 233, 319, -1, -1, -1, $WS_EX_ACCEPTFILES, $DTS_Interface)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_Add")
	GUISetIcon(@ScriptDir & '\Images\Download.ico')
	;$AddWorkstationName = GUICtrlCreateInput("Workstation Name", 16, 40, 129, 21)
	$AddWorkstationName = GUICtrlCreateInput("", 16, 64, 129, 21) ;Testing Purposes only
	;$AddIP = _GUICtrlIpAddress_Create($AddWorkstation, 16, 104, 129, 21)
	$AddIP = _GUICtrlIpAddress_Create($AddWorkstation, 16, 128, 129, 21) ;Testing Purposes only
;~ 	_GUICtrlIpAddress_Set($AddIP, "")
	;$AddSourceInput = GUICtrlCreateInput("Source", 14, 163, 201, 21)
	$AddSourceInput = GUICtrlCreateInput("", 14, 187, 201, 21) ;Testing Purposes only
	$AddDestinationInput = GUICtrlCreateInput("", 16, 240, 201, 21)
	GUICtrlSetState($AddSourceInput, $GUI_DROPACCEPTED)
	GUICtrlSetState($AddDestinationInput, $GUI_DROPACCEPTED)
	;$AddDestinationInput = GUICtrlCreateInput("Destination", 14, 217, 201, 21)
	$AddWorkstationNameLabel = GUICtrlCreateLabel("Workstation Name", 16, 40, 100, 17)
	$AddIPLabel = GUICtrlCreateLabel("Workstation IP", 16, 104, 100, 17)
	$AddSourceLabel = GUICtrlCreateLabel("Source Location", 16, 168, 82, 17)
	$AddDestinationLabel = GUICtrlCreateLabel("Destination Location", 16, 221, 100, 17)
	$TransferTypebox = GUICtrlCreateCheckbox("Check if share transfer", 16, 8, 130, 17)
	GUICtrlSetOnEvent(-1, "_TransferType")
	$AddWorkstationButton = GUICtrlCreateButton("Add", 64, 280, 97, 25)

	GUICtrlSetOnEvent($AddWorkstationButton, '_Add_Workstation')
	GUISetState(@SW_SHOW)
	GUICtrlSetState($AddDestinationInput, $GUI_DISABLE)
	GUICtrlSetState($TransferTypebox, $GUI_DISABLE)
	_Main_Interface_State_Change()
	#EndRegion ### END Koda GUI section ###
	#cs
	#Region ### START Koda GUI section ### Form=g:\is-server\djthornton\scripts\dts project\scripts\addworkstation.kxf
	$AddWorkstation = GUICreate("Add Workstation", 233, 319, 423, 207, -1, BitOR($WS_EX_ACCEPTFILES,$WS_EX_WINDOWEDGE))
	$AddWorkstationName = GUICtrlCreateInput("jwx11c1", 16, 64, 129, 21)
	$AddSourceInput = GUICtrlCreateInput("\\austech\is-server\djthornton\scripts\FTP Project\Testing\Source1\Test.txt", 14, 187, 201, 21)
	$AddWorkstationNameLabel = GUICtrlCreateLabel("Workstation Name", 16, 40, 100, 17)
	$AddIPLabel = GUICtrlCreateLabel("Workstation IP", 16, 104, 100, 17)
	$AddSourceLabel = GUICtrlCreateLabel("Source Location", 16, 168, 82, 17)
	$AddWorkstationButton = GUICtrlCreateButton("Add", 64, 280, 97, 25, $WS_GROUP)
	$AddDestinationInput = GUICtrlCreateInput("AddDestinationInput", 16, 240, 201, 21)
	$AddDestinationLabel = GUICtrlCreateLabel("DestinationLabel", 16, 224, 83, 17)
	$Checkbox1 = GUICtrlCreateCheckbox("Check if share transfer", 16, 8, 130, 17)
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###
	#ce
EndFunc   ;==>_Add_Workstation_Interface

Func _INI_Viewer()
	Local $Viewer, $oINI_File, $INI_Info
	$oINI_File = FileOpen($INI_File, 0)
	$INI_Info = FileRead($oINI_File)
	FileClose($oINI_File)

	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\DTS Project\Scripts\INI Viewer.kxf
	$INI_Viewer = GUICreate("INI Viewer", 1004, 457, 128, 134, $WS_OVERLAPPEDWINDOW, $WS_EX_WINDOWEDGE)
	GUISetBkColor(0xFAFAFA)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_INI_Viewer")
	GUISetIcon(@ScriptDir & '\Images\Download.ico')
	$Viewer = GUICtrlCreateEdit("", 0, 0, 1002, 455, BitOr($GUI_SS_DEFAULT_EDIT, $ES_READONLY))
	GUICtrlSetResizing(-1, $GUI_DOCKBORDERS)
	GUICtrlSetData(-1, $INI_Info)
	GUISetState(@SW_SHOW)
;~ 	_Main_Interface_State_Change()
	#EndRegion ### END Koda GUI section ###
EndFunc   ;==>_INI_Viewer

Func _Change_Password()
	Global $CP1, $CP2, $SPBox, $Save_Password, $Change_Password_Label
	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\DTS Project\Scripts\Save Password Dialog.kxf
	$Change_Password_GUI = GUICreate("Change Password", 230, 191, 259, 189)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_Change_Password")
	GUISetIcon(@ScriptDir & '\Images\Download.ico')
	$CP1 = GUICtrlCreateInput("", 32, 40, 161, 21)
	$CP2 = GUICtrlCreateInput("", 32, 88, 161, 21)
	_GUICtrlEdit_SetPasswordChar($CP1, '*')
	_GUICtrlEdit_SetPasswordChar($CP2, '*')
	$Change_Password_Label = GUICtrlCreateLabel("Change Password", 64, 16, 90, 17)
	$Save_Password = GUICtrlCreateButton("Save", 72, 120, 81, 25, 0)
	GUICtrlSetOnEvent($Save_Password, '_Save_Password')
	$SPBox = GUICtrlCreateCheckbox("Show Password", 16, 160, 97, 17)
	GUICtrlSetOnEvent($SPBox, '_Show_Password')
	GUISetState(@SW_SHOW)
	_Main_Interface_State_Change()
	#EndRegion ### END Koda GUI section ###
EndFunc   ;==>_Change_Password

Func _Service_Interface()
	Global $Service_Prompt, $Status_Label
	#Region ### START Koda GUI section ### Form=\\austech\is-server\djthornton\Scripts\DTS Project\Scripts\Service.kxf
	$Service_Interface = GUICreate("Transfer Engine Service", 338, 146, 293, 580)
	GUISetBkColor(0xffffff)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_Service_Interface")
	GUISetIcon(@ScriptDir & '\Images\Download.ico')
	$Service_Prompt = GUICtrlCreateLabel("The server transfer engine can be stopped and started from here.", 16, 16, 311, 17)
	$Status_Label = GUICtrlCreateLabel("Current status:", 16, 64)
	$Status_State = GUICtrlCreateLabel("Acquiring...", 104, 64)
	$Start_Button = GUICtrlCreateButton("Start", 56, 104, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, '_Service_Action')
	$Stop_Button = GUICtrlCreateButton("Stop", 208, 104, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, '_Service_Action')
	$Status_Pic = GUICtrlCreatePic("", 200, 56, 33, 25, BitOR($SS_NOTIFY, $WS_GROUP, $WS_CLIPSIBLINGS))
	GUISetState(@SW_SHOW)
	_Main_Interface_State_Change()
	Service_Status()
	#EndRegion ### END Koda GUI section ###
EndFunc   ;==>_Service_Interface

Func _Help()
	ShellExecute(".\DTS Help.chm", "", @ScriptDir, "open", @SW_MAXIMIZE)
EndFunc

Func _About()
	#Region ### START Koda GUI section ### Form=G:\IS-SERVER\djthornton\Scripts\DTS Project\Scripts\About.kxf
	$About_GUI = GUICreate("DTS About", 420, 286, 426, 209)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_About_Interface")
	;$About_Logo = GUICtrlCreatePic(".\Images\Download-icon 2.jpg", 160.5, 0, 105, 105, BitOR($SS_NOTIFY,$WS_GROUP,$WS_CLIPSIBLINGS))
	$About_Logo = GUICtrlCreatePic(".\Images\Download-icon.jpg", 160.5, 0, 105, 105, BitOR($SS_NOTIFY,$WS_GROUP,$WS_CLIPSIBLINGS))
	$Label1 = GUICtrlCreateLabel("Downtime Transfer System v"&FileGetVersion(@ScriptFullPath), 132, 104, 154, 17)
	$Label2 = GUICtrlCreateLabel("The Downtime Transfer System is a program used to transfer the contents of a", 16, 136, 371, 17)
	$Label3 = GUICtrlCreateLabel("Designed and Created by:", 138, 216, 127, 17)
	$Label4 = GUICtrlCreateLabel("Seton Family of Hospitals", 140, 264, 122, 17)
	$Label5 = GUICtrlCreateLabel("Dominique J. Thornton", 146, 240, 111, 17)
	$Label6 = GUICtrlCreateLabel("directory\share to a workstation. It will perform checks to validate the", 37, 160, 328, 17)
	$Label7 = GUICtrlCreateLabel("availability of its workstations, files, and the success of its transfers.", 42, 184, 318, 17)

	;~ $Label3 = GUICtrlCreateLabel("Designed and Created by: Dominique J. Thornton", 147, 216, 125, 41)
	GUISetState(@SW_SHOW)
	;$About_Info = GUICtrlCreateEdit("", 40, 104, 337, 161)
	;GUISetBkColor('0xffffff', $About_GUI)
	GUISetBkColor('0x585858', $About_GUI)
	GUISetIcon(@ScriptDir & '\Images\Download.ico')
	;GUICtrlSetData(-1, "About_Info")
	GUICtrlSetBkColor(-1, 0x585858)
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###
	_Main_Interface_State_Change()
EndFunc


#EndRegion Sub Interfaces

#Region Destroy Interfaces
;--------------------------------------------------------------------------------
;Destroy Interfaces
;--------------------------------------------------------------------------------
Func _Exit_Interface()
	;Delete the GUI then end the script
	_GUICtrlListView_UnRegisterSortCallBack($Transfer_List)
;~ 	GUIRegisterMsg($WM_NOTIFY, "")
	XSkinAnimate($DTS_Interface, "", 2)
	GUIDelete($DTS_Interface)
	_FileWriteLog($Interface_Log, 'The DTS Interface has been shutdown')
	Exit
EndFunc   ;==>_Exit_Interface

Func _Destroy_Add()
	GUIDelete($AddWorkstation)
	_Main_Interface_State_Change()
EndFunc   ;==>_Destroy_Add

Func _Destroy_Remove()
	GUIDelete($RemoveWorkstation)
	_Main_Interface_State_Change()
EndFunc   ;==>_Destroy_Remove

Func _Destroy_Deploy()
	GUIDelete($ClientDeploy)
	_Main_Interface_State_Change()
EndFunc   ;==>_Destroy_Deploy

Func _Destroy_Client_Removal()
	GUIDelete($ClientRemove)
	_Main_Interface_State_Change()
EndFunc   ;==>_Destroy_Client_Removal

Func _Destroy_Change_Password()
	GUIDelete($Change_Password_GUI)
	_Main_Interface_State_Change()
EndFunc   ;==>_Destroy_Change_Password

Func _Destroy_INI_Viewer()
	GUIDelete($INI_Viewer)
;~ 	_Main_Interface_State_Change()
EndFunc   ;==>_Destroy_INI_Viewer

Func _Destroy_General_Log_Interface_Viewer()
	GUIDelete($General_Interface_Log_Viewer)
;~ 	_Main_Interface_State_Change()
EndFunc   ;==>_Destroy_General_Log_Interface_Viewer

Func _Destroy_Error_Log_Interface_Viewer()
	GUIDelete($Error_Interface_Log_Viewer)
;~ 	_Main_Interface_State_Change()
EndFunc   ;==>_Destroy_Error_Log_Interface_Viewer

Func _Destroy_Log_Interface()
	GUIDelete($Log_Interface)
	_Main_Interface_State_Change()
EndFunc   ;==>_Destroy_Log_Interface

Func _Destroy_Service_Interface()
	GUIDelete($Service_Interface)
	_Main_Interface_State_Change()
EndFunc   ;==>_Destroy_Service_Interface

Func _Destroy_About_Interface()
	GUIDelete($About_GUI)
	_Main_Interface_State_Change()
EndFunc

;
;Debugging
;
Func MyErrFunc()

	Local $HexNumber

	$HexNumber = Hex($oMyError.number, 8)

	If $HexNumber = '800708AC' Then Return 'Does Not Exist'

	MsgBox(0, "", "We intercepted a COM Error ! Please take note of the information below and provide it to your System Administrator." & @CRLF & _
			"Number is: " & $HexNumber & @CRLF & _
			"Linenbr is: " & $oMyError.scriptline & @CRLF & _
			"Description is: " & $oMyError.description & @CRLF & _
			"Windescription is: " & $oMyError.windescription)

	SetError(1); something to check for when this function returns
EndFunc   ;==>MyErrFunc

;
;Misc
;
Func _XSkinGroup($Text, $Pleft, $Ptop, $Pwidth, $Pheight, $color)
	Local $XS_n, $PControl
	If StringInStr(@OSType, "WIN32_NT") Then
		$XS_n = DllCall("uxtheme.dll", "int", "GetThemeAppProperties")
		DllCall("uxtheme.dll", "none", "SetThemeAppProperties", "int", 0)
	EndIf
	$PControl = GUICtrlCreateGroup($Text, $Pleft, $Ptop, $Pwidth, $Pheight)
	;GUICtrlSetBkColor($PControl, $color)
	GUICtrlSetColor($PControl, $color)
	If StringInStr(@OSType, "WIN32_NT") And IsArray($XS_n) Then
		DllCall("uxtheme.dll", "none", "SetThemeAppProperties", "int", $XS_n[0])
	EndIf
	Return $PControl
EndFunc   ;==>_XSkinGroup
#EndRegion Destroy Interfaces