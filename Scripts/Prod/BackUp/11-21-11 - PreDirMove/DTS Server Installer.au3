#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=Images\Download.ico
#AutoIt3Wrapper_UseX64=n
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
;Installer

;This programs function is to install the DTS Server Side application
Opt('MustDeclareVars', 1)

#include <Misc.au3>
_Singleton("DTS Server Installer")
;Opt("GUIOnEventMode", 1)
#Region FileInstalls
FileInstall('.\Images\Download.ico', @TempDir & '\DTS Logo.ico', 1)
FileInstall('.\Images\DTS 5.jpg', @TempDir & '\DTS 5.jpg', 1)
FileInstall('.\Images\DTS 5.jpg', @TempDir & '\DTS 5.jpg', 1)
#EndRegion FileInstalls

#Region Includes
;#include <AD.au3>
#include <File.au3>
#include <Array.au3>
#include <GuiEdit.au3>
#include <Services.au3>
#include <GuiListView.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <ServiceControl.au3>
#include <ButtonConstants.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#EndRegion Includes

#Region Globals
;GUI Globals
Global $Server_Installer, $Logo, $Solid, $Title, $Body, $Back, $Next, $Finished, $Cancel, $NetworkToNertwork, $NetworkToDesktop, $Install_Location, _
		$Search, $Type, $Dir, $instance, $Username, $Password1, $Password2, $Sleep, $Attempts, $instance_Label, $Username_Label, _
		$Password_Label, $Sleep_Label, $Attempts_Label, $Progress, $Progress_Info, $Installation, $Uninstall, $ServiceList, _
		$Downtime_Label, $System_Label, $DTS_Pic, $SPBox

;Misc Globals
Global $nMsg, $oMyError = ObjEvent("AutoIt.Error", "_ADoError") ; Install a custom error handler
#EndRegion Globals

#Region ### START Koda GUI section ### Form=\\austech\IS-VoIP\djthornton\Scripts\FTP Project\Scripts\Server Installer.kxf
$Server_Installer = GUICreate("DTS Server Installer", 500, 444, 315, 173, Default, $WS_EX_ACCEPTFILES)
GUISetIcon(@TempDir & '\DTS Logo.ico')
;~ GUISetBkColor(0xffffff)
GUISetBkColor(0xFAFAFA)
;~ GUISetBkColor('0x585858')
$Logo = GUICtrlCreatePic(@TempDir & '\DTS 5.jpg', 0, 0, 500, 100, BitOR($SS_NOTIFY, $WS_GROUP, $WS_CLIPSIBLINGS))

;Verbiage
$Title = GUICtrlCreateLabel('', 8, 120, 250, 25)
$Body = GUICtrlCreateEdit("", 4, 160, 492, 100, $ES_READONLY)
GUICtrlSetBkColor(-1, 0xffffff)
;~ $Body  = GUICtrlCreateLabel("", 8, 170, 492, 50)

;Input
$Install_Location = GUICtrlCreateInput(@ProgramFilesDir, 8, 300, 281, 21)
$instance = GUICtrlCreateInput('', 8, 217, 100, 21)
$Username = GUICtrlCreateInput('', 8, 259, 100, 21)
$Password1 = GUICtrlCreateInput('', 8, 307, 100, 21)
$Password2 = GUICtrlCreateInput('', 115, 307, 100, 21)
$Sleep = GUICtrlCreateInput('', 8, 343, 25, 21)
$Attempts = GUICtrlCreateInput('', 220, 343, 25, 21)

;Password Characters
_GUICtrlEdit_SetPasswordChar($Password1, '*')
_GUICtrlEdit_SetPasswordChar($Password2, '*')

;Boxes
$SPBox = GUICtrlCreateCheckbox('Show Password', 220, 307)

;Labels
$instance_Label = GUICtrlCreateLabel('Instance Name (3 character minimum)', 112, 217)
$Username_Label = GUICtrlCreateLabel('DTS Transfer Account Name (Seton Active Directory Account)', 112, 259)
$Password_Label = GUICtrlCreateLabel('Password                    Verify Password  (Must be Complex)', 8, 285)
$Sleep_Label = GUICtrlCreateLabel('File Check Interval (in minutes)', 40, 343)
$Attempts_Label = GUICtrlCreateLabel('Attempts before email', 252, 343)
$Progress_Info = GUICtrlCreateLabel("", 88, 281, 400, 40)

;Radio
$Installation = GUICtrlCreateRadio('Install', 8, 280, 250, 30)
$Uninstall = GUICtrlCreateRadio('Uninstall', 8, 320, 200, 30)
$NetworkToNertwork = GUICtrlCreateRadio('Network Share To Network Share Transfers - (This feature is not yet available)', 8, 280, 500, 30)
$NetworkToDesktop = GUICtrlCreateRadio('Network Share To Workstation', 8, 320, 200, 30)

;Buttons
$Search = GUICtrlCreateButton("...", 296, 300, 24, 24, 0)
$Back = GUICtrlCreateButton("Back", 128, 400, 89, 25, 0)
$Next = GUICtrlCreateButton("Next", 248, 400, 89, 25, 0)
$Cancel = GUICtrlCreateButton("Cancel", 384, 400, 89, 25, 0)
$Finished = GUICtrlCreateButton("Finished", 384, 408, 89, 25, 0)

;Listview
$ServiceList = GUICtrlCreateListView("", 32, 210, 441, 153)
GUICtrlSetState(-1, $GUI_DROPACCEPTED)

;Progress
$Progress = GUICtrlCreateProgress(85.5, 310, 329, 25)

;Divider
GUICtrlCreatePic('.\Images\Divider.jpg', 0, 370, 500, 1)
GUICtrlCreatePic('.\Images\Divider.jpg', 0, 101, 500, 1)

_Welcome_Page()
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			_Cancel()
		Case $Next
			_Step_Forward()
		Case $Back
			_Step_Back()
		Case $Cancel
			_Cancel()
		Case $NetworkToNertwork
			$Type = 'NTN'
		Case $NetworkToDesktop
			$Type = 'NTD'
		Case $Search
			$Dir = FileSelectFolder('Installation Location', '', 7)
			If @error Then
				;Do Nothing
				$Dir = GUICtrlRead($Install_Location)
			Else
				If Not FileExists($Dir) Then
					MsgBox(16, 'Error', 'The location selected is not a valid installation location. Please select a different ' & _
							'location or use the default.')
				Else
					GUICtrlSetData($Install_Location, $Dir)
				EndIf
			EndIf
		Case $SPBox
			_Show_Password()
		Case $Finished
			GUIDelete($Server_Installer)
			FileDelete(@TempDir & '\DTS*.jpg')
			Exit
	EndSwitch
WEnd

;===============================================================================
; Name:   _Welcome_Page()
; Description:   Welcome page of GUI Installer
; Syntax:   _Welcome_Page()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   -
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Welcome_Page()
	GUICtrlSetData($Title, 'Welcome!')
	GUICtrlSetFont($Title, 16, 600)
	GUICtrlSetData($Body, "This application will install/uninstall Seton's DTS Server Services on this device. " & _
			'If you wish to continue please select which proceedure you want and click "Next" else "Cancel".')
	GUICtrlSetFont($Body, 10, 500)
	GUICtrlSetImage($Logo, @TempDir & '\DTS Logo.ico')
	GUICtrlSetState($Installation, $GUI_SHOW)
	GUICtrlSetState($Uninstall, $GUI_SHOW)
	GUICtrlSetState($Back, $GUI_HIDE)
	GUICtrlSetState($Finished, $GUI_HIDE)
	GUICtrlSetState($NetworkToNertwork, $GUI_HIDE)
	GUICtrlSetState($NetworkToDesktop, $GUI_HIDE)
	GUICtrlSetState($Install_Location, $GUI_HIDE)
	GUICtrlSetState($Search, $GUI_HIDE)
	GUICtrlSetState($instance, $GUI_HIDE)
	GUICtrlSetState($Username, $GUI_HIDE)
	GUICtrlSetState($Password1, $GUI_HIDE)
	GUICtrlSetState($Password2, $GUI_HIDE)
	GUICtrlSetState($Sleep, $GUI_HIDE)
	GUICtrlSetState($Attempts, $GUI_HIDE)
	GUICtrlSetState($instance_Label, $GUI_HIDE)
	GUICtrlSetState($Username_Label, $GUI_HIDE)
	GUICtrlSetState($Password_Label, $GUI_HIDE)
	GUICtrlSetState($SPBox, $GUI_HIDE)
	GUICtrlSetState($Sleep_Label, $GUI_HIDE)
	GUICtrlSetState($Attempts_Label, $GUI_HIDE)
	GUICtrlSetState($Progress, $GUI_HIDE)
	GUICtrlSetState($Progress_Info, $GUI_HIDE)
	GUICtrlSetState($ServiceList, $GUI_HIDE)

EndFunc   ;==>_Welcome_Page

;===============================================================================
; Name:   _System_Type()
; Description:   Define transfer type
; Syntax:   _System_Type()
; Parameter(s):   None
; Requirement(s):  None
; Return Value(s):   None
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _System_Type()
	GUICtrlSetData($Title, 'System Type')
	GUICtrlSetFont($Title, 16, 600)
	GUICtrlSetData($Body, 'Please select the type of system this will be.')
	GUICtrlSetFont($Body, 10, 450)
;~     GUICtrlSetImage($Logo, @TempDir&'\FTP2.jpg')
	GUICtrlSetState($Back, $GUI_SHOW)
	GUICtrlSetState($NetworkToNertwork, $GUI_SHOW)
	GUICtrlSetState($NetworkToNertwork, $GUI_DISABLE)
	GUICtrlSetState($NetworkToDesktop, $GUI_SHOW)
	GUICtrlSetState($Install_Location, $GUI_HIDE)
	GUICtrlSetState($Search, $GUI_HIDE)
	GUICtrlSetState($Installation, $GUI_HIDE)
	GUICtrlSetState($Uninstall, $GUI_HIDE)
EndFunc   ;==>_System_Type

;===============================================================================
; Name:   _System_Installation_Location()
; Description:   Determine where to install the application.
; Syntax:   _System_Installation_Location()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   None
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _System_Installation_Location()
	GUICtrlSetData($Title, 'Installation Location')
	GUICtrlSetFont($Title, 16, 600)
	ControlMove('', '', $Body, 4, 160, 492, 100)
	GUICtrlSetData($Body, 'Please select the location to install Server Components.')
	GUICtrlSetFont($Body, 10, 450)
;~     GUICtrlSetImage($Logo, @TempDir&'\FTP3.jpg')
	GUICtrlSetState($NetworkToNertwork, $GUI_HIDE)
	GUICtrlSetState($NetworkToDesktop, $GUI_HIDE)
	GUICtrlSetState($instance, $GUI_HIDE)
	GUICtrlSetState($Username, $GUI_HIDE)
	GUICtrlSetState($Password1, $GUI_HIDE)
	GUICtrlSetState($Password2, $GUI_HIDE)
	GUICtrlSetState($Sleep, $GUI_HIDE)
	GUICtrlSetState($instance_Label, $GUI_HIDE)
	GUICtrlSetState($Username_Label, $GUI_HIDE)
	GUICtrlSetState($Password_Label, $GUI_HIDE)
	GUICtrlSetState($Sleep_Label, $GUI_HIDE)
	GUICtrlSetState($Install_Location, $GUI_SHOW)
	GUICtrlSetState($Search, $GUI_SHOW)
EndFunc   ;==>_System_Installation_Location

;===============================================================================
; Name:   _Configurations()
; Description:   Page to configure system settings
; Syntax:   _Configurations()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   None
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Configurations()
	GUICtrlSetData($Title, 'Configurations')
	GUICtrlSetFont($Title, 16, 600)
	ControlMove('', '', $Body, 4, 160, 492, 40)
;~ 	MsgBox(64,'Move', @error)
;~ 	4, 160, 492, 100
;~     GUICtrlSetImage($Logo, @TempDir&'\FTP4.jpg')
	If FileExists(@ScriptDir & '\Configuration.ini') Then
		GUICtrlSetData($Body, 'Please create an instance name for this DTS Server Service.' & @CRLF & @CRLF & 'e.g., Downtime; ' & _
				'IS-Security')
	Else
		GUICtrlSetData($Body, 'Please create an instance name for this DTS Server Service and fill in the Server settings .' & _
				' e.g., Downtime; IS-Security')
		GUICtrlSetState($Username, $GUI_SHOW)
		GUICtrlSetState($Username_Label, $GUI_SHOW)
		GUICtrlSetState($Password_Label, $GUI_SHOW)
		GUICtrlSetState($Password1, $GUI_SHOW)
		GUICtrlSetState($Password2, $GUI_SHOW)
		GUICtrlSetState($Sleep, $GUI_SHOW)
		GUICtrlSetState($Attempts, $GUI_SHOW)
		GUICtrlSetState($Sleep_Label, $GUI_SHOW)
		GUICtrlSetState($Attempts_Label, $GUI_SHOW)
		GUICtrlSetState($SPBox, $GUI_SHOW)
	EndIf
	GUICtrlSetFont($Body, 10, 450)
	GUICtrlSetData($Next, 'Next')
	GUICtrlSetState($instance, $GUI_SHOW)
	GUICtrlSetState($instance_Label, $GUI_SHOW)
	GUICtrlSetState($Install_Location, $GUI_HIDE)
	GUICtrlSetState($Search, $GUI_HIDE)

EndFunc   ;==>_Configurations

;===============================================================================
; Name:   _Install_Ready()
; Description:   Confirm configurations
; Syntax:   _Install_Ready()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   None
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Install_Ready()
	GUICtrlSetData($Title, 'Installation Ready')
	GUICtrlSetFont($Title, 16, 600)
	ControlMove('', '', $Body, 4, 160, 492, 100)
	GUICtrlSetData($Body, 'All required information has been gathered. You are ready to install the DTS Server Service.' & _
			@CRLF & @CRLF & 'Click "Install" to complete installation.')
	GUICtrlSetFont($Body, 10, 450)
;~     GUICtrlSetImage($Logo, @TempDir&'\FTP5.jpg')
	GUICtrlSetData($Next, 'Install')
	GUICtrlSetState($instance, $GUI_HIDE)
	GUICtrlSetState($Username, $GUI_HIDE)
	GUICtrlSetState($Password1, $GUI_HIDE)
	GUICtrlSetState($Password2, $GUI_HIDE)
	GUICtrlSetState($SPBox, $GUI_HIDE)
	GUICtrlSetState($Sleep, $GUI_HIDE)
	GUICtrlSetState($Attempts, $GUI_HIDE)
	GUICtrlSetState($instance_Label, $GUI_HIDE)
	GUICtrlSetState($Username_Label, $GUI_HIDE)
	GUICtrlSetState($Password_Label, $GUI_HIDE)
	GUICtrlSetState($Sleep_Label, $GUI_HIDE)
	GUICtrlSetState($Attempts_Label, $GUI_HIDE)
EndFunc   ;==>_Install_Ready

;===============================================================================
; Name:   _Uninstall()
; Description:   Uninstall the installation
; Syntax:   _Uninstall()
; Parameter(s):   None
; Requirement(s):   None
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Uninstall()
	Local $aServices, $aTemp[1], $x
	GUICtrlSetState($Installation, $GUI_HIDE)
	GUICtrlSetState($Uninstall, $GUI_HIDE)
	GUICtrlSetState($ServiceList, $GUI_SHOW)
	GUICtrlSetData($Title, 'Uninstall')
	GUICtrlSetFont($Title, 16, 600)
	ControlMove('', '', $Body, 4, 160, 492, 40)
	GUICtrlSetData($Body, 'Please select the Instance(s) you want to uninstall from this device.')
	GUICtrlSetFont($Body, 10, 450)
	_GUICtrlListView_AddColumn($ServiceList, 'DTS Services', 440)
	$aServices = _Services_ListInstalled()
	;_ArrayDisplay($aServices)
	$x = 0
	For $Service In $aServices
		If StringInStr($Service, 'DTS - ') Then
			_GUICtrlListView_AddItem($ServiceList, $Service)
		EndIf
	Next
	If _GUICtrlListView_GetItemCount($ServiceList) = 0 Then
		MsgBox(48, 'No Installs', 'The are no installations of DTS Server Services detected.')
		_Finished(1)
	EndIf
	;_ArrayDisplay($aTemp)
	;_GUICtrlListView_AddItem($ServiceList, $aTemp)
	;MsgBox(64,'','look')
EndFunc   ;==>_Uninstall

;===============================================================================
; Name:   _Finished()
; Description:   Completion prompt
; Syntax:   _Finished([$Remove])
; Parameter(s):   Remove = 0 then installing; Remove = 1 then uninstalling system
; Requirement(s):   None
; Return Value(s):   None
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Finished($Remove = 0)
	GUICtrlSetData($Title, 'Finished')
	GUICtrlSetFont($Title, 16, 600)
	ControlMove('', '', $Body, 4, 160, 492, 100)
	If $Remove = 1 Then
		GUICtrlSetData($Body, 'The removal of DTS Server Services is complete.')
		GUICtrlSetFont($Body, 10, 450)
	Else
		GUICtrlSetData($Body, 'The installation of the DTS Server Service is complete. The service will need to be manually started the first time.')
		GUICtrlSetFont($Body, 10, 450)
	EndIf
;~     GUICtrlSetImage($Logo, @TempDir&'\FTP6.jpg')
	GUICtrlSetState($Cancel, $GUI_HIDE)
	GUICtrlSetState($Back, $GUI_HIDE)
	GUICtrlSetState($Next, $GUI_HIDE)
	GUICtrlSetState($Finished, $GUI_SHOW)
	GUICtrlSetState($Install_Location, $GUI_HIDE)
	GUICtrlSetState($Search, $GUI_HIDE)
	GUICtrlSetState($Progress, $GUI_HIDE)
	GUICtrlSetState($Progress_Info, $GUI_HIDE)
	GUICtrlSetState($ServiceList, $GUI_HIDE)

EndFunc   ;==>_Finished

;===============================================================================
; Name:   _Step_Forward()
; Description:   The Next button to move forward through GUI
; Syntax:   _Step_Forward()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   None
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Step_Forward()
	Local $Place, $Return, $err, $Selected_Services, $String, $Verify, $Count, $z
	$Place = GUICtrlRead($Title)
	Switch $Place
		Case 'Welcome!'
			If BitAND(GUICtrlRead($Installation), $GUI_CHECKED) = 0 And BitAND(GUICtrlRead($Uninstall), $GUI_CHECKED) = 0 Then
				MsgBox(16, 'Error', 'You must first choose a proceedure before proceeding.')
			ElseIf BitAND(GUICtrlRead($Installation), $GUI_CHECKED) = 1 Then
				_System_Type()
			ElseIf BitAND(GUICtrlRead($Uninstall), $GUI_CHECKED) = 1 Then
				_Uninstall()
			EndIf
		Case 'System Type'
			If BitAND(GUICtrlRead($NetworkToNertwork), $GUI_CHECKED) = 0 And BitAND(GUICtrlRead($NetworkToDesktop), $GUI_CHECKED) = 0 Then
				MsgBox(16, 'Error', 'You must first choose an DTS Service type before proceeding.')
			Else
				_System_Installation_Location()
			EndIf
		Case 'Installation Location'
			$Dir = GUICtrlRead($Install_Location)
			If FileExists($Dir) Then
				_Configurations()
			Else
				MsgBox(16, 'Error', 'The location selected is not a valid installation location. Please select a different ' & _
						'location or use the default.')
			EndIf
		Case 'Configurations'
			If _Service_Exists('DTS' & GUICtrlRead($instance)) = True Or _
					FileExists($Dir & '\DTS Transfer ' & StringUpper(GUICtrlRead($instance))) Or _
					StringLen(GUICtrlRead($instance)) < 3 Then
				MsgBox(16, 'Error', 'The instance name you selected is already in use, ' & _
						'or does not meet the 3 or more character requirement. Please select a different name')
			Else
				If _ADObjectExists(GUICtrlRead($Username)) = 0 Then
					MsgBox(16, 'Error', 'The account name used could not be found in AD. Please check the account name and ' & _
							'try again.')
				Else
					If GUICtrlRead($Password1) == GUICtrlRead($Password2) Then
						If _IsComplex(GUICtrlRead($Password1)) = 1 Then
							If Not StringIsAlNum(GUICtrlRead($Sleep)) > 0 Then
								MsgBox(16, 'Error', 'The Check Interval time must be a whole number greater than 0. Please try again.')
							Else
								_Install_Ready()
							EndIf
						ElseIf StringLen(GUICtrlRead($Password1)) = 0 And StringLen(GUICtrlRead($Password2)) = 0 Then
							MsgBox(16, 'Error', 'Please enter a complex password.')
						Else
							MsgBox(16, 'Error', 'The Password must be a complex password atleast 8 characters in length. ' & _
									'Please try again.')
						EndIf
					Else
						MsgBox(16, 'Error', 'The passwords provided do not match. Please try again.')
					EndIf
				EndIf
			EndIf
		Case 'Installation Ready'
			GUISetState($Server_Installer, @SW_DISABLE)
			$Return = _Install()
			If @error = -1 Then
				GUICtrlSetData($Progress, 0)
				MsgBox(16, 'Error', 'There was an error during installation. Please check the logs.' & @CRLF & @CRLF & 'Reverting ' & _
						'system back to previous state.' & @CRLF & @CRLF & 'Error: ' & $Return)
				_Undo()
				Exit
			EndIf
			GUICtrlSetData($Progress, 100)
			Sleep(2000)
			GUISetState($Server_Installer, @SW_ENABLE)
			_Finished()
		Case 'Uninstall'
			$Selected_Services = _Get_Selected()
			If $Selected_Services = 0 Then
				MsgBox(16, 'Error', 'No Services were selected. Please try again.')
			Else
				For $Service In $Selected_Services
					$String = $String & $Service & @CRLF
				Next
				$Verify = MsgBox(36, 'Remove', 'The following services were chosen for removal:' & @CRLF & @CRLF & $String & @CRLF & 'Are you sure you want to remove these services?')
				If $Verify = 6 Then
					For $Service In $Selected_Services
						_Undo($Service)
						$z = $z + 1
					Next
				EndIf
				_Finished(1)
			EndIf
		Case 'Finished'
			GUIDelete($Server_Installer)
			Exit
	EndSwitch
EndFunc   ;==>_Step_Forward

;===============================================================================
; Name:   _Step_Back()
; Description:   The Back button to move backwards through GUI
; Syntax:   _Step_Back()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   None
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Step_Back()
	Local $Place
	$Place = GUICtrlRead($Title)
	Switch $Place
		Case 'Installation Ready'
			_Configurations()
		Case 'Configurations'
			_System_Installation_Location()
		Case 'Installation Location'
			_System_Type()
		Case 'System Type'
			_Welcome_Page()
	EndSwitch
EndFunc   ;==>_Step_Back

;===============================================================================
; Name:   _Cancel()
; Description:   Cancel button to begin undo and exit
; Syntax:   _Cancel()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   None
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Cancel()
	Local $Place, $Verify
	$Place = GUICtrlRead($Title)
	;MsgBox(64,'Place', $Place)
	If $Place = 'Finished' Then
		Exit
	Else
		GUISetState(@SW_DISABLE)
		$Verify = MsgBox(262180, 'Abort?', 'You have not completed the istallation. If you quite now no changes will be made to your system.' & @CRLF & @CRLF & _
				'Are you sure you want to quite?')
		If $Verify = 6 Then
			GUIDelete($Server_Installer)
			Exit
		EndIf
		GUISetState(@SW_ENABLE)
	EndIf
EndFunc   ;==>_Cancel

;===============================================================================
; Name:   _Install()
; Description:   Begin Installation with all settings configured
; Syntax:   _Install()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):  Failures -
; 						Logs - Unable to create Log folder
;						Backups - Unable to create Backup folder
;						Settings - Unable to create Settings folder
;						Client Executables - Unable to create Client Executables folder
;						DTS Server.exe - Unable to unpack DTS Server.exe
;						srvany.exe - Unable to unpack srvany.exe
;						DTS Client Verification.exe - Unable to unpack DTS Client Verification.exe
;						DTS Client Interface.exe - Unable to unpack DTS Client Interface.exe
;						Config File - Unable to create/open/write Config File
;						Computers.csv - Unable to create Computers.csv
;						Client Log - Unable to create Clients log
;						Transfer Service Error.log - Unable to create Transfer Service Error.log
;						Client Error.log - Unable to create Transfer Client Error.log
;						Error Count.log - Unable to create Error Count.log
;						$Install_Service = Server Service Object Not Found
;						$Install_Service = Service Creation Failed
;					Success -
;						$Install_Service = 1
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Install()

	Local $Create, $fCopy, $fInstall, $Line, $Install_Service, $oFile

	;Directories
	GUICtrlSetState($Progress, $GUI_SHOW)
	GUICtrlSetState($Progress_Info, $GUI_SHOW)
	GUICtrlSetData($Progress_Info, 'Installing DTS Server Services')
	$Dir = $Dir & '\DTS Transfer ' & StringUpper(GUICtrlRead($instance))
	$Create = DirCreate($Dir & '\Logs')
	GUICtrlSetData($Progress_Info, 'Creating ' & $Dir & '\Logs')
	GUICtrlSetData($Progress, 4.5)
	If $Create = 0 Then
		SetError(-1)
		Return 'Logs'
	EndIf
	$Create = DirCreate($Dir & '\Computers\Backups')
	GUICtrlSetData($Progress_Info, 'Creating ' & $Dir & '\Computers\Backups')
	GUICtrlSetData($Progress, 9)
	If $Create = 0 Then
		SetError(-1)
		Return 'Backups'
	EndIf
	$Create = DirCreate($Dir & '\Settings')
	GUICtrlSetData($Progress_Info, 'Creating ' & $Dir & '\Settings')
	GUICtrlSetData($Progress, 13.5)
	If $Create = 0 Then
		SetError(-1)
		Return 'Settings'
	EndIf
	$Create = DirCreate($Dir & '\Client Executables')
	GUICtrlSetData($Progress_Info, 'Creating ' & $Dir & '\Client Executables')
	GUICtrlSetData($Progress, 18)
	If $Create = 0 Then
		SetError(-1)
		Return 'Client Executables'
	EndIf
	$Create = DirCreate($Dir & '\Images')
	GUICtrlSetData($Progress_Info, 'Creating ' & $Dir & '\Images')
	GUICtrlSetData($Progress, 22.5)
	If $Create = 0 Then
		SetError(-1)
		Return 'Images'
	EndIf
	$Create = DirCreate($Dir & '\Admin Console')
	GUICtrlSetData($Progress_Info, 'Creating ' & $Dir & '\Admin Console')
	GUICtrlSetData($Progress, 24.5)
	If $Create = 0 Then
		SetError(-1)
		Return 'Admin Console'
	EndIf
	;Images
	$fInstall = FileInstall('.\Images\Download-icon.jpg', $Dir & '\Images\', 9)
	GUICtrlSetData($Progress_Info, 'Unpacking Download-icon.jpg to ' & $Dir & '\Images')
	GUICtrlSetData($Progress, 27)
	If $fInstall = 0 Then
		SetError(-1)
		Return 'Download-icon.jpg'
	EndIf
	$fInstall = FileInstall('.\Images\DTS 5.jpg', $Dir & '\Images\', 9)
	GUICtrlSetData($Progress_Info, 'Unpacking DTS 5.jpg to ' & $Dir & '\Images')
	GUICtrlSetData($Progress, 31.5)
	If $fInstall = 0 Then
		SetError(-1)
		Return 'DTS 5.jpg'
	EndIf
	$fInstall = FileInstall('.\Images\Download.ico', $Dir & '\Images\', 9)
	GUICtrlSetData($Progress_Info, 'Unpacking Download.ico to ' & $Dir & '\Images')
	GUICtrlSetData($Progress, 35.5)
	If $fInstall = 0 Then
		SetError(-1)
		Return 'Download.ico'
	EndIf
	$fInstall = FileInstall('.\Images\Divider.jpg', $Dir & '\Images\', 9)
	GUICtrlSetData($Progress_Info, 'Unpacking Divider.jpg to ' & $Dir & '\Images')
	GUICtrlSetData($Progress, 40)
	If $fInstall = 0 Then
		SetError(-1)
		Return 'Divider.jpg'
	EndIf
	$fInstall = FileInstall('.\Images\SetonLogo.jpg', $Dir & '\Images\', 9)
	GUICtrlSetData($Progress_Info, 'Unpacking SetonLogo.jpg to ' & $Dir & '\Images')
	GUICtrlSetData($Progress, 44.5)
	If $fInstall = 0 Then
		SetError(-1)
		Return 'SetonLogo.jpg'
	EndIf
	$fInstall = FileInstall('.\Images\On.jpg', $Dir & '\Images\', 9)
	GUICtrlSetData($Progress_Info, 'Unpacking On.jpg to ' & $Dir & '\Images')
	GUICtrlSetData($Progress, 44.5)
	If $fInstall = 0 Then
		SetError(-1)
		Return 'SetonLogo.jpg'
	EndIf
	$fInstall = FileInstall('.\Images\Off.jpg', $Dir & '\Images\', 9)
	GUICtrlSetData($Progress_Info, 'Unpacking Off.jpg to ' & $Dir & '\Images')
	GUICtrlSetData($Progress, 44.5)
	If $fInstall = 0 Then
		SetError(-1)
		Return 'SetonLogo.jpg'
	EndIf
	$fCopy = DirCopy('.\Images', $Dir & '\Admin Console\Images', 1)
	GUICtrlSetData($Progress_Info, 'Copy Images Folder to ' & $Dir & '\Admin Console\Images')
	GUICtrlSetData($Progress, 44.5)
	If $fInstall = 0 Then
		SetError(-1)
		Return 'SetonLogo.jpg'
	EndIf
	;Executables
	$fInstall = FileInstall('.\DTS Server.exe', $Dir & '\', 1)
	GUICtrlSetData($Progress_Info, 'Unpacking DTS Server.exe to ' & $Dir & '\DTS Server.exe')
	GUICtrlSetData($Progress, 49)
	If $fInstall = 0 Then
		SetError(-1)
		Return 'DTS Server.exe'
	EndIf
	$fInstall = FileInstall('.\srvany.exe', $Dir & '\', 1)
	GUICtrlSetData($Progress_Info, 'Unpacking srvany.exe to ' & $Dir & '\srvany.exe')
	GUICtrlSetData($Progress, 53.5)
	If $fInstall = 0 Then
		SetError(-1)
		Return 'srvany.exe'
	EndIf
	$fInstall = FileInstall('.\DTS Client Verification.exe', $Dir & '\Client Executables\', 1)
	GUICtrlSetData($Progress_Info, 'Unpacking DTS Client Verification.exe to ' & $Dir & '\Client Executables\DTS Client Verification.exe')
	GUICtrlSetData($Progress, 58)
	If $fInstall = 0 Then
		SetError(-1)
		Return 'DTS Client Verification.exe'
	EndIf
	$fInstall = FileInstall('.\DTS Client Interface.exe', $Dir & '\Client Executables\', 1)
	GUICtrlSetData($Progress_Info, 'Unpacking DTS Client Interface.exe to ' & $Dir & '\Client Executables\DTS Client Interface.exe')
	GUICtrlSetData($Progress, 62.5)
	If $fInstall = 0 Then
		SetError(-1)
		Return 'DTS Client Interface.exe'
	EndIf
	$fInstall = FileInstall('.\DTS Interface.exe', $Dir & '\Admin Console\', 1)
	GUICtrlSetData($Progress_Info, 'Unpacking DTS Interface.exe to ' & $Dir & '\Admin Console\DTS Client Interface.exe')
	GUICtrlSetData($Progress, 64.5)
	If $fInstall = 0 Then
		SetError(-1)
		Return 'DTS Interface.exe'
	EndIf
	$fInstall = FileInstall('.\DTS Help.chm', $Dir & '\Admin Console\', 1)
	GUICtrlSetData($Progress_Info, 'Unpacking DTS Interface Help File to ' & $Dir & '\Admin Console\DTS Help.chm')
	GUICtrlSetData($Progress, 64.5)
	If $fInstall = 0 Then
		SetError(-1)
		Return 'DTS Help.chm'
	EndIf
	$fInstall = FileInstall('.\DTS Help.chm', $Dir & '\', 1)
	GUICtrlSetData($Progress_Info, 'Unpacking DTS Interface Help File to ' & $Dir & '\DTS Help.chm')
	GUICtrlSetData($Progress, 64.5)
	If $fInstall = 0 Then
		SetError(-1)
		Return 'DTS Help.chm'
	EndIf
	$fInstall = FileInstall('.\DTS Help.htm', $Dir & '\Admin Console\', 1)
	GUICtrlSetData($Progress_Info, 'Unpacking DTS Interface Help File to ' & $Dir & '\Admin Console\DTS Help.htm')
	GUICtrlSetData($Progress, 64.5)
	If $fInstall = 0 Then
		SetError(-1)
		Return 'DTS Help.htm'
	EndIf
	$fInstall = FileInstall('.\DTS Help.htm', $Dir & '\', 1)
	GUICtrlSetData($Progress_Info, 'Unpacking DTS Interface Help File to ' & $Dir & '\DTS Help.htm')
	GUICtrlSetData($Progress, 64.5)
	If $fInstall = 0 Then
		SetError(-1)
		Return 'DTS Help.htm'
	EndIf

	;Server Service
	$oFile = FileOpen($Dir & '\Settings\Configurations.ini', 1)
	GUICtrlSetData($Progress_Info, 'Creating Configuration.ini file.')
	GUICtrlSetData($Progress, 67)
	If $oFile = -1 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	$Line = FileWriteLine($oFile, '[Service]')
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	$Line = FileWriteLine($oFile, 'ServiceName=DTS - ' & GUICtrlRead($instance))
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	$Line = FileWriteLine($oFile, 'SystemName=' & @ComputerName)
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf

	;Server Settings for Client
	$Line = FileWriteLine($oFile, @CRLF)
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	$Line = FileWriteLine($oFile, '[DTS Client]')
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	$Line = FileWriteLine($oFile, 'Username=' & GUICtrlRead($Username))
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	$Line = FileWriteLine($oFile, 'Password=' & GUICtrlRead($Password1))
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	$Line = FileWriteLine($oFile, 'Install_Location=DTS Transfer ' & GUICtrlRead($instance))
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	$Line = FileWriteLine($oFile, 'Client_Executables=DTS Client Verification.exe|DTS Client Interface.exe')
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	$Line = FileWriteLine($oFile, 'Sleep=' & GUICtrlRead($Sleep))
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	;Interface Settings
	$Line = FileWriteLine($oFile, @CRLF)
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	$Line = FileWriteLine($oFile, '[DTS Interface]')
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	$Line = FileWriteLine($oFile, 'Local_Install="' & GUICtrlRead($Install_Location) & '\DTS Interface ' & GUICtrlRead($instance) & '"')
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	;Email Settings
	$Line = FileWriteLine($oFile, @CRLF)
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	$Line = FileWriteLine($oFile, '[DTS Server]')
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	$Line = FileWriteLine($oFile, 'Workstations=Computers.csv')
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	$Line = FileWriteLine($oFile, 'ToEmail=DTS@seton.org')
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	$Line = FileWriteLine($oFile, 'FromEmail=DTSTransferSystem@seton.org')
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	$Line = FileWriteLine($oFile, 'FromName=DTS Notifications')
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	$Line = FileWriteLine($oFile, 'SMTP=ausexfe.seton.org')
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	$Line = FileWriteLine($oFile, 'AttemptsToEmail=' & GUICtrlRead($Attempts))
	If $Line = 0 Then
		SetError(-1)
		Return 'Config File'
	EndIf
	FileClose($oFile)

	;Admin Interface Config File
	$oFile = FileOpen($Dir & '\Admin Console\Admin Config.ini', 1)
	GUICtrlSetData($Progress_Info, 'Creating Admin Config.ini file.')
	GUICtrlSetData($Progress, 69)
	If $oFile = -1 Then
		SetError(-1)
		Return 'Admin Config File'
	EndIf
	$Line = FileWriteLine($oFile, '[Server]')
	If $Line = 0 Then
		SetError(-1)
		Return 'Admin Config File'
	EndIf
	$Line = FileWriteLine($oFile, 'Loc=\\' & @ComputerName & '\' & StringLeft($Dir, 1) & '$\' & StringTrimLeft($Dir, 3))
	If $Line = 0 Then
		SetError(-1)
		Return 'Admin Config File'
	EndIf
	FileClose($oFile)

	;Client Settings File
	$oFile = FileOpen($Dir & '\Client Executables\Settings.ini', 1)
	If $oFile = -1 Then
		SetError(-1)
		Return 'Client Settings File'
	EndIf
	$Line = FileWriteLine($oFile, '[Client]')
	If $Line = 0 Then
		SetError(-1)
		Return 'Client Settings File'
	EndIf
	$Line = FileWriteLine($oFile, 'Username=DTS-Transfer')
	If $Line = 0 Then
		SetError(-1)
		Return 'Client Settings File'
	EndIf
	$Line = FileWriteLine($oFile, 'Agreement=')
	If $Line = 0 Then
		SetError(-1)
		Return 'Client Settings File'
	EndIf
	$Line = FileWriteLine($oFile, 'LoginNote=')
	If $Line = 0 Then
		SetError(-1)
		Return 'Client Settings File'
	EndIf

	;Create Files
	$Create = _FileCreate($Dir & '\Computers\Computers.csv')
	GUICtrlSetData($Progress_Info, 'Creating Computers.csv')
	GUICtrlSetData($Progress, 71.5)
	If $Create = 0 Then
		SetError(-1)
		Return 'Computers.csv'
	EndIf
	$Create = _FileCreate($Dir & '\Logs\Clients.log')
	GUICtrlSetData($Progress_Info, 'Creating Clients.log')
	GUICtrlSetData($Progress, 76)
	If $Create = 0 Then
		SetError(-1)
		Return 'Clients.log'
	EndIf
	$Create = _FileCreate($Dir & '\Logs\Transfer Service Error.log')
	GUICtrlSetData($Progress_Info, 'Creating Transfer Service Error.log')
	GUICtrlSetData($Progress, 80.5)
	If $Create = 0 Then
		SetError(-1)
		Return 'Transfer Service Error.log'
	EndIf
	$Create = _FileCreate($Dir & '\Logs\Client Error.log')
	GUICtrlSetData($Progress_Info, 'Creating Client Error.log')
	GUICtrlSetData($Progress, 85)
	If $Create = 0 Then
		SetError(-1)
		Return 'Client Error.log'
	EndIf
	$Create = _FileCreate($Dir & '\Logs\Error Count.log')
	GUICtrlSetData($Progress_Info, 'Creating Error Count.log')
	GUICtrlSetData($Progress, 89.5)
	If $Create = 0 Then
		SetError(-1)
		Return 'Error Count.log'
	EndIf

	$Install_Service = _Install_Service()
	GUICtrlSetData($Progress_Info, 'Creating Windows Service for DTS Server Services')
	If $Install_Service = 1 Then
		GUICtrlSetData($Progress, 95)
		Return 1
	Else
		SetError(-1)
		GUICtrlSetData($Progress, 95)
		Return $Install_Service
	EndIf
EndFunc   ;==>_Install

;===============================================================================
; Name:   _Undo()
; Description:   Installs server engine service
; Syntax:   _Undo([$service])
; Parameter(s):   Service name - Remove serice if it makes it that far
; Requirement(s):   None
; Return Value(s):   None
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Undo($Service = '')
	Local $Path, $RMDir, $SearchDir, $File

	If $Service = '' Then
		_Service_Delete('DTS' & GUICtrlRead($instance))
		$SearchDir = FileFindFirstFile($Dir & '\*')
		If $SearchDir = -1 Then
			MsgBox(0, "Error", "No files/directories matched the search pattern")
			;Exit
		Else
			While 1
				$File = FileFindNextFile($SearchDir)
				If @error Then ExitLoop
				FileDelete($File)
			WEnd
			If $Dir <> @ProgramFilesDir Then DirRemove($Dir, 1)
		EndIf
	Else
		$Path = _Service_GetPath($Service)
;~ 		MsgBox(64,'Path',$Path)
		$Path = StringReplace($Path, '\srvany.exe', '')
;~         MsgBox(64,'Removing Service Dir',$Path)
		_Service_Delete($Service)
		$Path = StringReplace($Path, '"', '')
		$SearchDir = FileFindFirstFile($Path)
		If $SearchDir = -1 Then
			MsgBox(0, "Error", "No files/directories matched the search pattern: " & $Path)
			;Exit
		Else
			While 1
				$File = FileFindNextFile($SearchDir)
				If @error Then ExitLoop
				FileDelete($File)
			WEnd
		EndIf
		$RMDir = DirRemove($Path, 1)
;~ 		MsgBox(64,'Remove Dir', $RMDir)
	EndIf
EndFunc   ;==>_Undo

;===============================================================================
; Name:   _Get_Selected()
; Description:   Installs server engine service
; Syntax:   _Get_Selected()
; Parameter(s):
; Requirement(s):   ListView to choose from
; Return Value(s):   Success - array of selected items names
;                    Failure - ''
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Get_Selected()
	;$ServiceList
	Local $rows, $Selected, $Selected_Services[1], $k, $Name
	$rows = _GUICtrlListView_GetItemCount($ServiceList)
	$k = 0
	For $x = 0 To $rows - 1
		;	MsgBox(64,'x',$x)
		$Selected = _GUICtrlListView_GetItemSelected($ServiceList, $x)
		;	MsgBox(64,'Selected',$Selected)
		If $Selected = 'True' Then
			ReDim $Selected_Services[$k + 1]
			;	_ArrayDisplay($Selected_Services)
			$Name = _GUICtrlListView_GetItemText($ServiceList, $x)
			$Selected_Services[$k] = $Name ;Index
			$k = $k + 1
		EndIf
	Next
	;_ArrayDisplay($Selected_Services)
	If $Selected_Services[0] = '' Then
		Return 0
	Else
		Return $Selected_Services
	EndIf
EndFunc   ;==>_Get_Selected

;===============================================================================
; Name:   _ADObjectExists()
; Description:   Checks that Domain account exist
; Syntax:   _ADObjectExists()
; Parameter(s):   Account name - $object
; Requirement(s):   Active Directory
; Return Value(s):   Success - 1
;                    Failure - 0
; Author(s)   Unknown
; Note(s):
; Example(s):
;===============================================================================
Func _ADObjectExists($object)
	Local $objConnection, $objRootDSE, $strHostServer, $strDNSDomain, $strQuery, $objRecordSet
	$objConnection = ObjCreate("ADODB.Connection") ; Create COM object to AD
	$objConnection.ConnectionString = "Provider=ADsDSOObject"
	$objConnection.Open("Active Directory Provider")

	$objRootDSE = ObjGet("LDAP://RootDSE")
	$strHostServer = $objRootDSE.Get("dnsHostName")
	$strDNSDomain = $objRootDSE.Get("defaultNamingContext")

	$strQuery = "<LDAP://" & $strHostServer & "/" & $strDNSDomain & ">;(sAMAccountName=" & $object & ");ADsPath;subtree"
	$objRecordSet = $objConnection.Execute($strQuery) ; Retrieve the FQDN for the group, if it exists

	If Not IsObj($objRecordSet) Then ;Return -1
		MsgBox(16, 'Error', 'Unable to bind to AD for account name check.')
		Exit
	EndIf

	If $objRecordSet.RecordCount = 1 Then
		$objRecordSet = 0
		Return 1
	Else
		$objRecordSet = 0
		Return 0
	EndIf
EndFunc   ;==>_ADObjectExists

;===============================================================================
; Name:   _Install_Service()
; Description:   Installs server engine service
; Syntax:   _Install_Service()
; Parameter(s):   None
; Requirement(s):   WMI capable operating system and administrator rights on the computer
; Return Value(s):   Success - 1
;                    Failure - 'Service Creation Failed'
; Author(s)   Unknown
; Note(s):
; Example(s):
;===============================================================================
Func _Install_Service()
	Local $errReturn, $SrvAny = $Dir & '\srvany.exe'
	#cs
		Const $OWN_PROCESS = 0x00000010, _;16 is own process
		$INTERACTIVE = False, _ ;True changes the $Own_Process to 272 is interact with desktop
		$NORMAL_ERROR_CONTROL = 1

		$objWMIService = ObjGet("winmgmts:" & "{impersonationLevel=impersonate}!\\" & $Server & "\root\cimv2")
		If Not IsObj($objWMIService) Then Return 'Server Service Object Not Found'
		$objService = $objWMIService.Get("Win32_BaseService")
		$errReturn = $objService.Create('DTS '&GUICtrlRead($instance), 'DTS Transfer '&GUICtrlRead($instance), _
		(Chr(34) & $SrvAny & Chr(34)), $OWN_PROCESS, $NORMAL_ERROR_CONTROL, _
		"Automatic", $INTERACTIVE, "SETON\"&GUICtrlRead($Username), GUICtrlRead($Password1))
		If $errReturn > 0 Then Return 'Service Creation Failed'
	#ce
	$errReturn = _CreateService('', 'DTS - ' & GUICtrlRead($instance), 'Downtime Transfer System - ' & GUICtrlRead($instance), _
			$SrvAny, "SETON\" & GUICtrlRead($Username), GUICtrlRead($Password1))
	If @error Then Return @error
	RegWrite("HKLM\SYSTEM\CurrentControlSet\Services\" & 'DTS - ' & GUICtrlRead($instance) & "\Parameters", "Application", "REG_SZ", $Dir & '\DTS Server.exe')
	Return 1
EndFunc   ;==>_Install_Service

;===============================================================================
; Name:   _Service_GetPath()
; Description:   Returns File path of service
; Syntax:   _Service_GetPath()
; Parameter(s):   $sServiceName, $Computer='localhost'
; Requirement(s):   WMI capable operating system and administrator rights on the computer
; Return Value(s):   Success - Variable pathname
;                    Failure - -1
; Author(s)   Unknown
; Note(s):
; Example(s):
;===============================================================================
Func _Service_GetPath($sServiceName, $Computer = "localhost")
	If (Not _Service_Exists($sServiceName)) Then Return SetError(1)
	_sServErrorHandlerRegister()
	Local $Service = ObjGet("winmgmts:\\" & $Computer & "\root\cimv2")
	Local $sQuery = "Select * from Win32_Service where name like '" & $sServiceName & "'"
	Local $sItems = $Service.ExecQuery($sQuery)
	_sServErrorHandlerDeRegister()
	For $objService In $sItems
		Return $objService.PathName
	Next
	Return -1
EndFunc   ;==>_Service_GetPath

;===============================================================================
; Name:   _Show_Password()
; Description:   Hide/Unhide password
; Syntax:   _Show_Password()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   Alternates password hiding
; Author(s)   Unknown
; Note(s):
; Example(s):
;===============================================================================
Func _Show_Password()
	If BitAND(GUICtrlRead($SPBox), $GUI_CHECKED) = 0 Then
		;MsgBox(0,'Unchecked',BitAND(GUICtrlRead($SPBox),$GUI_CHECKED))
		_GUICtrlEdit_SetPasswordChar($Password1, '*')
		_GUICtrlEdit_SetPasswordChar($Password2, '*')
		GUICtrlSetState($Password2, $GUI_FOCUS)
		GUICtrlSetState($Password1, $GUI_FOCUS)
	Else
		;MsgBox(0,'Checked',BitAND(GUICtrlRead($SPBox),$GUI_CHECKED))
		_GUICtrlEdit_SetPasswordChar($Password1)
		_GUICtrlEdit_SetPasswordChar($Password2)
		GUICtrlSetState($Password2, $GUI_FOCUS)
		GUICtrlSetState($Password1, $GUI_FOCUS)
	EndIf
EndFunc   ;==>_Show_Password

;===============================================================================
; Name:   _IsComplex()
; Description:   Checks password complexity
; Syntax:   _IsComplex($Password)
; Parameter(s):   $Password
; Requirement(s):   None
; Return Value(s):   Success - 1 ;strength rating of 5 or better
;                    Failure - (-1)
; Author(s)   Unknown
; Note(s):
; Example(s):
;===============================================================================
Func _IsComplex($Password)
	Local $Strength = 0, $Return

	;Check Length
	If StringLen($Password) >= 8 Then
		$Strength = $Strength + 1
		;MsgBox(0, "$Strength = ", $Strength)
	Else
		Return (-1)
	EndIf

	;Check for Lowercase letters
	$Return = _CheckValue(97, 122, $Password)
	If $Return <> 0 Then
		$Strength = $Strength + 1
		$Return = ""
	EndIf

	;Check for Uppercase letters
	$Return = _CheckValue(65, 90, $Password)
	If $Return <> 0 Then
		$Strength = $Strength + 1
		$Return = ""
	EndIf

	;Check for numbers
	$Return = _CheckValue(48, 57, $Password)
	If $Return <> 0 Then
		$Strength = $Strength + 1
		$Return = ""
	EndIf

	;Check for special characters
	$Return = _CheckValue(33, 47, $Password)
	If $Return <> 0 Then
		$Strength = $Strength + 1
		$Return = ""
	EndIf
	$Return = _CheckValue(58, 64, $Password)
	If $Return <> 0 Then
		$Strength = $Strength + 1
		$Return = ""
	EndIf
	$Return = _CheckValue(91, 96, $Password)
	If $Return <> 0 Then
		$Strength = $Strength + 1
		$Return = ""
	EndIf
	$Return = _CheckValue(123, 255, $Password)
	If $Return <> 0 Then
		$Strength = $Strength + 1
		$Return = ""
	EndIf

	;MsgBox(0, "$Strength = ", $Strength)
	If $Strength >= 5 Then
		Return (1)
	Else
		Return (-1)
	EndIf
EndFunc   ;==>_IsComplex

;===============================================================================
; Name:   _CheckValue()
; Description:   Checks for present characters
; Syntax:   _CheckValue()
; Parameter(s):   character range & password; ($x, $y, $Password)
; Requirement(s):   None
; Return Value(s):   Success - 1
;                    Failure - ''
; Author(s)   Unknown
; Note(s):
; Example(s):
;===============================================================================
Func _CheckValue($x, $y, $Password)
	Local $iLoopvar = 0
	For $iLoopvar = $x To $y
		If StringInStr($Password, Chr($iLoopvar), 1, 1, 1) > 0 Then
			;MsgBox(0, "$iLoopvar = ", Chr($iLoopvar))
			Return (1)
		EndIf
	Next
EndFunc   ;==>_CheckValue

;===============================================================================
; Name:   _ADoError()
; Description:   AD Com object detector
; Syntax:   _Service_Delete()
; Parameter(s):   None
; Requirement(s):  Must register Error even ObjEvent("AutoIt.Error", "_ADoError")
; Return Value(s):   Error Message box
;                    File log of error
; Author(s)   Unknown
; Note(s):
; Example(s):
;===============================================================================
Func _ADoError()
	Local $HexNumber, $objConnection
	$HexNumber = Hex($oMyError.number, 8)

	If $HexNumber = 80020009 Then
		SetError(3)
		Return
	EndIf

	If $HexNumber = "8007203A" Then
		SetError(4)
		Return
	EndIf

	If $HexNumber = "00000002" Then
		SetError(5)
		Return 'The system cannot find the file specified.'
	EndIf

	If $HexNumber = "00000035" Then
		SetError(6)
		Return 'Network path cannot be found'
	EndIf

	If $HexNumber = "00000005" Then
		SetError(7)
		Return 'Access Denied'
	EndIf

	If $HexNumber = "80070005" Then
		SetError(8)
		Return 'Access Denied'
	EndIf

	If $HexNumber = "800706BA" Then
		SetError(9)
		Return 'The RPC server is unavailable'
	EndIf

	MsgBox(262144, "", "We intercepted a COM Error !" & @CRLF & _
			"Number is: " & $HexNumber & @CRLF & _
			"Windescription is: " & $oMyError.windescription & @CRLF & _
			"Script Line number is: " & $oMyError.scriptline & @CRLF & _
			"Script Character number is: " & Hex($oMyError.number, 8))

	Select
		Case $oMyError.windescription = "Access is denied."
			$objConnection.Close("Active Directory Provider")
			$objConnection.Open("Active Directory Provider")
			SetError(2)
		Case 1
			SetError(1)
	EndSelect

EndFunc   ;==>_ADoError



