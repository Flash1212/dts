#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_icon=Images\Download.ico
#AutoIt3Wrapper_UseX64=n
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
;DTS Client Interface 2

#include <Misc.au3>
_Singleton("DTS Client Verification")

#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>


Opt('MustDeclareVars', 1)
Opt("GUIOnEventMode", 1)

Global 	$PHI_Conf_Agree, $Agreement, $Down_Time_Client, $Username_Input, $Password_Input, $Continue, $Cancel, _
		$INI = @ScriptDir&'\Settings.ini', $Agree, $RunUser, $Agreement_Verbage, $rAccept, $rDecline, $LoginNote, _
		$ContactAdmin = @CRLF & @CRLF & 'If this is your first time recieving this message then please try again. If not then please contact you system Administrator.'

;~ FileInstall('\\austech\is-si\Development\icons\compass.ico',@ScriptDir & '\Images\compass.ico',1)
;~ FileInstall('\\austech\is-server\djthornton\Scripts\DTS Project\Images\Compass\DTS.JPG',@ScriptDir & '\Images\DTS.jpg',1)
;~ FileInstall("\\austech\IS-SERVER\djthornton\Scripts\DTS Project\Images\Login.jpg",@ScriptDir & '\Images\Login.jpg',1)

$Agreement_Verbage = IniRead($INI, 'Client', 'Agreement', 'Unknown')
If $Agreement_Verbage = 'Unknown' Then
	MsgBox(16,'DTS Client Verification Error', 'Agreement not Found. The ini file used to run this program is incorrectly configured. '&$ContactAdmin)
ElseIf $Agreement_Verbage = '' Then
	MsgBox(16,'DTS Client Verification Error','The Agreement for the DTS Client could not be determined. '&$ContactAdmin)
EndIf

$RunUser = IniRead($INI, 'Client', 'Username', 'Unknown')
If $RunUser = 'Unknown' Then
	MsgBox(16,'DTS Client Verification Error', 'Username not found. The ini file used to run this program is incorrectly configured. '&$ContactAdmin)
ElseIf $RunUser = '' Then
	MsgBox(16,'DTS Client Verification Error','The username used for running the DTS Client could not be determined. '&$ContactAdmin)
EndIf


_Acceptance_Agreement()

_Login()


Func _Acceptance_Agreement()
	#Region ### START Koda GUI section ### Form=
	Local	$AgreementPic, $Agreement
	$PHI_Conf_Agree = GUICreate("PHI Confidentiality Agreement", 504, 431)
	GUISetIcon(@ScriptDir & '\Images\Download.ico')
	GUISetOnEvent($GUI_EVENT_CLOSE,'_Exit')
	$AgreementPic = GUICtrlCreatePic(@ScriptDir & '\Images\DTS 5.jpg', 1, 0, 502, 95)
;~ 	MsgBox(64,'Pic', $AgreementPic& '-' &@ScriptDir & '\Images\compass_banner.jpg')
	$Agreement = GUICtrlCreateEdit($Agreement_Verbage, 9.5, 104, 485, 169, $ES_READONLY)
	GUICtrlSetBkColor(-1, 0xffffff)
	GUICtrlSetFont(-1, 12)
	$rAccept = GUICtrlCreateRadio("I accept the terms in the Confidentiality Agreement", 24, 296, 321, 17)
	$rDecline = GUICtrlCreateRadio("I decline the terms in the Confidentiality Agreement", 24, 328, 305, 17)
	GUICtrlCreatePic('.\Images\Divider.jpg', 0, 363, 504, 2) ;Divider
;~ 	GUICtrlCreateButton("", 0, 363, 504, 3) ;Divider
	$Continue = GUICtrlCreateButton("Continue", 280, 376, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, '_Login')
	$Cancel = GUICtrlCreateButton("Cancel", 392, 376, 75, 25, $WS_GROUP)
	GUICtrlSetOnEvent(-1, '_Cancel')
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###

	While 1
		Sleep(250)
	WEnd
EndFunc

Func _Login()
	Local $LoginInfo, $LoginPic, $OK, $Cancel
	If BitAND(GUICtrlRead($rDecline), $GUI_CHECKED) Then
		If _Cancel() = 1 Then Return
	ElseIf Not BitAND(GUICtrlRead($rAccept), $GUI_CHECKED) Then
		MsgBox(48, 'No Selection', 'You must first select an option to continue.')
		Return
	EndIf
	GUIDelete($PHI_Conf_Agree)
	#Region ### START Koda GUI section ### Form=\\austech\is-server\djthornton\scripts\DTS project\scripts\client login screen.kxf
	$Down_Time_Client = GUICreate("Downtime System", 468, 336)
	GUISetOnEvent($GUI_EVENT_CLOSE,'_Cancel')
	GUISetIcon(@ScriptDir & '\Images\Download.ico')
	$LoginPic = GUICtrlCreatePic(@ScriptDir & '\Images\DTS 5.jpg' , 0, 0, 465, 100, BitOR($SS_NOTIFY,$WS_GROUP,$WS_CLIPSIBLINGS))
	$LoginInfo = GUICtrlCreateLabel("Please log in using your Seton Username but, "&@CRLF&"use the password provided to you for"&@CRLF&"Downtime Procedures.", 85, 110, 428, 60)
	GUICtrlSetFont(-1, 12, 400, 0, "MS Sans Serif")
	$Username_Input = GUICtrlCreateInput("Username", 104, 184, 233, 21)
	$Password_Input = GUICtrlCreateInput("", 104, 224, 233, 21,$ES_PASSWORD)
	$OK = GUICtrlCreateButton("OK", 104, 280, 97, 25, 0)
	GUICtrlSetOnEvent(-1, '_Submit')
	$Cancel = GUICtrlCreateButton("Cancel", 242, 278, 97, 25, 0)
	GUICtrlSetOnEvent(-1, '_Cancel')
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###

EndFunc

Func _Submit()
	Local $Username, $Password, $Return
	$Username = GUICtrlRead($Username_Input)
	$Password = GUICtrlRead($Password_Input)
	If $Password = '' Then
		MsgBox(16,'Error','The password field is blank. Please type in a password.')
		Return -1
	EndIf
;~ 	MsgBox(64,'Run',$RunUser&' - '&$Password)
	$Return = RunAs($RunUser,@ComputerName, $Password, 0, @ScriptDir&'\DTS Client Interface.exe /'&$Username, @ScriptDir)
;~ 	$Return = Run(@ScriptDir&'\DTS Client Interface.exe /'&$Username, @ScriptDir)
;~ 	MsgBox(64,'@error',@error)
	If $Return = 0 Then
		MsgBox(16,'Error','The password provided is incorrect. Please try again.')
		Return -1
	EndIf
;~ 	MsgBox(0,'Return Value',$Return)
;~ 	MsgBox(0,'Input','Username: '&$Username&@CRLF&'Password: '&$Password);&@CRLF&@CRLF&'Password: '&GUICtrlRead($Password_Input))
	_Exit()
EndFunc

Func _Exit()
	GUIDelete($PHI_Conf_Agree)
	Exit
EndFunc

Func _Cancel()
	Local $Verify
	If MsgBox(52,'Exiting', 'You have chosen not to continue. Are you sure you want to exit?') = 7 Then Return 1
	GUIDelete($Down_Time_Client)
	Exit
EndFunc

