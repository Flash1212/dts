;FTP Client verification


#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include <StaticConstants.au3>
#include <EditConstants.au3>

Opt('MustDeclareVars', 1)
Opt("GUIOnEventMode", 1)

Global 	$Agreement, $Down_Time_Client, $Username_Input, $Password_Input, $INI = @ScriptDir&'\Settings.ini', $Agreement_Verbage, $RunUser, _
		$ContactAdmin = @CRLF & @CRLF & 'If this is your first time recieving this message then please try again. If not then please contact you system Administrator.'

FileInstall('\\austech\is-si\Development\icons\compass.ico',@TempDir&'\compass.ico',1)

$Agreement_Verbage = IniRead($INI, 'Client', 'Agreement', 'Unknown')
If $Agreement_Verbage = 'Unknown' Then
	MsgBox(16,'DTS Client Verification Error', 'Agreement not Found. The ini file used to run this program is incorrectly configured. '&$ContactAdmin)
ElseIf $Agreement_Verbage = '' Then
	MsgBox(16,'DTS Client Verification Error','The Agreement for the FTP Client could not be determined. '&$ContactAdmin)
EndIf

$RunUser = IniRead($INI, 'Client', 'Username', 'Unknown')
If $RunUser = 'Unknown' Then
	MsgBox(16,'FTP Client Verification Error', 'Username not found. The ini file used to run this program is incorrectly configured. '&$ContactAdmin)
ElseIf $RunUser = '' Then
	MsgBox(16,'FTP Client Verification Error','The username used for running the FTP Client could not be determined. '&$ContactAdmin)
EndIf

_Acceptance_Agreement()

_Login()

Func _Acceptance_Agreement()
	Local	$Acceptance, $Agree, $Deny, $Edit1
	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\FTP Project\Scripts\Acceptance.kxf
	$Agreement = GUICreate("Confidentiality Agreement", 351, 351, 401, 240)
	GUISetIcon(@TempDir&'\compass.ico')
	GUISetOnEvent($GUI_EVENT_CLOSE,'_Exit')
	GUICtrlSetFont(-1, 12, 400, 0, "MS Sans Serif")
	$Agree = GUICtrlCreateButton("I Agree", 72, 300, 83, 25, 0)
	GUICtrlSetOnEvent(-1, '_Login')
	$Deny = GUICtrlCreateButton("I Do Not Agree", 192, 300, 83, 25, 0)
	GUICtrlSetOnEvent(-1, '_Exit')
	$Acceptance = GUICtrlCreateEdit("", 19, 19, 313, 250, $ES_READONLY)
	GUICtrlSetData(-1, $Agreement_Verbage)
	GUICtrlSetFont($Acceptance,14, 500)
	GUICtrlSetBkColor($Acceptance, 0xffffff)
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###

	While 1
		Sleep(250)
	WEnd
EndFunc

Func _Login()
	Local $LoginInfo, $LoginPic, $OK, $Cancel
	GUIDelete($Agreement)
	#Region ### START Koda GUI section ### Form=\\austech\is-server\djthornton\scripts\ftp project\scripts\client login screen.kxf
	$Down_Time_Client = GUICreate("Down Time Client", 468, 336, 297, 165)
	GUISetOnEvent($GUI_EVENT_CLOSE,'_Cancel')
	GUISetIcon(@TempDir&'\compass.ico')
	$LoginPic = GUICtrlCreatePic("\\austech\IS-SERVER\djthornton\Scripts\FTP Project\Images\Login.jpg" , 0, 0, 465, 108, BitOR($SS_NOTIFY,$WS_GROUP,$WS_CLIPSIBLINGS))
	$LoginInfo = GUICtrlCreateLabel("Please log in using your Seton Username but, "&@CRLF&"use the password provided to you for"&@CRLF&"Downtime Procedures." _
									, 85, 110, 428, 60)
	GUICtrlSetFont(-1, 12, 400, 0, "MS Sans Serif")
	$Username_Input = GUICtrlCreateInput("Username", 104, 184, 233, 21)
	$Password_Input = GUICtrlCreateInput("", 104, 224, 233, 21,$ES_PASSWORD)
	$OK = GUICtrlCreateButton("OK", 104, 280, 97, 25, 0)
	GUICtrlSetOnEvent(-1, '_Submit')
	$Cancel = GUICtrlCreateButton("Cancel", 242, 278, 97, 25, 0)
	GUICtrlSetOnEvent(-1, '_Cancel')
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###

EndFunc

Func _Submit()
	Local $Username, $Password, $Return
	$Username = GUICtrlRead($Username_Input)
	$Password = GUICtrlRead($Password_Input)
	If $Password = '' Then
		MsgBox(16,'Error','The password field is blank. Please type in a password.')
		Return -1
	EndIf
	;MsgBox(64,'Run',$RunUser&' - '&$Password)
	$Return = RunAs($RunUser, 'Seton', $Password, 0, @ScriptDir&'\DTS Client Interface.exe /'&$Username, @ScriptDir)
	;MsgBox(64,'@error',@error)
	If $Return = 0 Then
		MsgBox(16,'Error','The password provided is incorrect. Please try again.')
		Return -1
	EndIf
	;MsgBox(0,'Return Value',$Return)
	;MsgBox(0,'Input','Username: '&$Username&@CRLF&'Password: '&$Password);&@CRLF&@CRLF&'Password: '&GUICtrlRead($Password_Input))
	_Exit()
EndFunc

Func _Exit()
	GUIDelete($Agreement)
	Exit
EndFunc

Func _Cancel()
	GUIDelete($Down_Time_Client)
	Exit
EndFunc

