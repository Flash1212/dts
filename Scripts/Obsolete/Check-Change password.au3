;Client password check/change


#cs
Func _Password_Check()
	$Password = IniRead('..\Settings\Configuration.ini','FTP Client','PassWord','Not Found')
	$objUser = ObjGet("WinNT://" & $strWorkstationName & "/" & $UserName)
	$objUser.SetPassword($Password)
	$objUser.SetInfo
EndFunc   ;==>_Password_Change
#ce

$strWorkstationName = 'testsvr2003'
TCPStartup()
$strWorkstationIP = TCPNameToIP($strWorkstationName)
$UserName = 'FTP Test'
$Password = 'P@ssword'
$Existgroup = 0
; Init objects
	Const $ADS_UF_DONT_EXPIRE_PASSWD = 0X10000
	Const $ADS_UF_PASSWD_CANT_CHANGE = 0X40
	
	; Check if account exists .. if not create it
	$objUser = ObjGet("WinNT://" & $strWorkstationIP & "/" & $UserName)
	If @error Then
		;MsgBox(0,'Not Detected','No account was detected. Setting up now.')
		$colAccounts = ObjGet("WinNT://" & $strWorkstationName & "")
		$objUser = $colAccounts.Create("user", $UserName)
		$objUser.SetPassword($Password)
		$objUser.Put("Fullname", "FTP Account")
		$objUser.Put("Description", "FTP Transfer Account")
		$objUser.SetInfo
		;Set params
		$oldFlags = $objUser.Get("UserFlags")
		$newFlags = BitOR($oldFlags, $ADS_UF_DONT_EXPIRE_PASSWD)
		$objUser.Put("UserFlags", $newFlags) ;expire the password
		$objUser.SetInfo
		$oldFlags = $objUser.Get("UserFlags")
		$newFlags = BitOR($oldFlags, $ADS_UF_PASSWD_CANT_CHANGE)
		$objUser.Put("UserFlags", $newFlags) ;expire the password
		$objUser.SetInfo
	Else
		$ExistUser = 1 ;User already exist
		$objUser.SetPassword($Password)
		$objUser.SetInfo
		;MsgBox(0,'Detected','The account was detected. Skipping account creation.')
	EndIf
	; Read current settings and Bitor to ensure the "Don't expire password swith is on and user cannot change password"
	
	;Add User to users group**********************************************
	$objGroup = ObjGet("WinNT://" & $strWorkstationName & "/Users,group")
	
	For $objUsers In $objGroup.Members
		$User = ($objUsers.AdsPath)
		If $User = 'winNT://SETON/' & $strWorkstationName & '/' & $UserName Then $Existgroup = 1
	Next
	
	If $Existgroup = 0 Then $objGroup.Add($objUser.ADsPath)

