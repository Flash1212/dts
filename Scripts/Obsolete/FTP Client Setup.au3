
_Account_Setup('testsvr2003', 'FTP Transfer', 'P@ssword')
_Dir_Setup('testsvr2003', 'FTP Transfer')

Global $oMyError = ObjEvent("AutoIt.Error", "MyErrFunc"); Install a custom error handler

Func _Account_Setup($strComputer,$UserName, $Password)
	Local $oMyError, $objUser, $colAccounts, $oldFlags, $newFlags
	; Init objects
	Const $ADS_UF_DONT_EXPIRE_PASSWD = 0X10000
	Const $ADS_UF_PASSWD_CANT_CHANGE = 0X40

	; Check if account exists .. if not create it
	$objUser = ObjGet("WinNT://" & $strComputer & "/" & $UserName)
	If @error Then
		$colAccounts = ObjGet("WinNT://" & $strComputer & "")
		$objUser = $colAccounts.Create("user", $UserName)
		$objUser.SetPassword($Password)
		$objUser.Put("Fullname", "FTP Account")
		$objUser.Put("Description", "FTP Transfer Account")
		$objUser.SetInfo
	Else
		$ExistUser = 1
		MsgBox(0,'User','The user already exist.')
	EndIf

	; Read current settings and Bitor to ensure the "Don't expire password swith is on and user cannot change password"
	$oldFlags = $objUser.Get("UserFlags")
	$newFlags = BitOR($oldFlags, $ADS_UF_DONT_EXPIRE_PASSWD)
	$objUser.Put("UserFlags", $newFlags) ;expire the password
	$objUser.SetInfo
	$oldFlags = $objUser.Get("UserFlags")
	$newFlags = BitOR($oldFlags, $ADS_UF_PASSWD_CANT_CHANGE)
	$objUser.Put("UserFlags", $newFlags) ;expire the password
	$objUser.SetInfo
	;MsgBox(262144, 'Debug line ~' & @ScriptLineNumber, 'Selection:' & @LF & ' dont Password Expired');### Debug MSGBOX

	;Add User to admin group**********************************************
	$objGroup = ObjGet("WinNT://" & $strComputer & "/Users,group")
	For  $objUsers in $objGroup.Members
		$User = ($objUsers.AdsPath)
		If $User = 'winNT://SETON/'&$strComputer&'/FTP Transfer' Then $Existgroup = 1
	Next

	If $Existgroup = 1 Then
		MsgBox(0,'User Name','User is already apart of Users group.')
	Else
		$objGroup.Add($objUser.ADsPath)
	EndIf
	;*********************************************************************
EndFunc   ;==>_Account_Setup

Func _Dir_Setup($strComputer, $UserName)
	Local $acls
	DirCreate('\\'&$strComputer&'\c$\FTP Transfers\')
	DirCreate('\\'&$strComputer&'\c$\FTP Transfers\Logs')
	$acls = ('cacls.exe \\'&$strComputer&'\c$\FTP Transfers\ /E /T /C /G ' & $UserName & ':R')
	;MsgBox(0,'',$acls)
	Run(@ComSpec & " /C " & $acls, "", @SW_HIDE);_RunDOS($acls)
EndFunc   ;==>_Dir_Setup

Func MyErrFunc()
	Local $HexNumber
	$HexNumber = Hex($oMyError.number, 8)
	MsgBox(0, "", "We intercepted a COM Error !" & @CRLF & _
			"Number is: " & $HexNumber & @CRLF & _
			"Linenbr is: " & $oMyError.scriptline & @CRLF & _
			"Description is: " & $oMyError.description & @CRLF & _
			"Windescription is: " & $oMyError.windescription)

	SetError(1); something to check for when this function returns
EndFunc   ;==>MyErrFunc