#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_icon=Images\Download.ico
#AutoIt3Wrapper_UseX64=n
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

Opt('MustDeclareVars', 1)


#include <File.au3>
#include <Array.au3>
#include <WinAPI.au3>
#include <Constants.au3>
#include <GuiListView.au3>
#include <Sendmessage.au3>
#include <GuiImageList.au3>
#include <FileTypeIcon.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <ListViewConstants.au3>

#Region ### Global Variables
Global	$DTS_Client, _
		$ListFiles, _
		$Open, _
		$nMsg

Global	$Log = @ScriptDir&'\Logs\FTP Client.log', _
		$File_Location = @ScriptDir&'\Files\', _;'C:\Temp\Dest5\', _
		$Username = StringTrimLeft($CmdLineRaw, 1)
#EndRegion ### End Global Variables



#Region ### START Koda GUI section ### Form=\\austech\is-server\djthornton\Scripts\FTP Project\Scripts\DTS Client Interface 2.kxf
$DTS_Client = GUICreate("DTS Explorer - Running as "&@UserName, 640, 580,-1, -1, BitOR($WS_OVERLAPPEDWINDOW, $WS_CLIPSIBLINGS, $WS_CLIPCHILDREN))
GUISetIcon(@ScriptDir & '\Images\Download.ico')
GUISetBkColor('0xffffff', $DTS_Client)
$ListFiles = GUICtrlCreateListView("Name|Date Modified|Type|Size", 0, 0, 634, 574, $LVS_REPORT + $LVS_SHOWSELALWAYS, BitOR($LVS_EX_FULLROWSELECT, $LVS_EX_SUBITEMIMAGES))
;GUICtrlRegisterListViewSort(-1, '_Sort')
_GUICtrlListView_SetColumnWidth($ListFiles, 0, (3/7) * 640)
_GUICtrlListView_SetColumnWidth($ListFiles, 1, (1/3) * 367)
_GUICtrlListView_SetColumnWidth($ListFiles, 2, (1/3) * 367)
_GUICtrlListView_SetColumnWidth($ListFiles, 3, (1/3) * 367)
_List_Folder_Contents()
GUIRegisterMsg($WM_NOTIFY, "WM_NOTIFY")
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

#Region ### Loop & Message Get
While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			_FileWriteLog($Log, 'FTP Client has been shutdown.')
			_GUICtrlListView_RegisterSortCallBack($ListFiles)
			Exit
		Case $ListFiles
			_Sort()
	EndSwitch
WEnd
#EndRegion ### Loop & Message Get

#Region ### Code
Func _List_Folder_Contents()
	Local	$search, $x = 0, $file, $oFile, $iTem, $fTime, $hTIme, $fSize, $fType, $fIcon, $FullPath, _
			$tSHFILEINFO, $aDllRet, $sIconFile, $imageItem, $replace, $fSIcon, $iCount, $aPath[1]
	;$oLog = FileOpen($Log, 1)
	_FileWriteLog($Log, 'FTP Client has been initiated.')
	; Shows the filenames of all files in the current directory.
;~ 	MsgBox(64,'Files', $File_Location)
	$search = FileFindFirstFile($File_Location & '\*.*')

	; Check if the search was successful
	If $search = -1 Then
		MsgBox(16, "Error", "No files were found to open")
		_FileWriteLog($Log, "Error - No files were found to open")
		Exit
	EndIf

	While 1
		$file = FileFindNextFile($search)
		If @error Then ExitLoop
		$FullPath = $File_Location & $file
		ReDim $aPath[$x + 1]
		$aPath[$x] = $FullPath
		$fTime =  FileGetTime($FullPath, 0) ; creation Time
        $hTIme = $fTime[1] & "/" & $fTime[2] & "/" & $fTime[0] & " " & $fTime[3] & ":" & $fTime[4] & ":" & $fTime[5] ; detailed
		$fSize = FileGetSize($FullPath)
		$fType = _FileGetType($FullPath)
		If $fSize >= 1048576 Then
			$fSize = Round($fSize/1048576, 0)&' MB'
		Else
			$fSize = Round($fSize/1024, 0)&' KB'
		EndIf
		$iTem = GUICtrlCreateListViewItem($file&'|'&$hTIme&'|'&$fType&'|'&$fSize, $ListFiles)
		GUICtrlSetImage(-1, 'shell32.dll', '')
		$x = $x + 1
	WEnd
	GUISetState(@SW_SHOW)
	_GUICtrlListView_RegisterSortCallBack($ListFiles)
	$iCount = _GUICtrlListView_GetItemCount($ListFiles)
	$imageItem = _GUIImageList_Create(16, 16, 5)

	For $x = 0 To UBound($aPath) - 1
		$fIcon = _GetFileIcon($aPath[$x])
		_GUIImageList_ReplaceIcon($imageItem, -1, $fIcon )
		_WinAPI_DestroyIcon($fIcon)
	Next

	_GUICtrlListView_SetImageList($ListFiles, $imageItem, 1)
	; Close the search handle
	FileClose($search)
EndFunc   ;==>_List_Folder_Contents



Func _GetFileIcon($sPath, $bExt = 1, $iAttributes = 0)
    Static $tSHFILEINFO = DllStructCreate("ptr hIcon; int iIcon; DWORD dwAttributes; WCHAR szDisplayName[255]; WCHAR szTypeName[80];"), $p_tSHFILEINFO = DllStructGetPtr($tSHFILEINFO)
    Local $iFlags = BitOR(0x100, 0x1) ;$SHGFI_SMALLICON, $SHGFI_ICON
	Local $Result
    If $bExt Then $iFlags = BitOR($iFlags, 0x10);SHGFI_USEFILEATTRIBUTES

    Local $Ret = DllCall('shell32.dll', 'dword_ptr', 'SHGetFileInfoW', 'wstr', $sPath, 'dword', $iAttributes, 'ptr', $p_tSHFILEINFO, 'uint', DllStructGetSize($tSHFILEINFO), 'uint', $iFlags)
    If @error Then Return SetError(1, 0, 0)
    $Result = DllStructGetData($tSHFILEINFO, 'hIcon')

    Return $Result
EndFunc   ;==>__GetFileIcon

;~ ========================================================
;~ This thing is responcible for click events
;~ ========================================================
Func _Sort()
	_GUICtrlListView_SortItems($ListFiles, GUICtrlGetState($ListFiles))
EndFunc

;~ ========================================================
;~ This thing is responcible for click events
;~ ========================================================
Func _Open()
	Local	$Selected, $Selected_File
	$Selected = _GUICtrlListView_GetNextItem($ListFiles)
	If $Selected = -1 Then
		MsgBox(16, 'Error', 'You did not select a file. Please select a file and try again.')
	Else
		$Selected_File = _GUICtrlListView_GetItemText($ListFiles, $Selected)
		;MsgBox(0,'',$Selected_File)
		ShellExecute($File_Location & '\' & $Selected_File)
		_FileWriteLog($Log, $Selected_File& ' was opened by '&$Username&'.')
	EndIf
EndFunc   ;==>_Open

;~ ========================================================
;~ This thing is responcible for click events
;~ ========================================================
Func WM_NOTIFY($hWnd, $iMsg, $iwParam, $ilParam)
    Local $hWndFrom, $iCode, $tNMHDR, $hWndListView, $Index, $item

    $hWndListView = $ListFiles
    If Not IsHWnd($ListFiles) Then $hWndListView = GUICtrlGetHandle($ListFiles)

    $tNMHDR = DllStructCreate($tagNMHDR, $ilParam)
    $hWndFrom = HWnd(DllStructGetData($tNMHDR, "hWndFrom"))
    $iCode = DllStructGetData($tNMHDR, "Code")
    Switch $hWndFrom
        Case $hWndListView
            Switch $iCode
				Case $NM_DBLCLK  ; Sent by a list-view control when the user double-clicks an item with the left mouse button

					_Open()
					#cs
					Local $tInfo = DllStructCreate($tagNMITEMACTIVATE, $ilParam)
					$Index = _GUICtrlListView_GetSelectedIndices($ListFiles, True)
					If IsArray($Index) Then
						$item = StringSplit(_GUICtrlListView_GetItemTextString($ListFiles, $Index[1]),'|')
						$item = $item[1]

						MsgBox(64,'DB Click', $item )
					EndIf
					#ce
            EndSwitch
    EndSwitch
    Return $GUI_RUNDEFMSG
EndFunc   ;==>WM_NOTIFY
#EndRegion
