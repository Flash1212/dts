;FTP Client

Opt('MustDeclareVars', 1)

#include <GUIConstantsEx.au3>
#include <GuiListView.au3>
#include <File.au3>

Global	$FTP_CLient_Interface, _
		$Files, _
		$Open, _
		$nMsg

Global	$Log = @ScriptDir&'\Logs\FTP Client.log', _
		$File_Location = @ScriptDir&'\Files', _
		$Username = StringTrimLeft($CmdLineRaw, 1)
		
FileInstall('\\austech\is-si\Development\icons\compass.ico', @TempDir & '\compass.ico', 1)

#Region ### START Koda GUI section ### Form=C:\Documents and Settings\djthornton\My Documents\Projects\FTP Project\Client.kxf
$FTP_CLient_Interface = GUICreate("FTP Client", 300, 216, 704, 171)
GUISetIcon(@TempDir & '\compass.ico')
$Files = GUICtrlCreateListView("", 16, 20, 267, 145)
;GUICtrlSetData(-1, "File")
_GUICtrlListView_AddColumn($Files, 'Files', (267 / 2) - 18)
_GUICtrlListView_AddColumn($Files, 'Last Modification', (267 / 2) + 10)
$Open = GUICtrlCreateButton("Open File", 85.5, 175, 129, 25, 0)
_List_Folder_Contents()
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			_FileWriteLog($Log, 'FTP Client has been shutdown.')
			Exit
		Case $Open
			_Open()
	EndSwitch
WEnd

Func _List_Folder_Contents()
	Local	$search, $x, $file, $oFile
	;$oLog = FileOpen($Log, 1)
	_FileWriteLog($Log, 'FTP Client has been initiated.')
	; Shows the filenames of all files in the current directory.
	$search = FileFindFirstFile($File_Location & '\*.*')

	; Check if the search was successful
	If $search = -1 Then
		MsgBox(16, "Error", "No files were found to open")
		_FileWriteLog($Log, "Error - No files were found to open")
		Exit
	EndIf

	$x = 0

	While 1
		$file = FileFindNextFile($search)
		If @error Then ExitLoop
		_GUICtrlListView_AddItem($Files, $file)
		_GUICtrlListView_AddSubItem($Files, $x, _Time_Stamp(FileGetTime($File_Location & '\' & $file, 0, 0)), 1)
		$x = $x + 1
	WEnd
	;FileWriteLine($oLog, $x & ' File(s) were opened by '&$Username&'.'&@CRLF&@TAB&)
	; Close the search handle
	FileClose($search)
	;FileClose($oLog)
EndFunc   ;==>_List_Folder_Contents

Func _Open()
	Local	$Selected, $Selected_File
	$Selected = _GUICtrlListView_GetNextItem($Files)
	If $Selected = -1 Then
		MsgBox(16, 'Error', 'You did not select a file. Please select a file and try again.')
	Else
		$Selected_File = _GUICtrlListView_GetItemText($Files, $Selected)
		;MsgBox(0,'',$Selected_File)
		ShellExecute($File_Location & '\' & $Selected_File)
		_FileWriteLog($Log, $Selected_File& ' was opened by '&$Username&'.')
	EndIf
EndFunc   ;==>_Open

Func _Time_Stamp($T)
	Local $M, $TY, $TM, $TD, $TH, $TMin, $TSec, $T_Stamp
	;_ArrayDisplay($T)
	$M = ''
	$TY = $T[0]
	$TM = $T[1]
	$TD = $T[2]
	If $T[3] = 12 Then
		$TH = $T[3]
		$M = 'pm'
	ElseIf $T[3] > 11 Then
		$TH = $T[3] - 12
		$M = 'pm'
	Else
		$TH = $T[3]
		$M = 'am'
	EndIf
	$TMin = $T[4]
	$TSec = $T[5]
	$T_Stamp = $TH & ':' & $TMin & ':' & $TSec & ' ' & $M & ' - ' & $TM & '\' & $TD & '\' & $TY
	Return $T_Stamp
	;MsgBox(0, '', $ST_Stamp)
EndFunc   ;==>_Time_Stamp


