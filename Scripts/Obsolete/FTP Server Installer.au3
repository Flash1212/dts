;Installer

;This programs function is to install the FTP DTF Server Side application
Opt('MustDeclareVars', 1)
;Opt("GUIOnEventMode", 1)
#Region FileInstalls
FileInstall('\\austech\is-si\Development\icons\compass.ico',@TempDir&'\compass.ico',1)
FileInstall('\\austech\IS-Server\djthornton\Scripts\FTP Project\Images\Seton FTP1.jpg',@TempDir&'\FTP1.jpg',1)
FileInstall('\\austech\IS-Server\djthornton\Scripts\FTP Project\Images\Seton FTP2.jpg',@TempDir&'\FTP2.jpg',1)
FileInstall('\\austech\IS-Server\djthornton\Scripts\FTP Project\Images\Seton FTP3.jpg',@TempDir&'\FTP3.jpg',1)
FileInstall('\\austech\IS-Server\djthornton\Scripts\FTP Project\Images\Seton FTP4.jpg',@TempDir&'\FTP4.jpg',1)
FileInstall('\\austech\IS-Server\djthornton\Scripts\FTP Project\Images\Seton FTP5.jpg',@TempDir&'\FTP5.jpg',1)
FileInstall('\\austech\IS-Server\djthornton\Scripts\FTP Project\Images\Seton FTP6.jpg',@TempDir&'\FTP6.jpg',1)
#EndRegion FileInstalls

#Region Includes
;#include <AD.au3>
#include <File.au3>
#include <Array.au3>
#include <Services.au3>
#Include <GuiListView.au3>
#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#EndRegion

#Region Globals
;GUI Globals
Global    $Server_Installer, $Logo, $Solid, $Title,  $Body, $Back, $Next, $Finished, $Cancel, $NetworkToNertwork, $NetworkToDesktop, $Install_Location, _
        $Search, $Type, $Dir, $instance, $Username, $Password1, $Password2, $Sleep, $instance_Label, $Username_Label, _
        $Password_Label, $Sleep_Label, $Progress, $Progress_Info, $Installation, $Uninstall, $ServiceList

;Misc Globals
Global    $nMsg, $oMyError = ObjEvent("AutoIt.Error", "_ADDoError") ; Install a custom error handler
#EndRegion

#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\FTP Project\Scripts\Server Installer.kxf
$Server_Installer = GUICreate("FTP Server Installer", 500, 444, 315, 173, Default, $WS_EX_ACCEPTFILES)
GUISetBkColor(0xFAFAFA)
$Logo = GUICtrlCreatePic(@TempDir&'\FTP1.jpg', 0, 0, 500, 100, BitOR($SS_NOTIFY,$WS_GROUP,$WS_CLIPSIBLINGS))
$Title = GUICtrlCreateLabel('', 8, 120, 250, 25)
$Body  = GUICtrlCreateLabel("", 8, 170, 492, 50)
$Install_Location = GUICtrlCreateInput(@ProgramFilesDir, 8, 230, 281, 21)
$instance_Label = GUICtrlCreateLabel('Instance Name (3 character minimum)', 8, 217)
$instance = GUICtrlCreateInput('', 8, 230, 100, 21)
$Username_Label = GUICtrlCreateLabel('FTP Transfer Account Name (Seton Active Directory Account)', 8, 259)
$Username = GUICtrlCreateInput('', 8, 272, 100, 21)
$Password_Label = GUICtrlCreateLabel('Password                    Verify Password  (Must be Complex)', 8, 302)
$Password1 = GUICtrlCreateInput('', 8, 315, 100, 21)
$Password2 = GUICtrlCreateInput('', 115, 315, 100, 21)
$Sleep_Label = GUICtrlCreateLabel('File Check Interval (in minutes)', 8, 343)
$Sleep = GUICtrlCreateInput('', 8, 356, 100, 21)
$Progress = GUICtrlCreateProgress(85.5, 272, 329, 25)
$Progress_Info = GUICtrlCreateLabel("", 88, 256, 250, 12)
$Installation = GUICtrlCreateRadio('Install', 8, 240, 250, 30 )
$Uninstall = GUICtrlCreateRadio('Uninstall', 8, 280, 200, 30 )
$ServiceList = GUICtrlCreateListView("", 32, 224, 441, 153)
GUICtrlSetState(-1, $GUI_DROPACCEPTED)
$Search = GUICtrlCreateButton("...", 296, 228, 24, 24, 0)
$NetworkToNertwork = GUICtrlCreateRadio('Network Share To Network Share Transfers', 8, 220, 250, 30 )
$NetworkToDesktop = GUICtrlCreateRadio('Network Share To Desktop Computer', 8, 260, 200, 30 )
$Back = GUICtrlCreateButton("Back", 128, 408, 89, 25, 0)
$Next = GUICtrlCreateButton("Next", 248, 408, 89, 25, 0)
$Cancel= GUICtrlCreateButton("Cancel", 384, 408, 89, 25, 0)
$Finished = GUICtrlCreateButton("Finished", 384, 408, 89, 25, 0)

_Welcome_Page()
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

While 1
    $nMsg = GUIGetMsg()
    Switch $nMsg
        Case $GUI_EVENT_CLOSE
            _Cancel()
        Case $Next
            _Step_Forward()
        Case $Back
            _Step_Back()
        Case $Cancel
            _Cancel()
        Case $NetworkToNertwork
            $Type = 'NTN'
        Case $NetworkToDesktop
            $Type = 'NTD'
        Case $Search
            $Dir = FileSelectFolder('Installation Location', '',7)
            If @error Then
                ;Do Nothing
                $Dir = GUICtrlRead($Install_Location)
            Else
                If Not FileExists($Dir) Then
                    MsgBox(16,'Error','The location selected is not a valid installation location. Please select a different '& _
                                'location or use the default.')
                Else
                    GUICtrlSetData($Install_Location, $Dir)
                EndIf
            EndIf
        Case $Finished
            GUIDelete($Server_Installer)
            FileDelete(@TempDir&'\FTP*.jpg')
            Exit
    EndSwitch
WEnd

Func _Welcome_Page()
    GUICtrlSetData($Title, 'Welcome!')
    GUICtrlSetFont($Title, 16, 600)
    GUICtrlSetData($Body,    "This application will install/uninstall Seton's FTP Server Services on this device. "& _
                            'If you wish to continue please select which proceedure you want and click "Next" else "Cancel".')
    GUICtrlSetFont($Body, 10, 500)
    GUICtrlSetImage($Logo, @TempDir&'\FTP1.jpg')
    GUICtrlSetState($Installation, $GUI_SHOW)
    GUICtrlSetState($Uninstall, $GUI_SHOW)
    GUICtrlSetState($Back, $GUI_HIDE)
    GUICtrlSetState($Finished, $GUI_HIDE)
    GUICtrlSetState($NetworkToNertwork, $GUI_HIDE)
    GUICtrlSetState($NetworkToDesktop, $GUI_HIDE)
    GUICtrlSetState($Install_Location, $GUI_HIDE)
    GUICtrlSetState($Search, $GUI_HIDE)
    GUICtrlSetState($instance, $GUI_HIDE)
    GUICtrlSetState($Username, $GUI_HIDE)
    GUICtrlSetState($Password1, $GUI_HIDE)
    GUICtrlSetState($Password2, $GUI_HIDE)
    GUICtrlSetState($Sleep, $GUI_HIDE)
    GUICtrlSetState($instance_Label, $GUI_HIDE)
    GUICtrlSetState($Username_Label, $GUI_HIDE)
    GUICtrlSetState($Password_Label, $GUI_HIDE)
    GUICtrlSetState($Sleep_Label, $GUI_HIDE)
    GUICtrlSetState($Progress, $GUI_HIDE)
    GUICtrlSetState($Progress_Info, $GUI_HIDE)
    GUICtrlSetState($ServiceList, $GUI_HIDE)

EndFunc

Func _System_Type()
    GUICtrlSetData($Title, 'System Type')
    GUICtrlSetFont($Title, 16, 600)
    GUICtrlSetData($Body, 'Please select the type of system this will be.')
    GUICtrlSetFont($Body, 10, 450)
    GUICtrlSetImage($Logo, @TempDir&'\FTP2.jpg')
    GUICtrlSetState($Back, $GUI_SHOW)
    GUICtrlSetState($NetworkToNertwork, $GUI_SHOW)
    GUICtrlSetState($NetworkToDesktop, $GUI_SHOW)
    GUICtrlSetState($Install_Location, $GUI_HIDE)
    GUICtrlSetState($Search, $GUI_HIDE)
    GUICtrlSetState($Installation, $GUI_HIDE)
    GUICtrlSetState($Uninstall, $GUI_HIDE)
EndFunc

Func _System_Installation_Location()
    GUICtrlSetData($Title, 'Installation Location')
    GUICtrlSetFont($Title, 16, 600)
    GUICtrlSetData($Body, 'Please select the location to install Server Components.')
    GUICtrlSetFont($Body, 10, 450)
    GUICtrlSetImage($Logo, @TempDir&'\FTP3.jpg')
    GUICtrlSetState($NetworkToNertwork, $GUI_HIDE)
    GUICtrlSetState($NetworkToDesktop, $GUI_HIDE)
    GUICtrlSetState($instance, $GUI_HIDE)
    GUICtrlSetState($Username, $GUI_HIDE)
    GUICtrlSetState($Password1, $GUI_HIDE)
    GUICtrlSetState($Password2, $GUI_HIDE)
    GUICtrlSetState($Sleep, $GUI_HIDE)
    GUICtrlSetState($instance_Label, $GUI_HIDE)
    GUICtrlSetState($Username_Label, $GUI_HIDE)
    GUICtrlSetState($Password_Label, $GUI_HIDE)
    GUICtrlSetState($Sleep_Label, $GUI_HIDE)
    GUICtrlSetState($Install_Location, $GUI_Show)
    GUICtrlSetState($Search, $GUI_Show)
EndFunc

Func _Configurations()
    GUICtrlSetData($Title, 'Configurations')
    GUICtrlSetFont($Title, 16, 600)
    GUICtrlSetImage($Logo, @TempDir&'\FTP4.jpg')
    If FileExists(@ScriptDir&'\Configuration.ini') Then
        GUICtrlSetData($Body, 'Pleae create an instance name for this FTP Server Service.'&@CRLF&@CRLF&'e.g., Downtime; '& _
                                'IS-Security')
    Else
        GUICtrlSetData($Body, 'Please create an instance name for this FTP Server Service and fill in the Server settings .'& _
                                ' e.g., Downtime; IS-Security')
        GUICtrlSetState($Username, $GUI_Show)
        GUICtrlSetState($Username_Label, $GUI_Show)
        GUICtrlSetState($Password_Label, $GUI_Show)
        GUICtrlSetState($Password1, $GUI_Show)
        GUICtrlSetState($Password2, $GUI_Show)
        GUICtrlSetState($Sleep, $GUI_Show)
        GUICtrlSetState($Sleep_Label, $GUI_Show)
    EndIf
    GUICtrlSetFont($Body, 10, 450)
    GUICtrlSetData($Next, 'Next')
    GUICtrlSetState($instance, $GUI_Show)
    GUICtrlSetState($instance_Label, $GUI_Show)
    GUICtrlSetState($Install_Location, $GUI_HIDE)
    GUICtrlSetState($Search, $GUI_HIDE)

EndFunc

Func _Install_Ready()
    GUICtrlSetData($Title, 'Installation Ready')
    GUICtrlSetFont($Title, 16, 600)
    GUICtrlSetData($Body, 'All requeired information has been gathered. You are ready to install the FTP Server Service.'& _
                            @CRLF&@CRLF&'Click "Install" to complete installation.')
    GUICtrlSetFont($Body, 10, 450)
    GUICtrlSetImage($Logo, @TempDir&'\FTP5.jpg')
    GUICtrlSetData($Next, 'Install')
    GUICtrlSetState($instance, $GUI_HIDE)
    GUICtrlSetState($Username, $GUI_HIDE)
    GUICtrlSetState($Password1, $GUI_HIDE)
    GUICtrlSetState($Password2, $GUI_HIDE)
    GUICtrlSetState($Sleep, $GUI_HIDE)
    GUICtrlSetState($instance_Label, $GUI_HIDE)
    GUICtrlSetState($Username_Label, $GUI_HIDE)
    GUICtrlSetState($Password_Label, $GUI_HIDE)
    GUICtrlSetState($Sleep_Label, $GUI_HIDE)
EndFunc

Func _Uninstall()
    Local    $aServices, $aTemp[1], $x
    GUICtrlSetState($Installation, $GUI_HIDE)
    GUICtrlSetState($Uninstall, $GUI_HIDE)
    GUICtrlSetState($ServiceList, $GUI_SHOW)
    GUICtrlSetData($Title, 'Uninstall')
    GUICtrlSetFont($Title, 16, 600)
    GUICtrlSetData($Body,    'Please select the Instance(s) you want to uninstall from this device.')
    GUICtrlSetFont($Body, 10, 450)
    _GUICtrlListView_AddColumn($ServiceList,'FTP Services', 440)
    $aServices = _Services_ListInstalled()
    ;_ArrayDisplay($aServices)
    $x = 0
    For $Service In $aServices
        If StringLeft($Service, 3) == 'FTP' Then
            _GUICtrlListView_AddItem($ServiceList, $Service)
        EndIf
    Next
    If _GUICtrlListView_GetItemCount($ServiceList) = 0 Then
        MsgBox(48, 'No Installs','The are no installations of FTP Server Services detected.')
        _Finished(1)
    EndIf
    ;_ArrayDisplay($aTemp)
    ;_GUICtrlListView_AddItem($ServiceList, $aTemp)
    ;MsgBox(64,'','look')
EndFunc

Func _Finished($Remove = 0)
    GUICtrlSetData($Title, 'Finished')
    GUICtrlSetFont($Title, 16, 600)
    If $Remove = 1 Then
        GUICtrlSetData($Body,    'The removal of FTP Server Services is complete.')
        GUICtrlSetFont($Body, 10, 450)
    Else
        GUICtrlSetData($Body,    'The installation of the FTP Server Service is complete.')
        GUICtrlSetFont($Body, 10, 450)
    EndIf
    GUICtrlSetImage($Logo, @TempDir&'\FTP6.jpg')
    GUICtrlSetState($Cancel, $GUI_HIDE)
    GUICtrlSetState($Back, $GUI_HIDE)
    GUICtrlSetState($Next, $GUI_HIDE)
    GUICtrlSetState($Finished, $GUI_SHOW)
    GUICtrlSetState($Install_Location, $GUI_HIDE)
    GUICtrlSetState($Search, $GUI_HIDE)
    GUICtrlSetState($Progress, $GUI_HIDE)
    GUICtrlSetState($Progress_Info, $GUI_HIDE)
    GUICtrlSetState($ServiceList, $GUI_HIDE)

EndFunc

Func _Step_Forward()
    Local    $Place, $Return, $err, $Selected_Services, $String, $Verify, $Count, $z
    $Place = GUICtrlRead($Title)
    Switch $Place
        Case 'Welcome!'
            If BitAND(GUICtrlRead($Installation), $GUI_CHECKED) = 0 And BitAND(GUICtrlRead($Uninstall), $GUI_CHECKED) = 0 Then
                MsgBox(16,'Error', 'You must first choose a proceedure before proceeding.')
            ElseIf BitAND(GUICtrlRead($Installation), $GUI_CHECKED) = 1 Then
                _System_Type()
            ElseIf BitAND(GUICtrlRead($Uninstall), $GUI_CHECKED) = 1 Then
                _Uninstall()
            EndIf
        Case 'System Type'
            If BitAND(GUICtrlRead($NetworkToNertwork), $GUI_CHECKED) = 0 And BitAND(GUICtrlRead($NetworkToDesktop), $GUI_CHECKED) = 0 Then
                MsgBox(16,'Error', 'You must first choose an FTP Service type before proceeding.')
            Else
                _System_Installation_Location()
            EndIf
        Case 'Installation Location'
            $Dir = GUICtrlRead($Install_Location)
            If FileExists($Dir) Then
                _Configurations()
            Else
                MsgBox(16,'Error','The location selected is not a valid installation location. Please select a different '& _
                                    'location or use the default.')
            EndIf
        Case 'Configurations'
            If _Service_Exists('FTP'&GUICtrlRead($instance)) = True Or _
                FileExists($Dir&'\FTP Transfer '&StringUpper(GUICtrlRead($instance))) Or _
                StringLen(GUICtrlRead($instance)) < 3 Then
                MsgBox(16,'Error','The instance name you selected is already in use, '& _
                                    'or does not meet the 3 or more character requirement. Please select a different name')
            Else
                If _ADObjectExists(GUICtrlRead($Username)) = 0 Then
                    MsgBox(16,'Error','The account name used could not be found in AD. Please check the account name and '& _
                                        'try again.')
                Else
                    If GUICtrlRead($Password1) == GUICtrlRead($Password2) Then
                        If _IsComplex(GUICtrlRead($Password1)) = 1 Then
                            If Not StringIsAlNum(GUICtrlRead($Sleep)) > 0 Then
                                MsgBox(16,'Error','The Check Interval time must be a whole number greater than 0. Please try again.')
                            Else
                                _Install_Ready()
                            EndIf
                        ElseIf StringLen(GUICtrlRead($Password1)) = 0 And StringLen(GUICtrlRead($Password2)) = 0 Then
                            MsgBox(16,'Error','Please enter a complex password.')
                        Else
                            MsgBox(16,'Error','The Password must be a complex password atleast 8 characters in length. '& _
                                                'Please try again.')
                        EndIf
                    Else
                        MsgBox(16,'Error','The passwords provided do not match. Please try again.')
                    EndIf
                EndIf
            EndIf
        Case 'Installation Ready'
            GUISetState($Server_Installer, @SW_DISABLE)
            $Return = _Install()
            If @error = -1 Then
                GUICtrlSetData($Progress, 0)
                MsgBox(16,'Error','There was an error during installation. Please check the logs.'&@CRLF&@CRLF&'Reverting '& _
                                    'system back to previous state.'&@CRLF&@CRLF&'Error: '&$Return)
                _Undo()
                Exit
            EndIf
            GUICtrlSetData($Progress, 100)
            Sleep(2000)
            GUISetState($Server_Installer, @SW_ENABLE)
            _Finished()
        Case 'Uninstall'
            $Selected_Services = _Get_Selected()
			If $Selected_Services = 0 Then
				MsgBox(16,'Error','No Services were selected. Please try again.')
			Else
				For $Service In $Selected_Services
					$String = $String&$Service&@CRLF
				Next
				$Verify = MsgBox(36,'Remove','The following services were chosen for removal:'&@CRLF&@CRLF&$String&@CRLF&'Are you sure you want to remove these services?')
				If $Verify = 6 Then
					For $Service In $Selected_Services
						_Undo($Service)
						$z = $z + 1
					Next
				EndIf
				_Finished(1)
			EndIf
        Case 'Finished'
            GUIDelete($Server_Installer)
            Exit
    EndSwitch
EndFunc

Func _Step_Back()
    Local    $Place
    $Place = GUICtrlRead($Title)
    Switch $Place
        Case 'Installation Ready'
            _Configurations()
        Case 'Configurations'
            _System_Installation_Location()
        Case 'Installation Location'
            _System_Type()
        Case 'System Type'
            _Welcome_Page()
    EndSwitch
EndFunc

Func _Cancel()
    Local    $Place, $Verify
    $Place = GUICtrlRead($Title)
    ;MsgBox(64,'Place', $Place)
    If $Place = 'Finished' Then
        Exit
    Else
        GUISetState(@SW_DISABLE)
        $Verify = MsgBox(262180,'Abort?','You have not completed the istallation. If you quite now no changes will be saved.'&@CRLF&@CRLF& _
                            'Are you sure you want to quite?')
        If $Verify = 6 Then
            GUIDelete($Server_Installer)
            Exit
        EndIf
        GUISetState(@SW_ENABLE)
    EndIf
EndFunc

Func _Install()
    Local    $Create, $Install, $Line, $Install_Service, $oFile
    GUICtrlSetState($Progress, $GUI_Show)
    GUICtrlSetState($Progress_Info, $GUI_SHOW)
    GUICtrlSetData($Progress_Info,'Installing FTP Server Services')
   ; MsgBox(64,'','')
    $Dir = $Dir&'\FTP Transfer '&StringUpper(GUICtrlRead($instance))
    ;MsgBox(64,'Dir',$dir)
    $Create = DirCreate($Dir&'\Logs')
    GUICtrlSetData($Progress_Info,'Creating '&$Dir&'\Logs')
    GUICtrlSetData($Progress, 8)
    If $Create = 0 Then
        SetError(-1)
        Return 'Logs'
    EndIf
    $Create = DirCreate($Dir&'\Computers\Backups')
    GUICtrlSetData($Progress_Info,'Creating '&$Dir&'\Computers\Backups')
    GUICtrlSetData($Progress, 16)
    If $Create = 0 Then
        SetError(-1)
        Return 'Backups'
    EndIf
    $Create = DirCreate($Dir&'\Settings')
    GUICtrlSetData($Progress_Info,'Creating '&$Dir&'\Settings')
    GUICtrlSetData($Progress, 24)
    If $Create = 0 Then
        SetError(-1)
        Return 'Settings'
    EndIf
	$Create = DirCreate($Dir&'\Client Executables')
    GUICtrlSetData($Progress_Info,'Creating '&$Dir&'\Client Executables')
    GUICtrlSetData($Progress, 24)
    If $Create = 0 Then
        SetError(-1)
        Return 'Client Executables'
    EndIf
    $Install = FileInstall('\\austech\IS-Server\djthornton\Scripts\FTP Project\Scripts\DTS Server.exe', $Dir&'\',1)
    GUICtrlSetData($Progress_Info,'Unpackaging FTP Server.exe to '&$Dir&'\FTP Server.exe')
    GUICtrlSetData($Progress, 32)
    If $Install = 0 Then
        SetError(-1)
        Return 'FTP Server.exe'
    EndIf
    $Install = FileInstall('\\austech\IS-Server\djthornton\Scripts\FTP Project\Accessories\srvany.exe', $Dir&'\', 1)
    GUICtrlSetData($Progress_Info,'Unpackaging srvany.exe to '&$Dir&'\srvany.exe')
    GUICtrlSetData($Progress, 40)
    If $Install = 0 Then
        SetError(-1)
        Return 'srvany.exe'
    EndIf
	    $Install = FileInstall('\\austech\IS-Server\djthornton\Scripts\FTP Project\Scripts\DTS Client Verfication.exe', $Dir&'\Client Executables\', 1)
    GUICtrlSetData($Progress_Info,'Unpackaging DTS Client Verfication.exe to '&$Dir&'\Client Executables\DTS Client Verfication.exe')
    GUICtrlSetData($Progress, 40)
    If $Install = 0 Then
        SetError(-1)
        Return 'srvany.exe'
    EndIf
	    $Install = FileInstall('\\austech\IS-Server\djthornton\Scripts\FTP Project\Scripts\DTS Client Interface.exe', $Dir&'\Client Executables\', 1)
    GUICtrlSetData($Progress_Info,'Unpackaging DTS Client Interface.exe to '&$Dir&'\Client Executables\DTS Client Interface.exe')
    GUICtrlSetData($Progress, 40)
    If $Install = 0 Then
        SetError(-1)
        Return 'srvany.exe'
    EndIf
    ;Server Settings for Client
    $oFile = FileOpen($dir&'\Settings\Configurations.ini', 1)
    GUICtrlSetData($Progress_Info,'Creating Configuration.ini file.')
    GUICtrlSetData($Progress, 48)
    If $oFile = -1 Then
        SetError(-1)
        Return 'Config File'
    EndIf
    $Line = FileWriteLine($oFile, '[FTP Client]')
    If $Line = 0 Then
        SetError(-1)
        Return 'Config File'
    EndIf
    $Line = FileWriteLine($oFile, 'Username='&GUICtrlRead($Username))
    If $Line = 0 Then
        SetError(-1)
        Return 'Config File'
    EndIf
    $Line = FileWriteLine($oFile, 'PassWord='&GUICtrlRead($Password1))
    If $Line = 0 Then
        SetError(-1)
        Return 'Config File'
    EndIf
    $Line = FileWriteLine($oFile, 'Install_Location=FTP Transfer '&GUICtrlRead($instance))
    If $Line = 0 Then
        SetError(-1)
        Return 'Config File'
    EndIf
    $Line = FileWriteLine($oFile, 'Client_Executables='&@ComputerName)
    If $Line = 0 Then
        SetError(-1)
        Return 'Config File'
    EndIf
    $Line = FileWriteLine($oFile, 'Sleep='&GUICtrlRead($Sleep))
    If $Line = 0 Then
        SetError(-1)
        Return 'Config File'
    EndIf
    ;Interface Settings
    $Line = FileWriteLine($oFile, @CRLF)
    If $Line = 0 Then
        SetError(-1)
        Return 'Config File'
    EndIf
    $Line = FileWriteLine($oFile, '[FTP Interface]')
    If $Line = 0 Then
        SetError(-1)
        Return 'Config File'
    EndIf
    $Line = FileWriteLine($oFile, 'Local_Install="C:\Program Files\FTP Interface '&GUICtrlRead($instance)&'"')
    If $Line = 0 Then
        SetError(-1)
        Return 'Config File'
    EndIf
    $Line = FileWriteLine($oFile, 'Workstations=Computers.csv')
    If $Line = 0 Then
        SetError(-1)
        Return 'Config File'
    EndIf
    FileClose($oFile)

    $Create = _FileCreate($Dir&'\Computers\Computers.csv')
    GUICtrlSetData($Progress_Info,'Creating Computers.csv')
    GUICtrlSetData($Progress, 56)
    If $Create = 0 Then
        SetError(-1)
        Return 'Computers.csv'
    EndIf
    $Create = _FileCreate($Dir&'\Logs\Clients.log')
    GUICtrlSetData($Progress_Info,'Creating Clients.log')
    GUICtrlSetData($Progress, 64)
    If $Create = 0 Then
        SetError(-1)
        Return 'Clients.log'
    EndIf
    $Create = _FileCreate($Dir&'\Logs\Transfer Service Error.log')
    GUICtrlSetData($Progress_Info,'Creating Transfer Service Error.log')
    GUICtrlSetData($Progress, 72)
    If $Create = 0 Then
        SetError(-1)
        Return 'Transfer Service Error.log'
    EndIf
    $Create = _FileCreate($Dir&'\Logs\Client Error.log')
    GUICtrlSetData($Progress_Info,'Creating Client Error.log')
    GUICtrlSetData($Progress, 80)
    If $Create = 0 Then
        SetError(-1)
        Return 'Client Error.log'
    EndIf
    $Create = _FileCreate($Dir&'\Logs\Error Count.log')
    GUICtrlSetData($Progress_Info,'Creating Error Count.log')
    GUICtrlSetData($Progress, 88)
    If $Create = 0 Then
        SetError(-1)
        Return 'Error Count.log'
    EndIf

    $Install_Service = _Install_Service()
    GUICtrlSetData($Progress_Info,'Creating Windows Service for FTP Server Services')
    If $Install_Service = 1 Then
        GUICtrlSetData($Progress, 95)
        Return 1
    Else
        SetError(-1)
        GUICtrlSetData($Progress, 95)
        Return $Install_Service
    EndIf
EndFunc

Func _Undo($Service = '')
    Local    $Path, $RMDir, $SearchDir, $File
    If $Service = '' Then
        _Service_Delete('FTP'&GUICtrlRead($instance))
		$SearchDir = FileFindFirstFile($Dir&'\*')
		If $SearchDir = -1 Then
			MsgBox(0, "Error", "No files/directories matched the search pattern")
			;Exit
		Else
			While 1
				$File = FileFindNextFile($SearchDir)
				If @error Then ExitLoop
				FileDelete($File)
			WEnd
			If $Dir <> @ProgramFilesDir Then DirRemove($Dir, 1)
		EndIf
    Else
        $Path = _Service_GetPath($Service)
		MsgBox(64,'Path',$Path)
        $Path = StringReplace($Path,'\srvany.exe','')
        MsgBox(64,'Removing Service Dir',$Path)
		_Service_Delete($Service)
		$Path = StringReplace($Path, '"','')
		$SearchDir = FileFindFirstFile($Path)
		If $SearchDir = -1 Then
			MsgBox(0, "Error", "No files/directories matched the search pattern: "&$Path)
			;Exit
		Else
			While 1
				$File = FileFindNextFile($SearchDir)
				If @error Then ExitLoop
				FileDelete($File)
			WEnd
		EndIf
        $RMDir = DirRemove($Path, 1)
		MsgBox(64,'Remove Dir', $RMDir)
    EndIf
EndFunc

Func _Get_Selected()
	;$ServiceList
	Local	$rows, $Selected, $Selected_Services[1], $k, $Name
	$rows = _GUICtrlListView_GetItemCount($ServiceList)
	$k = 0
	For $x = 0 To $rows - 1
		;	MsgBox(64,'x',$x)
		$Selected = _GUICtrlListView_GetItemSelected($ServiceList, $x)
		;	MsgBox(64,'Selected',$Selected)
		If $Selected = 'True' Then
			ReDim $Selected_Services[$k + 1]
			;	_ArrayDisplay($Selected_Services)
			$Name = _GUICtrlListView_GetItemText($ServiceList, $x)
			$Selected_Services[$k] = $Name ;Index
			$k = $k + 1
		EndIf
	Next
	;_ArrayDisplay($Selected_Services)
	If $Selected_Services[0] = '' Then
		Return 0
	Else
		Return $Selected_Services
	EndIf
EndFunc

Func _ADObjectExists($object)
    Local    $objConnection, $objRootDSE, $strHostServer, $strDNSDomain, $strQuery, $objRecordSet
    $objConnection = ObjCreate("ADODB.Connection") ; Create COM object to AD
    $objConnection.ConnectionString = "Provider=ADsDSOObject"
    $objConnection.Open("Active Directory Provider")

    $objRootDSE = ObjGet("LDAP://RootDSE")
    $strHostServer = $objRootDSE.Get("dnsHostName")
    $strDNSDomain = $objRootDSE.Get("defaultNamingContext")

    $strQuery = "<LDAP://" & $strHostServer & "/" & $strDNSDomain & ">;(sAMAccountName=" & $object & ");ADsPath;subtree"
    $objRecordSet = $objConnection.Execute($strQuery) ; Retrieve the FQDN for the group, if it exists

    If Not IsObj($objRecordSet) Then ;Return -1
        MsgBox(16,'Error','Unable to bind to AD for account name check.')
        Exit
    EndIf

    If $objRecordSet.RecordCount = 1 Then
        $objRecordSet = 0
        Return 1
    Else
        $objRecordSet = 0
        Return 0
    EndIf
EndFunc   ;==>_ADObjectExists

Func _Install_Service()
    Local    $objWMIService, $errReturn, $objService, $Server = '.', $SrvAny = $dir&'\srvany.exe'
    Const $OWN_PROCESS = 0x00000010, _;16 is own process
            $INTERACTIVE = False, _ ;True changes the $Own_Process to 272 is interact with desktop
            $NORMAL_ERROR_CONTROL = 1

    $objWMIService = ObjGet("winmgmts:" & "{impersonationLevel=impersonate}!\\" & $Server & "\root\cimv2")
    If Not IsObj($objWMIService) Then Return 'Server Service Object Not Found'
    $objService = $objWMIService.Get("Win32_BaseService")
    $errReturn = $objService.Create('FTP'&GUICtrlRead($instance), 'FTP Transfer '&GUICtrlRead($instance), _
                (Chr(34) & $SrvAny & Chr(34)), $OWN_PROCESS, $NORMAL_ERROR_CONTROL, _
                "Automatic", $INTERACTIVE, "SETON\"&GUICtrlRead($Username), 'P@ssword12')
    If $errReturn > 0 Then Return 'Service Creation Failed'
    Return 1
EndFunc   ;==>_Install_Service

Func _Service_GetPath($sServiceName, $Computer = "localhost")
  If (NOT _Service_Exists($sServiceName)) Then Return SetError(1)
  _sServErrorHandlerRegister()
  Local $Service = ObjGet("winmgmts:\\" & $Computer & "\root\cimv2")
  Local $sQuery = "Select * from Win32_Service where name like '"& $sServiceName & "'"
  Local $sItems = $Service.ExecQuery ($sQuery)
  _sServErrorHandlerDeRegister()
  For $objService In $sItems
    Return $objService.PathName
  Next
EndFunc   ;<==> _Service_GetFilePath()

Func _IsComplex($Password)
    Local    $Strength = 0, $Return

    ;Check Length
    If StringLen($Password) >= 8 Then
        $Strength = $Strength + 1
        ;MsgBox(0, "$Strength = ", $Strength)
    Else
        Return(-1)
    EndIf

    ;Check for Lowercase letters
    $Return = _CheckValue(97,122,$Password)
    If $Return <> 0 Then
        $Strength = $Strength + 1
        $Return = ""
    EndIf

    ;Check for Uppercase letters
    $Return = _CheckValue(65,90,$Password)
    If $Return <> 0 Then
        $Strength = $Strength + 1
        $Return = ""
    EndIf

    ;Check for numbers
    $Return = _CheckValue(48,57,$Password)
    If $Return <> 0 Then
        $Strength = $Strength + 1
        $Return = ""
    EndIf

    ;Check for special characters
    $Return = _CheckValue(33,47,$Password)
    If $Return <> 0 Then
        $Strength = $Strength + 1
        $Return = ""
    EndIf
    $Return = _CheckValue(58,64,$Password)
    If $Return <> 0 Then
        $Strength = $Strength + 1
        $Return = ""
    EndIf
    $Return = _CheckValue(91,96,$Password)
    If $Return <> 0 Then
        $Strength = $Strength + 1
        $Return = ""
    EndIf
    $Return = _CheckValue(123,255,$Password)
    If $Return <> 0 Then
        $Strength = $Strength + 1
        $Return = ""
    EndIf

    ;MsgBox(0, "$Strength = ", $Strength)
    If $Strength >= 5 Then
        Return(1)
    Else
        Return(-1)
    EndIf
EndFunc

Func _CheckValue($x, $y, $Password)
    Local    $iLoopvar = 0
    For $iLoopvar = $x To $y
        If StringInStr($Password, Chr($iLoopvar), 1,1, 1) > 0 Then
            ;MsgBox(0, "$iLoopvar = ", Chr($iLoopvar))
            Return(1)
        EndIf
    Next
EndFunc

Func _ADDoError()
    Local    $HexNumber, $objConnection
    $HexNumber = Hex($oMyError.number, 8)

    If $HexNumber = 80020009 Then
        SetError(3)
        Return
    EndIf

    If $HexNumber = "8007203A" Then
        SetError(4)
        Return
    EndIf

    If $HexNumber = "00000002" Then
        SetError(5)
        Return 'The system cannot find the file specified.'
    EndIf

    If $HexNumber = "00000035" Then
        SetError(6)
        Return 'Network path cannot be found'
    EndIf

    If $HexNumber = "00000005" Then
        SetError(7)
        Return 'Access Denied'
    EndIf

    If $HexNumber = "80070005" Then
        SetError(8)
        Return 'Access Denied'
    EndIf

    If $HexNumber = "800706BA" Then
        SetError(9)
        Return 'The RPC server is unavailable'
    EndIf

    MsgBox(262144, "", "We intercepted a COM Error !" & @CRLF & _
            "Number is: " & $HexNumber & @CRLF & _
            "Windescription is: " & $oMyError.windescription & @CRLF & _
            "Script Line number is: " & $oMyError.scriptline & @CRLF & _
            "Script Character number is: " & Hex($oMyError.number, 8))

    Select
        Case $oMyError.windescription = "Access is denied."
            $objConnection.Close("Active Directory Provider")
            $objConnection.Open("Active Directory Provider")
            SetError(2)
        Case 1
            SetError(1)
    EndSelect

EndFunc   ;==>_ADDoError




