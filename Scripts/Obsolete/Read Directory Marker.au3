#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_icon=Images\Download.ico
#AutoIt3Wrapper_UseX64=n
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#include <File.au3>
#cs
Dim $Marker = 'C:\DTS Marker.txt', _
	$DTS_Path, $fcShortcut


If FileExists($Marker) Then
	$DTS_Path = FileReadLine($Marker, 1)
	_FileWriteLog($DTS_Path & '\Files\Shortcut.txt', 'Marker found')
	$fcShortcut = FileCreateShortcut($DTS_Path&'\DTS Client Verification.exe', @DesktopDir&'\DTS Downtime.lnk', _
						$DTS_Path, '', 'Downtime System Client', @ScriptDir&'\Downlad.ico')
	MsgBox(64,'Shortcut', 'Created')
	_FileWriteLog($DTS_Path & '\Files\Shortcut.txt', 'Shortcut creation return: '&$fcShortcut&'. 0 = failure.')
EndIf
#ce

Dim $Marker = 'C:\DTS Marker.txt', _
	$DTS_Path, $fcShortcut

MsgBox(64,'Marker', FileExists($Marker))
If FileExists($Marker) Then
	$DTS_Path = FileReadLine($Marker, 1)
	_FileWriteLog($DTS_Path & '\Files\Shortcut.txt', 'Marker found')
	$fcShortcut = FileCreateShortcut($DTS_Path&'\DTS Client Verification.exe', @DesktopDir&'\DTS Downtime.lnk', _
						$DTS_Path&'\', '', 'Downtime System Client', @ScriptDir&'\Download.ico')
	MsgBox(64,'Shortcut', $fcShortcut & ' - ' & $DTS_Path&'\DTS Client Verification.exe')
	_FileWriteLog($DTS_Path & '\Files\Shortcut.txt', 'Shortcut creation return: '&$fcShortcut&'. 0 = failure.')
EndIf