;DTS - GUI Side application

;This piece of the application is the Graphical User interface for easier use with the application
Opt('MustDeclareVars', 1)
Opt("GUIOnEventMode", 1)



#Region Includes
#include <GUIConstantsEx.au3>
#include <GuiListView.au3>
#include <GuiIPAddress.au3>
#include <GuiEdit.au3>
#include <WindowsConstants.au3>
#include <StaticConstants.au3>
#include <EditConstants.au3>
#include <ButtonConstants.au3>
#include <array.au3>
#include <file.au3>
#include <Date.au3>
#include <string.au3>
#EndRegion

#Region Declarations
;GUI Global Variables
;GUI
Global 	$DTS_Interface, $AddWorkstation, $RemoveWorkstation, $ClientDeploy, $ClientRemove, $Change_Password_GUI, _
		$INI_Viewer, $Log_Interface, $General_Interface_Log_Viewer, $Error_Interface_Log_Viewer

;Menu
Global 	$File_Menu, $Add_Workstation, $Remove_Workstation, $Exititem, _
		$Action_Menu, $Start_Transfer_Service, $Stop_Transfer_Service, $Disable_Monitoring, $Enable_Monitoring, $Deploy_Client, _
		$Remove_Client, $Change_Password, $Separator, $View_menu, $Download_DTS_Transfer_Log, $View_Log_Selector, _
		$View_General_Interface_Log, $View_Error_Interface_Log, $View_INI, $Refresh_view, $Help_Menu, $Help_file, $About

;Components
Global 	$Transfer_List, $Workstation_Input, $Search_For_Workstation, $Input_Group, $Seton_Pic, $Atos_Pic, $Initiate, $Diff, _
		$oMyError, $Count_Down, $Item_Index, $CheckBoxTimer, $CheckBoxDiff, $Save_Changes, $Delete, $Screen_Refresh

;Misc
Global 	$oMyError, $INI_File, $ComputerFolder, $WorkstationFile, $Backup_Dir, $Log_Dir, $DeployWorkstationNameInput, $DeployIP, _
		$CP1, $CP2, $SPBox, $AddWorkstationName, $AddIP, $AddSourceInput, $AddDestinationInput, $RemoveWorkstationName, _
		$RemoveWorkstationNameInput, $RemoveIP, $Remove_Client, $Search_Item, $Search_Func, $Save_Item, $Save_Func, _
		$Auto_Removal, $Auto_Item, $Download_DTS_Transfer_Log, $Download_Interface_Log, $Download_All_Client_Logs, _
		$Downlaod_Single_Client_Log, $Workstation_Log, $Interface_Log, $ErrLog, $Server_Program_Installation_Path, $Local_Install_Path, _
		$ContactAdmin = @CRLF & @CRLF & 'If this is your first time recieving this message then please try again. If not then please contact you system Administrator.'
		;$RegSysError = "HKEY_LOCAL_MACHINE\SOFTWARE\DTS Transfer\System Error Count",

Global $oMyError = ObjEvent("AutoIt.Error", "MyErrFunc"); Install a custom COM error handler
#EndRegion

#Region Configurations
$Server_Program_Installation_Path = IniRead(@ScriptDir&'\Admin Config.ini', 'Server', 'Loc','Unknown'); Location server
If Not FileExists($Server_Program_Installation_Path) Or $Server_Program_Installation_Path = 'Unkown' Then
	MsgBox(16, 'Error', 'Unable to locate Server Program Installation Path. Please verify that the ini file is in the same '& _
				'directory as the "DTS Interface executable. '&$ContactAdmin)
	Exit
EndIf

$INI_File = $Server_Program_Installation_Path & '\Settings\Configurations.ini'
;~ MsgBox(64,'Path', $INI_File)
If Not FileExists($INI_File) Then
	MsgBox(16, 'Error', 'Unable to locate Configuration file.')
	Exit
EndIf

$WorkstationFile =  $Server_Program_Installation_Path&'\computers\'&IniRead($INI_File, 'DTS Server', 'Workstations', 'Unknown') ;This is where all transfer systems are housed
If Not FileExists($WorkstationFile)  Or $WorkstationFile = 'Unknown' Then
	MsgBox(16, 'Error', 'Unable to read ini file or unable to locate "Computers.csv" file on Server. '&$ContactAdmin)
	Exit
EndIf

$Backup_Dir = $Server_Program_Installation_Path&'\Computers\Backups'; This is where we will backup the transfer systems after modification
If Not FileExists($Backup_Dir) Then
	DirCreate($Backup_Dir)
EndIf

#cs
$Local_Install_Path = IniRead($INI_File, 'DTS Interface', 'Local_Install','Unknown'); This is where the DTS Interface will install
If Not FileExists($Local_Install_Path) Then
	MsgBox(16, 'Error', 'Unable to locate Local Install path')
	Exit
EndIf
#ce

$Log_Dir = @ScriptDir&'\Logs'; This is where all DTS Interface logs will be held
If Not FileExists($Log_Dir) Then
	DirCreate($Log_Dir)
EndIf

Global	$Interface_Log = $Log_Dir&'\General Interface.log', _
		$ErrLog = $Log_Dir&'\Interface Error.log', _
		$ClientErrorCount = $Server_Program_Installation_Path&'\Logs\Error Count.log'


#EndRegion

#Region Images
FileInstall('\\austech\is-si\Development\icons\compass.ico', @TempDir & '\compass.ico', 1)
#endregion

;#Region Code

_FileWriteLog($Interface_Log,'The DTS Interface has been initialized.')
_DTS_Interface()

#Region Main GUI
;--------------------------------------------------------------------------------
;Main Function GUI
;--------------------------------------------------------------------------------
Func _DTS_Interface()
	#Region ### START Koda GUI section ### Form=C:\Documents and Settings\djthornton\My Documents\Projects\DTS Project\DTS Interface.kxf
	;create Gui
	$DTS_Interface = GUICreate("DTF DTS Interface", 1023, 631, -1, -1, $WS_OVERLAPPEDWINDOW, $WS_EX_WINDOWEDGE + $WS_EX_DLGMODALFRAME)
	GUICtrlSetResizing($DTS_Interface, $GUI_DOCKAUTO)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Exit_Interface")
	GUISetIcon(@TempDir & '\compass.ico')

	; Create File menu
	$File_Menu = GUICtrlCreateMenu("&File")
	$Add_Workstation = GUICtrlCreateMenuItem("Add Workstation", $File_Menu)
	GUICtrlSetOnEvent(-1, '_Add_Workstation_Interface')
;	$Remove_Workstation = GUICtrlCreateMenuItem("Remove Workstation", $File_Menu)
;	GUICtrlSetOnEvent(-1, '_Get_Selected')
	$Separator = GUICtrlCreateMenuItem("", $File_Menu)
	$Exititem = GUICtrlCreateMenuItem("Exit", $File_Menu)
	GUICtrlSetOnEvent(-1, '_Exit_Interface')

	;Action Menu
	$Action_Menu = GUICtrlCreateMenu("&Action")
	$Start_Transfer_Service = GUICtrlCreateMenuItem("Start Transfer Service", $Action_Menu)
	GUICtrlSetOnEvent(-1, '')
	$Stop_Transfer_Service = GUICtrlCreateMenuItem("Stop Transfer Service", $Action_Menu)
	GUICtrlSetOnEvent(-1, '')
	$Separator = GUICtrlCreateMenuItem("", $Action_Menu)
	$Change_Password = GUICtrlCreateMenuItem("Change Client Password", $Action_Menu)
	GUICtrlSetOnEvent(-1, '_Change_Password')
	$Deploy_Client = GUICtrlCreateMenuItem("Deploy Client", $Action_Menu)
	GUICtrlSetOnEvent(-1, '_Deploy')
	$Remove_Client = GUICtrlCreateMenuItem("Remove Single Client", $Action_Menu)
	GUICtrlSetOnEvent(-1, '_Remove_Client_Interface')
	$Separator = GUICtrlCreateMenuItem("", $Action_Menu)
	$Disable_Monitoring = GUICtrlCreateMenuItem("Disable All Monitoring", $Action_Menu)
	GUICtrlSetOnEvent(-1, '_Disable_All_Monitoring')
	$Enable_Monitoring = GUICtrlCreateMenuItem("Enable All Monitoring", $Action_Menu)
	GUICtrlSetOnEvent(-1, '_Enable_All_Monitoring')

	;Create View Menu
	$View_menu = GUICtrlCreateMenu("&View")
	$View_Log_Selector = GUICtrlCreateMenuItem("Log Selector", $View_menu)
	GUICtrlSetOnEvent(-1, '_Log_Interface')
	$View_General_Interface_Log = GUICtrlCreateMenuItem("View General Log (Read-Only)", $View_menu)
	GUICtrlSetOnEvent(-1, 'General_DTS_Interface_Log')
	$View_Error_Interface_Log = GUICtrlCreateMenuItem("View Error Log (Read-Only)", $View_menu)
	GUICtrlSetOnEvent(-1, 'Error_DTS_Interface_Log')
	$View_INI = GUICtrlCreateMenuItem("View INI (Read-Only)", $View_menu)
	GUICtrlSetOnEvent(-1, '_INI_Viewer')
	$Refresh_view = GUICtrlCreateMenuItem("Refresh List", $View_menu)
	GUICtrlSetOnEvent(-1, '_Load_TransferList')

	;Creae Help Menu
	$Help_Menu = GUICtrlCreateMenu("&Help")
	$Help_file = GUICtrlCreateMenuItem("Help File", $Help_Menu)
	GUICtrlSetOnEvent(-1, '')
	$About = GUICtrlCreateMenuItem("About", $Help_Menu)
	GUICtrlSetOnEvent(-1, '')

	;Create Gui Components
	$Save_Changes = GUICtrlCreateButton("Save Changes", 30, 80, 89, 25, 0)
	GUICtrlSetOnEvent(-1, '_Save_Changes')
	$Delete = GUICtrlCreateButton("Delete", 168, 80, 89, 25, 0)
	GUICtrlSetOnEvent(-1, '_Remove_Workstations')
	$Workstation_Input = GUICtrlCreateInput("Workstation or IP", 30, 30, 153, 21)
	GUICtrlSetState(-1, $GUI_FOCUS)
	$Search_For_Workstation = GUICtrlCreateButton("Search", 200, 30, 60, 20)
	GUICtrlSetFont(-1, 11, 400, 0, "MS Sans Serif")
	GUICtrlSetOnEvent($Search_For_Workstation, '_Search_Workstation')
	$Input_Group = GUICtrlCreateGroup("Search for a Workstation or IP", 8, 14, 270, 49)
	;GUICtrlCreateGroup("", -99, -99, 1, 1)
	$Seton_Pic = GUICtrlCreatePic("..\Images\FamilyHospital_horiz_color_Mod.JPG", 288, 16, 700, 80, BitOR($SS_NOTIFY, $WS_GROUP, $WS_CLIPSIBLINGS))
	GUICtrlSetResizing(-1, $GUI_DOCKHEIGHT)
	;$Atos_Pic = GUICtrlCreatePic("..\Images\atosorigin_logo.gif", 950,30, 0, 0, BitOR($WS_GROUP,$WS_CLIPSIBLINGS))
	;;$Count_Down = GUICtrlCreateLabel("Count Down", 16, 584, 66, 17)
	_Load_TransferList()
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###


	While 1
		Sleep(250)
	WEnd

EndFunc   ;==>_DTS_Interface
#EndRegion

#cs
Func _Load_TransferList()
	;Declare Variables
	Local $exStyles = BitOR($LVS_EX_FULLROWSELECT, $LVS_EX_CHECKBOXES), $Workstations, $x, $y, $Workstation_Info, $WorkstationName, $FailedAttempts, $noload
	;Destroy List so that when the refresh is invoked it will clear all values and start again.
	_GUICtrlListView_Destroy($Transfer_List)
	;Create Transfer List View
;~ 	$Transfer_List = GUICtrlCreateListView("", 0, 127, 1023, 485, $LVS_REPORT + $LVS_SHOWSELALWAYS)
	$Transfer_List = _GUICtrlListView_Create($DTS_Interface, '', 0, 127, 1023, 485, $LVS_REPORT + $LVS_SHOWSELALWAYS)
	_GUICtrlListView_RegisterSortCallBack($Transfer_List)
	;GUICtrlSetResizing(-1,$GUI_DOCKBORDERS)
	_GUICtrlListView_SetExtendedListViewStyle($Transfer_List, $exStyles)
	_GUICtrlListView_AddColumn($Transfer_List, 'Monitor', 60)
	_GUICtrlListView_AddColumn($Transfer_List, 'Workstation - IP', 152)
	;GUICtrlSetResizing(-1,$GUI_DOCKBORDERS)
	_GUICtrlListView_AddColumn($Transfer_List, 'Source', 355) ;
	;GUICtrlSetResizing(-1,$GUI_DOCKBORDERS)
	_GUICtrlListView_AddColumn($Transfer_List, 'Destination', 355)
	;GUICtrlSetResizing(-1,$GUI_DOCKBORDERS)
;~ 	_GUICtrlListView_AddColumn($Transfer_List, 'Time Stamps       Source    -     Destination', 276) ;No longer needed because folder contents are being moved and not just individual files - Tied to row Timestamps
	;GUICtrlSetResizing(-1,$GUI_DOCKBORDERS)
	_GUICtrlListView_AddColumn($Transfer_List, 'Failed Transfers', 97)
	;GUICtrlSetResizing(-1,$GUI_DOCKBORDERS)
	;Load the file into an array
	_FileReadToArray($WorkstationFile, $Workstations)
	If @error = 1  Then
		MsgBox(16, "DTS Transfer System Error", " There was an error reading the Transfer List. DTS Transfer System is unable to start." & @CRLF & @CRLF & 'Please contact ' & _
				'your System Administrator for assistance.' & @CRLF & @CRLF & '@error: '&@error)
		Exit
	ElseIf @error = 2 Then
		MsgBox(16, "DTS Transfer System Error", " There was an error reading the Transfer List. The list may be empty." & @CRLF & @CRLF & '@error: '&@error)
		$noload = 1
		;MsgBox(0,'Loaded','List Loaded.')
	EndIf

;~ 	_ArrayDisplay($Workstations)
	If $noload = 1 Then
		;The list is believed to be empty. So there is no need to read it.
	Else
		;Y is used to increment the row for each entry in second array
		Local $y = 0
		;For each row in the array we will ready its information then break it into pices by the "," delimeter and give each it own value slot in an array.
		;To view the array before it's broken into pieces remove the ";" from the _arraydisplay below.
		;_Arraydisplay($Workstations)
		_FileWriteLog($Interface_Log, $Workstations[0]&' workstation(s) were listed.')
		For $x = 1 To $Workstations[0]
			$Workstation_Info = StringSplit($Workstations[$x], ',')
			_GUICtrlListView_AddItem($Transfer_List,'', -1, $x) ;Name
			_GUICtrlListView_AddSubItem($Transfer_List, $y, StringTrimLeft($Workstation_Info[1], 1), 1) ;Name
			$WorkstationName = StringSplit($Workstation_Info[1],' - ')
			;GUICtrlSetResizing(-1,$GUI_DOCKBORDERS)
			_GUICtrlListView_AddSubItem($Transfer_List, $y, $Workstation_Info[2], 2) ;Source
			;GUICtrlSetResizing(-1,$GUI_DOCKBORDERS)
			_GUICtrlListView_AddSubItem($Transfer_List, $y, $Workstation_Info[3], 3);Destination
			;GUICtrlSetResizing(-1,$GUI_DOCKBORDERS)
;~ 			_Time_Format($y, $Workstation_Info[2], $Workstation_Info[3]) ; No longer needed because folder contents are being moved and not just individual files - Tied to column time stamps
			$FailedAttempts = _Read_Failed_Transfers(StringTrimLeft($WorkstationName[1], 1)) ;Trim the pike in the file name ex: |jwxlc2j
				If $FailedAttempts = '' Then $FailedAttempts = 'No Entry'
			_GUICtrlListView_AddSubItem($Transfer_List, $y, $FailedAttempts, 4)
			If $Workstation_Info[4] = 'True' Then _GUICtrlListView_SetItemChecked($Transfer_List, $y)
			$y = $y + 1 ; Increment so that next time the row will jump to the next
		Next
		$Workstations = ''
	EndIf
	GUISetState()
;~ 	_GUICtrlListView_RegisterSortCallBack($Transfer_List)
	GUIRegisterMsg($WM_NOTIFY, "_WM_NOTIFY")

EndFunc   ;==>_Load_TransferList
#ce
#Region Functions
Func _Load_TransferList()
	;Declare Variables
	Local 	$exStyles = BitOR($LVS_EX_FULLROWSELECT, $LVS_EX_CHECKBOXES), _
			$Workstations, $Workstation_Info, $WorkstationName, $WorkstationIP, $Folder_Source, $Folder_Destination, $Monitoring, $x, $y, $FailedAttempts, $noload
	;Destroy List so that when the refresh is invoked it will clear all values and start again.
	_GUICtrlListView_Destroy($Transfer_List)
	;Create Transfer List View
	$Transfer_List = GUICtrlCreateListView("Monitoring|Workstation|IP|Source|Destination|Failed Transfers", 0, 127, 1023, 485, $LVS_REPORT + $LVS_SHOWSELALWAYS, $exStyles)
	GUICtrlSetOnEvent(-1, '_Sort')
	_FileReadToArray($WorkstationFile, $Workstations)
	If @error = 1  Then
		MsgBox(16, "DTS Transfer System Error", " There was an error reading the Transfer List. DTS Transfer System is unable to start." & @CRLF & @CRLF & 'Please contact ' & _
				'your System Administrator for assistance.' & @CRLF & @CRLF & '@error: '&@error)
		Exit
	ElseIf @error = 2 Then
		MsgBox(16, "DTS Transfer System Error", " There was an error reading the Transfer List. The list may be empty." & @CRLF & @CRLF & '@error: '&@error)
		$noload = 1
		;MsgBox(0,'Loaded','List Loaded.')
	EndIf

;~ 	_ArrayDisplay($Workstations)
	If $noload = 1 Then
		;The list is believed to be empty. So there is no need to read it.
	Else
		;Y is used to increment the row for each entry in second array
		Local $y = 0
		;For each row in the array we will ready its information then break it into pices by the "," delimeter and give each it own value slot in an array.
		;To view the array before it's broken into pieces remove the ";~" from the _arraydisplay below.
;~		_ArrayDisplay($Workstations)
		_FileWriteLog($Interface_Log, $Workstations[0]&' workstation(s) were listed.')
		For $x = 1 To $Workstations[0]
			$Workstation_Info = StringSplit($Workstations[$x], ',')
;~			_Arraydisplay($Workstation_Info)
			$Monitoring = $Workstation_Info[1]
			$WorkstationName = $Workstation_Info[2]
			$WorkstationIP = $Workstation_Info[3]
			$Folder_Source = $Workstation_Info[4]
			$Folder_Destination = $Workstation_Info[5]
			$FailedAttempts = _Read_Failed_Transfers($WorkstationName)
			If $FailedAttempts = '' Then $FailedAttempts = 'No Entry'
			If $Monitoring = 'YES' Then
				GUICtrlCreateListViewItem('Yes|'&$WorkstationName&'|'&$WorkstationIP&'|'&$Folder_Source&'|'&$Folder_Destination&'|'&$FailedAttempts, $Transfer_List)
				_GUICtrlListView_SetItemChecked($Transfer_List, $x - 1)
			Else
				GUICtrlCreateListViewItem('No|'&$WorkstationName&'|'&$WorkstationIP&'|'&$Folder_Source&'|'&$Folder_Destination&'|'&$FailedAttempts, $Transfer_List)
			EndIf
;~ 			_Time_Format($y, $Workstation_Info[2], $Workstation_Info[3]) ; No longer needed because folder contents are being moved and not just individual files - Tied to column time stamps
			_GUICtrlListView_SetColumnWidth($Transfer_List, 0, 72)
			_GUICtrlListView_SetColumnWidth($Transfer_List, 1, 75)
			_GUICtrlListView_SetColumnWidth($Transfer_List, 2, 81)
			_GUICtrlListView_SetColumnWidth($Transfer_List, 3, 349)
			_GUICtrlListView_SetColumnWidth($Transfer_List, 4, 349)
			_GUICtrlListView_SetColumnWidth($Transfer_List, 5, 94)
		Next
		$Workstations = ''
		$Workstation_Info = ''
	EndIf
	GUISetState()
	_GUICtrlListView_RegisterSortCallBack($Transfer_List)
;~ 	GUIRegisterMsg($WM_NOTIFY, "_WM_NOTIFY")

EndFunc   ;==>_Load_TransferList

Func _Sort()
	_GUICtrlListView_SortItems($Transfer_List, GUICtrlGetState($Transfer_List))
EndFunc

Func _Read_Failed_Transfers($Workstation)
	Local	$fRead, $aFailed, $sPlit, $name

	$fRead = FileRead($ClientErrorCount)
	If $fRead = '' Then
		$fRead = 'Empty'
		Return 0
	Else
;~ 		MsgBox(64,'File Error Count Read', $fRead&'   -   '&@error)
		If Not _FileReadToArray($ClientErrorCount, $aFailed) Then
			MsgBox(16,'Error','Unable to parse Client Error Count log')
			;Exit
		Else
			For $entry In $aFailed
				$sPlit = StringSplit($entry,',')
				$name = $sPlit[1]
				If $Workstation = $name Then Return $sPlit[2]
;~ 				MsgBox(64,'Workstation',$Workstation)
;~ 				_ArrayDisplay($sPlit, 'Error Count Split')
			Next
			Return 0
		EndIf
	EndIf
EndFunc

#cs
Func _WM_NOTIFY($hWnd, $iMsg, $iwParam, $ilParam)
    Local $hWndFrom, $iIDFrom, $iCode, $tNMHDR, $hWndListView
    $hWndListView = $Transfer_List
    If Not IsHWnd($Transfer_List) Then $hWndListView = GUICtrlGetHandle($Transfer_List)

    $tNMHDR = DllStructCreate($tagNMHDR, $ilParam)
    $hWndFrom = HWnd(DllStructGetData($tNMHDR, "hWndFrom"))
    $iCode = DllStructGetData($tNMHDR, "Code")

    Switch $hWndFrom
    Case $hWndListView
        Switch $iCode
        Case $LVN_COLUMNCLICK
            Local $tInfo = DllStructCreate($tagNMLISTVIEW, $ilParam)
            Local $ColumnIndex = DllStructGetData($tInfo, "SubItem")
            _GUICtrlListView_SortItems($Transfer_List, $ColumnIndex)
        EndSwitch
    EndSwitch

    Return $GUI_RUNDEFMSG
EndFunc
#ce


Func _Time_Format($y, $Src, $Des)
	Local $ST, $DT, $ST_Stamp, $DT_Stamp, $Stamp_Check, $Times
	$ST = FileGetTime($Src, 0, 0)
	If @error Then
		$ST_Stamp = 'Unknown'
		$DT_Stamp = 'Unknown'
		$Times = '                           ' & $ST_Stamp & ' - ' & $DT_Stamp
		;MsgBox(16,'Error','No Source file detected')
	Else
		$ST_Stamp = _Time_Stamp($ST)
		$DT = FileGetTime($Des, 0, 0)
		If @error Then
			If Not FileExists($Des) Then
				$DT_Stamp = '    No File'
			Else
				$DT_Stamp = '    Unknown'
			EndIf
		Else
			$DT_Stamp = _Time_Stamp($DT)
		EndIf
		$Times = $ST_Stamp & ' - ' & $DT_Stamp
		;$Stamp_Check = _checkDate($ST,$DT)

	EndIf
	_GUICtrlListView_AddSubItem($Transfer_List, $y, $Times, 3)
EndFunc   ;==>_Time_Format

Func _Time_Stamp($T)
	Local $M, $TY, $TM, $TD, $TH, $TMin, $TSec, $T_Stamp
	;_ArrayDisplay($T)
	$M = ''
	$TY = $T[0]
	$TM = $T[1]
	$TD = $T[2]
	If $T[3] = 12 Then
		$TH = $T[3]
		$M = 'pm'
	ElseIf $T[3] > 11 Then
		$TH = $T[3] - 12
		$M = 'pm'
	Else
		$TH = $T[3]
		$M = 'am'
	EndIf
	$TMin = $T[4]
	$TSec = $T[5]
	$T_Stamp = $TH & ':' & $TMin & ':' & $TSec & ' ' & $M & ' - ' & $TM & '\' & $TD & '\' & $TY
	Return $T_Stamp
	;MsgBox(0, '', $ST_Stamp)
EndFunc   ;==>_Time_Stamp

Func _Add_Workstation()
	Local $Name, $IP, $Src, $DestName, $Install_Path, $Dest, $Add_Search_Name, $Downtime_Files, $ext, $oComputerFile, $Success

	$Name = GUICtrlRead($AddWorkstationName)
	;MsgBox(0,'Name',$Name)
	$IP = _GUICtrlIpAddress_Get($AddIP)
	;MsgBox(0,'IP',$IP)
	$Src = GUICtrlRead($AddSourceInput)
	;MsgBox(0,'Src',$Src)
	$Install_Path = IniRead($INI_File, 'DTS Client', 'Install_Location', 'Error')
	;MsgBox(0,'Install Path',$Install_Path)
	If $Install_Path = 'Error' Then
		MsgBox(16, 'Add Workstation - Error', 'Unable to locate "Installation Path" in ini file. Aborting "Workstation Add".')
		Return 0
	EndIf
	#cs
	$Downtime_Files = IniRead($INI_File, 'DTS Client', 'Downtime_Files', 'Error')
	If $Downtime_Files = 'Error' Then
		MsgBox(16, 'Client Deployment - Error', 'Unable to locate "Downtime File path" in ini file. Aborting "Workstation Add".')
		Return 0
	EndIf
	#ce
	ProgressOn('Adding Workstation', 'Adding ' & $Name, 'Please wait...' & @CRLF & $Name & ' is being added to the DTS Transfer list.', Default, 300, 16)
	Sleep(2000)
	ProgressSet(10)
	;MsgBox(0,'','Wait')
	TCPStartup()
	;MsgBox(0,'Compare',TCPNameToIP($Name)&' '&$IP)
	If TCPNameToIP($Name) <> $IP Then
		ProgressOff()
		MsgBox(16, 'Add Workstation - Error', 'The provided Workstationname and IP do not match. Plase check Workstation ID and IP address.')
		TCPShutdown()
		Return -1
	EndIf
	TCPShutdown()
	If Ping($ip) = 0 Then
		ProgressOff()
		MsgBox(16, 'Add Workstation - Error', 'The provided Workstationname cannot be pinged. Please verify that the asset is online.')
		Return -1
	EndIf
	ProgressSet(20)
	If Not FileExists($Src) Then
		ProgressOff()
		MsgBox(16, 'Add Workstation - Error', 'The   could not be found.')
		Return -1
	EndIf
	ProgressSet(30)
	If Not StringInStr(FileGetAttrib($Src), 'D') Then
		ProgressOff()
		MsgBox(16, 'Add Workstation - Error', 'The source must be a folder. Please check the source and try again.')
		Return -1
	EndIf
	ProgressSet(40)
;---------------------------------------------------------------------------------------
;New destination idea - Build destination for them so it is consistant

	$Dest = '\\'&$Name&'\c$\'&$Install_Path&'\Files\'
;---------------------------------------------------------------------------------------
	ProgressSet(50)
	$Add_Search_Name = _Search_Workstation($Name, 1)
	ProgressSet(60)
	If $Add_Search_Name = -1 Then
		ProgressOff()
		Return -1
	ElseIf $Add_Search_Name = 1 Then
		ProgressSet(70)
		;Skip IP Check User is aware of workstation not being listed
	ElseIf $Add_Search_Name = 0 Then
		If _Search_Workstation($IP, 1) = -1 Then Return 0
	EndIf
	ProgressSet(70)
	$oComputerFile = FileOpen($WorkstationFile, 1)
	If $oComputerFile = -1 Then
		ProgressOff()
		MsgBox(16, 'Add Workstation - Error', 'The Workstations list could not be accessed.' & $ContactAdmin)
		Return 0
	EndIf
	ProgressSet(80)
	$Success = FileWriteLine($oComputerFile, 'Yes,' & $Name & ',' & $IP & ',' & $Src & ',' & $Dest)
	If $Success <> 1 Then
		ProgressOff()
		MsgBox(16, 'Workstation Add - Error', 'There was an error adding ' & $Name & ' to the DTS Transfer List.' & $ContactAdmin)
		Return -1
	EndIf
	FileClose($oComputerFile)
	ProgressSet(100, 'Done')
	Sleep(2000)
	ProgressOff()
	MsgBox(64, 'Workstation Add - Complete', $Name & ' has been successfully added to the DTS Transfer list.')
	_Destroy_Add()
	_Load_TransferList()
EndFunc   ;==>_Add_Workstation

#cs
Func _Remove_Workstation_Interface()
	Local $RemoveWorkstationNameLabel, $RemoveIPLabel, $RemoveWorkstationButton
	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\DTS Project\Scripts\Add Computer Interface.kxf
	$RemoveWorkstation = GUICreate("Remove Workstation", 163, 145, 523, 268, -1, $WS_EX_ACCEPTFILES)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_Remove")
	GUISetIcon(@TempDir & '\compass.ico')
	;$RemoveWorkstationName = GUICtrlCreateInput("Workstation Name", 16, 40, 129, 21)
	$RemoveWorkstationName = GUICtrlCreateInput("jwx11c1", 16, 40, 129, 21) ;Testing Purposes only
	$RemoveWorkstationNameLabel = GUICtrlCreateLabel("Workstation Name", 16, 16, 100, 17)
	$RemoveWorkstationButton = GUICtrlCreateButton("Remove", 32, 105, 97, 25)
	GUICtrlSetOnEvent($RemoveWorkstationButton, '_Remove_Workstation')
	$Remove_Client = GUICtrlCreateCheckbox("Remove Client", 32, 74, 97, 17)
	GUICtrlSetOnEvent($Remove_Client, '')
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###
EndFunc   ;==>_Remove_Workstation_Interface

Func _Remove_Workstation()
	Local $Name, $Remove_Search_Name, $Remove_IP, $Selected_Workstations

	$Selected_Workstations = _Get_Selected()

	;MsgBox(0,'Name',$Name)

	$Remove_Search_Name = _Search_Workstation($Name, 3)

	If $Remove_Search_Name = -1 Then
		ProgressOff()
		Return -1
	Else
		$Remove_IP = StringTrimLeft(_GUICtrlListView_GetItem($Transfer_List, $Remove_Search_Name), $Name & ' - ')
		;MsgBox(0,'IP',$Remove_IP)
		;Continue
	EndIf

	_Save_Changes(1, $Name)
	#cs
		TCPStartup()
		If  TCPNameToIP($Name) = '' Then
	#ce

	_Destroy_Remove()
	_Load_TransferList()
EndFunc   ;==>_Remove_Workstation
#ce

Func _Remove_Workstations()
	Local $rows, $Selected, $Text, $divide, $Name, $k, $b, $1st, $Verify, $iColumn_info
	Dim $Removal_Items[1]
	;_ArrayDisplay($Removal_Items)
	$rows = _GUICtrlListView_GetItemCount($Transfer_List)
	;MsgBox(64,'Rows',$rows)
	$k = 0
	$b = 1
	For $x = 0 To $rows -1
	;	MsgBox(64,'x',$x)
		$Selected = _GUICtrlListView_GetItemSelected($Transfer_List,$x)
	;	MsgBox(64,'Selected',$Selected)
		If $Selected = 'True' Then
			ReDim $Removal_Items[$b]
		;	_ArrayDisplay($Removal_Items)
			For $y = 1 To _GUICtrlListView_GetColumnCount($Transfer_List) ;Get number of columns
				$iColumn_info = _GUICtrlListView_GetColumn($Transfer_List, $y)
				If $iColumn_info[5] = 'Workstation' Then $Name = _GUICtrlListView_GetItemText($Transfer_List, $x, $y) ;pull name from column
			Next
;~ 			MsgBox(64,'Name found',$Name)
			$Removal_Items[$k] = $Name
			$k = $k + 1
			$b = $b + 1
		EndIf
	Next
;~	Exit
	If $Removal_Items[0] = '' Then
		MsgBox(16,'Workstation Removal - Error','No items were selected. Please first select an Workstation to remove.')
		Return -1
	EndIf
	$Verify = MsgBox(36,'Multiple Workstation Removal','You are about to remove '&UBound($Removal_Items)&' item(s). Are you sure you want to continue?')
		If $Verify = 7 Then Return -1
	;_ArrayDisplay($Removal_Items)
	_Save_Changes(1, $Removal_Items)
EndFunc

Func _Deploy()
	Local 	$Workstations, $UserName, $Password, $Install_Path, $Client_Executables, $Downtime_Files, $Deploy_Search_Name, $Dupe, $objUser, _
			$objGroup, $colAccounts, $oldFlags, $newFlags, $ExistUser, $User, $Existgroup, $objUsers, $aErr[1], $Return, $z, $err

	;$strWorkstationName = GUICtrlRead($DeployWorkstationNameInput)

	$Workstations = _Get_Selected()

	ProgressOn('Client Deployment', 'Deploying Client to selected Workstations. Please wait...', _
			Default, 300, 16)
	Sleep(2000)
	;MsgBox(0,'Name',$strWorkstationName)
	ProgressSet(5)
	If Not IsArray($Workstations) Or $Workstations[0][0] = '' Then
		ProgressOff()
		MsgBox(16, 'Client Deployment - Error', 'You must first select one or more Workstations. Please try again.')
		Return 0
	EndIf

	$UserName = IniRead($INI_File, 'DTS Client', 'Username', 'Error')
	;MsgBox(0,'User Name',$UserName)
	If $UserName = 'Error' Then
		ProgressOff()
		MsgBox(16, 'Client Deployment - Error', 'Unable to locate "Username" in ini file.Aborting "Workstation Deployment".')
		Return 0
	EndIf

	$Password = IniRead($INI_File, 'DTS Client', 'Password', 'Error')
	;MsgBox(0,'Password',$Password)
	If $Password = 'Error' Then
		ProgressOff()
		MsgBox(16, 'Client Deployment - Error', 'Unable to locate "Password" in ini file. Aborting "Workstation Deployment".')
		Return 0
	EndIf
	ProgressSet(10)
	$Install_Path = IniRead($INI_File, 'DTS Client', 'Install_Location', 'Error')
	;MsgBox(0,'Install Path',$Install_Path)
	If $Install_Path = 'Error' Then
		ProgressOff()
		MsgBox(16, 'Client Deployment - Error', 'Unable to locate "Installation Path" in ini file. Aborting "Workstation Deployment".')
		Return 0
	EndIf

	$Client_Executables = IniRead($INI_File, 'DTS Client', 'Client_Executables', 'Error')
	;MsgBox(0,'Install Path',$Install_Path)
	If $Client_Executables = 'Error' Then
		ProgressOff()
		MsgBox(16, 'Client Deployment - Error', 'Unable to locate "Client Executables path" in ini file. Aborting "Workstation Deployment".')
		Return 0
	EndIf

	ProgressSet(20)
	#cs
	$Deploy_Search_Name = _Search_Workstation($strWorkstationName, 2)
	If $Deploy_Search_Name = -1 Then
		ProgressOff()
		Return 0
	ElseIf $Deploy_Search_Name = 1 Then
		;Skip IP Check User is aware of workstation not being listed
	ElseIf $Deploy_Search_Name = 0 Then
		If _Search_Workstation($strWorkstationIP, 2) = -1 Then Return 0
	EndIf
	ProgressSet(50)
	TCPStartup()
	;MsgBox(0,'Compare',TCPNameToIP($strWorkstationName)&' '&$strWorkstationIP)
	If TCPNameToIP($strWorkstationName) <> $strWorkstationIP Then
		ProgressOff()
		MsgBox(16, 'Client Deployment - Error', 'The provided Workstationname and IP do not match. Plase check Workstation ID and IP address.')
		TCPShutdown()
		Return 0
	EndIf
	TCPShutdown()
	#ce
	$z = 0
	For $x = 0 To UBound($Workstations) - 1
		ProgressSet(20 + (70 * ($x/UBound($Workstations))))
	;-----------------------------------------------------------------------------
		$Existgroup = 0
	; Init objects
		Const $ADS_UF_DONT_EXPIRE_PASSWD = 0X10000
		Const $ADS_UF_PASSWD_CANT_CHANGE = 0X40

		; Check if account exists .. if not create it
		$objUser = ObjGet("WinNT://" & $Workstations[$x][0] & "/" & $UserName)
		If @error Then
			;MsgBox(0,'Not Detected','No account was detected. Setting up now.')
			$colAccounts = ObjGet("WinNT://" & $Workstations[$x][0] & "")
			$objUser = $colAccounts.Create("user", $UserName)
			$objUser.SetPassword($Password)
			$objUser.Put("Fullname", "DTS Account")
			$objUser.Put("Description", "DTS Transfer Account")
			$objUser.SetInfo
			; Read current settings and Bitor to ensure the "Don't expire password swith is on and user cannot change password"
			$oldFlags = $objUser.Get("UserFlags")
			$newFlags = BitOR($oldFlags, $ADS_UF_DONT_EXPIRE_PASSWD)
			$objUser.Put("UserFlags", $newFlags) ;expire the password
			$objUser.SetInfo
			$oldFlags = $objUser.Get("UserFlags")
			$newFlags = BitOR($oldFlags, $ADS_UF_PASSWD_CANT_CHANGE)
			$objUser.Put("UserFlags", $newFlags) ;expire the password
			$objUser.SetInfo
		Else
			$ExistUser = 1 ;User already exist
			$objUser.SetPassword($Password)
			$objUser.SetInfo
			;MsgBox(0,'Detected','The account was detected. Skipping account creation.')
		EndIf

		;Add User to users group**********************************************
		$objGroup = ObjGet("WinNT://" & $Workstations[$x][0] & "/Users,group")

		For $objUsers In $objGroup.Members
			If ($objUsers.AdsPath) = 'winNT://SETON/' & $Workstations[$x][0] & '/' & $UserName Then $Existgroup = 1
		Next

		If $Existgroup = 0 Then $objGroup.Add($objUser.ADsPath)

		;Create Directory
		$err = _Dir_Setup($Install_Path, $Downtime_Files, $Client_Executables, $Workstations[$x][0], $UserName)
		If $err <> 0 Then ;1 = failed logs folder; 2 = failed Downtime files foder; 3 = failed all folders
			ReDim $aErr[$z + 1]
			$aErr[$z] = $Workstations[$x][0]&' failed to setup completely.'
		EndIf
	Next
	If $aErr[0] <> '' Then MsgBox(16,'Errors Detected', 'There was an error in the depoloyment. Look to error log for details.')
	ProgressSet(100)
	Sleep(1000)
	ProgressOff()
EndFunc   ;==>_Deploy

Func _Dir_Setup($Install_Path, $Downtime_Files, $Client_Executables, $Workstation, $UserName)
	Local $err = 0
	;MsgBox(0,'Dir setup','')
	Local $acls, $Success, $ErrorCounter = 0
	DirCreate('\\' & $Workstation & '\c$\' & $Install_Path & '\Logs\')
	If @error Then $err = 1
	DirCreate('\\' & $Workstation & '\c$\' & $Install_Path & '\Files\')
	If @error Then $err = $err + 2
	$acls = ('cacls.exe \\' & $Workstation & '\c$\DTS Transfers\ /E /T /C /G ' & $UserName & ':R')
	Run(@ComSpec & " /C " & $acls, "", @SW_HIDE);_RunDOS($acls)
	Return $err
EndFunc   ;==>_Dir_Setup

Func _Remove_Client()
	Local $Workstation, $UserName, $Install_Path, $objUser, $Verify, $objUserDelete

	$Workstation = GUICtrlRead($RemoveWorkstationNameInput)
	ProgressOn('Client Removal', 'Removing client from ' & $Workstation, 'Please wait...' & @CRLF & 'Client configurations are being removed from ' & $Workstation & '.', _
			Default, 300, 16)
	Sleep(1000)
	If $Workstation = '' Or $Workstation = 'Workstation Name' Then
		ProgressOff()
		MsgBox(16, 'Client Removal - Error', 'You must first enter a valid Workstation name. Please try again.')
		Return -1
	EndIf
	ProgressSet(20)
	$UserName = IniRead($INI_File, 'DTS Client', 'Username', 'Error')
	;MsgBox(0,'User Name',$UserName)
	If $UserName = 'Error' Then
		ProgressOff()
		MsgBox(16, 'Client Removal - Error', 'Unable to locate "Username" in ini file.Aborting "Workstation Add".')
		Return -1
	EndIf
	ProgressSet(40)
	$Install_Path = IniRead($INI_File, 'DTS Client', 'Install_Location', 'Error')
	;MsgBox(0,'Install Path',$Install_Path)
	If $Install_Path = 'Error' Then
		ProgressOff()
		MsgBox(16, 'Client Removal - Error', 'Unable to locate "Installation Path" in ini file. Aborting "Workstation Add".')
		Return 0
	EndIf
	ProgressSet(60)
	; Check if account exists .. if so delete it
	$objUser = ObjGet("WinNT://" & $Workstation & "")
	If @error Then
		;does not exist
	Else
		$Verify = MsgBox(36, 'Client Removal - Verification', 'You are about to delete the DTS Transfer account and settings from ' & $Workstation & '.' & @CRLF & @CRLF & _
				'Are you sure you want to continue?')
		If $Verify = 7 Then
			ProgressOff()
			Return -1
		Else
			$objUserDelete = $objUser.Delete("user", $UserName)
		EndIf
	EndIf
	ProgressSet(90)
	DirRemove('\\' & $Workstation & '\c$\' & $Install_Path & '\', 1)
	ProgressSet(100, 'Done')
	Sleep(2000)
	ProgressOff()
	_Destroy_Client_Removal()
EndFunc   ;==>_Remove_Client

Func _Change_Password()
	Global $CP1, $CP2, $SPBox, $Save_Password, $Change_Password_Label
	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\DTS Project\Scripts\Save Password Dialog.kxf
	$Change_Password_GUI = GUICreate("Change Password", 230, 191, 259, 189)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_Change_Password")
	GUISetIcon(@TempDir & '\compass.ico')
	$CP1 = GUICtrlCreateInput("", 32, 40, 161, 21)
	$CP2 = GUICtrlCreateInput("", 32, 88, 161, 21)
	_GUICtrlEdit_SetPasswordChar($CP1, '*')
	_GUICtrlEdit_SetPasswordChar($CP2, '*')
	$Change_Password_Label = GUICtrlCreateLabel("Change Password", 64, 16, 90, 17)
	$Save_Password = GUICtrlCreateButton("Save", 72, 120, 81, 25, 0)
	GUICtrlSetOnEvent($Save_Password, '_Save_Password')
	$SPBox = GUICtrlCreateCheckbox("Show Password", 16, 160, 97, 17)
	GUICtrlSetOnEvent($SPBox, '_Show_Password')
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###
EndFunc   ;==>_Change_Password

Func _Show_Password()
	If BitAND(GUICtrlRead($SPBox), $GUI_CHECKED) = 0 Then
		;MsgBox(0,'Unchecked',BitAND(GUICtrlRead($SPBox),$GUI_CHECKED))
		_GUICtrlEdit_SetPasswordChar($CP1, '*')
		_GUICtrlEdit_SetPasswordChar($CP2, '*')
		GUICtrlSetState($CP2, $GUI_FOCUS)
		GUICtrlSetState($CP1, $GUI_FOCUS)
	Else
		;MsgBox(0,'Checked',BitAND(GUICtrlRead($SPBox),$GUI_CHECKED))
		_GUICtrlEdit_SetPasswordChar($CP1)
		_GUICtrlEdit_SetPasswordChar($CP2)
		GUICtrlSetState($CP2, $GUI_FOCUS)
		GUICtrlSetState($CP1, $GUI_FOCUS)
	EndIf
EndFunc   ;==>_Show_Password

Func _Save_Password()
	Local $Password1, $Password2, $wINI
	$Password1 = GUICtrlRead($CP1)
	$Password2 = GUICtrlRead($CP2)
	If $Password1 == $Password2 Then
		$wINI = IniWrite($INI_File, 'DTS Client', 'PassWord', $Password1)
		If $wINI = 0 Then
			MsgBox(16, 'Change Password - Error', 'Unable to update password in INI file.' & $ContactAdmin)
		Else
			MsgBox(64, 'Change Password - Success', 'The INI file has been updated with the new client password. Please wait up to 10 minutes for all passwords to apply.')
			_Destroy_Change_Password()
		EndIf
	Else
		MsgBox(16, 'Change Password - Error', 'The provided passwords do not match. Please try again.')
		Return -1
	EndIf
EndFunc   ;==>_Save_Password

Func _Disable_All_Monitoring()
	Local $rows
	$rows = _GUICtrlListView_GetItemCount($Transfer_List)

	For $c = 0 To $rows - 1
		_GUICtrlListView_SetItemChecked($Transfer_List, -1, False)
	Next
EndFunc   ;==>_Disable_All_Monitoring

Func _Enable_All_Monitoring()
	Local $rows
	$rows = _GUICtrlListView_GetItemCount($Transfer_List)

	For $c = 0 To $rows - 1
		_GUICtrlListView_SetItemChecked($Transfer_List, -1, True)
	Next
EndFunc   ;==>_Enable_All_Monitoring

Func _Search_Workstation($Search_Item = '', $Search_Func = 0)
	;MsgBox(0,'Search',$Search_Item&' - '&$Search_Func)
	Local $Dupe, $Verify, $Item_Index, $i = -1
	;MsgBox(0,'',GUICtrlRead($Workstation_Input))
	Switch $Search_Func
		Case 0  ; 0 is used to search for a device in the list
			$Search_Item = GUICtrlRead($Workstation_Input)
			If $Search_Item = 'Workstation or IP' Then
				MsgBox(16, 'Workstation Search - Error', 'You must first enter a valid Search parameter.' & @CRLF & @CRLF & 'Please try again.')
				Return 0
			EndIf
			$Item_Index = _GUICtrlListView_FindInText($Transfer_List, $Search_Item)
			If $Item_Index = -1 Then
				MsgBox(4160, "Workstation Search - Result", $Search_Item & ' could not be found.')
			Else
				_GUICtrlListView_EnsureVisible($Transfer_List, $Item_Index)
				_GUICtrlListView_SetItemSelected($Transfer_List, $Item_Index)
				_GUICtrlListView_ClickItem($Transfer_List, $Item_Index)
			EndIf
		Case 1 ; 1 is used for a Workstation Add
			$Item_Index = _GUICtrlListView_FindInText($Transfer_List, $Search_Item)
			If $Item_Index = -1 Then
				;MsgBox(0,'',$Item_Index)
				Return 0;Continue
			Else
				$Dupe = MsgBox(36, 'Workstation Add - Duplicate Entry', $Search_Item & ' is already listed in the DTS Transfer System' & @CRLF & @CRLF & 'Do you want to continue?')
				If $Dupe = 7 Then
					MsgBox(48, 'Workstation Add - Aborted', 'Aborting Workstation Add for ' & $Search_Item & '.', 3)
					Return -1
				ElseIf $Dupe = 6 Then
					Return 1
				EndIf
			EndIf
		Case 2 ;2 is used for Client Deployment
			$Item_Index = _GUICtrlListView_FindInText($Transfer_List, $Search_Item)
			If $Item_Index = -1 Then
				$Verify = MsgBox(36, 'Client Deployment - Not Found', $Search_Item & ' is not listed in the DTS Transfer System' & @CRLF & @CRLF & 'Do you want to continue?')
				If $Verify = 7 Then
					MsgBox(48, 'Client Deployment - Aborted', 'Aborting Client Deployment to ' & $Search_Item & '.', 3)
					Return -1
				ElseIf $Verify = 6 Then
					Return 1
				EndIf
			Else
				Return 0;Continue
			EndIf
		Case 3 ;3 is used for Client Removal
			$Item_Index = _GUICtrlListView_FindInText($Transfer_List, $Search_Item)
			If $Item_Index = -1 Then
				$Verify = MsgBox(48, 'Workstation Removal - Not Found', $Search_Item & ' is not listed in the DTS Transfer System. ')
				Return -1
			Else
				$Verify = MsgBox(36, 'Workstation Removal - Verification', $Search_Item & ' is about to be removed from the DTS Transfer System.' & @CRLF & @CRLF & 'Do you want to continue?')
				If $Verify = 7 Then
					MsgBox(48, 'Workstation Removal - Aborted', 'Aborting Workstation removal for ' & $Search_Item & '.', 3)
					Return -1
				ElseIf $Verify = 6 Then
					Return $Item_Index
				EndIf
			EndIf
		Case 4 ;4 is used for Client Log Verification
			$Item_Index = _GUICtrlListView_FindInText($Transfer_List, $Search_Item)
			If $Item_Index = -1 Then
				$Verify = MsgBox(48, 'Workstation Log Retrieval - Not Found', $Search_Item & ' is not listed in the DTS Transfer System.' & @CRLF & @CRLF & 'Do you want to continue?')
				Return -1
			Else
				Return 1
			EndIf
	EndSwitch
EndFunc   ;==>_Search_Workstation

Func _Get_Selected()
	;$Transfer_List
	Local	$rows, $Selected, $Selected_Workstations[1][2], $k, $Workstation, $Name, $ip, $sPlit

	$rows = _GUICtrlListView_GetItemCount($Transfer_List)
	;MsgBox(64,'Rows',$rows)
	$k = 0
	For $x = 0 To $rows - 1
		;	MsgBox(64,'x',$x)
		$Selected = _GUICtrlListView_GetItemSelected($Transfer_List, $x)
		;	MsgBox(64,'Selected',$Selected)
		If $Selected = 'True' Then
			ReDim $Selected_Workstations[$k + 1][2]
			;	_ArrayDisplay($Selected_Workstations)
			$Workstation = _GUICtrlListView_GetItemText($Transfer_List, $x)
			$sPlit = StringSplit($Workstation, ' - ', 1)
			$Selected_Workstations[$k][0] = $sPlit[1] ;Name
			;$Selected_Workstations[$k][1] = $sPlit[2] ;IP
			$Selected_Workstations[$k][1] = $x ;Index
			$k = $k + 1
		EndIf
	Next
	;_ArrayDisplay($Selected_Workstations)
	Return $Selected_Workstations
EndFunc

Func _Save_Changes($Save_Func = 0, $Save_Item = '')
	Local $Save, $rows, $index, $DirSize, $oFile, $List, $Check, $Temp, $x, $y, $Workstation_Info, $Delete_count, $Delete_Item
	;Removal_Func = 1 : Means multiple system removal.
	;Removal_Func = 0 : Means single system removal
	If $Save_Func = 0 Then ;Used when user initiates form GUI Save button
		$Save = MsgBox(36, 'Save Changes - Verification', 'Are you sure you want to save these changes?')
		If $Save = 7 Then Return -1
	EndIf
	ProgressOn('Workstation List Save', 'Saving changes made to list', 'Backing up current list.', Default, Default, 16)
	Sleep(1000)
	$rows = _GUICtrlListView_GetItemCount($Transfer_List)
	ProgressSet(33)
	Sleep(1000)
	$DirSize = DirGetSize($Backup_Dir, 1)
	ProgressSet(66)
	Sleep(1000)
	;MsgBox(0,'',@MON&'-'&@MDAY&'-'&StringTrimLeft(@YEAR,2)&' '&'Backup '&$DirSize[1]+1)
	FileCopy($WorkstationFile, $Backup_Dir & '\' & @MON & '-' & @MDAY & '-' & StringTrimLeft(@YEAR, 2) & ' ' & 'Backup ' & $DirSize[1] + 1 & '.csv')
	ProgressSet(100)
	Sleep(1000)
	;Build a loop to verify that the file was transfered.
	ProgressSet(0, 'Committing changes to file.')
	Sleep(1000)
	For $c = 0 To $rows - 1
		ProgressSet(100 * ($c / $rows))
		$Check = _GUICtrlListView_GetItemChecked($Transfer_List, $c)
		$Temp = _GUICtrlListView_GetItemTextArray($Transfer_List, $c)

		If $Save_Func = 1 Then ; Used when removing items
			If IsArray($Save_Item) Then
				$Delete_count = UBound($Save_Item)
				$Delete_Item = 0
				For $j = 0 To $Delete_count - 1

					If	StringInStr($Temp[1], $Save_Item[$j]) Then $Delete_Item = 1

				Next
				If $Delete_Item = 0 Then $List = $List & $Check & ',' & $Temp[1] & ',' & $Temp[2] & ',' & $Temp[3] & @CRLF
			Else
				If StringInStr($Temp[1], $Save_Item) Then
					;don't write to file
				Else
					$List = $List & $Check &',' & $Temp[1] & ',' & $Temp[2] & ',' & $Temp[3] & @CRLF
				EndIf
			EndIf
			;don't write to file
		Else
			$List = $List & $Check & ',' & $Temp[1] & ',' & $Temp[2] & ',' & $Temp[3] & @CRLF
		EndIf
		;MsgBox(0,'',$index)
	Next
	;MsgBox(0,'List',$List)
	$oFile = FileOpen($WorkstationFile, 2)
	FileWrite($oFile, $List)
	FileClose($oFile)
	ProgressSet(100, 'Save Complete')
	Sleep(1000)
	ProgressOff()
	MsgBox(0, 'Save Changes - Done', 'Save Complete.')
	_Load_TransferList()
EndFunc   ;==>_Save_Changes

Func _Log_Downloader()
	Local $Transfer, $Interface, $ClientAll, $ClientSingle, $Workstation_Search_Log, $Save_Dir, $Stamp, $Drop_Files, $Go_Get_Workstation, $Client_Log, $Verify, _
			$Ping, $Install_Path, $Transfer_Copy = 1, $Interface_Copy = 1, $All_Client_Download = 1

	$Transfer = BitAND(GUICtrlRead($Download_DTS_Transfer_Log), $GUI_CHECKED)
	$Interface = BitAND(GUICtrlRead($Download_Interface_Log), $GUI_CHECKED)
	$ClientAll = BitAND(GUICtrlRead($Download_All_Client_Logs), $GUI_CHECKED)
	$ClientSingle = BitAND(GUICtrlRead($Downlaod_Single_Client_Log), $GUI_CHECKED)

	If $ClientSingle = 1 Then
		;MsgBox(0, '', 'See it.')
		$Go_Get_Workstation = GUICtrlRead($Workstation_Log)
		If $Go_Get_Workstation = "Workstation or IP" Or $Go_Get_Workstation = '' Then
			MsgBox(16, 'Client Log Retrieval - Error', 'You must first enter a valid Workstation before downloading. Please try again.')
			Return -1
		Else
			$Workstation_Search_Log = _Search_Workstation($Go_Get_Workstation, 4)
			If $Workstation_Log = -1 Then
				$ClientSingle = 0
			Else
				$Ping = Ping($Go_Get_Workstation)
				If $Ping = 0 Then
					MsgBox(16, 'Workstation Log Retrieval - Error', 'The Workstation ' & $Go_Get_Workstation & ' cannot be pinged. The Workstation may be offline ' & _
							'or the Workstation may not be on the network. Please validate that the asset is on the network.')
					$ClientSingle = 0
				EndIf
			EndIf
		EndIf
	EndIf

	If $Transfer = 1 Or $Interface = 1 Or $ClientAll = 1 Or $ClientSingle = 1 Then
		$Save_Dir = FileSelectFolder('Where do you want to save files?', '', 7, @DesktopDir, $Log_Interface)
		If @error Then Return -1
		$Stamp = StringReplace(_NowCalc(), '/', '.')
		$Stamp = StringReplace($Stamp, ':', '.')
		$Stamp = StringReplace($Stamp, ' ', ' -- ')
		$Stamp = StringTrimLeft($Stamp, 2)
		$Drop_Files = $Save_Dir & '\DTS Transfer Logs ' & $Stamp
		;MsgBox(0, 'Dir', $Drop_Files)
		DirCreate($Drop_Files)

		If $Transfer = 1 Then $Transfer_Copy = FileCopy($Server_Program_Installation_Path & '\Logs\*', $Drop_Files & '\Server Logs\', 8)
		If $Interface = 1 Then $Interface_Copy = FileCopy($Log_Dir & '\*', $Drop_Files & '\Interface Logs\', 8)
		If $ClientAll = 1 Then $All_Client_Download = _All_Client_Logs($Drop_Files) ;FileCopy($Log_Dir & '\DTS Client Logs\Client.log', $Drop_Files & '\Client.log', 1)
		If $ClientSingle = 1 Then
			$Install_Path = IniRead($INI_File, 'DTS Client', 'Install_Location', 'Error')
			$Client_Log = '\\' & $Go_Get_Workstation & '\c$\' & $Install_Path & '\Logs\'
			;MsgBox(0,'Install Path',$Install_Path)
			If $Install_Path = 'Error' Then
				ProgressOff()
				MsgBox(16, 'Client Log Retrieval - Error', 'Unable to locate "Installation Path" in ini file. Aborting "Client Log Retrieval".')
				Return -1
			EndIf
			If Not FileExists($Client_Log) Then
				MsgBox(16, 'Client Log Retrieval - Error', 'Unable to locate "Client Log" on ' & $Go_Get_Workstation & '. Aborting "Client Log Retrieval".')
				Return -1
			EndIf
			FileCopy($Client_Log, $Drop_Files & '\Client.log', 1)
		EndIf

		If $Transfer_Copy = 0 And $Interface_Copy = 0 Then
			MsgBox(16,'Copy Error','The Server DTS Log and Interface Log could not be downloaded. '&$ContactAdmin)
			_FileWriteLog($Interface_Log,'The Server DTS Log and Interface Log could not be downloaded.')
			_FileWriteLog($ErrLog,'The Server DTS Log and Interface Log could not be downloaded.')
		ElseIf $Transfer_Copy = 0 Then
			MsgBox(16,'Copy Error','The Server DTS Log could not be downloaded. '&$ContactAdmin)
			_FileWriteLog($Interface_Log,'The Server DTS Log could not be downloaded.')
			_FileWriteLog($ErrLog,'The Server DTS Log could not be downloaded.')
		ElseIf $Interface_Copy = 0 Then
			MsgBox(16,'Copy Error','The Interface Log could not be downloaded. '&$ContactAdmin)
			_FileWriteLog($Interface_Log,'The Interface Log could not be downloaded.')
			_FileWriteLog($ErrLog,'The Interface Log could not be downloaded.')
		ElseIf $All_Client_Download = 0 Then
			MsgBox(16,'Copy Error','There were errors downloading logs from 1 or more clients. Please view the Interface Log '& _
						'click View on the Interface and select "View DTS Interface Log".')
			_FileWriteLog($Interface_Log,'There were errors downloading logs from 1 or more clients.')
			_FileWriteLog($ErrLog,'There were errors downloading logs from 1 or more clients.')
		EndIf

	Else
		MsgBox(16, 'Log Download - Error', 'Please make a selection first.')
	EndIf

EndFunc   ;==>_Log_Downloader

Func _All_Client_Logs($Drop_Files)
	Local	$rows, $aComputers[1], $Temp, $sPlit, $Install_Path, $Copy, $Failed = 0, _
			$Successful_Copy = 'Successfully Copied Files From:'&@CRLF, $Failed_Copy = 'Failed To Copy Files From:'&@CRLF

	$rows = _GUICtrlListView_GetItemCount($Transfer_List)
	If $rows = 0 Then
		MsgBox(16,'Error','There are no clients to pull logs from. Please add clients first then try again.')
	Else
		ReDim $aComputers[$rows]
		For $i = 0 To $rows - 1
			$Temp = _GUICtrlListView_GetItemText($Transfer_List, $i, 0)
			$sPlit = StringSplit($Temp,' - ', 1)
			$aComputers[$i] = $sPlit[1]
		Next

		$Install_Path = IniRead($INI_File, 'DTS Client', 'Install_Location', 'Error')
		;MsgBox(0,'Install Path',$Install_Path)
		If $Install_Path = 'Error' Then
			ProgressOff()
			MsgBox(16, 'Client Deployment - Error', 'Unable to locate "Installation Path" in ini file. Aborting "Workstation Deployment".')
			Return 0
		EndIf

		For $Computer In $aComputers ;############Test Section Below############### Does If $Copy = Filecopy really work?
			If $Copy = FileCopy('\\'&$Computer&'\'&$Install_Path&'\Logs\DTS Client.log', $Drop_Files&'\'&$Computer&' DTS.log') Then
				$Successful_Copy = $Successful_Copy &@TAB&$Computer&@CRLF
			Else
				$Failed_Copy = $Failed_Copy &@TAB&$Computer&@CRLF
				$Failed = 1
			EndIf
		Next
		_FileWriteLog($Interface_Log,'All client log file copy initiated.'&@CRLF&$Successful_Copy&$Failed_Copy&'Copy complete...')
		_FileWriteLog($ErrLog,'All client log file copy initiated.'&@CRLF&$Successful_Copy&$Failed_Copy&'Copy complete...')
	EndIf

	If $Failed = 1 Then
		Return 0
	Else
		Return 1
	EndIf
EndFunc
#EndRegion

#Region Enable/Disable Focus
;--------------------------------------------------------------------------------
;Enable/Disable Main Interface
;--------------------------------------------------------------------------------
Func _Main_Interface_State_Change()
	Local	$state
	$state = WinGetState($DTS_Interface)
		If BitAND($state, 4) Then
			GUISetState(@SW_DISABLE, $DTS_Interface)
;~ 			GUISetBkColor(0x4A5F75)
		Else
			GUISetState(@SW_ENABLE, $DTS_Interface)
			WinActivate($DTS_Interface)
		EndIf
EndFunc
#EndRegion

#Region Sub Interfaces
;--------------------------------------------------------------------------------
;Sub Interfaces
;--------------------------------------------------------------------------------

Func Error_DTS_Interface_Log()
	Local	 $Viewer, $Error_Info, $oLog_File

	$oLog_File = FileOpen($ErrLog, 0)
	$Error_Info = FileRead($oLog_File)
	FileClose($oLog_File)

	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\DTS Project\Scripts\INI Viewer.kxf
	$Error_Interface_Log_Viewer = GUICreate("Interface Log Viewer", 1004, 457, 128, 134, $WS_OVERLAPPEDWINDOW, $WS_EX_WINDOWEDGE)
	GUISetBkColor(0xFAFAFA)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_Error_Log_Interface_Viewer")
	GUISetIcon(@TempDir & '\compass.ico')
	$Viewer = GUICtrlCreateEdit("", 0, 0, 1002, 455, $ES_READONLY)
	GUICtrlSetResizing(-1, $GUI_DOCKBORDERS)
	GUICtrlSetData(-1, $Error_Info)
	GUISetState(@SW_SHOW)
	_Main_Interface_State_Change()
	#EndRegion ### END Koda GUI section ###

EndFunc

Func General_DTS_Interface_Log()
	Local	 $Viewer, $General_Info, $oLog_File

	$oLog_File = FileOpen($Interface_Log, 0)
	$General_Info = FileRead($oLog_File)
	FileClose($oLog_File)

	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\DTS Project\Scripts\INI Viewer.kxf
	$General_Interface_Log_Viewer = GUICreate("Interface Log Viewer", 1004, 457, 128, 134, $WS_OVERLAPPEDWINDOW, $WS_EX_WINDOWEDGE)
	GUISetBkColor(0xFAFAFA)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_General_Log_Interface_Viewer")
	GUISetIcon(@TempDir & '\compass.ico')
	$Viewer = GUICtrlCreateEdit("", 0, 0, 1002, 455, $ES_READONLY)
	GUICtrlSetResizing(-1, $GUI_DOCKBORDERS)
	GUICtrlSetData($Viewer, $General_Info)
	GUISetState(@SW_SHOW)
	_Main_Interface_State_Change()
	#EndRegion ### END Koda GUI section ###

EndFunc

Func _Log_Interface()
	Local $Info_Label, $Transfer_Label, $Interface_Label, $Client_Label, $Download_Logs
	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\DTS Project\Scripts\Log Interface.kxf
	$Log_Interface = GUICreate("Log Selector", 321, 338, 281, 139)
	GUISetBkColor(0xFAFAFA)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_Log_Interface")
	GUISetIcon(@TempDir & '\compass.ico')
	$Info_Label = GUICtrlCreateLabel("Please choose what logs you would like to Download.", 24, 8, 265, 56)
	GUICtrlSetFont(-1, 12, 600, 0, "MS Sans Serif")
	$Transfer_Label = GUICtrlCreateLabel("Server DTS Log", 32, 70, 120, 20)
	GUICtrlSetFont(-1, 10, 600, 0, "MS Sans Serif")
	$Interface_Label = GUICtrlCreateLabel("DTS Interface", 32, 126, 110, 20)
	GUICtrlSetFont(-1, 10, 600, 0, "MS Sans Serif")
	$Client_Label = GUICtrlCreateLabel("DTS Client", 32, 182, 99, 20)
	GUICtrlSetFont(-1, 10, 600, 0, "MS Sans Serif")
	$Download_DTS_Transfer_Log = GUICtrlCreateCheckbox("Download Log", 56, 94, 97, 17)
	$Download_Interface_Log = GUICtrlCreateCheckbox("Download Log", 56, 150, 97, 17)
	GUICtrlCreateCheckbox('Please choose one', 32, 210, 250, 50, $BS_GROUPBOX)
	$Download_All_Client_Logs = GUICtrlCreateRadio("Download All Logs", 56, 230, 97, 17)
	;GUICtrlSetOnEvent($Download_All_Client_Logs, '_CheckBox_Switch')
	$Downlaod_Single_Client_Log = GUICtrlCreateRadio("Download Single Log", 160, 230, 97, 17)
	;GUICtrlSetOnEvent($Downlaod_Single_Client_Log, '_CheckBox_Switch')
	$Workstation_Log = GUICtrlCreateInput("Workstation or IP", 160, 285, 105, 21)
	$Download_Logs = GUICtrlCreateButton("Download", 40, 280, 89, 33)
	GUICtrlSetFont(-1, 12, 400, 0, "MS Sans Serif")
	GUICtrlSetOnEvent($Download_Logs, '_Log_Downloader')
	GUISetState(@SW_SHOW)
	_Main_Interface_State_Change()
	#EndRegion ### END Koda GUI section ###
EndFunc   ;==>_Log_Interface

Func _Remove_Client_Interface()
	Local $RemoveWorkstationNameLabel, $RemoveIPLabel, $Remove
	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\DTS Project\Images\Deploy Client.kxf
	$ClientRemove = GUICreate("Client Removal", 221, 184, 193, 115)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_Client_Removal")
	GUISetIcon(@TempDir & '\compass.ico')
	$RemoveWorkstationNameInput = GUICtrlCreateInput("Workstation Name", 16, 32, 185, 21)
	$RemoveWorkstationNameLabel = GUICtrlCreateLabel("Workstation Name", 16, 8, 100, 17)
	$RemoveIP = _GUICtrlIpAddress_Create($ClientRemove, 16, 88, 153, 25)
	_GUICtrlIpAddress_Set($RemoveIP, "0.0.0.0")
	$RemoveIPLabel = GUICtrlCreateLabel("Workstation IP", 16, 70, 100, 17)
	$Remove = GUICtrlCreateButton("Remove", 56, 144, 105, 25, 0)
	GUICtrlSetOnEvent(-1, '_Remove_Client')
	GUISetState(@SW_SHOW)
	_Main_Interface_State_Change()
	#EndRegion ### END Koda GUI section ###
EndFunc   ;==>_Remove_Client_Interface

Func _Client_Deploy_Interface()
	Local $DeployWorkstationNameLabel, $DeployIPLabel, $Deploy
	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\DTS Project\Images\Deploy Client.kxf
	$ClientDeploy = GUICreate("Client Deployment", 221, 184, 193, 115)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_Deploy")
	GUISetIcon(@TempDir & '\compass.ico')
	$DeployWorkstationNameInput = GUICtrlCreateInput("Workstation Name", 16, 32, 185, 21)
	$DeployWorkstationNameLabel = GUICtrlCreateLabel("Workstation Name", 16, 8, 100, 17)
	$DeployIP = _GUICtrlIpAddress_Create($ClientDeploy, 16, 88, 153, 25)
	_GUICtrlIpAddress_Set($DeployIP, "0.0.0.0")
	$DeployIPLabel = GUICtrlCreateLabel("Workstation IP", 16, 70, 100, 17)
	$Deploy = GUICtrlCreateButton("Deploy", 56, 144, 105, 25, 0)
	GUICtrlSetOnEvent(-1, '_Deploy')
	GUISetState(@SW_SHOW)
	_Main_Interface_State_Change()
	#EndRegion ### END Koda GUI section ###
EndFunc   ;==>_Client_Deploy_Interface

Func _Add_Workstation_Interface()
	Local $AddWorkstationNameLabel, $AddIPLabel, $AddSourceLabel, $AddWorkstationButton
	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\DTS Project\Scripts\Add Computer Interface.kxf
	$AddWorkstation = GUICreate("Add Workstation", 233, 249, 1234, 363, -1, $WS_EX_ACCEPTFILES, $DTS_Interface)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_Add")
	GUISetIcon(@TempDir & '\compass.ico')
	;$AddWorkstationName = GUICtrlCreateInput("Workstation Name", 16, 40, 129, 21)
	$AddWorkstationName = GUICtrlCreateInput("4SMCNN1", 16, 40, 129, 21) ;Testing Purposes only
	;$AddIP = _GUICtrlIpAddress_Create($AddWorkstation, 16, 104, 129, 21)
	$AddIP = _GUICtrlIpAddress_Create($AddWorkstation, 16, 104, 129, 21) ;Testing Purposes only
	_GUICtrlIpAddress_Set($AddIP, "10.20.167.113")
	;$AddSourceInput = GUICtrlCreateInput("Source", 14, 163, 201, 21)
	$AddSourceInput = GUICtrlCreateInput("\\austech\is-server\djthornton\scripts\FTP Project\Testing\Source1", 14, 163, 201, 21) ;Testing Purposes only
	GUICtrlSetState($AddSourceInput, $GUI_DROPACCEPTED)
	;$AddDestinationInput = GUICtrlCreateInput("Destination", 14, 217, 201, 21)
	$AddWorkstationNameLabel = GUICtrlCreateLabel("Workstation Name", 16, 16, 100, 17)
	$AddIPLabel = GUICtrlCreateLabel("Workstation IP", 16, 80, 100, 17)
	$AddSourceLabel = GUICtrlCreateLabel("Source Location", 16, 144, 82, 17)
	$AddWorkstationButton = GUICtrlCreateButton("Add", 64, 208, 97, 25)
	GUICtrlSetOnEvent($AddWorkstationButton, '_Add_Workstation')
	GUISetState(@SW_SHOW)
	_Main_Interface_State_Change()
	#EndRegion ### END Koda GUI section ###
EndFunc   ;==>_Add_Workstation_Interface

Func _INI_Viewer()
	Local $Viewer, $oINI_File, $INI_Info
	$oINI_File = FileOpen($INI_File, 0)
	$INI_Info = FileRead($oINI_File)
	FileClose($oINI_File)

	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\DTS Project\Scripts\INI Viewer.kxf
	$INI_Viewer = GUICreate("INI Viewer", 1004, 457, 128, 134, $WS_OVERLAPPEDWINDOW, $WS_EX_WINDOWEDGE)
	GUISetBkColor(0xFAFAFA)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_INI_Viewer")
	GUISetIcon(@TempDir & '\compass.ico')
	$Viewer = GUICtrlCreateEdit("", 0, 0, 1002, 455, $ES_READONLY)
	GUICtrlSetResizing(-1, $GUI_DOCKBORDERS)
	GUICtrlSetData(-1, $INI_Info)
	GUISetState(@SW_SHOW)
	_Main_Interface_State_Change()
	#EndRegion ### END Koda GUI section ###
EndFunc   ;==>_INI_Viewer
#EndRegion

#Region Destroy Interfaces
;--------------------------------------------------------------------------------
;Destroy Interfaces
;--------------------------------------------------------------------------------
Func _Exit_Interface()
	;Delete the GUI then end the script
	_GUICtrlListView_UnRegisterSortCallBack($Transfer_List)
;~ 	GUIRegisterMsg($WM_NOTIFY, "")
	GUIDelete($DTS_Interface)
	Exit
EndFunc   ;==>_Exit_Interface

Func _Destroy_Add()
	GUIDelete($AddWorkstation)
	_Main_Interface_State_Change()
EndFunc   ;==>_Destroy_Add

Func _Destroy_Remove()
	GUIDelete($RemoveWorkstation)
EndFunc   ;==>_Destroy_Remove

Func _Destroy_Deploy()
	GUIDelete($ClientDeploy)
	_Main_Interface_State_Change()
EndFunc   ;==>_Destroy_Deploy

Func _Destroy_Client_Removal()
	GUIDelete($ClientRemove)
	_Main_Interface_State_Change()
EndFunc   ;==>_Destroy_Client_Removal

Func _Destroy_Change_Password()
	GUIDelete($Change_Password_GUI)
	_Main_Interface_State_Change()
EndFunc   ;==>_Destroy_Change_Password

Func _Destroy_INI_Viewer()
	GUIDelete($INI_Viewer)
	_Main_Interface_State_Change()
EndFunc   ;==>_Destroy_INI_Viewer

Func _Destroy_General_Log_Interface_Viewer()
	GUIDelete($General_Interface_Log_Viewer)
	_Main_Interface_State_Change()
EndFunc

Func _Destroy_Error_Log_Interface_Viewer()
	GUIDelete($Error_Interface_Log_Viewer)
	_Main_Interface_State_Change()
EndFunc

Func _Destroy_Log_Interface()
	GUIDelete($Log_Interface)
	_Main_Interface_State_Change()
EndFunc   ;==>_Destroy_Log_Interface

Func MyErrFunc()

	Local $HexNumber

	$HexNumber = Hex($oMyError.number, 8)

	If $HexNumber = '800708AC' Then Return 'Does Not Exist'

	MsgBox(0, "", "We intercepted a COM Error ! Please take note of the information below and provide it to your System Administrator." & @CRLF & _
			"Number is: " & $HexNumber & @CRLF & _
			"Linenbr is: " & $oMyError.scriptline & @CRLF & _
			"Description is: " & $oMyError.description & @CRLF & _
			"Windescription is: " & $oMyError.windescription)

	SetError(1); something to check for when this function returns
EndFunc   ;==>MyErrFunc
#EndRegion
