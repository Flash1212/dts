;Installer

;This programs function is to install the FTP DTF Server Side application
Opt('MustDeclareVars', 1)

FileInstall('\\austech\is-si\Development\icons\compass.ico',@TempDir&'\compass.ico',1)


;GUISetIcon(@TempDir&'\compass.ico')

RegWrite("HKEY_LOCAL_MACHINE\SOFTWARE\FTP Transfer\System Error Count")

DirCreate(@ProgramFilesDir & '\FTP DTF\Images')
DirCreate(@ProgramFilesDir & '\FTP DTF\Scripts')
DirCreate(@ProgramFilesDir & '\FTP DTF\Logs')
DirCreate(@ProgramFilesDir & '\FTP DTF\Computers')
DirCreate(@ProgramFilesDir & '\FTP DTF\Computers\Backups')