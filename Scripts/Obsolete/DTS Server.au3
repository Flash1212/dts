#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_icon=Images\Download.ico
#AutoIt3Wrapper_UseX64=n
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#cs
	DTS DTF - Server Side application
	The purpose of this portion of the application is to perform all the primary file transfers.
	This script will hold not only the file transfer portion but also the notification and password change script.
#ce

Opt('MustDeclareVars', 1)

#include <Date.au3>
#include <file.au3>
#include <Inet.au3>
#include <array.au3>
#include <WinApi.au3>
#include <Services.au3>

#Region Begin Declarations
;Declarations

;Log files
Global 	$ClientLog = @ScriptDir&'\Logs\Clients.log', _
		$SystemErrorLog = @ScriptDir&'\Logs\Transfer Service Error.log', _
		$ClientErrorLog = @ScriptDir&'\Logs\Client Error.log', _
		$ClientErrorCount = @ScriptDir&'\Logs\Error Count.log', _
		$WriteError[4], _
		$Boot = True, _
		$aWorkstationErrors[1][2]
		$WriteError[0] = 'ATTENTION!!!'
		$WriteError[1] = ''
		$WriteError[2] = 'The following System(s) are not transfering downtime files. Please contact a field engineer for initial investigation.'
		$WriteError[3] = ''

Dim $Sleep , $ContactAdmin = ' Please contact your System Administrator.'

Global	$INI_File = @ScriptDir&'\Settings\Configurations.ini'
		If Not FileExists($INI_File) Then
			;;MsgBox(16, 'Error', 'Unable to locate Configuration file.',20)
			_Notification('No INI file', 'Server')
			MsgBox(64,'Test', 'No INI file found')
			Exit
		EndIf


#EndRegion Declarations

If $CMDLineRaw = '/setup' Then
	MsgBox(64,'DTS Server Service', 'Successfully created DTS Service')
	_Service_Stop('DTS Transfer '&$CMDLine[2]);stop the service
	Exit
EndIf

While 1
	;MsgBox(64,'Begin','Enterig script')
	$Sleep = IniRead($INI_File,'DTS Client', 'Sleep','Error')
	If $Sleep = 'Error' Then
;~		MsgBox(64,'Sleep - Error','Sleep not found.', 20)
		_Notification('No wait interval', 'Server')
		$Sleep = 10 ;set default time minutes
	EndIf
;~ 	MsgBox(64,'Main','')
	_Main()
	Sleep(Number($Sleep) * 60000)
WEnd

Func _Main()
	;MsgBox(64,'Main','Entered')
	Local 	$WorkstationFile, $UserName, $Password, $Install_Path, $err, $Identity_Split, $y, $WorkstationName, $WorkstationIP, $Workstations, _
			$Source, $Destination, $Monitor, $Attribute, $Validation, $Transfer_List, $Transfer, $Workstation_Info, $aWorkstations[1][5], $errAdd

	ReDim $WriteError[4]
#Region Configs
	;$WorkstationFile = $Program_Installation_Path&'\Computers\'&IniRead($INI_File, 'DTS Interface','Workstations', 'Unknown') ;This is where all transfer systems are housed
	$WorkstationFile =  @ScriptDir&'\computers\'&IniRead($INI_File, 'DTS Server', 'Workstations', 'Unknown') ;This is where all transfer systems are housed
;~ 	MsgBox(64,'Workstation file', $WorkstationFile)
	If Not FileExists($WorkstationFile) Then
		MsgBox(16, 'Error', 'Unable to locate Workstation.csv file: '&$WorkstationFile,20)
		_Notification('No Workstation File', 'Server')
		Return -1
	EndIf
	$UserName = IniRead($INI_File, 'DTS Client', 'Username', 'Error')
;~ 	MsgBox(0,'Username',$UserName)
	If $UserName = 'Error' Then
		MsgBox(16, 'Client Deployment - Error', 'Unable to locate "Username" in ini file.Aborting "Workstation Add".',20)
		_Notification('No Username','Server')
		Return -1
	EndIf
	$Password = IniRead($INI_File, 'DTS Client', 'Password', 'Error')
;~ 	MsgBox(0,'Password',$Password)
	If $Password = 'Error' Then
		MsgBox(16, 'Client Deployment - Error', 'Unable to locate "Password" in ini file. Aborting "Workstation Add".',20)
		_Notification('No Password', 'Server')
		Return -1
	EndIf
#cs	$Install_Path = IniRead($INI_File, 'DTS Client', 'Install_Location', 'Error')
	;MsgBox(0,'Install Path',$Install_Path)
	If $Install_Path = 'Error' Then
		;;MsgBox(16, 'Client Deployment - Error', 'Unable to locate "Installation Path" in ini file. Aborting "Workstation Add".',20)
		_Notification('No Install Location', 'Server')
		Return -1
#ce	EndIf
	_FileReadToArray($WorkstationFile, $Workstations)
	$err = @error
	If $err = 1  Then
		MsgBox(16, "Error", " Error reading log to Array     error:" & @error, 20)
		_Notification('No Workstation File Load', 'Server')
		Return -1
	ElseIf $err = 2 Then
		_FileWriteLog($SystemErrorLog, 'Workstation file is empty.')
		Return -1
	EndIf
;~ 	MsgBox(64,'Settings','All lines read.')
	_ArrayDelete($Workstations, 0)
;~ 	_ArrayDisplay($Workstations)
#EndRegion Confisg
	_Array_Create($Workstations, $aWorkstations)
;~	$aWorkstations[$x][0] = Monitor, $aWorkstations[$x][1] = Name, $aWorkstations[$x][2] = IP
;~	$aWorkstations[$x][3] = Src, $aWorkstations[$x][4] = Dest
	If IsArray($aWorkstations) Then
		For $x = 0 To UBound($aWorkstations) - 1 ; x is now in play. Do not use in new loops
			$errAdd = False
			;_Registry_Add($WorkstationName)
			$Validation = _Validate_Host_Alive($aWorkstations[$x][1], $aWorkstations[$x][2], $aWorkstations[$x][0])
			If $Validation = - 1 Then
				MsgBox(64,'Validation - Failure','Skipping system.', 15)
				_Notification('No Validation', $aWorkstations[$x][1])
;~ 				_Error_Logger_Add($aWorkstations[$x][1], $aWorkstations[$x][2]);Not needed added in Validate func
				;skip
			Else
;~				MsgBox(64,'Validation - Successful','Submitting for account creation.')
				;_Account_Setup($aWorkstations[$x][1], $aWorkstations[$x][0], $UserName, $Password, $Install_Path)
				$Transfer_List = _Mod_Date($aWorkstations[$x][3], $aWorkstations[$x][4])
;~				_ArrayDisplay($Transfer_List, 'Transfer List')
;~				$Stamp_Check = _Time_Format($aWorkstations[$x][3], $aWorkstations[$x][4])
				If IsArray($Transfer_List) Then
					;$aWorkstations[$x][0] = Monitor, $aWorkstations[$x][1] = Name, $aWorkstations[$x][2] = IP
					;$aWorkstations[$x][3] = Src, $aWorkstations[$x][4] = Dest
					For $y = 0 To UBound($Transfer_List) - 1
						If $Transfer_List[$y][1] = True Then
							$Transfer = _File_Transfer($aWorkstations[$x][0], $aWorkstations[$x][1], $aWorkstations[$x][2], $Transfer_List[$y][0], $aWorkstations[$x][4], $Transfer_List[$y][2])
							If $Transfer = -1 Then $errAdd = True
						EndIf
					Next
					If $errAdd = False Then
						_Error_Logger_Remove($aWorkstations[$x][1])
					Else
						_Error_Logger_Add($aWorkstations[$x][1], $aWorkstations[$x][2], $aWorkstations[$x][0])
					EndIf
;~					MsgBox(64,'Transfer','Finish transferring docs')
				ElseIf $Transfer_List = 'Error' Then
					_Notification('Error Source Files', $aWorkstations[$x][1])
					_Error_Logger_Add($aWorkstations[$x][1], $aWorkstations[$x][2], $aWorkstations[$x][0])
					;MsgBox(64,'Stamp Check','No transfer needed. Moving on')
				ElseIf $Transfer_List = 'Empty' Then
					_Notification('Empty Source Files', $aWorkstations[$x][1])
				EndIf
			EndIf

		Next
		_Write_Error_Log()
;~		MsgBox(64,'Error Size',UBound($WriteError))
		If UBound($WriteError) > 4 Then _Email_Notification()
	Else
		_Notification('No Workstations')
	EndIf
EndFunc   ;==>_Main

Func _Array_Create($Workstations, ByRef $aWorkstations)
	Local	$Workstation_Info, $Identity_Split, $Found, $WorkstationsSize, $ErrorsSize, $iNdex, $Error_Rows, $Exist, $err, $aTemp, _
			$sPlit, $fRead
	For $z = 0 To UBound($Workstations) - 1
		ReDim $aWorkstations[$z + 1][5]
		$Workstation_Info = StringSplit($Workstations[$z], ',')
;~ 		$Identity_Split = StringSplit($Workstation_Info[1], ' - ', 1)
;~ 		_ArrayDisplay($Workstation_Info)
		;_ArrayDisplay($Identity_Split)
		;;MsgBox(0,'Name',$Identity_Split[1])
		;;MsgBox(0,'IP',$Identity_Split[2])
		$aWorkstations[$z][0] = $Workstation_Info[1] ;Monitor
		$aWorkstations[$z][1] = $Workstation_Info[2] ;Name
		$aWorkstations[$z][2] = $Workstation_Info[3] ; IP
		$aWorkstations[$z][3] = $Workstation_Info[4] ;Source
		$aWorkstations[$z ][4] = $Workstation_Info[5] ;Dest directory
		$Workstation_Info = ''
	Next
;~ 	_ArrayDisplay($aWorkstations, 'Array Workstations')
	;MsgBox(64,'Error Array Create','')
	If $Boot = True Then
		;MsgBox(64,'Boot',$Boot)
		If FileExists($ClientErrorCount) Then
			$fRead = FileRead($ClientErrorCount)
		;	MsgBox(64,'File',$ClientErrorCount)
			If $fRead = '' Then
				$fRead = 'Empty'
			Else
				If _FileReadToArray($ClientErrorCount, $aTemp) Then ;Rebuild error count array
					_ArrayDelete($aTemp, 0)
					ReDim $aWorkstationErrors[UBound($aTemp)][2]
;~					_ArrayDisplay($aTemp,'aTemp')
;~					_ArrayDisplay($aWorkstationErrors, 'Errors')
					For $y = 0 To UBound($aTemp) - 1
						$sPlit = StringSplit($aTemp[$y],',')
						;_ArrayDisplay($sPlit)
						$aWorkstationErrors[$y][0] = $sPlit[1] ;Name
						$aWorkstationErrors[$y][1] = $sPlit[2] ;Error Count
					Next
;~					_ArrayDisplay($aWorkstationErrors,'aTemp')
				Else
					_Notification('Client Error File Read')
	;~ 				MsgBox(16,'Error','Failed to read Error Count File.')
				EndIf
			EndIf
			$Boot = False
		EndIf
	EndIf

;~ 	_ArrayDisplay($aWorkstations, 'Array of Workstation info - 1')
;~ 	_ArrayDisplay($aWorkstationErrors, 'Array of Workstation Error - 1')

	If IsArray($aWorkstations) Then
		For $y = 0 To UBound($aWorkstations) - 1
			$Found = 0
			$Error_Rows = UBound($aWorkstationErrors)
			For $z = 0 To $Error_Rows - 1
				If $aWorkstations[$y][1] = $aWorkstationErrors[$z][0] Then ; If workstation found in error log note found
					$Found = 1
					ExitLoop
				EndIf
			Next
			If $Found = 0 Then ; If not found then add system name with 0 error count
				MsgBox(64,'Not Found in eror array','Adding '&$aWorkstations[$y][1])
				If $aWorkstationErrors[0][0] = '' Then
				Else
					ReDim $aWorkstationErrors[$Error_Rows + 1][2] ; Increase array size for new entry
				EndIf
				$aWorkstationErrors[UBound($aWorkstationErrors) - 1][0] = $aWorkstations[$y][1] ;Name
				$aWorkstationErrors[UBound($aWorkstationErrors) - 1][1] = 0 ;Count
			EndIf
		Next

		$WorkstationsSize = UBound($aWorkstations)
		$ErrorsSize = UBound($aWorkstationErrors)

		;This section is old system cleanup function
		If $ErrorsSize > $WorkstationsSize Then ; If a system has been removed from the list remove it from the array. (I think :) - noted 2 years laters)
			$z = 0
			For $w = 0 To $ErrorsSize - 1
				$Found = 0
				For $u = 0 To $WorkstationsSize - 1
					If $aWorkstationErrors[$w - $z][0] = $aWorkstations[$u][1] Then
;~ 						MsgBox(64,'Remove',$aWorkstationErrors[$w - $z][0] &' - '& $aWorkstations[$u][1])
						$Found = 1
						ExitLoop
					EndIf
				Next
				If $Found = 0 Then
					_ArrayDelete($aWorkstationErrors, $w - $z)
					$z += 1 ; account for lines removed so you don't exceed dimension range
				EndIf
			Next
		EndIf

		$Workstations = ''
;~ 		_ArrayDisplay($aWorkstations, 'Array of Workstation info - 3')
;~ 		_ArrayDisplay($aWorkstationErrors, 'Array of Workstation Error - 3')
;~ 		MsgBox(64,'Exiting','Array Creator')
	Else
		Return 'No Workstations'
	EndIf
EndFunc

Func _Validate_Host_Alive($WorkstationName, $WorkstationIP, $Monitor)
	;MsgBox(64,'Validating Host','Validating '&$WorkstationName&'.')
	Local $IP
	TCPStartup()
		$IP = TCPNameToIP($WorkstationName)
	TCPShutdown()

	If $WorkstationIP <> $IP Then
		;MsgBox(64,'Validation - Conflict','The IP conflicted with DNS.')
		_Error_Logger_Add($WorkstationName, $WorkstationIP, $Monitor)
		_Notification('DNS Conflict', $WorkstationName)
		Return -1
	Else
		If Ping($WorkstationIP) > 0 Then
			;MsgBox(64,'Validation - Ping','Successful ping for '&$WorkstationName&'.')
			Return 1
		Else
			;MsgBox(64,'Validation - Ping','There was no Ping for '&$WorkstationName&'.')
			_Error_Logger_Add($WorkstationName, $WorkstationIP, $Monitor)
			_Notification('No Ping', $WorkstationName)
			Return -1
		EndIf
	EndIf
EndFunc

#cs
Func _Account_Setup($WorkstationIP, $WorkstationName,$UserName, $Password, $Install_Path)
	Local 	$Deploy_Search_Name, $Dupe, $objUser, $objGroup, $colAccounts, $oldFlags, _
			$newFlags, $ExistUser, $User, $Existgroup, $objUsers

	$Existgroup = 0
; Init objects
	Const $ADS_UF_DONT_EXPIRE_PASSWD = 0X10000
	Const $ADS_UF_PASSWD_CANT_CHANGE = 0X40

	; Check if account exists .. if not create it
	$objUser = ObjGet("WinNT://" & $WorkstationIP & "/" & $UserName)
	If @error Then
		;MsgBox(0,'Not Detected','No account was detected. Setting up now.')
		$colAccounts = ObjGet("WinNT://" & $WorkstationName & "")
		$objUser = $colAccounts.Create("user", $UserName)
		$objUser.SetPassword($Password)
		$objUser.Put("Fullname", "DTS Account")
		$objUser.Put("Description", "DTS Transfer Account")
		$objUser.SetInfo
		;Set params
		$oldFlags = $objUser.Get("UserFlags")
		$newFlags = BitOR($oldFlags, $ADS_UF_DONT_EXPIRE_PASSWD)
		$objUser.Put("UserFlags", $newFlags) ;expire the password
		$objUser.SetInfo
		$oldFlags = $objUser.Get("UserFlags")
		$newFlags = BitOR($oldFlags, $ADS_UF_PASSWD_CANT_CHANGE)
		$objUser.Put("UserFlags", $newFlags) ;expire the password
		$objUser.SetInfo
	Else
		$ExistUser = 1 ;User already exist
		$objUser.SetPassword($Password)
		$objUser.SetInfo
		;MsgBox(0,'Detected','The account was detected. Skipping account creation.')
	EndIf
	; Read current settings and Bitor to ensure the "Don't expire password swith is on and user cannot change password"

	;Add User to users group**********************************************
	$objGroup = ObjGet("WinNT://" & $WorkstationName & "/Users,group")

	For $objUsers In $objGroup.Members
		If ($objUsers.AdsPath) = 'winNT://SETON/' & $WorkstationName & '/' & $UserName Then $Existgroup = 1
	Next

	If $Existgroup = 0 Then $objGroup.Add($objUser.ADsPath)
	;*********************************************************************
	_Dir_Setup($Install_Path, $WorkstationIP, $WorkstationName, $UserName)
EndFunc   ;==>_Account_Setup

Func _Dir_Setup($Install_Path, $WorkstationIP, $WorkstationName, $UserName)
	;;MsgBox(0,'Dir setup','')
	Local $acls, $Success, $ErrorCounter = 0
	DirCreate('\\' & $Workstation & '\c$\' & $Install_Path & '\Logs\')
	DirCreate('\\' & $Workstation & '\c$\' & $Install_Path & '\Files\)
	$acls = ('cacls.exe \\' & $WorkstationIP & '\c$\DTS Transfers\ /E /T /C /G ' & $UserName & ':R')
	;;MsgBox(0,'',$acls)
	Run(@ComSpec & " /C " & $acls, "", @SW_HIDE);_RunDOS($acls)
EndFunc   ;==>_Dir_Setup
#ce

Func _Mod_Date($SRC, $DES)
	Local $sList, $dList, $aMod[1][3], $sDate, $dDate
	$sList = _Search_Folder($SRC)
	If $sList = 'Empty' Then
		_FileWriteLog($SystemErrorLog, '"'&$SRC&'" is empty.')
		Return 'Empty'
	ElseIf $sList = 'Error' Then
		_FileWriteLog($SystemErrorLog, '"'&$SRC&'" could not be accessed.')
		Return 'Error'
	EndIf
	$dList = _Search_Folder($DES)

	;_Compare($SRC, $sList, $DES, $dList)
	ReDim $aMod[UBound($sList)][3]
	For $z = 0 To UBound($sList) - 1 ;Build modify array with same number of rows as source list. This array will be returned
		$aMod[$z][0] = $SRC&$sList[$z] ;Build in full path
		$aMod[$z][1] = True
		$aMod[$z][2] = $sList[$z]
	Next
	;_ArrayDisplay($aMod)

	If $dList = 'Empty' Or $dList = 'Error' Then
		Return $aMod;Transfer everything
	Else
		For $z = 0 To UBound($sList) - 1
			For $y = 0 To UBound($dList) - 1
				If $sList[$z] = $dList[$y] Then
					;Determine if source file exist on client. If found then True and compare. Else if not found set for transfer
					$sDate = FileGetTime($aMod[$z][0], 0, 1)
					$dDate = FileGetTime($DES&$dList[$y], 0, 1)
					If $SDate = 0 Or $dDate = 0 Then ExitLoop
					If $sDate = $dDate Then $aMod[$z][1] = False
					ExitLoop
					;$Mod_Flag = _Mod_Date($aMod[$z][0], $DES&$dList[$y])
				EndIf
			Next

		Next
		Return $aMod
	EndIf
EndFunc   ;==>_List

Func _Search_Folder($Folder)
	Local $aList
;~	MsgBox(64,'Folder',$Folder)
	$aList = _FileListToArray($Folder, '*', 1)
	If @error = 1 Then
;~		MsgBox(64,'List to Array Error', 'Error - 1')
		Return 'Error'
	ElseIf @error = 4 Then
;~		MsgBox(64,'List to Array Error', 'Empty - 4')
		Return 'Empty'
	EndIf
	_ArrayDelete($aList, 0)
	;_ArrayDisplay($aList, 'List')
	$Folder = ''
	Return $aList

EndFunc

Func _File_Transfer($Monitor, $WorkstationName, $WorkstationIP, $SRC, $DES, $fName)
	Local $Exist, $Cur_SRC, $New_DES, $copy, $FileName

	If Not FileExists($SRC) Then
		;_FileWriteLog($SystemErrorLog,
		MsgBox(64,'Source File','Source File could not be found.')
		_Notification('No Source File', $WorkstationName&': '&$SRC)
;~		_Error_Logger_Add($WorkstationName, $WorkstationIP, $Monitor)
		Return -1
	EndIf
	;$FileName = _Get_File_Name($SRC)
	If Not FileExists($DES) Then
		;_FileWriteLog($SystemErrorLog,
;~		MsgBox(64,'Destination directory','Destination directory could not be found.')
		_Notification('No Destination Directory', $WorkstationName&': '&$DES)
;~		_Error_Logger_Add($WorkstationName, $WorkstationIP, $Monitor)
		Return -1
	EndIf
	If Not FileCopy($SRC, $DES, 1) Then
		;_FileWriteLog($SystemErrorLog,
		MsgBox(64,'Filecopy','The filecopy failed.')
		_Notification('No File Transfer', $WorkstationName&': '&$SRC)
;~		_Error_Logger_Add($WorkstationName, $WorkstationIP, $Monitor)
		Return -1
	Else
		;_FileWriteLog($Dat,&','&$WorkstationName&','&@HOUR&':'&@MIN&':'&@SEC)
;~		MsgBox(64,'Success','Succsseful transfer')
	EndIf
;~	MsgBox(64,'Check', 'Check Destination')
	$DES = $DES&$fName
	If Not FileExists($DES) Then
		MsgBox(64,'Destination File','The destination file could not be found.')
		;_FileWriteLog($SystemErrorLog,
		_Notification('No Destination File', $WorkstationName&': '&$DES)
;~		_Error_Logger_Add($WorkstationName, $WorkstationIP, $Monitor)
		Return -1
	EndIf

	$Cur_SRC = FileGetTime($SRC, 0, 1)
	;$Cur_SRC = StringTrimRight($Cur_SRC,2)
	$New_DES = FileGetTime($DES, 0, 1)
	;$New_DES = StringTrimRight($New_DES,2)
	;MsgBox(64,'Comparison','Source '&$Cur_SRC&@CRLF&$SRC&' - '&@CRLF&'Destination '&$New_DES&@CRLF&$DES)

	If $Cur_SRC <> $New_DES Then
		MsgBox(64,'Source - Dest Cross Reference','The source and destination file do not have the same time stamp.')
		_Notification('No File Transfer', $WorkstationName)
;~		_Error_Logger_Add($WorkstationName, $WorkstationIP, $Monitor)
		Return -1
	Else
;~		MsgBox(64,'Source - Dest Cross Referemce', 'Both files have the same time stamp.')
;~		_Error_Logger_Remove($WorkstationName)
	EndIf

EndFunc   ;==>_File_Transfer

;Add systems to error log
Func _Error_Logger_Add($WorkstationName, $WorkstationIP, $Monitor)
;~	MsgBox(64,'Error Logger Add', $WorkstationName&'	-	'&$WorkstationIP)
	Local $ErrorCount, $Decide, $iNdex
	For $a = 0 To UBound($aWorkstationErrors) - 1
		If $WorkstationName = $aWorkstationErrors[$a][0] Then ; If the workstation is found note what index
			$iNdex = $a
			ExitLoop
		EndIf
	Next
	$ErrorCount = $aWorkstationErrors[$iNdex][1] ; Look at its index and determine how many failures have occured
	$ErrorCount += 1
	$Decide = $ErrorCount/3
	If $ErrorCount = 3 Or (StringIsInt($Decide) And $ErrorCount > 1) Then
;~		MsgBox(64,'Reporting', $ErrorCount)
		If $Monitor <> 'No' Then _Report_Error($WorkstationName, $WorkstationIP, $ErrorCount)
	EndIf
;~	MsgBox(64,'Error Loger','Adding error to '&$WorkstationName&', which now has '&$ErrorCount&' errors.')
;~	_ArrayDisplay($aWorkstationErrors, 'Before')
	$aWorkstationErrors[$iNdex][1] = $ErrorCount
;~	_ArrayDisplay($aWorkstationErrors, 'After')

	;_Write_Error_Log($aWorkstationErrors)
EndFunc

Func _Report_Error($WorkstationName, $WorkstationIP, $ErrorCount)
	Local $Indexes
;~	MsgBox(64,'Error Report','Reporting errors for '&$WorkstationName&' with a total count of '&$ErrorCount&' errors.')
	$Indexes = UBound($WriteError)
	ReDim $WriteError[$Indexes + 3] ; Add 2 extra lines for "Workstation - IP" AND "space"
;~	_ArrayDisplay($WriteError)
	$WriteError[$Indexes] = $WorkstationName & ' - ' & $WorkstationIP
	$WriteError[$Indexes + 1] = 'This device has failed at '&$ErrorCount&' attempts to transfer.'
;~	_ArrayDisplay($WriteError)
	$WriteError[$Indexes + 2] = ''; space for next entry
EndFunc

Func _Error_Logger_Remove($WorkstationName)
;~	MsgBox(64,'Removing Error Count','Resetting '&$WorkstationName&' error count to 0.')
	Local	$iNdex
	For $b = 0 To UBound($aWorkstationErrors) - 1
		If $WorkstationName == $aWorkstationErrors[$b][0] Then
			$iNdex = $b
			ExitLoop
		EndIf
	Next
	$aWorkstationErrors[$iNdex][1] = 0
;~	_ArrayDisplay($aWorkstationErrors, 'After')
EndFunc

Func _Notification($Problem, $WorkstationName = '')
;~	MsgBox(64,'Notification', $Problem& ' - ' &$WorkstationName)
	Local $ErrorCount, $iNdex
	If IsArray($aWorkstationErrors) Then
		For $c = 0 To UBound($aWorkstationErrors) - 1
			If $WorkstationName = $aWorkstationErrors[$c][0] Then
				$ErrorCount = $aWorkstationErrors[$c][1]
				;MsgBox(64,'Error Count',$ErrorCount)
				ExitLoop
			EndIf
		Next
	EndIf

	Select
		Case $Problem = 'No Install Path'
			_FileWriteLog($SystemErrorLog,"The System was unable to determine the program's installation path."&$ContactAdmin)
		Case $Problem = 'No wait interval'
			_FileWriteLog($SystemErrorLog,"The System was unable to determine the wait interval."&$ContactAdmin)
		Case $Problem = 'No INI file'
			_FileWriteLog($SystemErrorLog,"The System was unable to determine the location of the program's INI file."&$ContactAdmin)
		Case $Problem = 'No Workstation File'
			_FileWriteLog($SystemErrorLog,"The System was unable to determine the location of the program's Computer list."&$ContactAdmin)
		Case $Problem = 'No Username'
			_FileWriteLog($SystemErrorLog,"The System was unable to determine the Username for deployments."&$ContactAdmin)
		Case $Problem = 'No Password'
			_FileWriteLog($SystemErrorLog,"The System was unable to determine the Password for deployments."&$ContactAdmin)
		Case $Problem = 'No Install Location'
			_FileWriteLog($SystemErrorLog,"The System was unable to determine the Client Install directory for deployments."&$ContactAdmin)
		Case $Problem = 'No Workstation File Load'
			_FileWriteLog($SystemErrorLog,"The System was unable to load the Computer list."&$ContactAdmin)
		Case $Problem = 'No Workstations'
			_FileWriteLog($SystemErrorLog,"The System was unable to  find computers in the Computer list."&$ContactAdmin)
		Case $Problem = 'Client Error File read'
			_FileWriteLog($SystemErrorLog,"The system was unable to read the client error log."&$ContactAdmin)
		Case $Problem = 'No Source File'
			_FileWriteLog($ClientErrorLog,"The System was unable to locate the source file for "&$WorkstationName&".")
		Case $Problem = 'No Destination Directory'
			_FileWriteLog($ClientErrorLog,"The System was unable to locate the destination directory for "&$WorkstationName&".")
		Case $Problem = 'Error Source Files'
			_FileWriteLog($ClientErrorLog,"The System was unable to access the source files for "&$WorkstationName&".")
		Case $Problem = 'Empty Source Files'
			_FileWriteLog($ClientErrorLog,"The System was unable to locate source files for "&$WorkstationName&".")
		Case $Problem = 'No File Transfer'
			_FileWriteLog($ClientErrorLog,"The System was unable to transfer the source file from "&$WorkstationName&" for unknown reasons."&$ContactAdmin)
		Case $Problem = 'No Destination File'
			_FileWriteLog($ClientErrorLog,"The the client on "&$WorkstationName&" appears to have not recieved the file for unknown reasons."&$ContactAdmin)
		Case $Problem = 'DNS Conflict'
			_FileWriteLog($ClientErrorLog,"The provided static IP did not match the DNS entry for "&$WorkstationName&".")
		Case $Problem = 'No Ping'
			_FileWriteLog($ClientErrorLog,"The workstation "&$WorkstationName&" could not be pinged. Please validate that the system is on line.")
		Case $Problem = 'No Validation'
			If $ErrorCount = 1 Then $ErrorCount &= 'st'
			If $ErrorCount = 2 Then $ErrorCount &= 'nd'
			If $ErrorCount = 3 Then $ErrorCount &= 'rd'
			If $ErrorCount > 3 Then $ErrorCount &= 'th'
			_FileWriteLog($ClientErrorLog,"The workstation "&$WorkstationName&" could not be detected. The transfer process has been aborted. This is the "& _
											$ErrorCount&" time this system has failed transfer.")
	EndSelect
	;MsgBox(64,'Notification Activated','A notification with this probelm has been logged: '&$Problem)
EndFunc   ;==>_Notification

Func _Email_Notification()
	Local	$SmtpServer, $FromName, $FromAddress, $ToAddress, $Subject, $Helo, $Response, $err
;~		MsgBox(64, 'Emailing errors','Emailing all reported errors.')

	$SmtpServer = IniRead($INI_File, 'Server', 'SMTP', 'Unknown')
		If $SmtpServer = 'Unknown' Then $SmtpServer = 'Ausexfe.seton.org'
	$FromName = IniRead($INI_File, 'Server', 'FromName', 'Unknown')
		If $FromName = 'Unknown' Then $FromName = 'DTS Notifications'
	$FromAddress = IniRead($INI_File, 'Server', 'FromEmail', 'Unknown')
		If $FromAddress = 'Unknown' Then $FromAddress = @ComputerName&'-DTS@seton.org'
	$Subject = 'This is a test of the DTS Transfer Notification System'
	$ToAddress = IniRead($INI_File, 'Server', 'ToEmail', 'Unknown')
		If $ToAddress = 'Unknown' Then $ToAddress = 'djthornton@seton.org'
	$Helo = "EHLO "
;~	MsgBox(64,'SMTP', $SmtpServer&@crlf&$FromName&@crlf&$FromAddress&@crlf&$ToAddress&@crlf&$Subject&@crlf&$WriteError)
	$Response = _INetSmtpMail ( $SmtpServer, $FromName, $FromAddress, $ToAddress ,$Subject ,$WriteError, $Helo, -1)
	$err = @error
	If $Response = 1 Then
		MsgBox(0, "Success!", "Mail sent", 2)
	Else
		MsgBox(0, "Error!", "Mail failed with error code " & $err)
	EndIf
EndFunc

Func _Write_Error_Log()
	Local	$oClienErrorCount

	$oClienErrorCount = FileOpen($ClientErrorCount, 2)
	For $i = 0 To UBound($aWorkstationErrors) - 1
		FileWriteLine($oClienErrorCount,$aWorkstationErrors[$i][0]&','&$aWorkstationErrors[$i][1])
	Next
	FileClose($oClienErrorCount)
	;MsgBox(64,'Check','Check the error count')
EndFunc

Func MyErrFunc()
	Local $HexNumber, $oMyError
	$HexNumber = Hex($oMyError.number, 8)
	MsgBox(0, "", "We intercepted a COM Error !" & @CRLF & _
			"Number is: " & $HexNumber & @CRLF & _
			"Linenbr is: " & $oMyError.scriptline & @CRLF & _
			"Description is: " & $oMyError.description & @CRLF & _
			"Windescription is: " & $oMyError.windescription)

	SetError(1); something to check for when this function returns
EndFunc   ;==>MyErrFunc

