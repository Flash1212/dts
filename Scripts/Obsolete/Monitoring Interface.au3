
Opt('MustDeclareVars', 1)
Opt("GUIOnEventMode", 1)

#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include <StaticConstants.au3>
#include <GuiListView.au3>
#include <array.au3>
#include <file.au3>
#include <GuiIPAddress.au3>
#Include <GuiButton.au3>

Global $System_Monitor

_Monitoring_GUI()

Func _Monitoring_GUI()
	
	Local $System, $Yes_Radio, $No_Radio, $Instructions, $All_systems, $Radio1, $Radio2, $System2, $Radio3, $Radio4, $System3, $radio5, $radio6

	#Region ### START Koda GUI section ### Form=\\austech\is-server\djthornton\scripts\ftp project\images\monitoring.kxf
	$System_Monitor = GUICreate("System Monitor", 279, 220, 496, 217)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_System_Monitor")
	$System = GUICtrlCreateLabel("6dqjdd1_-_10.20.10.20", 16, 136, 136, 20)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	$Yes_Radio = GUICtrlCreateRadio( 168, 136, 41, 17)
	$No_Radio = GUICtrlCreateRadio(224, 136, 41, 17)
	$Instructions = GUICtrlCreateLabel("Please select the asset that you ..", 16, 16, 244, 20)
	GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
	$All_systems = GUICtrlCreateLabel("All Systems", 16, 88, 74, 20)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	$Radio1 = GUICtrlCreateRadio( 168, 88, 41, 17)
	$Radio2 = GUICtrlCreateRadio(224, 88, 41, 17)
	$System2 = GUICtrlCreateLabel("6dqjdd1_-_10.20.10.20", 15, 163, 136, 20)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	$Radio3 = GUICtrlCreateRadio( 167, 163, 41, 17)
	$Radio4 = GUICtrlCreateRadio(223, 163, 41, 17)
	$System3 = GUICtrlCreateLabel("6dqjdd1_-_10.20.10.20", 15, 187, 136, 20)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	$Radio5 = GUICtrlCreateRadio( 167, 187, 41, 17)
	$Radio6 = GUICtrlCreateRadio(223, 187, 41, 17)
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###

	While 1
	Sleep(250)
	WEnd

EndFunc

Func _Destroy_System_Monitor()
	GUIDelete($System_Monitor)
	Exit
EndFunc