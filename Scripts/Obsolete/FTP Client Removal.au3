
_Account_Remove('jwx11c1', 'FTP Transfer')


Global $oMyError = ObjEvent("AutoIt.Error", "MyErrFunc"); Install a custom error handler

Func _Account_Remove($strComputer,$UserName)
	Local $oMyError, $objUser, $colAccounts
	
	; Check if account exists .. if not create it
	$objUser = ObjGet("WinNT://" & $strComputer & "")
	If @error Then
		;does not exist
	Else
		$objUserDelete = $objUser.Delete("user", $UserName)
	EndIf
	_Dir_Remove('jwx11c1', 'FTP Transfer')
EndFunc   ;==>_Account_Setup

Func _Dir_Remove($strComputer, $UserName)
	Local $acls
	DirRemove('\\'&$strComputer&'\c$\FTP Transfers\', 1)
EndFunc   ;==>_Dir_Setup

Func MyErrFunc()
	Local $HexNumber
	$HexNumber = Hex($oMyError.number, 8)
	MsgBox(0, "", "We intercepted a COM Error !" & @CRLF & _
			"Number is: " & $HexNumber & @CRLF & _
			"Linenbr is: " & $oMyError.scriptline & @CRLF & _
			"Description is: " & $oMyError.description & @CRLF & _
			"Windescription is: " & $oMyError.windescription)

	SetError(1); something to check for when this function returns
EndFunc   ;==>MyErrFunc