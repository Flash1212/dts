
Opt('MustDeclareVars', 1)

#include <File.au3>
#include <Array.au3>
#include <WinAPI.au3>
#include <Constants.au3>
#include <GuiListView.au3>
#include <Sendmessage.au3>
#include <GuiImageList.au3>
#include <FileTypeIcon.au3>
#include "GetIconForFile.au3"
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <ListViewConstants.au3>

Global Const $tagSHFILEINFO = "hwnd hIcon;int iIcon;dword Attr;wchar DisplayName[260];char TypeName[80]"
Global Const $SHGFI_ICON = 256
Global Const $SHGFI_DISPLAYNAME  = 512
Global Const $SHGFI_TYPENAME = 1024
Global Const $SHGFI_ATTRIBUTES = 2048
Global Const $SHGFI_ICONLOCATION = 4096
Global Const $SHGFI_EXETYPE = 8192
Global Const $SHGFI_SYSICONINDEX = 16384
Global Const $SHGFI_LINKOVERLAY = 32768
Global Const $SHGFI_SELECTED = 65536
Global Const $SHGFI_ATTR_SPECIFIED = 131072
Global Const $SHGFI_LARGEICON = 0
Global Const $SHGFI_SMALLICON = 1
Global Const $SHGFI_OPENICON = 2
Global Const $SHGFI_SHELLICONSIZE = 4
Global Const $SHGFI_PIDL = 8
Global Const $SHGFI_USEFILEATTRIBUTES = 16
Global Const $STM_SETIMAGE = 0x0172

#Region ### Global Variables
Global	$DTS_Client, _
		$ListFiles, _
		$Open, _
		$nMsg

Global	$Log = @ScriptDir&'\Logs\FTP Client.log', _
		$File_Location = @ScriptDir&'\Files\', _
		$Username = StringTrimLeft($CmdLineRaw, 1)
#EndRegion ### End Global Variables

#Region ### START Koda GUI section ### Form=\\austech\is-server\djthornton\Scripts\FTP Project\Scripts\DTS Client Interface 2.kxf
$DTS_Client = GUICreate("Explorer Test", 640, 580,(@DesktopWidth-640)/2, (@DesktopHeight-580)/2, BitOR($WS_OVERLAPPEDWINDOW, $WS_CLIPSIBLINGS, $WS_CLIPCHILDREN))
GUISetIcon(@TempDir & '\compass.ico')
GUISetBkColor('0xffffff', $DTS_Client)
$ListFiles = GUICtrlCreateListView("Name|Date Modified|Type|Size", 0, 0, 634, 574)
_GUICtrlListView_SetColumnWidth($ListFiles, 0, (3/7) * 640)
_GUICtrlListView_SetColumnWidth($ListFiles, 1, (1/3) * 367)
_GUICtrlListView_SetColumnWidth($ListFiles, 2, (1/3) * 367)
_GUICtrlListView_SetColumnWidth($ListFiles, 3, (1/3) * 367)
_List_Folder_Contents()
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			_FileWriteLog($Log, 'FTP Client has been shutdown.')
			Exit
	EndSwitch
WEnd

Func _List_Folder_Contents()
	Local	$search, $x = 0, $file, $oFile, $iTem, $fTime, $hTIme, $fSize, $fType, $fIcon, $FullPath, _
			$tSHFILEINFO, $aDllRet, $sIconFile, $imageItem, $replace, $fSIcon, $iCount, $aPath[1]
	;$oLog = FileOpen($Log, 1)
	_FileWriteLog($Log, 'FTP Client has been initiated.')
	; Shows the filenames of all files in the current directory.
	$search = FileFindFirstFile($File_Location & '\*.*')

	; Check if the search was successful
	If $search = -1 Then
		MsgBox(16, "Error", "No files were found to open")
		_FileWriteLog($Log, "Error - No files were found to open")
		Exit
	EndIf

	While 1
		$file = FileFindNextFile($search)
		If @error Then ExitLoop
		$FullPath = $File_Location & $file
		ReDim $aPath[$x + 1]
		$aPath[$x] = $FullPath
		$fTime =  FileGetTime($FullPath, 0) ; creation Time
        $hTIme = $fTime[1] & "/" & $fTime[2] & "/" & $fTime[0] & " " & $fTime[3] & ":" & $fTime[4] & ":" & $fTime[5] ; detailed
		$fSize = FileGetSize($FullPath)
		$fType = _FileGetType($FullPath)
		If $fSize >= 1048576 Then
			$fSize = Round($fSize/1048576, 0)&' MB'
		Else
			$fSize = Round($fSize/1024, 0)&' KB'
		EndIf
		$iTem = GUICtrlCreateListViewItem($file&'|'&$hTIme&'|'&$fType&'|'&$fSize, $ListFiles)
		GUICtrlSetImage(-1, 'shell32.dll', 22)
		$x = $x + 1
	WEnd
	GUISetState(@SW_SHOW)
	$iCount = _GUICtrlListView_GetItemCount($ListFiles)
	$imageItem = _GUIImageList_Create(16, 16, 5)
;~ 	_ArrayDisplay($aPath)
;~ 	MsgBox(64,'Count', $iCount)
	For $x = 0 To UBound($aPath) - 1
		$fIcon = _GetFileIcon($aPath[$x])
;~ 		$fIcon = _GetIconForFile($aPath[$x], 32)
		_GUIImageList_ReplaceIcon($imageItem, -1, $fIcon )
		_WinAPI_DestroyIcon($fIcon)
	Next

	_GUICtrlListView_SetImageList($ListFiles, $imageItem, 1)
	;FileWriteLine($oLog, $x & ' File(s) were opened by '&$Username&'.'&@CRLF&@TAB&)
	; Close the search handle
	FileClose($search)
	;FileClose($oLog)
EndFunc   ;==>_List_Folder_Contents



Func _ShellGetAssocIcon(Const $szFile,Const $IconFlags = 4096)
    Local $tFileInfo = DllStructCreate($tagSHFILEINFO)
    If @error Then
		MsgBox(16,'DLL Error', @extended)
        Return SetError(1,@extended,0)
    EndIf

    Local $Ret = DllCall("shell32.dll","int","SHGetFileInfoW","wstr",$szFile,"dword",0, _
        "ptr",DllStructGetPtr($tFileInfo),"uint",DllStructGetSize($tFileInfo),"uint",BitOr($SHGFI_ICON,$IconFlags))
   If @error Then MsgBox(0,0,@error)
    Return DllStructGetData($tFileInfo,"hIcon")
EndFunc

Func _GetFileIcon($sPath, $bExt = 1, $iAttributes = 0)
    Static $tSHFILEINFO = DllStructCreate("ptr hIcon; int iIcon; DWORD dwAttributes; WCHAR szDisplayName[255]; WCHAR szTypeName[80];"), $p_tSHFILEINFO = DllStructGetPtr($tSHFILEINFO)
    Local $iFlags = BitOR(0x100, 0x1) ;$SHGFI_SMALLICON, $SHGFI_ICON
	Local $Result
    If $bExt Then $iFlags = BitOR($iFlags, 0x10);SHGFI_USEFILEATTRIBUTES

    Local $Ret = DllCall('shell32.dll', 'dword_ptr', 'SHGetFileInfoW', 'wstr', $sPath, 'dword', $iAttributes, 'ptr', $p_tSHFILEINFO, 'uint', DllStructGetSize($tSHFILEINFO), 'uint', $iFlags)
    If @error Then Return SetError(1, 0, 0)
    $Result = DllStructGetData($tSHFILEINFO, 'hIcon')

    Return $Result
EndFunc   ;==>__GetFileIcon
