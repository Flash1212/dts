#cs
FTP DTF - Server Side application
The purpose of this portion of the application is to perform all the primary file transfers.
This script will hold not only the file transfer portion but also the notification and password change script.
#ce
Opt('MustDeclareVars', 1)

#include <file.au3>
#include <array.au3>


Global $Log = '..\Logs\Error.log',$Dat = '..\Logs\Times.Dat', $Stamp_Check
;This is where all transfer systems are housed
Global $Original_file = '..\Computers\Computers.csv'
 
;While 1
	_Main()
	Sleep(490)
;WEnd

Func _Main()
	Local $Identity_Split, $y, $Asset_Name, $IP, $Computers, $Source, $Destination, $Freq, $Computer_Info
	If Not _FileReadToArray($Original_file,$Computers) Then
		MsgBox(4096,"Error", " Error reading log to Array     error:" & @error)
		Exit
	EndIf

	;_ArrayDisplay($Computers)
	
	;Y is used to increment the row for each entry
	$y = 0
	
	For $x = 1 To $Computers[0]
		$Computer_Info = StringSplit($Computers[$x],',')	
		$Identity_Split = StringSplit($Computer_Info[1],' - ',1)
		;_ArrayDisplay($Computer_Info)
		;_ArrayDisplay($Identity_Split)
		;MsgBox(0,'',$Identity_Split[1])
		;MsgBox(0,'',$Identity_Split[2])
			$Asset_Name = $Identity_Split[1]
			$IP = $Identity_Split[2]
		$Source = $Computer_Info[2]
		$Destination = $Computer_Info[3]
		$Freq = $Computer_Info[4] ; In minutes
		$Stamp_Check = _Time_Format($y, $Source,$Destination)
		;MsgBox(0,'',$Stamp_Check)
		If $Stamp_Check = 1 Then _File_Transfer($Asset_Name, $IP, $Source,$Destination)
		$y = $y + 1 ; Increment so that next time the row will jump to the next
	Next	
	
EndFunc
	
Func _File_Transfer($Asset_Name, $IP, $SRC,$DES)

	Local $Exist, $Cur_SRC, $New_DES, $copy
	$Exist = FileExists($SRC)
		If $Exist <> 1 Then
			;_FileWriteLog($Log,
			_Notification('Source File',$Asset_Name)
		EndIf
	$Exist = FileExists($DES)
		If $Exist <> 1 Then
			;_FileWriteLog($Log,
			_Notification('Destination File',$Asset_Name)
		EndIf
	$copy = FileCopy($SRC,$DES,9)
		If $copy <> 1 Then 
			;_FileWriteLog($Log, 
			_Notification('File Transfer',$Asset_Name)
		Else
			;_FileWriteLog($Dat,&','&$Asset_Name&','&@HOUR&':'&@MIN&':'&@SEC)
		EndIf
	$Cur_SRC = FileGetTime($Src,0,1)
	$New_DES = FileGetTime($DES,0,1)
	
	If $Cur_SRC <> $New_DES Then MsgBox(0,'Error','Transfer Failed.')
	
EndFunc

Func _Notification($Problem,$Asset_Name)
	Select
		Case $Problem = 'Source File'
			
		Case $Problem = 'File Transfer'
			
		Case $Problem = 'Destination File'		
	EndSelect
	
EndFunc

Func _Password_Reset()
	
EndFunc

Func _Time_Format($y, $Src, $Des)
	Local $ST, $DT, $ST_Stamp, $DT_Stamp, $Stamp_Check
	$ST = FileGetTime($Src, 0, 0)
	$ST_Stamp =	_Time_Stamp($ST)
	$DT = FileGetTime($Des, 0, 0)
	$DT_Stamp = _Time_Stamp($DT)
	$Stamp_Check = _checkDate($ST,$DT)
	Return $Stamp_Check
EndFunc

Func _Time_Stamp($T)
	Local $M, $TY, $TM, $TD, $TH, $TMin, $TSec, $T_Stamp
	;_ArrayDisplay($T)
	$M = ''
	$TY = $T[0]
	$TM = $T[1]
	$TD = $T[2]
	If $T[3] = 12 Then
		$TH = $T[3]
		$M = 'pm'
	ElseIf $T[3] > 11 Then
		$TH = $T[3] - 12
		$M = 'pm'
	Else
		$TH = $T[3]
		$M = 'am'
	EndIf
	$TMin = $T[4]
	$TSec = $T[5]
	$T_Stamp = $TH & ':' & $TMin & ':' & $TSec & ' ' & $M & ' - ' & $TM & '\' & $TD & '\' & $TY
	Return $T_Stamp
	;MsgBox(0, '', $ST_Stamp)
EndFunc

Func _checkDate($ST, $DT)
	;MsgBox(0,'',$STH & ':' & $STMin & ':' & $STSec & ' ' & $M & ' - ' & $STM & '\' & $STD & '\' & $STY)
	;MsgBox(0,'',$DTH & ':' & $DTMin & ':' & $DTSec & ' ' & $M & ' - ' & $DTM & '\' & $DTD & '\' & $DTY)
	If $ST[0] > $DT[0] Then
		;MsgBox(0,'',$ST[0]&' '&$DT[0])
		;FileWrite($Log, $DT[0] & ' is outdated againts ' & $ST[0] & @CRLF)
		Return 1
	EndIf
	If $ST[0] = $DT[0] And $ST[1] > $DT[1] Then
		;MsgBox(0,'',$ST[1]&' '&$DT[1])
		;FileWrite($Log, $DT[0] & ' is outdated againts ' & $ST[0] & @CRLF)
		Return 1
	EndIf
	If $ST[0] = $DT[0] And $ST[1] = $DT[1] And $ST[2] > $DT[2] Then
		;MsgBox(0,'',$ST[2]&' '&$DT[2])
		;FileWrite($Log, $DT[0] & ' is outdated againts ' & $ST[0] & @CRLF)
		Return 1
	EndIf
	If $ST[0] = $DT[0] And $ST[1] = $DT[1] And $ST[2] = $DT[2] And $ST[3] > $DT[3] Then
		;MsgBox(0,'',$ST[3]&' '&$DT[3])
		;FileWrite($Log, $DT[0] & ' is outdated againts ' & $ST[0] & @CRLF)
		Return 1
	EndIf
	If $ST[0] = $DT[0] And $ST[1] = $DT[1] And $ST[2] = $DT[2] And $ST[3] = $DT[3] And $ST[4] > $DT[4] Then
		;MsgBox(0,'',$ST[4]&' '&$DT[4])
		;FileWrite($Log, $DT[0] & ' is outdated againts ' & $ST[0] & @CRLF)
		Return 1
	EndIf
	If $ST[0] = $DT[0] And $ST[1] = $DT[1] And $ST[2] = $DT[2] And $ST[3] = $DT[3] And $ST[4] = $DT[4] And $ST[5] > $DT[5] Then
		;MsgBox(0,'',$ST[5]&' '&$DT[5])
		;FileWrite($Log, $DT[0] & ' is outdated againts ' & $ST[0] & @CRLF)
		Return 1
	EndIf
EndFunc   ;==>_checkDate

