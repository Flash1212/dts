;FTP DTF - GUI Side application

;This piece of the application is the Graphical User interface for easier use with the application
Opt('MustDeclareVars', 1)
Opt("GUIOnEventMode", 1)

#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include <StaticConstants.au3>
#include <GuiListView.au3>
#include <array.au3>
#include <file.au3>

;GUI Global Variables

;GUI
Global $FTP_Interface

;Menu
Global $File_Menu, $Add_Coputer, $Transfer_Service, $Deploy_Client, $Change_Password, $Separator, $Exititem _
		, $View_menu, $View_Log, $Refresh_view _
		, $Help_Menu, $Help_file, $About

;Components
Global $Transfer_List, $Asset_Input, $Search_For_Asset, $Input_Group, $Seton_Pic, $Atos_Pic, $Initiate, $Diff

FileInstall('\\austech\is-si\Development\icons\compass.ico', @TempDir & '\compass.ico', 1)

_Interface()

Func _Interface()

	#Region ### START Koda GUI section ### Form=C:\Documents and Settings\djthornton\My Documents\Projects\FTP Project\FTP Interface.kxf
	;create Gui
	$FTP_Interface = GUICreate("DTF FTP Interface", 1048, 626, 110, 60)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Exit_Interface")
	GUISetIcon(@TempDir & '\compass.ico')
	; Create File menu
	$File_Menu = GUICtrlCreateMenu("&File")
	$Add_Coputer = GUICtrlCreateMenuItem("Add Computer", $File_Menu)
	GUICtrlSetOnEvent(-1, '_Add_Computer')
	$Transfer_Service = GUICtrlCreateMenuItem("Stop Transfer Service", $File_Menu)
	GUICtrlSetOnEvent(-1, '')
	$Deploy_Client = GUICtrlCreateMenuItem("Deploy Client", $File_Menu)
	GUICtrlSetOnEvent(-1, '')
	$Change_Password = GUICtrlCreateMenuItem("Change Client Password", $File_Menu)
	GUICtrlSetOnEvent(-1, '')
	$Separator = GUICtrlCreateMenuItem("", $File_Menu, 4)
	$Exititem = GUICtrlCreateMenuItem("Exit Interface", $File_Menu)
	GUICtrlSetOnEvent(-1, '_Exit_Interface')

	;Create View Menu
	$View_menu = GUICtrlCreateMenu("&View")
	$View_Log = GUICtrlCreateMenuItem("View Log", $View_menu)
	GUICtrlSetOnEvent(-1, '')
	$Refresh_view = GUICtrlCreateMenuItem("Refresh List", $View_menu)
	GUICtrlSetOnEvent(-1, '_Load_TransferList')

	;Creae Help Menu
	$Help_Menu = GUICtrlCreateMenu("&Help")
	$Help_file = GUICtrlCreateMenuItem("Help File", $Help_Menu)
	GUICtrlSetOnEvent(-1, '')
	$About = GUICtrlCreateMenuItem("About", $Help_Menu)
	GUICtrlSetOnEvent(-1, '')

	;Create Gui Components
	_Load_TransferList()
	$Asset_Input = GUICtrlCreateInput("Asset or IP", 30, 64, 153, 21)
	$Search_For_Asset = GUICtrlCreateButton("Search", 200, 64, 60, 20)
	GUICtrlSetFont(-1, 11, 400, 0, "MS Sans Serif")
	$Input_Group = GUICtrlCreateGroup("Search for an Asset or IP", 16, 48, 270, 49)
	GUICtrlCreateGroup("", -99, -99, 1, 1)
	$Seton_Pic = GUICtrlCreatePic("..\Images\FamilyHospital_horiz_color_Mod.JPG", 310, 30, 0, 0, BitOR($WS_GROUP, $WS_CLIPSIBLINGS))
	;$Atos_Pic = GUICtrlCreatePic("..\Images\atosorigin_logo.gif", 950,30, 0, 0, BitOR($WS_GROUP,$WS_CLIPSIBLINGS))
	_Load_TransferList()
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###
	$Initiate = TimerInit()
	While 1
		$Diff = TimerDiff($Initiate) ;This will force a 30 second auto refresh
		If $Diff >= 30000 Then
			_Load_TransferList()
			$Initiate = TimerInit()
		EndIf
		;Perform a time differential to invoke the view refresh
		Sleep(250)
	WEnd

EndFunc   ;==>_Interface

Func _Exit_Interface()
	;Delete the GUI then end the script
	GUIDelete($FTP_Interface)
	Exit
EndFunc   ;==>_Exit_Interface

Func _Load_TransferList()
	;Declare Variables
	Local $Original_file, $Computers, $x, $y, $Computer_Info
	
	;Destroy List so that when the refresh is invoked it will clear all values and start again.
	_GUICtrlListView_Destroy($Transfer_List)
	
	;Create Transfer List View
	$Transfer_List = GUICtrlCreateListView("", 19, 128, 1009, 461)
	_GUICtrlListView_AddColumn($Transfer_List, 'Asset - IP', 120)
	_GUICtrlListView_AddColumn($Transfer_List, 'Source', 302.5)
	_GUICtrlListView_AddColumn($Transfer_List, 'Destination', 302.5)
	_GUICtrlListView_AddColumn($Transfer_List, 'Time Stamps       Source    -     Destination', 280)
	
	;This is where all transfer systems are housed
	$Original_file = '..\Computers\Computers.csv'
	
	;Load the file into an array
	If Not _FileReadToArray($Original_file, $Computers) Then
		MsgBox(4096, "Error", " Error reading log to Array     error:" & @error)
		Exit
	EndIf

	;_ArrayDisplay($Computers)
	
	;Y is used to increment the row for each entry
	$y = 0
	
	;For each row in the array we will ready its information then break it into pices by the "," delimeter and give each it own value slot in an array.
	;To view the array before it's broken into pieces remove the ";" from the _arraydisplay below.
	;_Arraydisplay($Computers)
	For $x = 1 To $Computers[0]
		$Computer_Info = StringSplit($Computers[$x], ',')
		_GUICtrlListView_AddItem($Transfer_List, $Computer_Info[1])
		_GUICtrlListView_AddSubItem($Transfer_List, $y, $Computer_Info[2], 1)
		_GUICtrlListView_AddSubItem($Transfer_List, $y, $Computer_Info[3], 2)
		_Time_Format($y, $Computer_Info[2], $Computer_Info[3])
		$y = $y + 1 ; Increment so that next time the row will jump to the next
	Next
EndFunc   ;==>_Load_TransferList

Func _Time_Format($y, $Src, $Des)
	Local $ST, $DT, $ST_Stamp, $DT_Stamp, $Stamp_Check
	$ST = FileGetTime($Src, 0, 0)
	If @error Then
		;MsgBox(16,'Error','No Source file detected')
	Else
		$ST_Stamp = _Time_Stamp($ST)
		$DT = FileGetTime($Des, 0, 0)
		If @error Then
		Else
			$DT_Stamp = _Time_Stamp($DT)
		EndIf
		
		;$Stamp_Check = _checkDate($ST,$DT)
		_GUICtrlListView_AddSubItem($Transfer_List, $y, $ST_Stamp & ' - ' & $DT_Stamp, 3)
	EndIf
EndFunc   ;==>_Time_Format

Func _Time_Stamp($T)
	Local $M, $TY, $TM, $TD, $TH, $TMin, $TSec, $T_Stamp
	;_ArrayDisplay($T)
	$M = ''
	$TY = $T[0]
	$TM = $T[1]
	$TD = $T[2]
	If $T[3] = 12 Then
		$TH = $T[3]
		$M = 'pm'
	ElseIf $T[3] > 11 Then
		$TH = $T[3] - 12
		$M = 'pm'
	Else
		$TH = $T[3]
		$M = 'am'
	EndIf
	$TMin = $T[4]
	$TSec = $T[5]
	$T_Stamp = $TH & ':' & $TMin & ':' & $TSec & ' ' & $M & ' - ' & $TM & '\' & $TD & '\' & $TY
	Return $T_Stamp
	;MsgBox(0, '', $ST_Stamp)
EndFunc   ;==>_Time_Stamp

Func _Add_Computer()

	Local $Add, $Computer_Name, $IP_Address, $Source, $Destination
	
	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\FTP Project\Scripts\Add Computer Interface.kxf
	$Add = GUICreate("Add Computer", 400, 364, 193, 125)
	$Computer_Name = GUICtrlCreateInput("Name", 40, 72, 321, 21)
	$IP_Address = GUICtrlCreateInput("IP", 39, 140, 321, 21)
	$Source = GUICtrlCreateInput("Source", 38, 211, 321, 21)
	$Destination = GUICtrlCreateInput("Destination", 38, 281, 321, 21)
	$Name_Label = GUICtrlCreateLabel("Computer Name", 40, 48, 36, 17)
	$IP_Label = GUICtrlCreateLabel("IP address", 40, 120, 36, 17)
	$Source_Label = GUICtrlCreateLabel("Source", 40, 184, 36, 17)
	$Destination_Label = GUICtrlCreateLabel("Destination", 40, 256, 36, 17)
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###

	While 1
		$nMsg = GUIGetMsg()
		Switch $nMsg
			Case $GUI_EVENT_CLOSE
				Exit

		EndSwitch
	WEnd


;### Tidy Error -> "endfunc" is closing previous "case"
			EndFunc   ;==>_Add_Computer

;### Tidy Error -> switch Not closed before "Func" statement.
;### Tidy Error -> "func" cannot be inside any IF/Do/While/For/Case/Func statement.
			Func _Account_Setup($strComputer, $UserName, $Password)
				; Init objects
				Const $ADS_UF_DONT_EXPIRE_PASSWD = 0X10000
				Const $ADS_UF_PASSWD_CANT_CHANGE = 0X40
				$oMyError = ObjEvent("AutoIt.Error", "MyErrFunc"); Install a custom error handler
				; Check if account exists .. if not create it
				$objUser = ObjGet("WinNT://" & $strComputer & "/" & $UserName)
				If @error Then
					$colAccounts = ObjGet("WinNT://" & $strComputer & "")
					$objUser = $colAccounts.Create("user", $UserName)
					$objUser.SetPassword($Password)
					$objUser.Put("Fullname", "FTP Account")
					$objUser.Put("Description", "FTP Transfer Account")
					$objUser.SetInfo
				EndIf

				; Read current settings and Bitor to ensure the "Don't expire password swith is on and user cannot change password"
				$oldFlags = $objUser.Get("UserFlags")
				$newFlags = BitOR($oldFlags, $ADS_UF_DONT_EXPIRE_PASSWD)
				$objUser.Put("UserFlags", $newFlags) ;expire the password
				$objUser.SetInfo
				$oldFlags = $objUser.Get("UserFlags")
				$newFlags = BitOR($oldFlags, $ADS_UF_PASSWD_CANT_CHANGE)
				$objUser.Put("UserFlags", $newFlags) ;expire the password
				$objUser.SetInfo
				MsgBox(262144, 'Debug line ~' & @ScriptLineNumber, 'Selection:' & @LF & ' dont Password Expired');### Debug MSGBOX

				;Add User to admin group**********************************************
				;$objGroup = ObjGet("WinNT://" & $strComputer & "/Administrators,group")
				;$objGroup.Add($objUser.ADsPath)
				;*********************************************************************
			EndFunc   ;==>_Account_Setup

;### Tidy Error -> switch Not closed before "Func" statement.
;### Tidy Error -> "func" cannot be inside any IF/Do/While/For/Case/Func statement.
			Func _Dir_Setup()
				DirCreate('C:\FTP Transfers')
				$acls = ('cacls.exe "\\' & $strComputer & '\c$\FTP Transfers\" /E /T /C /G ' & $user & '@seton":R')
				;MsgBox(0,'',$acls)
				Run(@ComSpec & " /C " & $acls, "", @SW_HIDE);_RunDOS($acls)
			EndFunc   ;==>_Dir_Setup

;### Tidy Error -> switch Not closed before "Func" statement.
;### Tidy Error -> "func" cannot be inside any IF/Do/While/For/Case/Func statement.
			Func MyErrFunc()
				$HexNumber = Hex($oMyError.number, 8)
				MsgBox(0, "", "We intercepted a COM Error !" & @CRLF & _
						"Number is: " & $HexNumber & @CRLF & _
						"Linenbr is: " & $oMyError.scriptline & @CRLF & _
						"Description is: " & $oMyError.description & @CRLF & _
						"Windescription is: " & $oMyError.windescription)

				SetError(1); something to check for when this function returns
			EndFunc   ;==>MyErrFunc

;### Tidy Error -> switch Not closed before "Func" statement.
;### Tidy Error -> "func" cannot be inside any IF/Do/While/For/Case/Func statement.
			Func _Change_Password($Password)
				; Init objects
				Const $ADS_UF_DONT_EXPIRE_PASSWD = 0X10000
				Const $ADS_UF_PASSWD_CANT_CHANGE = 0X40
				$oMyError = ObjEvent("AutoIt.Error", "MyErrFunc"); Install a custom error handler
				; Check if account exists .. if not create it
				$objUser = ObjGet("WinNT://" & $strComputer & "/" & $UserName)
				$objUser.SetPassword($Password)
			EndFunc   ;==>_Change_Password

