;Installer

;This programs function is to install the FTP DTF Server Side application
Opt('MustDeclareVars', 1)

DirCreate(@ProgramFilesDir&'\FTP DTF\Images')
DirCreate(@ProgramFilesDir&'\FTP DTF\Scripts')
DirCreate(@ProgramFilesDir&'\FTP DTF\Logs')
DirCreate(@ProgramFilesDir&'\FTP DTF\Computers')