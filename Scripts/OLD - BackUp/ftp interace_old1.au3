;FTP DTF - GUI Side application

;This piece of the application is the Graphical User interface for easier use with the application
Opt('MustDeclareVars', 1)
Opt("GUIOnEventMode", 1)

#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include <StaticConstants.au3>
#include <GuiListView.au3>
#include <array.au3>
#include <file.au3>

;GUI Global Variables

;GUI
Global $FTP_Interface

;Menu
Global $File_Menu, $Add_Coputer, $Transfer_Service, $Separator, $Exititem _
	   ,$View_menu, $View_Log, $Refresh_view _
	   ,$Help_Menu, $Help_file, $About

;Components   
Global $Transfer_List, $Asset_Input, $Search_For_Asset, $Input_Group, $Seton_Pic, $Atos_Pic, $Initiate, $Diff

_Interface()

Func _Interface()

#Region ### START Koda GUI section ### Form=C:\Documents and Settings\djthornton\My Documents\Projects\FTP Project\FTP Interface.kxf
;create Gui
$FTP_Interface = GUICreate("DTF FTP Interface", 1048, 626, 110, 60)
GUISetOnEvent($GUI_EVENT_CLOSE, "_Exit_Interface")
; Create File menu
$File_menu = GUICtrlCreateMenu("&File")
$Add_Coputer = GUICtrlCreateMenuItem("Add Computer", $File_Menu)
GUICtrlSetOnEvent(-1, '')
$Transfer_Service = GUICtrlCreateMenuItem("Stop Transfer Service", $File_Menu)
GUICtrlSetOnEvent(-1, '')
$Separator = GUICtrlCreateMenuItem("", $File_Menu, 3)
$Exititem = GUICtrlCreateMenuItem("Exit Interface", $File_Menu)
GUICtrlSetOnEvent(-1, '_Exit_Interface')

;Create View Menu
$View_menu= GUICtrlCreateMenu("&View")
$View_Log = GUICtrlCreateMenuItem("View Log", $View_menu)
GUICtrlSetOnEvent(-1, '')
$Refresh_view = GUICtrlCreateMenuItem("Refresh List", $View_menu)
GUICtrlSetOnEvent(-1, '_Load_TransferList')

;Creae Help Menu
$Help_Menu = GUICtrlCreateMenu("&Help")
$Help_file = GUICtrlCreateMenuItem("Help File", $Help_Menu)
GUICtrlSetOnEvent(-1, '')
$About = GUICtrlCreateMenuItem("About", $Help_Menu)
GUICtrlSetOnEvent(-1, '')

;Create Gui Components
_Load_TransferList()
$Asset_Input = GUICtrlCreateInput("Asset or IP", 30, 64, 153, 21)
$Search_For_Asset = GUICtrlCreateButton("Search", 200, 64, 60, 20)
GUICtrlSetFont(-1, 11, 400, 0, "MS Sans Serif")
$Input_Group = GUICtrlCreateGroup("Search for an Asset or IP", 16, 48, 270, 49)
GUICtrlCreateGroup("", -99, -99, 1, 1)
$Seton_Pic = GUICtrlCreatePic("..\Images\FamilyHospital_horiz_color_Mod.JPG", 310, 30, 0, 0, BitOR($WS_GROUP,$WS_CLIPSIBLINGS))
;$Atos_Pic = GUICtrlCreatePic("..\Images\atosorigin_logo.gif", 950,30, 0, 0, BitOR($WS_GROUP,$WS_CLIPSIBLINGS))
_Load_TransferList()
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###
$Initiate = TimerInit()
While 1
	$Diff = TimerDiff($Initiate) ;This will force a 30 second auto refresh
	If $Diff >= 30000 Then
		_Load_TransferList()
		$Initiate = TimerInit()
	EndIf
	;Perform a time differential to invoke the view refresh
	Sleep(250)
WEnd

EndFunc

Func _Exit_Interface()
	;Delete the GUI then end the script
	GUIDelete($FTP_Interface)
	Exit
EndFunc

Func _Load_TransferList()
	;Declare Variables
	Local $Original_file, $Computers, $x, $y, $Computer_Info
	
	;Destroy List so that when the refresh is invoked it will clear all values and start again.
	_GUICtrlListView_Destroy($Transfer_List)
	
	;Create Transfer List View
	$Transfer_List = GUICtrlCreateListView("", 19, 128, 1009, 461)
	_GUICtrlListView_AddColumn($Transfer_List, 'Asset - IP',120)
	_GUICtrlListView_AddColumn($Transfer_List, 'Source',302.5)
	_GUICtrlListView_AddColumn($Transfer_List, 'Destination',302.5)
	_GUICtrlListView_AddColumn($Transfer_List, 'Time Stamps       Source    -     Destination',280)
	
	;This is where all transfer systems are housed
	$Original_file = '..\Computers\Computers.csv'
	
	;Load the file into an array
	If Not _FileReadToArray($Original_file,$Computers) Then
		MsgBox(4096,"Error", " Error reading log to Array     error:" & @error)
		Exit
	EndIf

	;_ArrayDisplay($Computers)
	
	;Y is used to increment the row for each entry
	$y = 0
	
	;For each row in the array we will ready its information then break it into pices by the "," delimeter and give each it own value slot in an array.
	;To view the array before it's broken into pieces remove the ";" from the _arraydisplay below.
	;_Arraydisplay($Computers)
	For $x = 1 To $Computers[0]
		$Computer_Info = StringSplit($Computers[$x],',')	
		_GUICtrlListView_AddItem($Transfer_List,$Computer_Info[1])
		_GUICtrlListView_AddSubItem($Transfer_List,$y,$Computer_Info[2],1)
		_GUICtrlListView_AddSubItem($Transfer_List,$y,$Computer_Info[3],2)
		_Time_Format($y, $Computer_Info[2],$Computer_Info[3])
		$y = $y + 1 ; Increment so that next time the row will jump to the next
	Next
EndFunc

Func _Time_Format($y, $Src, $Des)
	Local $ST, $DT, $ST_Stamp, $DT_Stamp, $Stamp_Check
	$ST = FileGetTime($Src, 0, 0)
	$ST_Stamp =	_Time_Stamp($ST)
	$DT = FileGetTime($Des, 0, 0)
	$DT_Stamp = _Time_Stamp($DT)
	;$Stamp_Check = _checkDate($ST,$DT)
	_GUICtrlListView_AddSubItem($Transfer_List,$y,$ST_Stamp&' - '&$DT_Stamp,3)
EndFunc

Func _Time_Stamp($T)
	Local $M, $TY, $TM, $TD, $TH, $TMin, $TSec, $T_Stamp
	;_ArrayDisplay($T)
	$M = ''
	$TY = $T[0]
	$TM = $T[1]
	$TD = $T[2]
	If $T[3] = 12 Then
		$TH = $T[3]
		$M = 'pm'
	ElseIf $T[3] > 11 Then
		$TH = $T[3] - 12
		$M = 'pm'
	Else
		$TH = $T[3]
		$M = 'am'
	EndIf
	$TMin = $T[4]
	$TSec = $T[5]
	$T_Stamp = $TH & ':' & $TMin & ':' & $TSec & ' ' & $M & ' - ' & $TM & '\' & $TD & '\' & $TY
	Return $T_Stamp
	;MsgBox(0, '', $ST_Stamp)
EndFunc

#cs
Func _checkDate($ST, $DT)
	;MsgBox(0,'',$STH & ':' & $STMin & ':' & $STSec & ' ' & $M & ' - ' & $STM & '\' & $STD & '\' & $STY)
	;MsgBox(0,'',$DTH & ':' & $DTMin & ':' & $DTSec & ' ' & $M & ' - ' & $DTM & '\' & $DTD & '\' & $DTY)
	If $ST[0] > $DT[0] Then
		;MsgBox(0,'',$ST[0]&' '&$DT[0])
		;FileWrite($Log, $DT[0] & ' is outdated againts ' & $ST[0] & @CRLF)
		Return 1
	EndIf
	If $ST[0] = $DT[0] And $ST[1] > $DT[1] Then
		;MsgBox(0,'',$ST[1]&' '&$DT[1])
		;FileWrite($Log, $DT[0] & ' is outdated againts ' & $ST[0] & @CRLF)
		Return 1
	EndIf
	If $ST[0] = $DT[0] And $ST[1] = $DT[1] And $ST[2] > $DT[2] Then
		;MsgBox(0,'',$ST[2]&' '&$DT[2])
		;FileWrite($Log, $DT[0] & ' is outdated againts ' & $ST[0] & @CRLF)
		Return 1
	EndIf
	If $ST[0] = $DT[0] And $ST[1] = $DT[1] And $ST[2] = $DT[2] And $ST[3] > $DT[3] Then
		;MsgBox(0,'',$ST[3]&' '&$DT[3])
		;FileWrite($Log, $DT[0] & ' is outdated againts ' & $ST[0] & @CRLF)
		Return 1
	EndIf
	If $ST[0] = $DT[0] And $ST[1] = $DT[1] And $ST[2] = $DT[2] And $ST[3] = $DT[3] And $ST[4] > $DT[4] Then
		;MsgBox(0,'',$ST[4]&' '&$DT[4])
		;FileWrite($Log, $DT[0] & ' is outdated againts ' & $ST[0] & @CRLF)
		Return 1
	EndIf
	If $ST[0] = $DT[0] And $ST[1] = $DT[1] And $ST[2] = $DT[2] And $ST[3] = $DT[3] And $ST[4] = $DT[4] And $ST[5] > $DT[5] Then
		;MsgBox(0,'',$ST[5]&' '&$DT[5])
		;FileWrite($Log, $DT[0] & ' is outdated againts ' & $ST[0] & @CRLF)
		Return 1
	EndIf
EndFunc   ;==>_checkDate
#ce

