


Func _Account_Setup()
; Init objects
Const $ADS_UF_DONT_EXPIRE_PASSWD = 0X10000
Const $ADS_UF_PASSWD_CANT_CHANGE = 0X40
$UserName = 'Fred'
$Password = 'Wilma123'
$oMyError = ObjEvent("AutoIt.Error","MyErrFunc"); Install a custom error handler
$strComputer = '6dqjdd1'
; Check if account exists .. if not create it
$objUser = ObjGet("WinNT://" & $strComputer & "/" & $UserName)
If @Error then
    $colAccounts = ObjGet("WinNT://" & $strComputer & "")
    $objUser = $colAccounts.Create("user", $UserName)
    $objUser.SetPassword ($Password)
    $objUser.Put ("Fullname", "Test User")
    $objUser.Put ("Description", "Test User description")
    $objUser.SetInfo
EndIf
;
; Read current settings and Bitor to ensure the "Don't expire password swith is on and user cannot change password"
$oldFlags = $objUser.Get("UserFlags")
$newFlags = BitOR($oldFlags,$ADS_UF_DONT_EXPIRE_PASSWD)
$objUser.Put ("UserFlags", $newFlags) ;expire the password
$objUser.SetInfo
$oldFlags = $objUser.Get("UserFlags")
$newFlags = BitOR($oldFlags,$ADS_UF_PASSWD_CANT_CHANGE)
$objUser.Put ("UserFlags", $newFlags) ;expire the password
$objUser.SetInfo
msgBox(262144,'Debug line ~' & @ScriptLineNumber,'Selection:' & @lf & ' dont Password Expired');### Debug MSGBOX
;
#cs
; Read current settings and Xor to ensure the "Don't expire password swith is off"
$oldFlags = $objUser.Get("UserFlags")
$newFlags = BitXOR($oldFlags,$ADS_UF_DONT_EXPIRE_PASSWD)
$objUser.Put ("UserFlags", $newFlags) ;expire the password
$objUser.SetInfo
; Set the Password expire now
$objUser.Put ("PasswordExpired", 1) ;expire the password
$objUser.SetInfo
msgBox(262144,'Debug line ~' & @ScriptLineNumber,'Selection:' & @lf & 'PasswordExpired');### Debug MSGBOX
;
; Disable User ACcount
$objUser.AccountDisabled=1
$objUser.SetInfo
#ce
;
;Add User to group
$objGroup = ObjGet("WinNT://" & $strComputer & "/Administrators,group")
$objGroup.Add($objUser.ADsPath)
;
;
; This is my custom error handler

;~ $OldUser = "Fred"
;~ $NewUser = "Fredrenamed"
;~ $oUser = ObjGet("WinNT://" & @ComputerName & "/" _
;~                 & $OldUser & ",user")
;~ $oComputer = ObjGet("WinNT://" & @ComputerName)
;~                                                                     MsgBox(262144,'Debug line ~25','Selection:' & @lf & '$oComputer' & @lf & @lf & 'Return:' & @lf & $oComputer & @lf & @lf & '@Error:' & @lf & @Error);### Debug MSGBOX
;~; rename user
;~ $oNewUser = $oComputer.MoveHere($oUser.ADsPath, $NewUser)
EndFunc

Func _Dir_Setup()
	DirCreate('C:\FTP Transfers')
EndFunc

Func MyErrFunc()
   $HexNumber=hex($oMyError.number,8)
   Msgbox(0,"","We intercepted a COM Error !" & @CRLF & _
                "Number is: " & $HexNumber & @CRLF & _
                "Linenbr is: " & $oMyError.scriptline  & @CRLF & _
                "Description is: " & $oMyError.description  & @CRLF & _
                "Windescription is: " & $oMyError.windescription )

   SetError(1); something to check for when this function returns
Endfunc