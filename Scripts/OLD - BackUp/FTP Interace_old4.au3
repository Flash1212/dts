;FTP DTF - GUI Side application

;This piece of the application is the Graphical User interface for easier use with the application
Opt('MustDeclareVars', 1)
Opt("GUIOnEventMode", 1)

#include <GUIConstantsEx.au3>
#include <GuiListView.au3>
#include <GuiIPAddress.au3>
#Include <GuiEdit.au3>
#include <WindowsConstants.au3>
#include <StaticConstants.au3>
#include <EditConstants.au3>
#include <ButtonConstants.au3>
#include <array.au3>
#include <file.au3>
#include <Date.au3>

;GUI Global Variables
#Region
;GUI
Global $FTP_Interface, $AddWorkstation, $RemoveWorkstation, $ClientDeploy, $ClientRemove, $Change_Password_GUI, $INI_Viewer, $Log_Interface

;Menu
Global 	$File_Menu, $Add_Workstation,  $Remove_Workstation, $Exititem, _
		$Action_Menu, $Start_Transfer_Service, $Stop_Transfer_Service, $Disable_Monitoring, $Enable_Monitoring, $Deploy_Client, $Remove_Client, $Change_Password _
			,$Separator, _
		$View_menu, $View_FTP_Transfer_Log, $View_Log_Selector, $View_INI, $Refresh_view, _
		$Help_Menu, $Help_file, $About

;Components
Global 	$Transfer_List, $Workstation_Input, $Search_For_Workstation, $Input_Group, $Seton_Pic, $Atos_Pic, $Initiate, $Diff, $oMyError, $Count_Down, $Item_Index, _
		$CheckBoxTimer, $CheckBoxDiff, $Save_Changes, $Screen_Refresh
	
;Misc
Global	$oMyError, $INI_File, $ComputerFolder, $ComputerFile, $Backup_Dir, $Log_Dir, $DeployWorkstationNameInput, $DeployIP, $CP1, $CP2, $SPBox, _
		$AddWorkstationName, $AddIP, $AddSourceInput, $AddDestinationInput, $RemoveWorkstationName, $RemoveWorkstationNameInput, $RemoveIP, $Remove_Client, _
		$Search_Item, $Search_Func, $Removal_Item, $Removal_Func, $Auto_Removal, $Auto_Item, $View_FTP_Transfer_Log, $View_Interface_Log, $View_All_Client_Logs, _
		$View_Single_Client_Log, $Workstation_Log, _
		$ContactAdmin = @CRLF&@CRLF&'If this is your first time recieving this message then please try again. If not the please contact you system Administrator.'


Dim $Program_Installation_Path
#EndRegion

$oMyError = ObjEvent("AutoIt.Error", "MyErrFunc"); Install a custom error handler

#Region
	$Program_Installation_Path = '\\austech\is-server\djthornton\scripts\FTP Project'
		If Not FileExists($Program_Installation_Path) Then
			MsgBox(16,'Error','Unable to locate Program Installation Path.')
			Exit
		EndIf
		
	$INI_File = $Program_Installation_Path&'\Settings\Configuration.ini'
		If Not FileExists($INI_File) Then
			MsgBox(16,'Error','Unable to locate Configuration file.')
			Exit
		EndIf
	
	$ComputerFile = $Program_Installation_Path&'\Computers\Computers.csv' ;This is where all transfer systems are housed
		If Not FileExists($ComputerFile) Then 
			MsgBox(16,'Error','Unable to locate Workstation .csv file.')
			Exit
		EndIf
		
	$Backup_Dir = $Program_Installation_Path&'\Computers\Backups'; This is where we will backup the transfer systems after modification
		If Not FileExists($Backup_Dir) Then
			MsgBox(16,'Error','Unable to locate Backup path.')
			Exit
		EndIf
		
	$Log_Dir = $Program_Installation_Path&'\Logs'; This is where we will backup the transfer systems after modification
		If Not FileExists($Log_Dir) Then
			MsgBox(16,'Error','Unable to locate Log path.')
			Exit
		EndIf
#EndRegion

FileInstall('\\austech\is-si\Development\icons\compass.ico', @TempDir & '\compass.ico', 1)

_FTP_Interface()

Func _FTP_Interface()
	#Region ### START Koda GUI section ### Form=C:\Documents and Settings\djthornton\My Documents\Projects\FTP Project\FTP Interface.kxf
	;create Gui
	$FTP_Interface = GUICreate("DTF FTP Interface", 1023, 631, -1, -1, $WS_OVERLAPPEDWINDOW, $WS_EX_WINDOWEDGE + $WS_EX_DLGMODALFRAME )
	GUICtrlSetResizing ($FTP_Interface, $GUI_DOCKAUTO)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Exit_Interface")
	GUISetIcon(@TempDir & '\compass.ico')
	
	; Create File menu
	$File_Menu = GUICtrlCreateMenu("&File")
	$Add_Workstation = GUICtrlCreateMenuItem("Add Workstation", $File_Menu)
	GUICtrlSetOnEvent(-1, '_Add_Workstation_Interface')
	$Remove_Workstation = GUICtrlCreateMenuItem("Remove Workstation", $File_Menu)
	GUICtrlSetOnEvent(-1, '_Remove_Workstation_Interface')
	$Separator = GUICtrlCreateMenuItem("", $File_Menu)
	$Exititem = GUICtrlCreateMenuItem("Exit", $File_Menu)
	GUICtrlSetOnEvent(-1, '_Exit_Interface')

	;Action Menu
	$Action_Menu = GUICtrlCreateMenu("&Action")
	$Start_Transfer_Service = GUICtrlCreateMenuItem("Start Transfer Service", $Action_Menu)
	GUICtrlSetOnEvent(-1, '')
	$Stop_Transfer_Service = GUICtrlCreateMenuItem("Stop Transfer Service", $Action_Menu)
	GUICtrlSetOnEvent(-1, '')
	$Separator = GUICtrlCreateMenuItem("", $Action_Menu)
	$Change_Password = GUICtrlCreateMenuItem("Change Client Password", $Action_Menu)
	GUICtrlSetOnEvent(-1, '_Change_Password')
	$Deploy_Client = GUICtrlCreateMenuItem("Deploy Client", $Action_Menu)
	GUICtrlSetOnEvent(-1, '_Client_Deploy_Interface')
	$Remove_Client = GUICtrlCreateMenuItem("Remove Client", $Action_Menu)
	GUICtrlSetOnEvent(-1, '_Remove_Client_Interface')
	$Separator = GUICtrlCreateMenuItem("", $Action_Menu)
	$Disable_Monitoring = GUICtrlCreateMenuItem("Disable All Monitoring", $Action_Menu)
	GUICtrlSetOnEvent(-1, '_Disable_All_Monitoring')
	$Enable_Monitoring = GUICtrlCreateMenuItem("Enable All Monitoring", $Action_Menu)
	GUICtrlSetOnEvent(-1, '_Enable_All_Monitoring')
	
	;Create View Menu
	$View_menu = GUICtrlCreateMenu("&View")
	$View_Log_Selector = GUICtrlCreateMenuItem("View Log Selector", $View_menu)
	GUICtrlSetOnEvent(-1, '_Log_Interface')
	$View_INI = GUICtrlCreateMenuItem("View INI (Read-Only)", $View_menu)
	GUICtrlSetOnEvent(-1, '_INI_Viewer')
	$Refresh_view = GUICtrlCreateMenuItem("Refresh List", $View_menu)
	GUICtrlSetOnEvent(-1, '_Load_TransferList')

	;Creae Help Menu
	$Help_Menu = GUICtrlCreateMenu("&Help")
	$Help_file = GUICtrlCreateMenuItem("Help File", $Help_Menu)
	GUICtrlSetOnEvent(-1, '')
	$About = GUICtrlCreateMenuItem("About", $Help_Menu)
	GUICtrlSetOnEvent(-1, '')

	;Create Gui Components
	;_Load_TransferList()
	$Save_Changes = GUICtrlCreateButton("Save Changes", 30, 80, 89, 25, 0)
	GUICtrlSetOnEvent(-1,'_Save_Changes')
	$Workstation_Input = GUICtrlCreateInput("Workstation or IP", 30, 30, 153, 21)
	GUICtrlSetState(-1, $GUI_FOCUS)
	$Search_For_Workstation = GUICtrlCreateButton("Search", 200, 30, 60, 20)
	GUICtrlSetFont(-1, 11, 400, 0, "MS Sans Serif")
	GUICtrlSetOnEvent($Search_For_Workstation,'_Search_Workstation')
	$Input_Group = GUICtrlCreateGroup("Search for an Workstation or IP", 8, 14, 270, 49)
	;GUICtrlCreateGroup("", -99, -99, 1, 1)
	$Seton_Pic = GUICtrlCreatePic("..\Images\FamilyHospital_horiz_color_Mod.JPG", 288, 16, 700, 80,BitOR($SS_NOTIFY,$WS_GROUP,$WS_CLIPSIBLINGS))
	GUICtrlSetResizing(-1,$GUI_DOCKHEIGHT)
	;$Atos_Pic = GUICtrlCreatePic("..\Images\atosorigin_logo.gif", 950,30, 0, 0, BitOR($WS_GROUP,$WS_CLIPSIBLINGS))
	;;$Count_Down = GUICtrlCreateLabel("Count Down", 16, 584, 66, 17)
	_Load_TransferList()
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###
	$Initiate = TimerInit()
	;$CheckBoxTimer = TimerInit()
	While 1
		;;$Diff = TimerDiff($Initiate) ;This will force a 30 second auto refresh
		;$CheckBoxDiff = TimerDiff($CheckBoxTimer)
		;;GUICtrlSetData($Count_Down,30 - Round(TimerDiff($Initiate)/1000,0))
		;If $CheckBoxDiff >= 2000 Then
		;	_CheckMonitoring()
		;EndIf
		;Perform a time differential to invoke the view refresh
		;;If $Diff >= 30000 Then
		;;	_Load_TransferList()
		;;	$Initiate = TimerInit()
		;;EndIf
		Sleep(250)
	WEnd

EndFunc   ;==>_Interface

Func _Load_TransferList()
	;Declare Variables
	Local $exStyles = BitOR($LVS_EX_FULLROWSELECT, $LVS_EX_CHECKBOXES), $Workstations, $x, $y, $Workstation_Info
	
	;Destroy List so that when the refresh is invoked it will clear all values and start again.
	_GUICtrlListView_Destroy($Transfer_List)
	
	;Create Transfer List View
	;$Transfer_List = _GUICtrlListView_Create($FTP_Interface,"", 0, 127, 1023, 485, $LVS_REPORT + $LVS_SHOWSELALWAYS)
	$Transfer_List = GUICtrlCreateListView("", 0, 127, 1023, 485, $LVS_REPORT + $LVS_SHOWSELALWAYS)
	;GUICtrlSetResizing(-1,$GUI_DOCKBORDERS)
	_GUICtrlListView_SetExtendedListViewStyle($Transfer_List, $exStyles)
	
	_GUICtrlListView_AddColumn($Transfer_List, 'Workstation - IP', 150)
	;GUICtrlSetResizing(-1,$GUI_DOCKBORDERS)
	_GUICtrlListView_AddColumn($Transfer_List, 'Source', 295)
	;GUICtrlSetResizing(-1,$GUI_DOCKBORDERS)
	_GUICtrlListView_AddColumn($Transfer_List, 'Destination', 295)
	;GUICtrlSetResizing(-1,$GUI_DOCKBORDERS)
	_GUICtrlListView_AddColumn($Transfer_List, 'Time Stamps       Source    -     Destination', 279)
	;GUICtrlSetResizing(-1,$GUI_DOCKBORDERS)
	
	
	;Load the file into an array
	If Not _FileReadToArray($ComputerFile, $Workstations) Then
		MsgBox(16, "FTP Transfer System Error", " There was an error reading the Transfer List. FTP Transfer System is unable to start." & @CRLF&@CRLF&'Please contact '& _
		'your System Administrator for assistance.')
		Exit
	Else
		;MsgBox(0,'Loaded','List Loaded.')
	EndIf

	;_ArrayDisplay($Workstations)
	
	;Y is used to increment the row for each entry
	Local	$y = 0
	
	;For each row in the array we will ready its information then break it into pices by the "," delimeter and give each it own value slot in an array.
	;To view the array before it's broken into pieces remove the ";" from the _arraydisplay below.
	;_Arraydisplay($Workstations)
	For $x = 1 To $Workstations[0]
		$Workstation_Info = StringSplit($Workstations[$x], ',')
		_GUICtrlListView_AddItem($Transfer_List, StringTrimLeft($Workstation_Info[1],1))
		;GUICtrlSetResizing(-1,$GUI_DOCKBORDERS)
		_GUICtrlListView_AddSubItem($Transfer_List, $y, $Workstation_Info[2], 1)
		;GUICtrlSetResizing(-1,$GUI_DOCKBORDERS)
		_GUICtrlListView_AddSubItem($Transfer_List, $y, $Workstation_Info[3], 2)
		;GUICtrlSetResizing(-1,$GUI_DOCKBORDERS)
		_Time_Format($y, $Workstation_Info[2], $Workstation_Info[3])
		If $Workstation_Info[4] = 'True|' Then 	_GUICtrlListView_SetItemChecked($Transfer_List,$y)
		$y = $y + 1 ; Increment so that next time the row will jump to the next
	Next
	$Workstations = ''
EndFunc   ;==>_Load_TransferList

Func _INI_Viewer()
	Local $Viewer, $oINI_File, $INI_Info
	$oINI_File = FileOpen($INI_File,0)
	$INI_Info = FileRead($oINI_File)
	FileClose($oINI_File)
	
	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\FTP Project\Scripts\INI Viewer.kxf
	$INI_Viewer = GUICreate("INI Viewer", 1004, 457, 128, 134,$WS_OVERLAPPEDWINDOW, $WS_EX_WINDOWEDGE)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_INI_Viewer")
	GUISetIcon(@TempDir & '\compass.ico')
	$Viewer = GUICtrlCreateEdit("", 0, 0, 1002, 455, $ES_READONLY)
	GUICtrlSetResizing(-1, $GUI_DOCKBORDERS)
	GUICtrlSetData(-1, $INI_Info)
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###
EndFunc

Func _Time_Format($y, $Src, $Des)
	Local $ST, $DT, $ST_Stamp, $DT_Stamp, $Stamp_Check, $Times
	$ST = FileGetTime($Src, 0, 0)
	If @error Then
		$ST_Stamp = 'Unknown'
		$DT_Stamp = 'Unknown'
		$Times = '                           '&$ST_Stamp &' - '&$DT_Stamp    
		;MsgBox(16,'Error','No Source file detected')
	Else
		$ST_Stamp = _Time_Stamp($ST)
		$DT = FileGetTime($Des, 0, 0)
		If @error Then
		Else
			$DT_Stamp = _Time_Stamp($DT)
		EndIf
		$Times = $ST_Stamp & ' - ' & $DT_Stamp
		;$Stamp_Check = _checkDate($ST,$DT)
		
	EndIf
	_GUICtrlListView_AddSubItem($Transfer_List, $y, $Times, 3)
EndFunc   ;==>_Time_Format

Func _Time_Stamp($T)
	Local $M, $TY, $TM, $TD, $TH, $TMin, $TSec, $T_Stamp
	;_ArrayDisplay($T)
	$M = ''
	$TY = $T[0]
	$TM = $T[1]
	$TD = $T[2]
	If $T[3] = 12 Then
		$TH = $T[3]
		$M = 'pm'
	ElseIf $T[3] > 11 Then
		$TH = $T[3] - 12
		$M = 'pm'
	Else
		$TH = $T[3]
		$M = 'am'
	EndIf
	$TMin = $T[4]
	$TSec = $T[5]
	$T_Stamp = $TH & ':' & $TMin & ':' & $TSec & ' ' & $M & ' - ' & $TM & '\' & $TD & '\' & $TY
	Return $T_Stamp
	;MsgBox(0, '', $ST_Stamp)
EndFunc   ;==>_Time_Stamp

Func _Add_Workstation_Interface()
	Local $AddWorkstationNameLabel, $AddIPLabel, $AddSourceLabel, $AddDestinationLabel, $AddWorkstationButton
	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\FTP Project\Scripts\Add Computer Interface.kxf
	$AddWorkstation = GUICreate("Add Workstation", 232, 296, 521, 266,-1,$WS_EX_ACCEPTFILES)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_Add")
	GUISetIcon(@TempDir & '\compass.ico')
	;$AddWorkstationName = GUICtrlCreateInput("Workstation Name", 16, 40, 129, 21)
	$AddWorkstationName = GUICtrlCreateInput("jwx11c1", 16, 40, 129, 21) ;Testing Purposes only
	;$AddIP = _GUICtrlIpAddress_Create($AddWorkstation, 16, 104, 129, 21)
	$AddIP = _GUICtrlIpAddress_Create($AddWorkstation, 16, 104, 129, 21) ;Testing Purposes only
	_GUICtrlIpAddress_Set($AddIP, "10.20.164.15")
	;$AddSourceInput = GUICtrlCreateInput("Source", 14, 163, 201, 21)
	$AddSourceInput = GUICtrlCreateInput("\\austech\is-server\djthornton\scripts\FTP Project\Testing\Source1\Test.txt", 14, 163, 201, 21) ;Testing Purposes only
	GUICtrlSetState($AddSourceInput,$GUI_DROPACCEPTED)
	;$AddDestinationInput = GUICtrlCreateInput("Destination", 14, 217, 201, 21)
	$AddDestinationInput = GUICtrlCreateInput("\\austech\is-server\djthornton\scripts\FTP Project\Testing\Dest1", 14, 217, 201, 21) ;Testing Purposes only
	GUICtrlSetState($AddDestinationInput,$GUI_DROPACCEPTED)
	$AddWorkstationNameLabel = GUICtrlCreateLabel("Workstation Name", 16, 16, 100, 17)
	$AddIPLabel = GUICtrlCreateLabel("Workstation IP", 16, 80, 100, 17)
	$AddSourceLabel = GUICtrlCreateLabel("Source Location", 16, 144, 82, 17)
	$AddDestinationLabel = GUICtrlCreateLabel("Destination Location", 16, 200, 101, 17)
	$AddWorkstationButton = GUICtrlCreateButton("Add", 64, 256, 97, 25)
	GUICtrlSetOnEvent($AddWorkstationButton,'_Add_Workstation')
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###
EndFunc   ;==>_Add_Workstation_Interface

Func _Add_Workstation()
	Local $Name, $IP, $Src, $Dest, $Add_Search_Name, $oComputerFile, $Success
	
	$Name = GUICtrlRead($AddWorkstationName)
	;MsgBox(0,'Name',$Name)
	$IP = _GUICtrlIpAddress_Get($AddIP)
	;MsgBox(0,'IP',$IP)
	$Src = GUICtrlRead($AddSourceInput)
	;MsgBox(0,'Src',$Src)
	$Dest = GUICtrlRead($AddDestinationInput)
	;MsgBox(0,'Dest',$Dest)
	ProgressOn('Adding Workstation','Adding '&$Name,'Please wait...'&@CRLF&$Name&' is being added to the FTP Transfer list.', Default,300, 16)
	Sleep(2000)
	ProgressSet(10)
	;MsgBox(0,'','Wait')
	TCPStartup()
		;MsgBox(0,'Compare',TCPNameToIP($Name)&' '&$IP)
		If TCPNameToIP($Name) <> $IP Then
			ProgressOff()
			MsgBox(16,'Add Workstation - Error','The provided Workstationname and IP do not match. Plase check Workstation ID and IP address.')
			TCPShutdown()
			Return -1
		EndIf
	TCPShutdown()
		ProgressSet(20)
	If Not FileExists($Src) Then
		ProgressOff()
		MsgBox(16,'Add Workstation - Error','The source file could not be found.')
		Return -1
	EndIf
		ProgressSet(30)
	If StringInStr(FileGetAttrib($Src),'D') Then 
		ProgressOff()
		MsgBox(16,'Add Workstation - Error','The source must be a file. Please check the source and try again.')
		Return -1
	EndIf
		ProgressSet(40)
	If Not FileExists($Dest) Then 
		ProgressOff()
		MsgBox(16,'Add Workstation - Error','The destination file could not be found.')
		Return -1
	EndIf
		ProgressSet(50)
	$Add_Search_Name = _Search_Workstation($Name, 1) 
		ProgressSet(60)
	If $Add_Search_Name = -1 Then 
		ProgressOff()
		Return -1
	ElseIf $Add_Search_Name = 1 Then
		ProgressSet(70)
		;Skip IP Check User is aware of workstation not being listed
	ElseIf $Add_Search_Name = 0 Then
		If _Search_Workstation($IP,1) = -1 Then Return 0
	EndIf
	ProgressSet(70)
	$oComputerFile = FileOpen($ComputerFile,1)
		If $oComputerFile = -1 Then 
			ProgressOff()
			MsgBox(16,'Add Workstation - Error','The Workstations list could not be accessed.'&$ContactAdmin)
			Return 0
		EndIf
		ProgressSet(80)
	$Success = FileWriteLine($oComputerFile,'|'&$Name&' - '&$IP&','&$Src&','&$Dest&','&'True|')
		If $Success <> 1 Then
			ProgressOff()
			MsgBox(16,'Workstation Add - Error','There was an error adding '&$Name&' to the FTP Transfer List.'&$ContactAdmin)
			Return -1
		EndIf
	FileClose($oComputerFile)
	ProgressSet(100,'Done')
	Sleep(2000)
	ProgressOff()
	MsgBox(64,'Workstation Add - Complete',$Name&' has been successfully added to the FTP Transfer list.')
	_Destroy_Add()
	_Load_TransferList()
EndFunc

Func _Remove_Workstation_Interface()
		Local $RemoveWorkstationNameLabel, $RemoveIPLabel, $RemoveWorkstationButton
	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\FTP Project\Scripts\Add Computer Interface.kxf
	$RemoveWorkstation = GUICreate("Remove Workstation", 163, 145, 523, 268,-1,$WS_EX_ACCEPTFILES)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_Remove")
	GUISetIcon(@TempDir & '\compass.ico')
	;$RemoveWorkstationName = GUICtrlCreateInput("Workstation Name", 16, 40, 129, 21)
	$RemoveWorkstationName = GUICtrlCreateInput("jwx11c1", 16, 40, 129, 21) ;Testing Purposes only
	$RemoveWorkstationNameLabel = GUICtrlCreateLabel("Workstation Name", 16, 16, 100, 17)
	$RemoveWorkstationButton = GUICtrlCreateButton("Remove", 32, 105, 97, 25)
	GUICtrlSetOnEvent($RemoveWorkstationButton,'_Remove_Workstation')
	$Remove_Client = GUICtrlCreateCheckbox("Remove Client", 32, 74, 97, 17)
	GUICtrlSetOnEvent($Remove_Client,'')
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###
EndFunc

Func _Remove_Workstation()
	Local $Name, $Remove_Search_Name, $Remove_IP
	
	$Name = GUICtrlRead($RemoveWorkstationName)
	;MsgBox(0,'Name',$Name)
	
	$Remove_Search_Name = _Search_Workstation($Name, 3) 
		
	If $Remove_Search_Name = -1 Then 
		ProgressOff()
		Return -1
	Else
		$Remove_IP = StringTrimLeft(_GUICtrlListView_GetItem($Transfer_List,$Remove_Search_Name),$Name&' - ')
		;MsgBox(0,'IP',$Remove_IP)
		;Continue
	EndIf
		
	_Save_Changes(1,$Name)
	#cs
	TCPStartup()
		If  TCPNameToIP($Name) = '' Then
			#ce
		
	_Destroy_Remove()
	_Load_TransferList()
EndFunc

Func _Client_Deploy_Interface()
	Local $DeployWorkstationNameLabel, $DeployIPLabel, $Deploy
	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\FTP Project\Images\Deploy Client.kxf
	$ClientDeploy = GUICreate("Client Deployment", 221, 184, 193, 115)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_Deploy")
	GUISetIcon(@TempDir & '\compass.ico')
	$DeployWorkstationNameInput = GUICtrlCreateInput("Workstation Name", 16, 32, 185, 21)
	$DeployWorkstationNameLabel = GUICtrlCreateLabel("Workstation Name", 16, 8, 100, 17)
	$DeployIP = _GUICtrlIpAddress_Create($ClientDeploy, 16, 88, 153, 25)
	_GUICtrlIpAddress_Set($DeployIP, "0.0.0.0")
	$DeployIPLabel = GUICtrlCreateLabel("Workstation IP", 16, 72, 100, 17)
	$Deploy = GUICtrlCreateButton("Deploy", 56, 144, 105, 25, 0)
	GUICtrlSetOnEvent(-1, '_Deploy')
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###
EndFunc

Func _Remove_Client_Interface()
	Local $RemoveWorkstationNameLabel, $RemoveIPLabel, $Remove
	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\FTP Project\Images\Deploy Client.kxf
	$ClientRemove = GUICreate("Client Removal", 221, 184, 193, 115)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_Client_Removal")
	GUISetIcon(@TempDir & '\compass.ico')
	$RemoveWorkstationNameInput = GUICtrlCreateInput("Workstation Name", 16, 32, 185, 21)
	$RemoveWorkstationNameLabel = GUICtrlCreateLabel("Workstation Name", 16, 8, 100, 17)
	$RemoveIP = _GUICtrlIpAddress_Create($ClientRemove, 16, 88, 153, 25)
	_GUICtrlIpAddress_Set($RemoveIP, "0.0.0.0")
	$RemoveIPLabel = GUICtrlCreateLabel("Workstation IP", 16, 72, 100, 17)
	$Remove = GUICtrlCreateButton("Remove", 56, 144, 105, 25, 0)
	GUICtrlSetOnEvent(-1, '_Remove_Client')
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###
EndFunc

Func _Deploy()
	Local 	$strWorkstationName, $strWorkstationIP, $UserName, $Password, $Install_Path, $Deploy_Search_Name, $Dupe, $objUser, $objGroup, $colAccounts, $oldFlags, _
			$newFlags, $ExistUser, $User, $Existgroup, $objUsers
	$strWorkstationName = GUICtrlRead($DeployWorkstationNameInput)
	ProgressOn('Client Deployment','Deploying Client to '&$strWorkstationName,'Please wait...'&@CRLF&'Client configurations are being deployed to '&$strWorkstationName&'.', _
	Default, 300, 16)
	Sleep(2000)
	;MsgBox(0,'Name',$strWorkstationName)
	ProgressSet(5)
		If $strWorkstationName = '' Or $strWorkstationName = 'Workstation Name' Then
			ProgressOff()
			MsgBox(16,'Client Deployment - Error','You must first enter a valid Workstation name. Please try again.')
			Return 0
		EndIf
	ProgressSet(10)
	$strWorkstationIP = _GUICtrlIpAddress_Get($DeployIP)
	;MsgBox(0,'IP',$strWorkstationIP)
		If $strWorkstationIP = '' Or $strWorkstationIP = '0.0.0.0' Then
			ProgressOff()
			MsgBox(16,'Client Deployment - Error','You must first enter a valid IP address. Please try again.')
			Return 0
		EndIf
	ProgressSet(15)
	$UserName = IniRead($INI_File,'FTP Client','Username','Error')
	;MsgBox(0,'User Name',$UserName)
		If $UserName = 'Error' Then
			ProgressOff()
			MsgBox(16,'Client Deployment - Error','Unable to locate "Username" in ini file.Aborting "Workstation Add".')
			Return 0
		EndIf
	ProgressSet(20)
	$Password = IniRead($INI_File,'FTP Client','Password','Error')
	;MsgBox(0,'Password',$Password)
		If $Password = 'Error' Then
			ProgressOff()
			MsgBox(16,'Client Deployment - Error','Unable to locate "Password" in ini file. Aborting "Workstation Add".')
			Return 0
		EndIf
	ProgressSet(25)
	$Install_Path = IniRead($INI_File,'FTP Client','Install_Location','Error')
	;MsgBox(0,'Install Path',$Install_Path)
		If $Install_Path = 'Error' Then
			ProgressOff()
			MsgBox(16,'Client Deployment - Error','Unable to locate "Installation Path" in ini file. Aborting "Workstation Add".')
			Return 0
		EndIf
	ProgressSet(30)
	$Deploy_Search_Name = _Search_Workstation($strWorkstationName,2) 
	ProgressSet(40)
		If $Deploy_Search_Name = -1 Then 
			ProgressOff()
			Return 0
		ElseIf $Deploy_Search_Name = 1 Then
			ProgressSet(45)
			;Skip IP Check User is aware of workstation not being listed
		ElseIf $Deploy_Search_Name = 0 Then
			If _Search_Workstation($strWorkstationIP,2) = -1 Then Return 0
		EndIf
		ProgressSet(50)
		TCPStartup()
		;MsgBox(0,'Compare',TCPNameToIP($strWorkstationName)&' '&$strWorkstationIP)
		If TCPNameToIP($strWorkstationName) <> $strWorkstationIP Then
			ProgressOff()
			MsgBox(16,'Client Deployment - Error','The provided Workstationname and IP do not match. Plase check Workstation ID and IP address.')
			TCPShutdown()
			Return 0
		EndIf
		TCPShutdown()
		ProgressSet(55)
	; Init objects
	Const $ADS_UF_DONT_EXPIRE_PASSWD = 0X10000
	Const $ADS_UF_PASSWD_CANT_CHANGE = 0X40
	
	; Check if account exists .. if not create it
	$objUser = ObjGet("WinNT://" & $strWorkstationIP & "/" & $UserName)
	If @error Then
		ProgressSet(60)
		;MsgBox(0,'Not Detected','No account was detected. Setting up now.')
		$colAccounts = ObjGet("WinNT://" & $strWorkstationName & "")
		$objUser = $colAccounts.Create("user", $UserName)
		$objUser.SetPassword($Password)
		$objUser.Put("Fullname", "FTP Account")
		$objUser.Put("Description", "FTP Transfer Account")
		$objUser.SetInfo
	Else
		ProgressSet(60)
		$ExistUser = 1 ;User already exist
		;MsgBox(0,'Detected','The account was detected. Skipping account creation.')
	EndIf
	ProgressSet(70)
	; Read current settings and Bitor to ensure the "Don't expire password swith is on and user cannot change password"
	$oldFlags = $objUser.Get("UserFlags")
	$newFlags = BitOR($oldFlags, $ADS_UF_DONT_EXPIRE_PASSWD)
	$objUser.Put("UserFlags", $newFlags) ;expire the password
	$objUser.SetInfo
	ProgressSet(75)
	$oldFlags = $objUser.Get("UserFlags")
	$newFlags = BitOR($oldFlags, $ADS_UF_PASSWD_CANT_CHANGE)
	$objUser.Put("UserFlags", $newFlags) ;expire the password
	$objUser.SetInfo
	ProgressSet(80)
	;MsgBox(262144, 'Debug line ~' & @ScriptLineNumber, 'Selection:' & @LF & ' dont Password Expired');### Debug MSGBOX

	;Add User to users group**********************************************
	$objGroup = ObjGet("WinNT://" & $strWorkstationName & "/Users,group")
	For  $objUsers in $objGroup.Members
		$User = ($objUsers.AdsPath)
		If $User = 'winNT://SETON/'&$strWorkstationName&'/FTP Transfer' Then $Existgroup = 1
	Next 
	
	If $Existgroup = 1 Then 
		;MsgBox(0,'User Name','User is already apart of Users group.')
	Else
		$objGroup.Add($objUser.ADsPath)
	EndIf
	ProgressSet(85)
	;*********************************************************************
	
	;Create Directory
	_Dir_Setup($Install_Path, $strWorkstationIP, $UserName)
EndFunc   ;==>_Account_Setup

Func _Dir_Setup($Install_Path, $strWorkstationIP, $UserName)
	;MsgBox(0,'Dir setup','')
	Local $acls, $Success, $ErrorCounter = 0
	$Success = DirCreate('\\'&$strWorkstationIP&'\c$\'&$Install_Path&'\')
		If $Success <> 1 Then $ErrorCounter = $ErrorCounter + 1
	ProgressSet(90)
	$Success = DirCreate('\\'&$strWorkstationIP&'\c$\'&$Install_Path&'\Logs\')
		If $Success <> 1 Then $ErrorCounter = $ErrorCounter + 1
	ProgressSet(95)
	$acls = ('cacls.exe \\'&$strWorkstationIP&'\c$\FTP Transfers\ /E /T /C /G ' & $UserName & ':R')
	;MsgBox(0,'',$acls)
	Run(@ComSpec & " /C " & $acls, "", @SW_HIDE);_RunDOS($acls)
	ProgressSet(100,'Done')
	Sleep(2000)
	ProgressOff()
	If $ErrorCounter <> 0 Then
		MsgBox(64,'Client Deployment - Complete','The deployment is complete, but with '&$ErrorCounter&' error(s) during the directory setup. Please try running the deployment again.')
	Else
		MsgBox(64,'Client Deployment - Complete','The deployment is complete.')
	EndIf
	_Destroy_Deploy()
EndFunc   ;==>_Dir_Setup

Func _Remove_Client()
	Local $Workstation, $UserName, $Install_Path, $objUser, $Verify, $objUserDelete
	
	$Workstation = GUICtrlRead($RemoveWorkstationNameInput)
	ProgressOn('Client Removal','Removing client from '&$Workstation,'Please wait...'&@CRLF&'Client configurations are being removed from '&$Workstation&'.', _
	Default, 300, 16)
	Sleep(1000)
	If $Workstation = '' Or $Workstation = 'Workstation Name' Then
		ProgressOff()
		MsgBox(16,'Client Removal - Error','You must first enter a valid Workstation name. Please try again.')
		Return -1
	EndIf
	ProgressSet(20)
	$UserName = IniRead($INI_File,'FTP Client','Username','Error')
	;MsgBox(0,'User Name',$UserName)
	If $UserName = 'Error' Then
		ProgressOff()
		MsgBox(16,'Client Removal - Error','Unable to locate "Username" in ini file.Aborting "Workstation Add".')
		Return -1
	EndIf
	ProgressSet(40)
	$Install_Path = IniRead($INI_File,'FTP Client','Install_Location','Error')
	;MsgBox(0,'Install Path',$Install_Path)
	If $Install_Path = 'Error' Then
		ProgressOff()
		MsgBox(16,'Client Removal - Error','Unable to locate "Installation Path" in ini file. Aborting "Workstation Add".')
		Return 0
	EndIf
	ProgressSet(60)
	; Check if account exists .. if so delete it
	$objUser = ObjGet("WinNT://" & $Workstation & "")
	If @error Then
		;does not exist
	Else
		$Verify = MsgBox(36,'Client Removal - Verification','You are about to delete the FTP Transfer account and settings from '&$Workstation&'.'&@CRLF&@CRLF& _
							'Are you sure you want to continue?')
			If $Verify = 7 Then
				ProgressOff()
				Return -1
			Else
				$objUserDelete = $objUser.Delete("user", $UserName)
			EndIf
	EndIf
	ProgressSet(90)
	DirRemove('\\'&$Workstation&'\c$\'&$Install_Path&'\', 1)
	ProgressSet(100,'Done')
	Sleep(2000)
	ProgressOff()
	_Destroy_Client_Removal()
EndFunc

Func _Change_Password()
	Global $CP1, $CP2, $SPBox, $Save_Password, $Change_Password_Label
	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\FTP Project\Scripts\Save Password Dialog.kxf
	$Change_Password_GUI = GUICreate("Change Password", 230, 191, 259, 189)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_Change_Password")
	GUISetIcon(@TempDir & '\compass.ico')
	$CP1 = GUICtrlCreateInput("", 32, 40, 161, 21)
	$CP2 = GUICtrlCreateInput("", 32, 88, 161, 21)
	_GUICtrlEdit_SetPasswordChar($CP1,'*')
	_GUICtrlEdit_SetPasswordChar($CP2,'*')
	$Change_Password_Label = GUICtrlCreateLabel("Change Password", 64, 16, 90, 17)
	$Save_Password = GUICtrlCreateButton("Save", 72, 120, 81, 25, 0)
	GUICtrlSetOnEvent($Save_Password,'_Save_Password')
	$SPBox = GUICtrlCreateCheckbox("Show Password", 16, 160, 97, 17)
	GUICtrlSetOnEvent($SPBox,'_Show_Password')
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###
EndFunc   ;==>_Change_Password

Func _Show_Password()
	If BitAND(GUICtrlRead($SPBox),$GUI_CHECKED) = 0 Then
		;MsgBox(0,'Unchecked',BitAND(GUICtrlRead($SPBox),$GUI_CHECKED))
		_GUICtrlEdit_SetPasswordChar($CP1,'*')
		_GUICtrlEdit_SetPasswordChar($CP2,'*')
		GUICtrlSetState($CP2, $GUI_FOCUS)
		GUICtrlSetState($CP1, $GUI_FOCUS)
	Else
		;MsgBox(0,'Checked',BitAND(GUICtrlRead($SPBox),$GUI_CHECKED))
		_GUICtrlEdit_SetPasswordChar($CP1)
		_GUICtrlEdit_SetPasswordChar($CP2)
		GUICtrlSetState($CP2, $GUI_FOCUS)
		GUICtrlSetState($CP1, $GUI_FOCUS)
	EndIf
EndFunc

Func _Save_Password()
	Local $Password1, $Password2, $wINI
	$Password1 = GUICtrlRead($CP1)
	$Password2 = GUICtrlRead($CP2)
	If $Password1 == $Password2 Then
		$wINI = IniWrite($INI_File,'FTP Client','PassWord',$Password1)
			If $wINI = 0 Then
				MsgBox(16,'Change Password - Error','Unable to update password.'&$ContactAdmin)
			Else
				MsgBox(64,'Change Password - Success','You have successfully changed the client password. Please wait up to 10 minutes for all passwords to apply.')
				_Destroy_Change_Password()
			EndIf
	Else
		MsgBox(16,'Change Password - Error','The provided passwords do not match. Please try again.')
		Return -1
	EndIf
EndFunc

Func _Disable_All_Monitoring()
	Local $rows
	$rows = _GUICtrlListView_GetItemCount($Transfer_List)
	
	For $c = 0 To $rows - 1
		_GUICtrlListView_SetItemChecked($Transfer_List,-1,False)
	Next
EndFunc

Func _Enable_All_Monitoring()
	Local $rows
	$rows = _GUICtrlListView_GetItemCount($Transfer_List)
	
	For $c = 0 To $rows - 1
		_GUICtrlListView_SetItemChecked($Transfer_List,-1,True)
	Next
EndFunc

Func _Search_Workstation($Search_Item = '', $Search_Func = 0)
	;MsgBox(0,'Search',$Search_Item&' - '&$Search_Func)
	Local $Dupe, $Verify, $Item_Index, $i = -1
	;MsgBox(0,'',GUICtrlRead($Workstation_Input))
	If $Search_Func = 0 Then
		$Search_Item = GUICtrlRead($Workstation_Input)
		If $Search_Item = 'Workstation or IP' Then
			MsgBox(16,'Workstation Search - Error','You must first enter a valid Search parameter.'&@CRLF&@CRLF&'Please try again.')
			Return 0
		EndIf	
		$Item_Index = _GUICtrlListView_FindInText($Transfer_List,$Search_Item)
		If $Item_Index = -1 Then
			MsgBox(4160, "Workstation Search - Result", $Search_Item&' could not be found.')
		Else
			_GUICtrlListView_EnsureVisible($Transfer_List, $Item_Index)
			_GUICtrlListView_SetItemSelected($Transfer_List, $Item_Index)
			_GUICtrlListView_ClickItem($Transfer_List,$Item_Index)
		EndIf
	ElseIf $Search_Func = 1 Then; 1 is used for a Workstation Add
		$Item_Index = _GUICtrlListView_FindInText($Transfer_List,$Search_Item)
		If $Item_Index = -1 Then
			;MsgBox(0,'',$Item_Index)
			Return 0;Continue
		Else
			$Dupe = MsgBox(36,'Workstation Add - Duplicate Entry',$Search_Item&' is already listed in the FTP Transfer System'&@CRLF&@CRLF&'Do you want to continue?')
			If $Dupe = 7 Then 
				MsgBox(48,'Workstation Add - Aborted','Aborting Workstation Add for '&$Search_Item&'.',3)
				Return -1
			ElseIf $Dupe = 6 Then
				Return 1
			EndIf
		EndIf
	ElseIf $Search_Func = 2 Then;2 is used for Client Deployment
		$Item_Index = _GUICtrlListView_FindInText($Transfer_List,$Search_Item)
		If $Item_Index = -1 Then
			$Verify = MsgBox(36,'Client Deployment - Not Found',$Search_Item&' is not listed in the FTP Transfer System'&@CRLF&@CRLF&'Do you want to continue?')
			If $Verify = 7 Then
				MsgBox(48,'Client Deployment - Aborted','Aborting Client Deployment to '&$Search_Item&'.',3)
				Return -1
			ElseIf $Verify = 6 Then
				Return 1
			EndIf
		Else
			Return 0;Continue
		EndIf
	ElseIf $Search_Func = 3 Then;3 is used for Client Removal
		$Item_Index = _GUICtrlListView_FindInText($Transfer_List,$Search_Item)
		If $Item_Index = -1 Then
			$Verify = MsgBox(48,'Workstation Removal - Not Found',$Search_Item&' is not listed in the FTP Transfer System. ')
			Return -1
		Else
			MsgBox(36,'Workstation Removal - Verification',$Search_Item&' is about to be removed from the FTP Transfer System.'&@CRLF&@CRLF&'Do you want to continue?')
			If $Verify = 7 Then
				MsgBox(48,'Workstation Removal - Aborted','Aborting Workstation removal for '&$Search_Item&'.',3)
				Return -1
			ElseIf $Verify = 6 Then
				Return $Item_Index
			EndIf
		EndIf
	ElseIf $Search_Func = 4 Then;4 is used for Client Log Verification
		$Item_Index = _GUICtrlListView_FindInText($Transfer_List,$Search_Item)
		If $Item_Index = -1 Then
			$Verify = MsgBox(48,'Workstation Log Retrieval - Not Found',$Search_Item&' is not listed in the FTP Transfer System.'&@CRLF&@CRLF&'Do you want to continue?')
			Return -1
		Else
			Return 1
		EndIf
	EndIf
EndFunc

Func _Save_Changes($Removal_Func = 0, $Removal_Item = '')
	Local $Save, $rows, $index, $DirSize, $oFile, $List, $Check, $Temp, $x, $y, $Workstation_Info

	If $Removal_Func = 0 Then
		$Save = MsgBox(36, 'Save Changes - Verification','Are you sure you want to save these changes?')
		If $Save = 7 Then Return -1
	EndIf
		ProgressOn('Workstation List Save','Saving changes made to list','Backing up current list.',Default,Default,16)
		Sleep(1000)
		$rows = _GUICtrlListView_GetItemCount($Transfer_List)
		ProgressSet(33)
		Sleep(1000)
		$DirSize = DirGetSize($Backup_Dir, 1)
		ProgressSet(66)
		Sleep(1000)
		;MsgBox(0,'',@MON&'-'&@MDAY&'-'&StringTrimLeft(@YEAR,2)&' '&'Backup '&$DirSize[1]+1)
		FileCopy($ComputerFile, $Backup_Dir&'\'&@MON&'-'&@MDAY&'-'&StringTrimLeft(@YEAR,2)&' '&'Backup '&$DirSize[1]+1&'.csv')
		ProgressSet(100)
		Sleep(1000)
		;Build a loop to verify that the file was transfered.
		ProgressSet(0,'Committing changes to file.')
		Sleep(1000)
		For $c = 0 To $rows - 1
			ProgressSet(100 * ($c/$rows ))
			$Check = _GUICtrlListView_GetItemChecked($Transfer_List,$c)
			$Temp = _GUICtrlListView_GetItemTextArray($Transfer_List,$c)
			;_ArrayDisplay($List)
			If $Removal_Func = 1 And StringInStr($Temp[1],$Removal_Item) Then
				;don't write to file
			Else
				$List = $List&'|'&$Temp[1]&','&$Temp[2]&','&$Temp[3]&','&$Check&'|'&@CRLF
			EndIf
			;MsgBox(0,'',$index)
		Next
		;MsgBox(0,'List',$List)
		$oFile = FileOpen($ComputerFile,2)
		FileWrite($oFile,$List)
		FileClose($oFile)
		ProgressSet(100,'Save Complete')
		Sleep(1000)
		ProgressOff()
		MsgBox(0,'Save Changes - Done','Save Complete.')
		_Load_TransferList()
EndFunc

Func _Log_Interface()
	Local 	$Info_Label, $Transfer_Label, $Interface_Label, $Client_Label, $Download_Logs
	#Region ### START Koda GUI section ### Form=\\austech\IS-Server\djthornton\Scripts\FTP Project\Scripts\Log Interface.kxf
	$Log_Interface = GUICreate("Log Selector", 321, 338, 281, 139)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Destroy_Log_Interface")
	GUISetIcon(@TempDir & '\compass.ico')
	$Info_Label = GUICtrlCreateLabel("Please choose what logs you would like to Download.", 24, 8, 265, 56)
	GUICtrlSetFont(-1, 12, 400, 0, "MS Sans Serif")
	$Transfer_Label = GUICtrlCreateLabel("FTP Transfer Log", 32, 80, 109, 20)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	$Interface_Label = GUICtrlCreateLabel("FTP Interface Log", 32, 136, 110, 20)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	$Client_Label = GUICtrlCreateLabel("FTP Client Logs", 32, 192, 99, 20)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	$View_FTP_Transfer_Log = GUICtrlCreateCheckbox("View Log", 56, 104, 97, 17)
	$View_Interface_Log = GUICtrlCreateCheckbox("View Log", 56, 160, 97, 17)
	GUICtrlCreateCheckbox('Please choose one',32, 210, 250, 50, $BS_GROUPBOX)
	$View_All_Client_Logs = GUICtrlCreateRadio("View All Logs", 56, 230, 97, 17)
	;GUICtrlSetOnEvent($View_All_Client_Logs, '_CheckBox_Switch')
	$View_Single_Client_Log = GUICtrlCreateRadio("View Single Log", 160, 230, 97, 17)
	;GUICtrlSetOnEvent($View_Single_Client_Log, '_CheckBox_Switch')
	$Workstation_Log = GUICtrlCreateInput("Workstation or IP", 160, 270, 105, 21)
	$Download_Logs = GUICtrlCreateButton("Download", 40, 280, 89, 33)
	GUICtrlSetFont(-1, 12, 400, 0, "MS Sans Serif")
	GUICtrlSetOnEvent($Download_Logs,'_Log_Downloader')
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###
EndFunc

Func _Log_Downloader()
	Local 	$Transfer, $Interface, $ClientAll, $ClientSingle, $Workstation_Search_Log, $Save_Dir, $Stamp, $Drop_Files, $Go_Get_Workstation, $Client_Log, $Verify, _
			$Ping, $Install_Path
	
	$Transfer = BitAND(GUICtrlRead($View_FTP_Transfer_Log),$GUI_CHECKED)
	
	$Interface = BitAND(GUICtrlRead($View_Interface_Log),$GUI_CHECKED)
	
	$ClientAll = BitAND(GUICtrlRead($View_All_Client_Logs),$GUI_CHECKED)
	
	$ClientSingle = BitAND(GUICtrlRead($View_Single_Client_Logs),$GUI_CHECKED)
	
	If $ClientSingle = 1 Then 
		MsgBox(0,'','See it.')
		$Go_Get_Workstation = GUICtrlRead($Workstation_Log) 
		If $Go_Get_Workstation = "Workstation or IP" Or $Go_Get_Workstation = '' Then
			MsgBox(16,'Client Log Retrieval - Error','You must first enter a valid Workstation before downloading. Please try again.')
			Return -1
		Else
			$Workstation_Search_Log = _Search_Workstation($Go_Get_Workstation, 4)
			If $Workstation_Log = -1 Then 
				$ClientSingle = 0
			Else
				$Ping = Ping($Go_Get_Workstation)
					If $Ping = 0 Then
						MsgBox(16,'Workstation Log Retrieval - Error','The Workstation '&$Go_Get_Workstation&' cannot be pinged. The Workstation may be offline '& _
						'or the Workstation may not be on the network. Please validate that the asset is on the network.')
						$ClientSingle = 0
					EndIf
			EndIf
		EndIf
	EndIf
	
	If $Transfer = 1  Or $Interface = 1 Or $ClientAll = 1 Or $ClientSingle = 1 Then 
		$Save_Dir = FileSelectFolder('Where do you want to save files?', '', 7, @DesktopDir,$Log_Interface)
		If @error Then Return -1
		$Stamp = StringReplace(_NowCalc(),'/','.')
		$Stamp = StringReplace($Stamp,':','.')
		$Stamp = StringReplace($Stamp,' ',' -- ')
		$Stamp = StringTrimLeft($Stamp, 2)
		
		$Drop_Files = $Save_Dir&'\FTP Transfer Logs '&$Stamp
		MsgBox(0,'Dir',$Drop_Files)
		
		DirCreate($Drop_Files)
		
		If $Transfer = 1 Then FileCopy($Log_Dir&'\FTP Transfer Logs\Transfers.log', $Drop_Files&'\Transfer.log',1)
		If $Interface = 1 Then FileCopy($Log_Dir&'\FTP Interface Logs\Interface.log', $Drop_Files&'\Interface.log',1)
		If $ClientAll = 1 Then FileCopy($Log_Dir&'\FTP Client Logs\Client.log', $Drop_Files&'\Client.log',1)
		If $ClientSingle = 1 Then 
			$Install_Path = IniRead($INI_File,'FTP Client','Install_Location','Error')
			$Client_Log = '\\'&$Go_Get_Workstation&'\c$\'&$Install_Path&'\Logs\'
			;MsgBox(0,'Install Path',$Install_Path)
			If $Install_Path = 'Error' Then
				ProgressOff()
				MsgBox(16,'Client Log Retrieval - Error','Unable to locate "Installation Path" in ini file. Aborting "Client Log Retrieval".')
				Return -1
			EndIf
			If Not FileExists($Client_Log) Then
				MsgBox(16,'Client Log Retrieval - Error','Unable to locate "Client Log" on '&$Go_Get_Workstation&'. Aborting "Client Log Retrieval".')
				Return -1
			EndIf
			FileCopy($Client_Log, $Drop_Files&'\Client.log',1)
		EndIf
	Else
		MsgBox(16,'Log Download - Error','Please make a selection first.')
	EndIf
	
EndFunc

Func _Exit_Interface()
	;Delete the GUI then end the script
	GUIDelete($FTP_Interface)
	Exit
EndFunc   ;==>_Exit_Interface

Func _Destroy_Add()
	GUIDelete($AddWorkstation)
EndFunc

Func _Destroy_Remove()
	GUIDelete($RemoveWorkstation)
EndFunc

Func _Destroy_Deploy()
	GUIDelete($ClientDeploy)
EndFunc

Func _Destroy_Client_Removal()
	GUIDelete($ClientRemove)
EndFunc

Func _Destroy_Change_Password()
	GUIDelete($Change_Password_GUI)
EndFunc

Func _Destroy_INI_Viewer()
	GUIDelete($INI_Viewer)
EndFunc

Func _Destroy_Log_Interface()
	GUIDelete($Log_Interface)
EndFunc

Func MyErrFunc()
	
	Local $HexNumber
	
	$HexNumber = Hex($oMyError.number, 8)
	
	If $HexNumber = '800708AC' Then Return 'Does Not Exist'
	
	MsgBox(0, "", "We intercepted a COM Error ! Please take note of the information below and provide it to your System Administrator." & @CRLF & _
			"Number is: " & $HexNumber & @CRLF & _
			"Linenbr is: " & $oMyError.scriptline & @CRLF & _
			"Description is: " & $oMyError.description & @CRLF & _
			"Windescription is: " & $oMyError.windescription)

	SetError(1); something to check for when this function returns
EndFunc   ;==>MyErrFunc


